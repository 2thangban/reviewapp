import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';

import '../models/province/province.dart';

/// This class manage get current location or change location by user
class LocationManager {
  /// Fuction getPosition : get current postinon by user.
  static Future<Position> getPosition() async {
    bool serviceEnable; // check service
    LocationPermission permission; // check permission
    try {
      /// check service is enable?
      /// if enabled then check request permission is allow ?
      serviceEnable = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnable) {
        permission = await Geolocator.checkPermission();

        /// if permission is denied then sent request permission.
        if (permission == LocationPermission.denied) {
          /// case permission deny by user then return.
          permission = await Geolocator.requestPermission();
          if (permission == LocationPermission.denied) return null;
        }

        /// case permission deny forever by user
        /// then return and don't show permission dialog.
        if (permission == LocationPermission.deniedForever) return null;
      }

      /// case permission allowed then return current position of user.
      return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high,
      );
    } catch (e) {
      Future.error(e);
    }
    return null;
  }

  /// getAddressLocation: convert from position(lat,lng) to address.
  static Future<String> getAddress(Position position) async {
    try {
      if (position == null) return Future.error('param: position is null');
      final coordinate = Coordinates(position.latitude, position.longitude);
      var address =
          await Geocoder.local.findAddressesFromCoordinates(coordinate);
      return address.first.adminArea;
    } catch (e) {
      return 'Hồ Chí Minh';
    }
  }

  /// get province from address.
  static Province convertToProvince(
    String address,
    List<Province> lstProvinces,
  ) {
    for (var city in lstProvinces) {
      if (address.contains(city.name)) {
        return city;
      }
    }
    return null;
  }
}
