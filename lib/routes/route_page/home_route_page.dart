import 'package:flutter/cupertino.dart';

import '../../screens/comment/detail_comment_screen/detail_comment_screen.dart';
import '../../screens/comment/edit_comment/edit_comment.dart';
import '../../screens/comment/list_all_comment_screen/ui/list_comment_screen.dart';
import '../../screens/hashtag/detail_hashtag/ui/detail_hashtag_screen.dart';
import '../../screens/home_screen/home_screen_main.dart';
import '../../screens/home_screen/post/create_post_screen/ui/create_post_screen.dart';
import '../../screens/home_screen/post/detail_post_screen/ui/post_detail_screen.dart';
import '../../screens/home_screen/post/draft_post_screen/ui/draf_post_screen.dart';
import '../../screens/home_screen/post/report_post/ui/report_post_screen.dart';
import '../../screens/home_screen/user/edit_pass_screen.dart/ui/edit_pass_screen.dart';
import '../../screens/home_screen/user/edit_profile_screen/ui/edit_profile_screen.dart';
import '../../screens/home_screen/user/top_user_screen/ui/top_user_screen.dart';
import '../../screens/location/change_address_screen/ui/change_address.dart';
import '../../screens/location/create_location_screen/ui/create_location_screen.dart';
import '../../screens/location/detail_location_screen/ui/detail_location_screen.dart';
import '../../screens/location/search_location_screen/ui/search_location_screen.dart';
import '../../screens/location/top_location_screen/ui/top_location_screen.dart';
import '../../screens/search_screen/ui/search_screen.dart';
import '../../screens/setting_app_screen/setting_app_screen.dart';
import '../route.dart';
import '../route_name.dart';

class HomeRoutePage implements RouteInterFace {
  @override
  CupertinoPageRoute routePage(RouteSettings settings) {
    return CupertinoPageRoute(
      settings: settings,
      builder: (context) {
        switch (settings.name) {

          /// Navigate to list comment screen
          case RouteName.detailHashtag:
            return DetailHashtagScreen(settings.arguments);
            break;

          /// Navigate to list comment screen
          case RouteName.listComment:
            return ListCommentScreen(params: settings.arguments);
            break;

          /// Navigate to details comment screen
          case RouteName.detailComment:
            return DetailCommentScreen(settings.arguments);
            break;

          /// Navigate to list comment screen
          case RouteName.editcomment:
            return EditCommentScreen(settings.arguments);
            break;

          /// Navigate to detail post screen
          case RouteName.postDetail:
            return DetailPostScreen(settings.arguments);

          /// Navigate to report post screen
          case RouteName.reportPost:
            return ReportPostScreen(postId: settings.arguments);

          /// Navigate to draft post screen
          case RouteName.drafPost:
            return DrafPostScreen();

          /// Navigate to edit profile screen
          case RouteName.editProfile:
            return EditProfileScreen(settings.arguments);

          /// Navigate to edit pass screen
          case RouteName.editPass:
            return const EditPassScreen();

          /// Navigate to setting screen
          case RouteName.setting:
            return const SettingAppScreen();

          /// Navigate to create post screen
          case RouteName.createPost:
            return CreaterPostScreen(settings.arguments);

          /// Navigate to create location screen
          case RouteName.createLocation:
            return const CreateLocationScreen();

          /// Navigate to search location screen
          case RouteName.searchLocation:
            return const SearchLocationScreen();

          /// Navigate to change address screen
          case RouteName.changeAddress:
            return ChangeAddressScreen(boxProvince: settings.arguments);

          /// Navigate to search screen
          case RouteName.search:
            return const SearchScreen();

          /// Navigate to top user screen
          case RouteName.topUser:
            return const TopUserScreen();

          /// Navigate to top location screen
          case RouteName.topLocation:
            return const TopLocationScreen();

          /// Navigate to detail location screen
          case RouteName.detailLocation:
            return DetailLocationScreen(settings.arguments);

          default:
            return const HomeScreen();
        }
      },
    );
  }
}
