import 'package:flutter/cupertino.dart';

import '../../screens/login_signup/login_screen/ui/login_screen.dart';
import '../../screens/login_signup/signup_info_screen/ui/signup_info_screen.dart';
import '../../screens/login_signup/signup_screen/ui/signup_screen.dart';
import '../route.dart';
import '../route_name.dart';

class LoginRoutePage implements RouteInterFace {
  @override
  CupertinoPageRoute routePage(RouteSettings settings) {
    return CupertinoPageRoute(
      settings: settings,
      builder: (context) {
        switch (settings.name) {

          /// 'login/signUp': navigate to signup screen
          case RouteName.signUp:
            return SignUpScreen();
            break;

          /// 'login/signupinfo': navigate to signup infomation screen
          case RouteName.signUpInfo:
            return SignUpInfo(settings.arguments);
            break;

          ///'login' : navigate to login screen
          default:
            return LoginScreen();
        }
      },
    );
  }
}
