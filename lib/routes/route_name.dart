class RouteName {
  /// Splash screen
  static const String initial = '/';

  /// Show image screen
  static const String image = '/image';

  /// Follow user screen
  static const String follow = '/follow';

  /// Related post list screen
  static const String relatedPost = '/ralatedPost';

  /// Follow user screen
  static const String detailHashtag = '$home/hashtag';

  /// Related post list screen
  static const String reportPost = '$home/reportPost';

  /// list comment screen
  static const String listComment = '$home/listComment';

  /// detail comment screen
  static const String detailComment = '$home/detailComment';

  /// edit comment screen
  static const String editcomment = '$home/editComment';

  /// Gallery scrren
  static const String createGallery = '/createGallery';
  static const String detailGallery = '/detailGallery';

  /// Home screen
  static const String home = '/home';
  static const String postDetail = '$home/postDetail';

  static const String createPost = '$home/createPost';
  static const String changeAddress = '$home/changeAddress';
  static const String search = '$home/search';
  static const String topUser = '$home/topUser';
  static const String drafPost = '$home/drafPost';

  /// Location screen
  static const String topLocation = '$home/topLocation';
  static const String detailLocation = '$home/locationDetail';
  static const String listImageLocation = '$home/listImageLocation';
  static const String searchLocation = '$home/searchLocation';
  static const String createLocation = '$home/createLocation';

  /// Profile screen
  static const String profile = '/profile';
  static const String editProfile = '$home/editProfile';
  static const String editPass = '$home/editPassword';
  static const String riviuMember = '$home/riviuMember';
  static const String setting = '$home/setting';
  static const String logout = '$home/logout';

  /// Login screen
  static const String login = '/login';
  static const String signUp = '$login/signUp';
  static const String signUpInfo = '$login/signUpInfo';
}
