import 'package:flutter/cupertino.dart';

import '../screens/follow_user_screen/ui/follow_user_screen.dart';
import '../screens/gallery/create_gallery_screen/ui/create_gallery_screen.dart';
import '../screens/gallery/detail_gallery_screen/ui/detail_gallery_screen.dart';
import '../screens/home_screen/related_post_list_screen/ui/related_post_list_screen.dart';
import '../screens/home_screen/user/profile_user_screen/ui/profile_user_screen.dart';
import '../screens/show_image_screen/ui/show_image_screen.dart';
import '../screens/splash_screen/splash_screen.dart';
import 'route_name.dart';
import 'route_page/home_route_page.dart';
import 'route_page/login_route_page.dart';

abstract class RouteInterFace {
  CupertinoPageRoute routePage(RouteSettings settings);
}

class Routes implements RouteInterFace {
  final _routeLogin = new LoginRoutePage();
  final _routeHome = new HomeRoutePage();
  @override
  CupertinoPageRoute routePage(RouteSettings settings) {
    switch (_groupName(settings.name)) {

      /// Navigate to home route
      case RouteName.home:
        return _routeHome.routePage(settings);

      /// Navigate to show image screen
      case RouteName.image:
        return CupertinoPageRoute(
          builder: (context) => ShowImageScreen(settings.arguments),
          settings: settings,
        );

      /// Navigate to realted screen
      case RouteName.relatedPost:
        return CupertinoPageRoute(
          builder: (context) => RelatedPostListScreen(),
          settings: settings,
        );

      /// Navigate to follow user screen
      case RouteName.follow:
        return CupertinoPageRoute(
          builder: (context) => FollowUserScreen(settings.arguments),
          settings: settings,
        );

      /// Navigate to create gallery screen
      case RouteName.createGallery:
        return CupertinoPageRoute(
          builder: (context) => CreateGalleryScreen(settings.arguments),
          settings: settings,
        );

      /// Navigate to detail gallery screen
      case RouteName.detailGallery:
        return CupertinoPageRoute(
          builder: (context) => DetailGalleryScreen(settings.arguments),
          settings: settings,
        );

      /// Navigate to profile user screen
      case RouteName.profile:
        return CupertinoPageRoute(
          builder: (context) => ProfileUserScreen(settings.arguments),
          settings: settings,
        );

      /// Navigate to login route
      case RouteName.login:
        return _routeLogin.routePage(settings);

      /// initial navigate to flash screen
      default:
        return CupertinoPageRoute(
            builder: (context) => SplashScreen(), settings: settings);
    }
  }

  String _groupName(String routeName) {
    final routesplit = routeName.split('/');
    if (routesplit.length >= 1) return "/${routesplit[1]}";
    return routeName;
  }
}
