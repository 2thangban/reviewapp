abstract class Decodable<T> {
  T decode(dynamic data);
}

class TypeDecodable<T> implements Decodable<TypeDecodable<T>> {
  T value;

  TypeDecodable({this.value});

  @override
  TypeDecodable<T> decode(data) {
    value = data;
    return this;
  }
}
