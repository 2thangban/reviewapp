///list type request
enum APIType {
  ///report
  getAllReportTitle,
  reportPost,

  /// notification
  getAllNotity,
  listNowNotity,
  listOldNotity,
  getCountNewNotify,
  readNotify,
  readAllNotify,
  seenAllNotify,

  /// search
  searchAll,
  searchHashtag,
  searchAccount,
  searchPost,

  /// hashTag
  detailHashtag,
  getAllPostByHashtag,

  /// post
  createPost,
  editPost,
  getAllPosts,
  getLikePosts,
  getAllVideos,
  getPostDetail,
  likePost,
  unlikePost,
  increamentView,

  ///categories
  getAllCategories,

  ///province
  getAllProvinces,

  /// gallery
  getAllGallery,
  getPostGallery,
  createGallery,
  editGallery,
  removeGallery,
  addPostToGallery,
  removePostFromGallery,
  changeStatusGalllery,

  /// user
  login,
  checkPhone,
  register,
  getAllUser,
  signUpSocial,
  getTopUser,
  getProfile,
  getAnotherProfile,
  getPostByUser,
  getListFollowing,
  getListFollower,
  changePassword,
  editProfile,
  changeAvtUser,
  followUser,
  unfollowUser,

  refreshToken,

  /// comment
  getAllComment,
  getAllReplyComment,
  getDetailComment,
  comment,
  editComment,
  deleteComment,
  likeComment,
  unlikeComment,

  /// location
  searchLocation,
  createLocation,
  detailLocation,
  getAllPostLocation,
  getTopLocation,
}
enum HttpMethod { post, get, put, delete, postMultiPart }

class APIConfig {
  final url;
  final method;
  Map<String, String> headers;

  APIConfig({
    this.url,
    this.method,
    this.headers,
  });
}

class APIManager {
  final APIType type;
  final String routeParams;
  String token;

  APIManager({
    this.type,
    this.routeParams,
    this.token,
  });

  final String baseUrl = "http://10.0.2.2/review-app/public";
  APIConfig getConfig() {
    final headers = {'Content-Type': 'application/json'};

    this.token = this.token == null ? '' : 'Bearer ${this.token}';
    final authHeaders = {'Authorization': this.token};

    authHeaders.addAll(headers);

    switch (type) {

      /// ================================ Hashtag ===========================
      /// get all the post by hashtag
      case APIType.getAllPostByHashtag:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/hashtag?$routeParams',
          headers: authHeaders,
        );

      /// report the post
      case APIType.detailHashtag:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/hashtag/detail?$routeParams',
          headers: authHeaders,
        );

      /// ============================== Report Post ==========================
      /// get all the report title list
      case APIType.getAllReportTitle:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/report/report-title',
          headers: authHeaders,
        );

      /// report the post
      case APIType.reportPost:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/report/post',
          headers: authHeaders,
        );

      /// ============================== Gallery ==============================
      /// get all the gallery list of the user
      case APIType.getAllGallery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/gallery/all$routeParams',
          headers: authHeaders,
        );

      /// get list post of gallery
      case APIType.getPostGallery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/post-by-gallery$routeParams',
          headers: authHeaders,
        );

      /// The user creates the gallery
      case APIType.createGallery:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/gallery/create',
          headers: authHeaders,
        );

      /// The user removes the gallery
      case APIType.removeGallery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/gallery/delete$routeParams',
          headers: authHeaders,
        );

      /// The user edits info the gallery
      case APIType.editGallery:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/gallery/edit?$routeParams',
          headers: authHeaders,
        );

      /// The user change status of the gallery
      case APIType.changeStatusGalllery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/gallery/set-private?$routeParams',
          headers: authHeaders,
        );

      /// add favorite post by user
      case APIType.addPostToGallery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/gallery/add-post?$routeParams',
          headers: authHeaders,
        );

      /// remove favorite post by user
      case APIType.removePostFromGallery:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/gallery/remove-post?$routeParams',
          headers: authHeaders,
        );

      /// ============================== Notification ==========================
      /// get num of the notification unseen
      case APIType.getCountNewNotify:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/count-unseen',
          headers: authHeaders,
        );

      /// get the now notification list
      case APIType.listNowNotity:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/get-new?$routeParams',
          headers: authHeaders,
        );

      /// get the now notification list
      case APIType.listOldNotity:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/get-earlier?$routeParams',
          headers: authHeaders,
        );

      /// get the notification list
      case APIType.getAllNotity:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/all?$routeParams',
          headers: authHeaders,
        );

      /// set seen all the notification list
      case APIType.seenAllNotify:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/seen-all',
          headers: authHeaders,
        );

      /// read the notification
      case APIType.readNotify:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/clicked?$routeParams',
          headers: authHeaders,
        );

      /// read the notification
      case APIType.readAllNotify:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/notification/clicked-all',
          headers: authHeaders,
        );

      /// ============================== Search ==================================
      /// search all
      case APIType.searchAll:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/search/all?$routeParams',
          headers: authHeaders,
        );

      /// search account
      case APIType.searchAccount:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/search/user?$routeParams',
          headers: authHeaders,
        );

      /// search hashtag
      case APIType.searchHashtag:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/search/hashtag?$routeParams',
          headers: authHeaders,
        );

      /// search post
      case APIType.searchPost:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/search/post?$routeParams',
          headers: authHeaders,
        );

      /// ============================== POST =====================================
      /// increament view of post
      case APIType.increamentView:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/add-view?$routeParams',
          headers: authHeaders,
        );

      /// get all post
      case APIType.getAllPosts:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/all?$routeParams',
          headers: authHeaders,
        );

      /// get all post
      case APIType.getLikePosts:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/liked-post?$routeParams',
          headers: authHeaders,
        );

      /// get all video
      case APIType.getAllVideos:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/video/all?$routeParams',
          headers: authHeaders,
        );

      /// get detail post
      case APIType.getPostDetail:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/detail?$routeParams',
          headers: authHeaders,
        );

      /// create new post
      case APIType.createPost:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/post/create',
          headers: authHeaders,
        );

      /// create new post
      case APIType.editPost:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/post/edit?$routeParams',
          headers: authHeaders,
        );

      /// ============================== COMMENT ==============================
      /// get detail comment
      case APIType.editComment:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/comment/edit?$routeParams',
          headers: authHeaders,
        );

      /// get detail comment
      case APIType.deleteComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/delete?$routeParams',
          headers: authHeaders,
        );

      /// get detail comment
      case APIType.getDetailComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/detail?$routeParams',
          headers: authHeaders,
        );

      /// get all comment
      case APIType.getAllComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/all?$routeParams',
          headers: authHeaders,
        );

      /// get all reply comment
      case APIType.getAllReplyComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/reply?$routeParams',
          headers: authHeaders,
        );

      /// create comment
      case APIType.comment:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/comment/create',
          headers: authHeaders,
        );

      /// like comment
      case APIType.likeComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/like?$routeParams',
          headers: authHeaders,
        );

      /// unlike comment
      case APIType.unlikeComment:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/comment/unlike?$routeParams',
          headers: authHeaders,
        );

      /// ================================ PROVINCE ===========================
      /// get all provinces
      case APIType.getAllProvinces:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/province/all',
          headers: authHeaders,
        );

      /// ================================ CATEGORY ==========================
      /// get all categories
      case APIType.getAllCategories:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/category/all',
          headers: authHeaders,
        );

      /// =============================== USER ================================
      /// change avatar user
      case APIType.getTopUser:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/top-user$routeParams',
          headers: authHeaders,
        );

      /// change avatar user
      case APIType.changeAvtUser:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/user/change-avatar',
          headers: authHeaders,
        );

      /// change password
      case APIType.changePassword:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/user/change-pass',
          headers: authHeaders,
        );

      /// edit profile
      case APIType.editProfile:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/user/edit-profile',
          headers: authHeaders,
        );

      /// get the list of 100 most follow users
      case APIType.getAllUser:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/top-follower$routeParams',
          headers: authHeaders,
        );

      /// get the follower list
      case APIType.getListFollower:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/list-follower$routeParams',
          headers: authHeaders,
        );

      /// get the following list
      case APIType.getListFollowing:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/list-following$routeParams',
          headers: authHeaders,
        );

      /// get profile
      case APIType.getProfile:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/detail',
          headers: authHeaders,
        );

      /// get profile another user
      case APIType.getAnotherProfile:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/profile$routeParams',
          headers: authHeaders,
        );

      /// get post by user
      case APIType.getPostByUser:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/post/post-by-user?$routeParams',
          headers: authHeaders,
        );

      /// like post
      case APIType.likePost:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/like-post?$routeParams',
          headers: authHeaders,
        );

      /// unlike post
      case APIType.unlikePost:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/unlike-post?$routeParams',
          headers: authHeaders,
        );

      ///follow user
      case APIType.followUser:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/follow?$routeParams',
          headers: authHeaders,
        );

      ///follow user
      case APIType.unfollowUser:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/user/unfollow?$routeParams',
          headers: authHeaders,
        );

      /// login
      case APIType.login:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/login',
          headers: authHeaders,
        );

      /// check PhoneNumber
      case APIType.checkPhone:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/check-phone',
          headers: authHeaders,
        );

      /// sign up gg/fb
      case APIType.signUpSocial:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/sign-up',
          headers: authHeaders,
        );

      /// register user with phone
      case APIType.register:
        return APIConfig(
          method: HttpMethod.postMultiPart,
          url: '$baseUrl/register',
          headers: authHeaders,
        );

      /// refreshToken
      case APIType.refreshToken:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/refresh-token',
          headers: authHeaders,
        );

      ///================================ location ============================
      /// get top locations
      case APIType.getTopLocation:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/location/top-location$routeParams',
          headers: authHeaders,
        );

      /// create new location
      case APIType.createLocation:
        return APIConfig(
          method: HttpMethod.post,
          url: '$baseUrl/location/create',
          headers: authHeaders,
        );

      /// search location
      case APIType.searchLocation:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/search/location$routeParams',
          headers: authHeaders,
        );

      /// search location
      case APIType.detailLocation:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/location/detail$routeParams',
          headers: authHeaders,
        );

      /// get list post by location
      case APIType.getAllPostLocation:
        return APIConfig(
          method: HttpMethod.get,
          url: '$baseUrl/location/post$routeParams',
          headers: authHeaders,
        );
      default:
        return null;
    }
  }
}
