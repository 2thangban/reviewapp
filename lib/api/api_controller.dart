import 'dart:convert';

import 'package:http/http.dart';

import 'api_manager.dart';
import 'api_response.dart';
import 'decodable.dart';

class APIController {
  APIController._();

  static Future<ResponseWraper<T>> request<T extends Decodable>({
    APIManager manager,
    Create<T> create,
    Map<String, dynamic> body,
    List<MultipartFile> listFile,
    MultipartFile sigleFile,
    Map<String, String> query,
  }) async {
    final config = manager.getConfig();

    final response = await requestHandler(
      config: config,
      body: body,
      query: query,
      listFiles: listFile,
      sigleFile: sigleFile,
    );

    final responseJson = jsonDecode(response.body);

    if (response.statusCode == 200) {
      return ResponseWraper.init(create: create, json: responseJson);
    }
    final error = ErrorResponse.fromJson(responseJson, response.statusCode);

    throw error ??
        ErrorResponse(
          message: "Request failed with status code: ${response.statusCode}",
          staticCode: response.statusCode,
        );
  }

  static Future<Response> requestHandler({
    APIConfig config,
    Map<String, dynamic> body,
    Map<String, String> query,
    List<MultipartFile> listFiles,
    MultipartFile sigleFile,
  }) async {
    Response response;

    switch (config.method) {
      case HttpMethod.post:
        final jsonPost = jsonEncode(body);
        response = await post(Uri.parse(config.url),
            headers: config.headers, body: jsonPost);
        break;

      case HttpMethod.get:
        response = await get(Uri.parse(config.url), headers: config.headers);
        break;

      case HttpMethod.put:
        response = await put(Uri.parse(config.url),
            headers: config.headers, body: body);
        break;

      case HttpMethod.delete:
        response = await delete(Uri.parse(config.url), headers: config.headers);
        break;

      case HttpMethod.postMultiPart:
        var request = MultipartRequest('POST', Uri.parse(config.url))
          ..headers.addAll(config.headers);
        if (body != null) request.fields.addAll(body);
        if (listFiles != null) request.files.addAll(listFiles);
        if (sigleFile != null) request.files.add(sigleFile);
        var streamedResponse = await Client().send(request);
        response = await Response.fromStream(streamedResponse);
        break;

      default:
        throw ErrorResponse(message: 'Failed to detect http method');
    }
    return response;
  }
}
