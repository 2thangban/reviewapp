import 'decodable.dart';

typedef Create<T> = T Function();

abstract class GenericObject<T> {
  Create<Decodable> create;

  GenericObject({this.create});

  T genericObject(dynamic data) {
    final item = create();
    return item.decode(data);
  }
}

class ResponseWraper<T> extends GenericObject<T> {
  T response;
  ErrorResponse error;

  ResponseWraper({Create<Decodable> create, this.error})
      : super(create: create);

  factory ResponseWraper.init(
      {Create<Decodable> create, Map<String, dynamic> json}) {
    final wrapper = ResponseWraper<T>(create: create);
    wrapper.response = wrapper.genericObject(json);
    return wrapper;
  }
}

class APIResponse<T> extends GenericObject<T>
    implements Decodable<APIResponse<T>> {
  String status;
  T data;

  APIResponse({Create<Decodable> create}) : super(create: create);

  @override
  APIResponse<T> decode(dynamic json) {
    this.status = json['status'];
    this.data = genericObject(json['data']);
    return this;
  }
}

class APIListReponse<T> extends GenericObject<T>
    implements Decodable<APIListReponse<T>> {
  String status;
  List<T> data;

  APIListReponse({Create<Decodable> create}) : super(create: create);

  @override
  APIListReponse<T> decode(dynamic json) {
    this.status = json['status'];
    data = [];
    json['data'].forEach((item) {
      data.add(genericObject(item));
    });
    return this;
  }
}

class ErrorResponse implements Exception {
  String message;
  int staticCode;
  ErrorResponse({this.message, this.staticCode});
  factory ErrorResponse.fromJson(
    Map<String, dynamic> json,
    int staticCode,
  ) {
    return ErrorResponse(
      message: json['message'] ?? "Something went wrong !!",
      staticCode: staticCode,
    );
  }

  @override
  String toString() {
    return this.message;
  }
}
