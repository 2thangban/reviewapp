import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../commons/helper/size_config.dart';
import '../../commons/untils/app_color.dart';
import '../../commons/untils/app_image.dart';
import '../../routes/route_name.dart';
import '../app_common_bloc/auth_bloc/auth_bloc.dart';
import 'cubit/get_token_cubit.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  AuthBloc authBloc;
  final GetTokenCubit getTokenCubit = GetTokenCubit();
  Widget iconWidget() => Image.asset(AppAssets.appIcon);
  @override
  void initState() {
    authBloc = BlocProvider.of<AuthBloc>(context);
    authBloc.add(CheckLogin());
    getTokenCubit.getNewToken();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    getTokenCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    final theme = Theme.of(context);
    return BlocListener<GetTokenCubit, GetTokenState>(
      bloc: getTokenCubit,
      listener: (context, state) {
        if (state.status == RefreshTokenStatus.success) {
          sleep(Duration(seconds: 1));
          Navigator.pushReplacementNamed(context, RouteName.home);
        } else if (state.status == RefreshTokenStatus.failure) {
          sleep(Duration(seconds: 1));
          Navigator.pushReplacementNamed(context, RouteName.home);
        }
      },
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              AppColors.secondaryColor,
              AppColors.primaryColor,
              // theme.backgroundColor,
              // AppColors.subLightColor,
            ],
          ),
          color: theme.backgroundColor,
        ),
        child: Center(
          child: Container(
            width: sizeScreen * 13,
            height: sizeScreen * 13,
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
            ),
            child: iconWidget(),
          ),
        ),
      ),
    );
  }
}
