import 'package:bloc/bloc.dart';

import '../../../models/token/token.dart';
import '../../../models/token/token_repo.dart';

part 'get_token_state.dart';

class GetTokenCubit extends Cubit<GetTokenState> {
  GetTokenCubit() : super(GetTokenState());

  void getNewToken() async {
    try {
      final token = await TokenRepo.refreshToken();
      await TokenRepo.write(token);
      emit(
        state.cloneWith(status: RefreshTokenStatus.success, token: token),
      );
    } catch (e) {
      emit(state.cloneWith(status: RefreshTokenStatus.failure));
    }
  }
}
