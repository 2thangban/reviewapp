part of 'get_token_cubit.dart';

enum RefreshTokenStatus { initial, success, failure }

class GetTokenState {
  final RefreshTokenStatus status;
  final Token token;

  GetTokenState({
    this.status = RefreshTokenStatus.initial,
    this.token,
  });

  GetTokenState cloneWith({RefreshTokenStatus status, Token token}) =>
      GetTokenState(
        status: status ?? this.status,
        token: token ?? this.token,
      );
}
