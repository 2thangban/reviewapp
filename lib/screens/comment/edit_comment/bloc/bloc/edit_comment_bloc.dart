import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/comment/comment.dart';
import '../../../../../models/comment/comment_repo.dart';
import '../../../../../models/token/token_repo.dart';

part 'edit_comment_event.dart';
part 'edit_comment_state.dart';

class EditCommentBloc extends Bloc<EditCommentEvent, EditCommentState> {
  final int commentId;
  EditCommentBloc(this.commentId) : super(const EditCommentState());

  @override
  Stream<EditCommentState> mapEventToState(
    EditCommentEvent event,
  ) async* {
    if (event is GetEditCommentEvent) {
      yield state.cloneWith(status: EditCommentStatus.loading);
      try {
        final result = await CommentRepo.edit(
          commentId,
          content: event.content,
          imageUpload: event.imageUpLoad,
        );
        yield state.cloneWith(
          status: EditCommentStatus.success,
          comment: result,
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Comment result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value).then((value) async {
              result = await CommentRepo.edit(commentId);
            });
          });
          yield state.cloneWith(
            status: EditCommentStatus.success,
            comment: result,
          );
        } else {
          yield state.cloneWith(
            status: EditCommentStatus.failure,
          );
        }
      } catch (e) {
        yield state.cloneWith(
          status: EditCommentStatus.failure,
        );
      }
    }
  }
}
