part of 'edit_comment_bloc.dart';

abstract class EditCommentEvent {
  const EditCommentEvent();
}

class GetEditCommentEvent extends EditCommentEvent {
  final String content;
  final File imageUpLoad;

  GetEditCommentEvent(this.content, {this.imageUpLoad});
}
