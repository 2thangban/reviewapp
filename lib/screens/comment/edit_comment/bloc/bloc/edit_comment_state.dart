part of 'edit_comment_bloc.dart';

enum EditCommentStatus { initial, loading, success, failure }

class EditCommentState {
  final EditCommentStatus status;
  final Comment comment;
  const EditCommentState({
    this.status = EditCommentStatus.initial,
    this.comment,
  });

  EditCommentState cloneWith({
    EditCommentStatus status,
    Comment comment,
  }) =>
      EditCommentState(
        status: status ?? this.status,
        comment: comment ?? this.comment,
      );
}
