import 'dart:io';

import 'package:bloc/bloc.dart';

class ChangeImageEditCommentCubit extends Cubit<File> {
  File imageUpload;
  ChangeImageEditCommentCubit(this.imageUpload) : super(imageUpload);

  void change(File image) => emit(image);
}
