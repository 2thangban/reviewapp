import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/untils/app_icon.dart';
import '../../../commons/untils/app_image.dart';
import '../../../commons/untils/app_picker.dart';
import '../../../commons/untils/app_shower.dart';
import '../../../commons/widgets/app_bar.dart';
import '../../../commons/widgets/app_button.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../commons/widgets/app_textfield.dart';
import '../../../models/comment/comment.dart';
import 'bloc/bloc/edit_comment_bloc.dart';
import 'bloc/cubit/change_image_edit_comment_cubit.dart';

class EditCommentScreen extends StatefulWidget {
  final Comment comment;
  const EditCommentScreen(this.comment, {Key key}) : super(key: key);
  @override
  _EditCommentScreenState createState() => _EditCommentScreenState();
}

class _EditCommentScreenState extends State<EditCommentScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final FocusNode focusNode = FocusNode();
  TextEditingController textEditingController;
  EditCommentBloc editCommentBloc;

  final ChangeImageEditCommentCubit changeImage =
      ChangeImageEditCommentCubit(null);
  @override
  void initState() {
    super.initState();
    focusNode.requestFocus();
    textEditingController = TextEditingController(
      text: widget.comment.content,
    );
    editCommentBloc = EditCommentBloc(widget.comment.id);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () {
            focusNode.unfocus();
            Future.delayed(
              const Duration(milliseconds: 150),
              () => Navigator.pop(context),
            );
          },
        ),
        titleWidget: Text(
          'Chỉnh sửa',
          style: theme.textTheme.headline5,
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                const Divider(color: AppColors.subLightColor),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: AppTextField.common(
                    controller: textEditingController,
                    focusNode: focusNode,
                    hintText: '',
                    backgroundColor: AppColors.backgroundSearchColor,
                    radius: 5.0,
                    maxLines: 10,
                  ),
                ),
                widget.comment.urlImage != null
                    ? GestureDetector(
                        onTap: () async {
                          await AppPicker.getImageFromGallery().then(
                            (value) {
                              if (value != null) {
                                changeImage.change(value.first);
                              }
                            },
                          );
                        },
                        child: Container(
                          clipBehavior: Clip.antiAlias,
                          margin: const EdgeInsets.all(10.0),
                          height: sizeScreen * 20.0,
                          width: sizeScreen * 20.0,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                              image: NetworkImage(
                                AppAssets.baseUrl + widget.comment.urlImage,
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                          child: BlocBuilder<ChangeImageEditCommentCubit, File>(
                            bloc: changeImage,
                            builder: (_, fileImage) {
                              if (fileImage == null)
                                return const SizedBox.shrink();
                              return Container(
                                color: theme.backgroundColor,
                                child: Stack(
                                  alignment: Alignment.topRight,
                                  children: [
                                    AspectRatio(
                                      aspectRatio: 1,
                                      child: Image.file(
                                        fileImage,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                    AppButton.icon(
                                      icon: AppIcon.delete,
                                      onTap: () {
                                        changeImage.change(null);
                                      },
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                      )
                    : const SizedBox.shrink(),
                Align(
                  alignment: Alignment.topRight,
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: AppButton.common(
                      labelText: 'Sửa ngay',
                      labelStyle: theme.textTheme.headline6.copyWith(
                        color: theme.backgroundColor,
                      ),
                      radius: 5.0,
                      width: sizeScreen * 18.0,
                      backgroundColor: theme.primaryColor,
                      shadowColor: theme.backgroundColor,
                      onPressed: () {
                        focusNode.unfocus();
                        Future.delayed(
                          const Duration(milliseconds: 100),
                          () => editCommentBloc.add(
                            GetEditCommentEvent(
                              textEditingController.text,
                              imageUpLoad: changeImage.imageUpload,
                            ),
                          ),
                        );
                      },
                      contentPadding: 10.0,
                    ),
                  ),
                ),
              ],
            ),
          ),
          BlocConsumer<EditCommentBloc, EditCommentState>(
            bloc: editCommentBloc,
            listener: (_, state) {
              if (state.status == EditCommentStatus.success) {
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  theme: theme,
                  message: "Sửa comment thành công !!",
                  iconColor: AppColors.sussess,
                );
              } else if (state.status == EditCommentStatus.failure) {
                AppShower.showFlushBar(
                  AppIcon.errorCheck,
                  context,
                  theme: theme,
                  message: "Sửa comment thất bại !!",
                  iconColor: AppColors.errorColor,
                );
              }
            },
            builder: (_, state) {
              if (state.status == EditCommentStatus.loading) {
                return Container(
                  color: AppColors.darkThemeColor.withOpacity(0.3),
                  child: Center(
                    child: AppLoading.threeBounce(size: 20.0),
                  ),
                );
              }
              return const SizedBox.shrink();
            },
          ),
        ],
      ),
    );
  }
}
