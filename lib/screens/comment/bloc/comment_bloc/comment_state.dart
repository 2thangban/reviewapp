part of 'comment_bloc.dart';

enum CommentStatus { initial, loading, success, failure, isComment, cancel }

abstract class CommentState {
  const CommentState();
}

class CommentInitialState extends CommentState {}

class SendComment extends CommentState {
  final Comment comment;
  final CommentStatus status;
  final int commentReplyId;
  final String mgsError;
  SendComment({
    this.comment,
    this.status = CommentStatus.initial,
    this.commentReplyId,
    this.mgsError,
  });
}

/// In case the user comments or replies to the comment.
class Commenting extends CommentState {
  /// In case the user reply comment
  final Comment replyComment;

  /// In case the user add image
  final File image;

  final CommentStatus status;

  /// The content of comment
  final String content;

  Commenting({
    this.image,
    this.replyComment,
    this.content,
    this.status = CommentStatus.cancel,
  });
}
