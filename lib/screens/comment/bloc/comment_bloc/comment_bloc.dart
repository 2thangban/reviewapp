import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';

import '../../../../api/api_response.dart';
import '../../../../models/comment/comment.dart';
import '../../../../models/comment/comment_repo.dart';
import '../../../../models/token/token_repo.dart';

part 'comment_event.dart';
part 'comment_state.dart';

class CommentBloc extends Bloc<CommentEvent, CommentState> {
  CommentBloc() : super(CommentInitialState());

  bool contentIsNotEmpty = false;
  String content;
  File imageUpload;

  @override
  Stream<CommentState> mapEventToState(CommentEvent event) async* {
    if (event is PushCommentEvent) {
      try {
        /// hanldes while push comment to server
        yield SendComment(
          comment: event.comment,
          commentReplyId: event.replyCommentId,
          status: CommentStatus.loading,
        );
        content = "";
        imageUpload = null;

        final result = await CommentRepo.comment(
          event.comment,
          commentId: event.replyCommentId,
        ).timeout(const Duration(seconds: 7));

        /// comments successfully then return new comment.
        yield SendComment(
          comment: result,
          status: CommentStatus.success,
          commentReplyId: event.replyCommentId,
        );
      } on ErrorResponse catch (e) {
        ///case expired token
        if (e.staticCode == 401) {
          Comment comment;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            comment = await CommentRepo.comment(
              event.comment,
              commentId: event.replyCommentId,
            );
          });
          content = "";
          imageUpload = null;
          yield SendComment(
            comment: comment,
            status: CommentStatus.success,
            commentReplyId: event.replyCommentId,
          );
        }
        yield SendComment(
          status: CommentStatus.success,
          commentReplyId: event.replyCommentId,
        );
      } on TimeoutException catch (e) {
        yield SendComment(
          comment: event.comment,
          status: CommentStatus.failure,
          commentReplyId: event.replyCommentId,
          mgsError: e.message,
        );
      } catch (e) {
        content = "";
        imageUpload = null;
        yield SendComment(
          comment: event.comment,
          status: CommentStatus.failure,
          commentReplyId: event.replyCommentId,
          mgsError: e.toString(),
        );
      }
    }

    /// handle while user comment
    else if (event is CommentPostEvent) {
      contentIsNotEmpty = (event.content != null && event.content != "");
      if (!event.isReply) {
        content = event.content;
        imageUpload = event.image;
      }

      yield Commenting(
        replyComment: event.commentReply,
        content: event.isReply ? content : event.content,
        image: event.isReply ? imageUpload : event.image,
      );
    }
  }
}
