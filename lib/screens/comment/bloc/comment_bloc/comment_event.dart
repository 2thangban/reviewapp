part of 'comment_bloc.dart';

abstract class CommentEvent {}

/// handles while push comment to sever
class PushCommentEvent extends CommentEvent {
  /// Is id of comment replied
  final int replyCommentId;
  final Comment comment;
  PushCommentEvent({this.replyCommentId, this.comment});
}

/// handles while user comment
class CommentPostEvent extends CommentEvent {
  final String content;
  final File image;
  final bool isReply;
  final Comment commentReply; // In case the user reply comment.
  CommentPostEvent({
    this.commentReply,
    this.isReply = false,
    this.content,
    this.image,
  });
}
