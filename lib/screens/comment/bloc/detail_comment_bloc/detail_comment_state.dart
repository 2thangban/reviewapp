part of 'detail_comment_bloc.dart';

enum DetailCommentStatus { initial, loading, success, failure }

class DetailCommentState extends Equatable {
  final Comment comment;
  final DetailCommentStatus status;
  const DetailCommentState({
    this.comment,
    this.status = DetailCommentStatus.initial,
  });

  DetailCommentState cloneWith({
    Comment comment,
    DetailCommentStatus status,
  }) =>
      DetailCommentState(
        comment: comment ?? this.comment,
        status: status ?? this.status,
      );
  @override
  List<Object> get props => [comment, status];
}
