part of 'detail_comment_bloc.dart';

abstract class DetailCommentEvent {
  const DetailCommentEvent();
}

class GetDetailCommentEvent extends DetailCommentEvent {
  final int commentId;
  GetDetailCommentEvent(this.commentId);
}
