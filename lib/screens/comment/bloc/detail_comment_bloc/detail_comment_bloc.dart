import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../api/api_response.dart';
import '../../../../models/comment/comment.dart';
import '../../../../models/comment/comment_repo.dart';
import '../../../../models/token/token_repo.dart';

part 'detail_comment_event.dart';
part 'detail_comment_state.dart';

class DetailCommentBloc extends Bloc<DetailCommentEvent, DetailCommentState> {
  DetailCommentBloc() : super(const DetailCommentState());

  @override
  Stream<DetailCommentState> mapEventToState(
    DetailCommentEvent event,
  ) async* {
    if (event is GetDetailCommentEvent) {
      yield state.cloneWith(status: DetailCommentStatus.loading);
      try {
        final result = await CommentRepo.getDetail(event.commentId);
        yield state.cloneWith(
          comment: result,
          status: DetailCommentStatus.success,
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Comment result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value).then((value) async {
              result = await CommentRepo.getDetail(event.commentId);
            });
          });
          yield state.cloneWith(
            comment: result,
            status: DetailCommentStatus.success,
          );
        }
      } catch (e) {
        yield state.cloneWith(status: DetailCommentStatus.failure);
      }
    }
  }
}
