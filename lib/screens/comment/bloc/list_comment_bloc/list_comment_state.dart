part of 'list_comment_bloc.dart';

enum ListCommentStatus { initial, loading, success, failure }

class ListCommentState extends Equatable {
  final List<Comment> listComment;
  final ListCommentStatus status;
  final bool hasEndPage;

  const ListCommentState({
    this.listComment = const <Comment>[],
    this.status = ListCommentStatus.initial,
    this.hasEndPage = false,
  });

  ListCommentState copyWith({
    List<Comment> list,
    ListCommentStatus status,
    bool hasEndPage,
  }) =>
      ListCommentState(
        listComment: list ?? this.listComment,
        status: status ?? this.status,
        hasEndPage: hasEndPage ?? this.hasEndPage,
      );

  @override
  List<Object> get props => [this.listComment, this.status, this.hasEndPage];
}
