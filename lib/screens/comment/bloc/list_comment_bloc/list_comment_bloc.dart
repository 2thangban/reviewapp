import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../api/api_response.dart';
import '../../../../models/comment/comment.dart';
import '../../../../models/comment/comment_repo.dart';
import '../../../../models/token/token_repo.dart';

part 'list_comment_event.dart';
part 'list_comment_state.dart';

class ListCommentBloc extends Bloc<ListCommentEvent, ListCommentState> {
  ListCommentBloc() : super(ListCommentState());
  int page = 0;
  final int limit = 4;
  int length = 0;
  @override
  Stream<ListCommentState> mapEventToState(ListCommentEvent event) async* {
    /// handle the user gets the comment list.
    if (event is GetListCommentEvent) {
      if (state.status != ListCommentStatus.initial || event.commentId != null)
        yield state.copyWith(status: ListCommentStatus.loading);

      try {
        final result = event.commentId != null
            ? await CommentRepo.getAllReply(
                event.commentId,
                page: page++,
                limit: limit,
                isAuth: event.isAuth,
              )
            : await CommentRepo.getAll(
                event.postId,
                page: page++,
                limit: limit,
                isAuth: event.isAuth,
              );
        length += result.length;
        yield state.copyWith(
          list: List.of(state.listComment)..addAll(result),
          status: ListCommentStatus.success,
          hasEndPage: result.length < limit,
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          page--;
          List<Comment> listComment;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            listComment = event.commentId != null
                ? await CommentRepo.getAllReply(
                    event.postId,
                    page: page++,
                    limit: limit,
                    isAuth: event.isAuth,
                  )
                : await CommentRepo.getAll(
                    event.postId,
                    page: page++,
                    limit: limit,
                    isAuth: event.isAuth,
                  );
          });
          length += listComment.length;
          yield state.copyWith(
            list: List.of(state.listComment)..addAll(listComment),
            status: ListCommentStatus.success,
            hasEndPage: state.listComment.length < limit,
          );
        } else {
          page--;
          yield state.copyWith(status: ListCommentStatus.failure);
        }
      } catch (e) {
        page--;
        yield state.copyWith(status: ListCommentStatus.failure);
      }
    }

    /// handle add comment to list
    else if (event is AddCommentEvent) {
      length++;
      yield state.copyWith(
        list: List.of(state.listComment)..insert(0, event.comment),
        status: ListCommentStatus.success,
      );
    } else {
      ///
    }
  }
}
