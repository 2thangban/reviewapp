part of 'list_comment_bloc.dart';

abstract class ListCommentEvent {}

/// In case the user gets comment list.
class GetListCommentEvent extends ListCommentEvent {
  final int postId;
  final int commentId;
  final bool isAuth;

  GetListCommentEvent({
    this.postId,
    this.commentId,
    this.isAuth = false,
  });
}

/// handle add comment
class AddCommentEvent extends ListCommentEvent {
  final Comment comment;

  /// The ID of the replied comment. Case user reply comment
  final int commentId;
  AddCommentEvent(this.comment, {this.commentId});
}
