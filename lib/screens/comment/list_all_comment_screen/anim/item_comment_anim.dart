import 'package:flutter/material.dart';

class ItemCommentAnim extends StatefulWidget {
  final Widget child;

  ItemCommentAnim({Key key, this.child}) : super(key: key);
  @override
  _ItemCommentAnimState createState() => _ItemCommentAnimState();
}

class _ItemCommentAnimState extends State<ItemCommentAnim> {
  bool _anim = false;
  bool _isStart = true;

  @override
  void initState() {
    super.initState();
    _isStart
        ? Future.delayed(Duration(milliseconds: 100), () {
            setState(() {
              _anim = true;
              _isStart = false;
            });
          })
        : _anim = true;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedOpacity(
      duration: Duration(milliseconds: 300),
      opacity: _anim ? 1 : 0,
      curve: Curves.easeInOutQuart,
      child: AnimatedPadding(
        duration: Duration(milliseconds: 500),
        padding:
            _anim ? const EdgeInsets.all(1.0) : const EdgeInsets.only(top: 20),
        child: widget.child,
      ),
    );
  }
}
