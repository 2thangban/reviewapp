import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_picker.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_textfield.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/comment/comment.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/favorite_cubit/favorite_cubit.dart';
import '../../../app_common_bloc/like_cubit/like_post_cubit.dart';
import '../../../home_screen/post/detail_post_screen/bloc/post_detail_bloc/post_detail_bloc.dart';
import '../../bloc/comment_bloc/comment_bloc.dart';

class ItemActionComment extends StatefulWidget {
  final FocusNode focusNode;
  final int postId;
  const ItemActionComment({
    Key key,
    this.focusNode,
    this.postId,
  }) : super(key: key);
  @override
  _ItemActionCommentState createState() => _ItemActionCommentState();
}

class _ItemActionCommentState extends State<ItemActionComment> {
  final TextEditingController _contentPostController = TextEditingController();
  final sizeScreen = SizedConfig.heightMultiplier;
  CommentBloc commentBloc;
  LikePostCubit likeCubit;
  FavoriteCubit favoriteCubit;
  AuthBloc authBloc;
  PostDetailBloc postDetailBloc;

  File imageUpload;
  Comment commentReply;
  bool enableSend = false;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    commentBloc = BlocProvider.of<CommentBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);

    return BlocConsumer<CommentBloc, CommentState>(
      listener: (_, state) {},
      builder: (context, state) {
        if (state is SendComment) {
          imageUpload = null;
          enableSend = false;
          commentReply = null;
        } else if (state is Commenting) {
          enableSend = (state.content != "");
          imageUpload = state.image;
          commentReply = state.replyComment;
        }
        return Container(
          color: theme.backgroundColor,
          padding: const EdgeInsets.symmetric(
            vertical: 5.0,
            horizontal: 10.0,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              imageUpload != null
                  ? Container(
                      margin: const EdgeInsets.symmetric(vertical: 10.0),
                      decoration: BoxDecoration(
                        color: AppColors.backgroundSearchColor,
                        borderRadius: BorderRadius.circular(5.0),
                      ),
                      child: Row(
                        children: [
                          Container(
                            alignment: Alignment.topRight,
                            margin: const EdgeInsets.all(5.0),
                            clipBehavior: Clip.antiAlias,
                            height: sizeScreen * 12,
                            width: sizeScreen * 14.0,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              image: DecorationImage(
                                image: FileImage(imageUpload),
                                fit: BoxFit.cover,
                              ),
                            ),
                            child: AppButton.icon(
                              icon: AppIcon.delete,
                              iconSize: sizeScreen * 3.0,
                              iconColor: theme.backgroundColor,
                              onTap: () {
                                /// UnAttach File
                                commentBloc.add(
                                  CommentPostEvent(
                                    image: null,
                                    content: _contentPostController.text,
                                    commentReply: commentReply,
                                  ),
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  : const SizedBox.shrink(),
              commentReply != null
                  ? Padding(
                      padding: const EdgeInsets.only(left: 40.0, bottom: 5.0),
                      child: RichText(
                        text: TextSpan(
                          style: theme.textTheme.bodyText1,
                          text: '${lang.locaized('reply')} : ',
                          children: [
                            TextSpan(
                              text: commentReply.account.userName,
                              style: theme.textTheme.bodyText1.copyWith(
                                color: theme.primaryColor,
                              ),
                            ),
                            WidgetSpan(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5.0),
                                child: AppButton.icon(
                                  icon: AppIcon.delete,
                                  iconSize: sizeScreen * 2.5,
                                  iconColor: AppColors.subLightColor,
                                  onTap: () {
                                    /// unReply comment
                                    commentBloc.add(
                                      CommentPostEvent(
                                        image: imageUpload,
                                        content: _contentPostController.text,
                                        commentReply: null,
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : const SizedBox.shrink(),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GestureDetector(
                    onTap: () async {
                      await AppPicker.getImageFromGallery().then(
                        (value) {
                          if (value != null) {
                            /// attach file
                            commentBloc.add(
                              CommentPostEvent(
                                image: value.first,
                                content: _contentPostController.text,
                                commentReply: commentReply,
                              ),
                            );
                          }
                        },
                      );
                    },
                    child: Icon(AppIcon.addImage, size: sizeScreen * 3.8),
                  ),
                  const SizedBox(width: 5.0),
                  Expanded(
                    child: AppTextField.common(
                      controller: _contentPostController,
                      focusNode: widget.focusNode,
                      hintText: '${lang.locaized('comment')}...',
                      hintStyle: theme.textTheme.bodyText1,
                      maxLines: 10,
                      radius: 30.0,
                      backgroundColor:
                          AppColors.subLightColor.withOpacity(0.15),
                      onChange: (String content) {
                        if (content != null && !commentBloc.contentIsNotEmpty) {
                          commentBloc.add(
                            CommentPostEvent(
                              image: imageUpload,
                              content: content,
                              commentReply: commentReply,
                            ),
                          );
                        } else if (content.isEmpty &&
                            commentBloc.contentIsNotEmpty) {
                          commentBloc.add(
                            CommentPostEvent(
                              image: imageUpload,
                              content: "",
                              commentReply: commentReply,
                            ),
                          );
                        }
                      },
                    ),
                  ),
                  const SizedBox(width: 10.0),
                  AppButton.icon(
                    icon: AppIcon.send,
                    iconColor: enableSend
                        ? theme.primaryColor
                        : AppColors.backgroundEditColor,
                    onTap: () {
                      if (enableSend) {
                        commentBloc.add(
                          PushCommentEvent(
                            comment: Comment(
                              content: _contentPostController.text,
                              imageUpload: imageUpload,
                              postId: widget.postId,
                            ),
                            replyCommentId: commentReply?.id ?? null,
                          ),
                        );
                        widget.focusNode.unfocus();
                        _contentPostController.clear();
                      }
                    },
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
