import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/comment/comment.dart';
import '../../../../routes/route_name.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../bloc/comment_bloc/comment_bloc.dart';
import '../../bloc/list_comment_bloc/list_comment_bloc.dart';
import 'item_action_comment.dart';
import 'item_comment.dart';

class ParamListComment {
  final int postId;
  final bool isComment;
  const ParamListComment(this.postId, {this.isComment = false});
}

class ListCommentScreen extends StatefulWidget {
  final ParamListComment params;

  ListCommentScreen({Key key, @required this.params}) : super(key: key);
  @override
  _ListCommentScreenState createState() => _ListCommentScreenState();
}

class _ListCommentScreenState extends State<ListCommentScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final TextEditingController commentText = TextEditingController();
  final FocusNode commentFocus = FocusNode();
  final ScrollController _scrollController = ScrollController();
  final ListCommentBloc listCommentBloc = ListCommentBloc();
  final CommentBloc commentBloc = CommentBloc();
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);

    /// call event get the comment list
    Future.delayed(
      Duration(milliseconds: 300),
      () => listCommentBloc.add(
        GetListCommentEvent(
          postId: widget.params.postId,
          isAuth: authBloc.isChecked,
        ),
      ),
    );

    /// set listener while scroll the list
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          !listCommentBloc.state.hasEndPage) {
        listCommentBloc.add(
          GetListCommentEvent(
            postId: widget.params.postId,
            isAuth: authBloc.isChecked,
          ),
        );
      }
    });

    if (widget.params.isComment) {
      Future.delayed(
        const Duration(milliseconds: 200),
        () => commentFocus.requestFocus(),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
    commentText.dispose();
    commentFocus.dispose();
    commentBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        titleWidget: Text(
          AppLocalization.of(context).locaized('comment'),
          style: theme.textTheme.headline5,
        ),
        centerTitle: true,
        leftBarButtonItem: AppButton.icon(
          icon: CupertinoIcons.back,
          onTap: () {
            commentFocus.unfocus();
            Future.delayed(
              Duration(milliseconds: 200),
              () => Navigator.pop(context),
            );
          },
        ),
      ),
      body: BlocProvider(
        create: (context) => commentBloc,
        child: Column(
          children: [
            const Divider(height: 1.0, thickness: 1),
            BlocBuilder<CommentBloc, CommentState>(
              bloc: commentBloc,
              buildWhen: (previous, current) => current is SendComment,
              builder: (_, state) {
                if (state is SendComment) {
                  if (state.commentReplyId == null) {
                    if (state.status == CommentStatus.success) {
                      listCommentBloc.add(
                        AddCommentEvent(state.comment),
                      );
                      return const SizedBox.shrink();
                    }
                    return Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            height: sizeScreen * 6.0,
                            width: sizeScreen * 6.0,
                            decoration: BoxDecoration(
                              color: AppColors.primaryColor,
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                image: NetworkImage(
                                  AppAssets.baseUrl + authBloc.account.avatar,
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Column(
                            children: [
                              ConstrainedBox(
                                constraints: BoxConstraints.loose(
                                  Size.fromWidth(sizeScreen * 40.0),
                                ),
                                child: Container(
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  padding: const EdgeInsets.all(10.0),
                                  decoration: BoxDecoration(
                                    color: AppColors.backgroundSearchColor,
                                    borderRadius: BorderRadius.circular(15.0),
                                  ),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        authBloc.account.userName,
                                        style: theme.textTheme.headline6,
                                      ),
                                      Text(
                                        state.comment.content,
                                        style: theme.textTheme.subtitle1,
                                        maxLines: 7,
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              state.comment.imageUpload != null
                                  ? Container(
                                      margin: const EdgeInsets.symmetric(
                                        vertical: 5.0,
                                      ),
                                      width: 150,
                                      height: 120,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        image: DecorationImage(
                                          image: FileImage(
                                            state.comment.imageUpload,
                                          ),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    )
                                  : const SizedBox.shrink(),
                            ],
                          ),
                        ],
                      ),
                    );
                  }
                }
                return const SizedBox.shrink();
              },
            ),
            Flexible(
              child: BlocBuilder<ListCommentBloc, ListCommentState>(
                bloc: listCommentBloc,
                builder: (context, state) {
                  if (state.status == ListCommentStatus.initial) {
                    return Center(child: AppLoading.threeBounce(size: 20));
                  } else if (state.status == ListCommentStatus.failure) {
                    return Container();
                  }
                  return ListView.builder(
                    controller: _scrollController,
                    padding: const EdgeInsets.symmetric(horizontal: 10.0),
                    itemCount: state.listComment.length + 1,
                    itemBuilder: (_, index) => index == state.listComment.length
                        ? Column(
                            children: [
                              state.status == ListCommentStatus.loading
                                  ? Center(
                                      child: AppLoading.threeBounce(size: 20),
                                    )
                                  : const SizedBox.shrink(),
                              const SizedBox(height: 50.0)
                            ],
                          )
                        : GestureDetector(
                            onLongPress: () => showOptionComment(
                              authBloc.account.id !=
                                  state.listComment[index].account.id,
                              state.listComment[index],
                            ),
                            child: ItemComment(
                              comment: state.listComment[index],
                              focusNode: commentFocus,
                              key: ValueKey<int>(index),
                            ),
                          ),
                  );
                },
              ),
            ),
            ItemActionComment(
              focusNode: commentFocus,
              postId: widget.params.postId,
            ),
          ],
        ),
      ),
    );
  }

  void showOptionComment(bool isAuth, Comment comment) async {
    return await showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: context,
      builder: (context) {
        final lang = AppLocalization.of(context);
        final theme = Theme.of(context);
        return Container(
          decoration: BoxDecoration(
            color: theme.backgroundColor,
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(10.0),
              right: Radius.circular(10.0),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    Text(
                      lang.locaized('comment'),
                      style: theme.textTheme.headline4,
                    ),
                    const Spacer(),
                    AppButton.icon(
                      icon: AppIcon.exitNavigate,
                      onTap: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
              const Divider(color: AppColors.subLightColor),
              Column(
                children: [
                  itemOptionComment(
                    AppIcon.comment,
                    'Trả lời',
                    () {},
                    theme,
                  ),
                  isAuth
                      ? const SizedBox.shrink()
                      : itemOptionComment(
                          AppIcon.edit,
                          'Chỉnh sửa',
                          () async => await Navigator.pushNamed(
                            context,
                            RouteName.editcomment,
                            arguments: comment,
                          ),
                          theme,
                        ),
                  isAuth
                      ? const SizedBox.shrink()
                      : itemOptionComment(
                          AppIcon.remove,
                          'Xóa',
                          () {
                            Navigator.pop(context);
                            Future.delayed(
                              const Duration(milliseconds: 150),
                              () => showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: Text(
                                    'Bạn có chắc xóa bình luận này',
                                    style: theme.textTheme.headline6,
                                  ),
                                  actions: [
                                    AppButton.text(
                                      label: 'Xóa',
                                      labelStyle:
                                          theme.textTheme.bodyText1.copyWith(
                                        color: theme.primaryColor,
                                      ),
                                      onTap: () {},
                                    ),
                                    AppButton.text(
                                      label: 'Hủy',
                                      labelStyle:
                                          theme.textTheme.bodyText1.copyWith(
                                        color: theme.primaryColor,
                                      ),
                                      onTap: () => Navigator.pop(context),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          theme,
                        ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget itemOptionComment(
    IconData icon,
    String label,
    Function onPressed,
    ThemeData theme,
  ) =>
      ListTile(
        leading: Icon(
          icon,
          color: theme.iconTheme.color,
        ),
        title: Text(
          label,
          style: theme.textTheme.headline6,
        ),
        onTap: onPressed,
      );
}
