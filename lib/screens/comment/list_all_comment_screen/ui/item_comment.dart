import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/animation/icon_button_anim.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/comment/comment.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/like_comment_cubit/like_comment_cubit.dart';
import '../../bloc/comment_bloc/comment_bloc.dart';
import '../../bloc/list_comment_bloc/list_comment_bloc.dart';
import '../anim/item_comment_anim.dart';

/// This class used show a comment
class ItemComment extends StatefulWidget {
  final Comment comment;
  final bool isReply;
  final FocusNode focusNode;
  const ItemComment({
    Key key,
    this.comment,
    this.isReply = false,
    this.focusNode,
  })  : assert(comment != null),
        super(key: key);

  @override
  _ItemCommentState createState() => _ItemCommentState();
}

class _ItemCommentState extends State<ItemComment>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final Widget noneWidget = SizedBox.shrink();
  final ListCommentBloc listReplyCommentBloc = ListCommentBloc();
  CommentBloc commentBloc;
  LikeCommentCubit likeCommentCubit;
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    likeCommentCubit = LikeCommentCubit(
      widget.comment.numOfLike,
      widget.comment.isLike,
      widget.comment.id,
    );
    authBloc = BlocProvider.of<AuthBloc>(context);
    commentBloc = BlocProvider.of<CommentBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    likeCommentCubit.close();
    listReplyCommentBloc.close();
  }

  void showOptionComment(bool isAuth) async {
    return await showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: context,
      builder: (context) {
        final lang = AppLocalization.of(context);
        final theme = Theme.of(context);
        return Container(
          decoration: BoxDecoration(
            color: theme.backgroundColor,
            borderRadius: BorderRadius.horizontal(
              left: Radius.circular(10.0),
              right: Radius.circular(10.0),
            ),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Row(
                  children: [
                    Text(
                      lang.locaized('comment'),
                      style: theme.textTheme.headline4,
                    ),
                    const Spacer(),
                    AppButton.icon(
                      icon: AppIcon.exitNavigate,
                      onTap: () => Navigator.pop(context),
                    ),
                  ],
                ),
              ),
              const Divider(color: AppColors.subLightColor),
              Column(
                children: [
                  isAuth
                      ? const SizedBox.shrink()
                      : itemOptionComment(
                          AppIcon.comment,
                          'Chỉnh sửa',
                          () {},
                          theme,
                        ),
                  isAuth
                      ? const SizedBox.shrink()
                      : itemOptionComment(
                          AppIcon.remove,
                          'Xóa',
                          () {},
                          theme,
                        ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }

  Widget itemOptionComment(
    IconData icon,
    String label,
    Function onPressed,
    ThemeData theme,
  ) =>
      ListTile(
        leading: Icon(
          icon,
          color: theme.iconTheme.color,
        ),
        title: Text(
          label,
          style: theme.textTheme.headline6,
        ),
        onTap: onPressed,
      );

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return ItemCommentAnim(
      key: widget.key,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          /// show user and content comment
          Padding(
            padding: widget.isReply
                ? const EdgeInsets.only(top: 5.0, bottom: 2.5)
                : const EdgeInsets.only(top: 10.0, bottom: 5.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => Future.delayed(
                    Duration(milliseconds: 100),
                    () => Navigator.pushNamed(
                      context,
                      RouteName.profile,
                      arguments: widget.comment.account.id,
                    ),
                  ),
                  child: Container(
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(AppAssets.imgBanner1),
                        fit: BoxFit.contain,
                      ),
                    ),
                    child: Image.network(
                      AppAssets.baseUrl + widget.comment.account.avatar,
                      fit: BoxFit.cover,
                      width: widget.isReply ? 40 : 45,
                      height: widget.isReply ? 40 : 45,
                    ),
                  ),
                ),
                const SizedBox(width: 10.0),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.all(10.0),
                      decoration: BoxDecoration(
                        color: Colors.grey.withOpacity(0.2),
                        borderRadius: BorderRadius.circular(15.0),
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            widget.comment.account.userName,
                            style: widget.isReply
                                ? theme.textTheme.bodyText1
                                : theme.textTheme.headline6,
                          ),
                          const SizedBox(height: 5.0),
                          ConstrainedBox(
                            constraints: BoxConstraints(
                              maxWidth: MediaQuery.of(context).size.width / 2,
                            ),
                            child: Text(
                              widget.comment.content,
                              style: theme.textTheme.bodyText1.copyWith(
                                fontWeight: AppFontWeight.regular,
                                height: 1.0,
                                fontSize: widget.isReply
                                    ? sizeText * 1.8
                                    : sizeText * 2.0,
                              ),
                              maxLines: 4,
                              overflow: TextOverflow.ellipsis,
                              softWrap: true,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 2.5),

                    /// comment's information
                    Row(
                      children: [
                        Text(
                          widget.comment.customeDate,
                          style: theme.textTheme.subtitle2.copyWith(
                            color: AppColors.subLightColor,
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                        widget.isReply
                            ? noneWidget
                            : GestureDetector(
                                onTap: () {
                                  widget.focusNode.requestFocus();
                                  commentBloc.add(
                                    CommentPostEvent(
                                      commentReply: widget.comment,
                                      isReply: true,
                                    ),
                                  );
                                },
                                child: RichText(
                                  text: TextSpan(
                                    text: ' . ',
                                    style: theme.textTheme.bodyText1,
                                    children: [
                                      TextSpan(
                                        text: lang.locaized('reply'),
                                        style:
                                            theme.textTheme.bodyText1.copyWith(
                                          color: theme.primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                      ],
                    ),

                    // /// comment's image
                    widget.comment.urlImage != ''
                        ? Container(
                            constraints: BoxConstraints(
                              maxHeight:
                                  sizeScreen * (widget.isReply ? 20 : 25.0),
                              maxWidth:
                                  sizeScreen * (widget.isReply ? 15 : 20.0),
                            ),
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              border: Border.all(
                                color: AppColors.backgroundSearchColor,
                              ),
                              borderRadius: BorderRadius.circular(10.0),
                              image: DecorationImage(
                                image: AssetImage(AppAssets.imgBanner1),
                              ),
                            ),
                            child: AspectRatio(
                              aspectRatio: 1,
                              child: Image.network(
                                AppAssets.baseUrl + widget.comment.urlImage,
                              ),
                            ),
                          )
                        : noneWidget,
                  ],
                ),
                const Spacer(),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10.0,
                  ),
                  child: BlocBuilder<LikeCommentCubit, bool>(
                    bloc: likeCommentCubit,
                    builder: (context, status) {
                      return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButtonAnim(
                            status: status,
                            child: AppButton.icon(
                              icon: status ? AppIcon.liked : AppIcon.unLike,
                              iconSize:
                                  sizeScreen * (widget.isReply ? 2.6 : 2.8),
                              iconColor: status
                                  ? AppColors.likeColor
                                  : AppColors.subLightColor,
                              onTap: () async {
                                likeCommentCubit.change(status);
                              },
                            ),
                          ),
                          const SizedBox(width: 2.0),
                          Text(
                            likeCommentCubit.numOfLikeComment != 0
                                ? likeCommentCubit.numOfLikeComment.toString()
                                : '',
                            style: widget.isReply
                                ? theme.textTheme.bodyText1
                                : theme.textTheme.headline6,
                          ),
                        ],
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          BlocBuilder<CommentBloc, CommentState>(
            bloc: commentBloc,
            buildWhen: (previous, current) => current is SendComment,
            builder: (_, state) {
              if (state is SendComment) {
                if (state.commentReplyId != null &&
                    state.commentReplyId == widget.comment.id) {
                  if (state.status == CommentStatus.success) {
                    listReplyCommentBloc.add(
                      AddCommentEvent(state.comment),
                    );
                    return const SizedBox.shrink();
                  }
                  return Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: sizeScreen * 5.0,
                          width: sizeScreen * 5.0,
                          decoration: BoxDecoration(
                            color: AppColors.primaryColor,
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: NetworkImage(
                                AppAssets.baseUrl + authBloc.account.avatar,
                              ),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ConstrainedBox(
                              constraints: BoxConstraints.loose(
                                Size.fromWidth(sizeScreen * 40.0),
                              ),
                              child: Container(
                                margin: const EdgeInsets.symmetric(
                                    horizontal: 10.0),
                                padding: const EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                  color: AppColors.backgroundSearchColor,
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      authBloc.account.userName,
                                      style: theme.textTheme.headline6,
                                    ),
                                    Text(
                                      state.comment.content,
                                      style: theme.textTheme.subtitle1,
                                      maxLines: 7,
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            state.comment.imageUpload != null
                                ? Container(
                                    margin: const EdgeInsets.symmetric(
                                      vertical: 5.0,
                                      horizontal: 10.0,
                                    ),
                                    width: 120,
                                    height: 100,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      image: DecorationImage(
                                        image: AssetImage(AppAssets.img1),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ],
                        ),
                      ],
                    ),
                  );
                }
              }
              return const SizedBox.shrink();
            },
          ),
          widget.comment.numOfReply != 0
              ? Padding(
                  padding: const EdgeInsets.only(left: 60.0, bottom: 10.0),
                  child: BlocBuilder<ListCommentBloc, ListCommentState>(
                    bloc: listReplyCommentBloc,
                    builder: (context, state) {
                      int numCommentReply =
                          widget.comment.numOfReply - state.listComment.length;
                      return ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: state.listComment.length + 1,
                        itemBuilder: (_, index) => index ==
                                state.listComment.length
                            ? state.status == ListCommentStatus.loading
                                ? const Center(
                                    child: Align(
                                      alignment: Alignment.topLeft,
                                      child: SizedBox(
                                        height: 18.0,
                                        width: 18.0,
                                        child: CircularProgressIndicator(
                                          strokeWidth: 2.0,
                                        ),
                                      ),
                                    ),
                                  )
                                : numCommentReply != 0
                                    ? GestureDetector(
                                        onTap: () => listReplyCommentBloc.add(
                                          GetListCommentEvent(
                                            commentId: widget.comment.id,
                                            isAuth: authBloc.isChecked,
                                          ),
                                        ),
                                        child: Text(
                                          '$numCommentReply câu trả lời khác ...',
                                          style: theme.textTheme.bodyText1,
                                        ),
                                      )
                                    : noneWidget
                            : GestureDetector(
                                onLongPress: () => showOptionComment(
                                    authBloc.account.id !=
                                        state.listComment[index].account.id),
                                child: ItemComment(
                                  key: ValueKey<int>(index),
                                  comment: state.listComment[index],
                                  isReply: true,
                                ),
                              ),
                      );
                    },
                  ),
                )
              : noneWidget,
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
