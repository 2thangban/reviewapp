// import 'package:app_review/commons/helper/size_config.dart';
// import 'package:app_review/commons/untils/app_color.dart';
// import 'package:app_review/commons/untils/app_icon.dart';
// import 'package:app_review/commons/widgets/app_button.dart';
// import 'package:app_review/screens/app_common_bloc/auth_bloc/auth_bloc.dart';
// import 'package:app_review/screens/app_common_bloc/comment_bloc/comment_bloc.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';

// class MoreActionComment extends StatefulWidget {
//   const MoreActionComment({Key key}) : super(key: key);
//   @override
//   _MoreActionCommentState createState() => _MoreActionCommentState();
// }

// class _MoreActionCommentState extends State<MoreActionComment> {
//   CommentBloc commentBloc;
//   AuthBloc authBloc;
//   @override
//   void initState() {
//     super.initState();
//     commentBloc = BlocProvider.of<CommentBloc>(context);
//     authBloc = BlocProvider.of<AuthBloc>(context);
//   }

//   @override
//   Widget build(BuildContext context) {
//     final theme = Theme.of(context);
//     final sizeScreen = SizedConfig.heightMultiplier;
//     return BlocBuilder<CommentBloc, CommentState>(
//       bloc: commentBloc,
//       builder: (context, state) {
//         return Visibility(
//           visible: !(state is SendComment),
//           child: Column(
//             mainAxisSize: MainAxisSize.min,
//             children: [
//               /// show comment's image
//               (state is Commenting)
//                   ? state.image != null
//                       ? Container(
//                           margin: const EdgeInsets.only(bottom: 5.0),
//                           padding: const EdgeInsets.symmetric(
//                             vertical: 5.0,
//                             horizontal: 10.0,
//                           ),
//                           decoration: BoxDecoration(
//                             color: AppColors.backgroundSearchColor,
//                             borderRadius: BorderRadius.circular(10.0),
//                             border: Border.all(
//                               color: AppColors.backgroundEditColor,
//                             ),
//                           ),
//                           child: Row(
//                             mainAxisAlignment: MainAxisAlignment.start,
//                             children: [
//                               Stack(
//                                 alignment: Alignment.topRight,
//                                 children: [
//                                   Container(
//                                     clipBehavior: Clip.antiAlias,
//                                     constraints: BoxConstraints(
//                                       maxHeight: sizeScreen * 10,
//                                       maxWidth: sizeScreen * 20,
//                                       minHeight: sizeScreen * 10,
//                                       minWidth: sizeScreen * 10,
//                                     ),
//                                     decoration: BoxDecoration(
//                                       borderRadius: BorderRadius.circular(10.0),
//                                     ),
//                                     child: Image.file(
//                                       commentBloc.currentComment.imageUpload,
//                                       fit: BoxFit.fill,
//                                     ),
//                                   ),
//                                   AppButton.icon(
//                                     padding: const EdgeInsets.all(5.0),
//                                     icon: AppIcon.delete,
//                                     iconSize: sizeScreen * 2.5,
//                                     iconColor: AppColors.lightThemeColor,
//                                     onTap: () {
//                                       /// call event remove image
//                                       commentBloc.add(
//                                         RemoveImageCommentEvent(true),
//                                       );
//                                     },
//                                   ),
//                                 ],
//                               ),
//                             ],
//                           ),
//                         )
//                       : const SizedBox()
//                   : const SizedBox(),

//               /// show user reply
//               state is Commenting
//                   ? Visibility(
//                       child: Visibility(
//                         visible: state.account != null,
//                         child: Container(
//                           color: theme.backgroundColor,
//                           child: Row(
//                             children: [
//                               Text.rich(
//                                 TextSpan(
//                                   text: 'Trả lời: ',
//                                   children: [
//                                     TextSpan(
//                                       text: state.account?.id ==
//                                               authBloc.account.id
//                                           ? 'Chính mình'
//                                           : state.account?.userName,
//                                       style: theme.textTheme.bodyText1,
//                                     ),
//                                   ],
//                                 ),
//                               ),
//                               const SizedBox(width: 10.0),
//                               AppButton.text(
//                                 label: 'Hủy',
//                                 labelStyle: theme.textTheme.bodyText1.copyWith(
//                                   color: theme.primaryColor,
//                                 ),
//                                 contentPadding: 5.0,
//                                 onTap: () {
//                                   commentBloc.add(
//                                     CommentPostEvent(
//                                       isComment:
//                                           state.image != null ? true : false,
//                                       content:
//                                           commentBloc.currentComment.content,
//                                     ),
//                                   );
//                                 },
//                               ),
//                             ],
//                           ),
//                         ),
//                       ),
//                     )
//                   : const SizedBox(),
//             ],
//           ),
//         );
//       },
//     );
//   }
// }
