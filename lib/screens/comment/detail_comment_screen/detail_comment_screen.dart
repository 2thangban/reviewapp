import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/untils/app_icon.dart';
import '../../../commons/untils/app_picker.dart';
import '../../../commons/widgets/app_bar.dart';
import '../../../commons/widgets/app_button.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../commons/widgets/app_textfield.dart';
import '../../../localizations/app_localization.dart';
import '../../../routes/route_name.dart';
import '../../../theme.dart';
import '../bloc/comment_bloc/comment_bloc.dart';
import '../bloc/list_comment_bloc/list_comment_bloc.dart';
import '../list_all_comment_screen/ui/item_comment.dart';
import 'item_comment_detail.dart';

class DetailCommentScreen extends StatefulWidget {
  final Map<String, dynamic> jsonParams;
  DetailCommentScreen(this.jsonParams, {Key key}) : super(key: key);
  @override
  _DetailCommentScreenState createState() => _DetailCommentScreenState();
}

class _DetailCommentScreenState extends State<DetailCommentScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.heightMultiplier;
  final FocusNode commentFocus = FocusNode();
  final TextEditingController commentController = TextEditingController();
  ListCommentBloc listCommentBloc = ListCommentBloc();
  CommentBloc commentBloc = CommentBloc();
  File file;
  @override
  void dispose() {
    super.dispose();
    listCommentBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider<ListCommentBloc>(create: (context) => listCommentBloc),
        BlocProvider<CommentBloc>(create: (context) => commentBloc),
      ],
      child: Scaffold(
        appBar: CommonAppBar(
          height: sizeScreen * 6.0,
          leftBarButtonItem: AppButton.icon(
            icon: AppIcon.popNavigate,
            onTap: () => Future.delayed(
              const Duration(milliseconds: 150),
              () => Navigator.pop(context),
            ),
          ),
          titleWidget: Text(
            lang.locaized('reply'),
            style: theme.textTheme.headline4,
          ),
        ),
        body: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Divider(color: AppColors.subLightColor),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: RichText(
                    text: TextSpan(
                      text: "Bình luận trên ",
                      style: theme.textTheme.headline6,
                      children: [
                        WidgetSpan(
                          child: AppButton.text(
                            contentPadding: 0.0,
                            label: lang.locaized('post'),
                            labelStyle: theme.textTheme.headline6.copyWith(
                              color: theme.primaryColor,
                            ),
                            onTap: () => Future.delayed(
                              const Duration(milliseconds: 150),
                              () => Navigator.pushNamed(
                                  context, RouteName.postDetail,
                                  arguments: widget.jsonParams['postId']),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Flexible(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        /// show parent comment
                        ItemCommentDetail(
                          widget.jsonParams["commentId"],
                          commentFocus,
                        ),

                        /// show list reply comment
                        BlocBuilder<ListCommentBloc, ListCommentState>(
                          bloc: listCommentBloc,
                          builder: (_, state) {
                            if (state.status == ListCommentStatus.failure) {
                              return Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                      'Đã xãy ra lỗi khi kết nối!!',
                                      style: theme.textTheme.headline6.copyWith(
                                        color: AppColors.subLightColor,
                                        fontWeight: AppFontWeight.medium,
                                        fontStyle: FontStyle.italic,
                                      ),
                                    ),
                                    const SizedBox(height: 5.0),
                                    AppButton.custom(
                                      icon: AppIcon.reload,
                                      width: sizeScreen * 20,
                                      label: 'Tải lại',
                                      textStyle: theme.textTheme.subtitle1,
                                      isLeftIcon: true,
                                      backgroundColor: AppColors.listBgColor[0],
                                      onPress: () => listCommentBloc.add(
                                        GetListCommentEvent(),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            }
                            return ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              padding: const EdgeInsets.only(left: 60.0),
                              itemCount: state.listComment.length + 1,
                              itemBuilder: (_, index) => index ==
                                      state.listComment.length
                                  ? state.status == ListCommentStatus.loading
                                      ? AppLoading.threeBounce(size: 20.0)
                                      : state.hasEndPage
                                          ? const SizedBox(height: 70.0)
                                          : AppButton.text(
                                              label: 'Bình luận cũ hơn',
                                              labelStyle: theme
                                                  .textTheme.bodyText1
                                                  .copyWith(
                                                color: theme.primaryColor,
                                              ),
                                              onTap: () => listCommentBloc.add(
                                                GetListCommentEvent(
                                                  commentId: widget
                                                      .jsonParams['commentId'],
                                                  isAuth: true,
                                                ),
                                              ),
                                            )
                                  : ItemComment(
                                      comment: state.listComment[index],
                                      isReply: true,
                                      key: ValueKey<int>(index),
                                    ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),

            /// show button comment
            BlocBuilder<CommentBloc, CommentState>(
              bloc: commentBloc,
              builder: (context, state) {
                return Container(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    color: theme.backgroundColor,
                    padding: const EdgeInsets.symmetric(
                      vertical: 5.0,
                      horizontal: 10.0,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () async {
                                await AppPicker.getImageFromGallery();
                              },
                              child: Icon(
                                AppIcon.addImage,
                                size: sizeScreen * 3.8,
                              ),
                            ),
                            const SizedBox(width: 5.0),
                            Expanded(
                              child: AppTextField.common(
                                controller: commentController,
                                focusNode: commentFocus,
                                hintText: '${lang.locaized('comment')}...',
                                hintStyle: theme.textTheme.bodyText1,
                                maxLines: 10,
                                radius: 30.0,
                                backgroundColor:
                                    AppColors.subLightColor.withOpacity(0.15),
                              ),
                            ),
                            const SizedBox(width: 10.0),
                            AppButton.icon(
                              icon: AppIcon.send,
                              iconSize: sizeScreen * 3.8,
                              onTap: () {},
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
