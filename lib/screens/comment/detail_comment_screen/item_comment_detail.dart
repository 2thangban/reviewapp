import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/animation/fade_anim.dart';
import '../../../commons/animation/icon_button_anim.dart';
import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/untils/app_icon.dart';
import '../../../commons/untils/app_image.dart';
import '../../../commons/widgets/app_button.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../localizations/app_localization.dart';
import '../../../routes/route_name.dart';
import '../../../theme.dart';
import '../../app_common_bloc/like_comment_cubit/like_comment_cubit.dart';
import '../bloc/detail_comment_bloc/detail_comment_bloc.dart';
import '../bloc/list_comment_bloc/list_comment_bloc.dart';

class ItemCommentDetail extends StatefulWidget {
  final int commentId;
  final FocusNode commentFocus;
  const ItemCommentDetail(
    this.commentId,
    this.commentFocus, {
    Key key,
  }) : super(key: key);
  @override
  _ItemCommentDetailState createState() => _ItemCommentDetailState();
}

class _ItemCommentDetailState extends State<ItemCommentDetail> {
  final DetailCommentBloc detailCommentBloc = DetailCommentBloc();
  final sizeText = SizedConfig.heightMultiplier;
  final sizeScreen = SizedConfig.heightMultiplier;
  ListCommentBloc listCommentBloc = ListCommentBloc();
  LikeCommentCubit likeCommentCubit;
  @override
  void initState() {
    super.initState();
    detailCommentBloc.add(GetDetailCommentEvent(widget.commentId));
    listCommentBloc = BlocProvider.of<ListCommentBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    detailCommentBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return BlocBuilder<DetailCommentBloc, DetailCommentState>(
      bloc: detailCommentBloc,
      builder: (_, state) {
        if (state.status == DetailCommentStatus.success) {
          likeCommentCubit = LikeCommentCubit(
            state.comment.numOfLike,
            state.comment.isLike,
            state.comment.id,
          );
          Future.delayed(
            const Duration(microseconds: 100),
            () => listCommentBloc.add(GetListCommentEvent(
              commentId: state.comment.id,
              isAuth: true,
            )),
          );
          return FadeAnim(
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(15.0, 10.0, 15.0, 5.0),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      GestureDetector(
                        onTap: () => Future.delayed(
                          Duration(milliseconds: 100),
                          () => Navigator.pushNamed(
                            context,
                            RouteName.profile,
                            arguments: state.comment.account.id,
                          ),
                        ),
                        child: Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                              image: AssetImage(AppAssets.imgBanner1),
                              fit: BoxFit.contain,
                            ),
                          ),
                          child: Image.network(
                            AppAssets.baseUrl + state.comment.account.avatar,
                            fit: BoxFit.cover,
                            width: 45,
                            height: 45,
                          ),
                        ),
                      ),
                      const SizedBox(width: 10.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: const EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              color: Colors.grey.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(15.0),
                            ),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  state.comment.account.userName,
                                  style: theme.textTheme.headline6,
                                ),
                                const SizedBox(height: 5.0),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                    maxWidth:
                                        MediaQuery.of(context).size.width / 2,
                                  ),
                                  child: Text(
                                    state.comment.content,
                                    style: theme.textTheme.bodyText1.copyWith(
                                      fontWeight: AppFontWeight.regular,
                                      height: 1.0,
                                      fontSize: sizeText * 2.0,
                                    ),
                                    maxLines: 4,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                          const SizedBox(height: 2.5),

                          /// comment's information
                          Row(
                            children: [
                              Text(
                                state.comment.customeDate,
                                style: theme.textTheme.subtitle2.copyWith(
                                  color: AppColors.subLightColor,
                                  fontWeight: AppFontWeight.regular,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  widget.commentFocus.requestFocus();
                                },
                                child: RichText(
                                  text: TextSpan(
                                    text: ' . ',
                                    style: theme.textTheme.bodyText1,
                                    children: [
                                      TextSpan(
                                        text: lang.locaized('reply'),
                                        style:
                                            theme.textTheme.bodyText1.copyWith(
                                          color: theme.primaryColor,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),

                          /// comment's image
                          state.comment.urlImage != ''
                              ? Container(
                                  constraints: BoxConstraints(
                                    maxHeight: sizeScreen * 25.0,
                                    maxWidth: sizeScreen * 20.0,
                                  ),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      color: AppColors.backgroundSearchColor,
                                    ),
                                    borderRadius: BorderRadius.circular(10.0),
                                    image: DecorationImage(
                                      image: AssetImage(AppAssets.imgBanner1),
                                    ),
                                  ),
                                  child: AspectRatio(
                                    aspectRatio: 1,
                                    child: Image.network(
                                      AppAssets.baseUrl +
                                          state.comment.urlImage,
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink(),
                        ],
                      ),
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 10.0,
                        ),
                        child: BlocBuilder<LikeCommentCubit, bool>(
                          bloc: likeCommentCubit,
                          builder: (context, status) {
                            return Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButtonAnim(
                                  status: status,
                                  child: AppButton.icon(
                                    icon:
                                        status ? AppIcon.liked : AppIcon.unLike,
                                    iconSize: sizeScreen * 2.8,
                                    iconColor: status
                                        ? AppColors.likeColor
                                        : AppColors.subLightColor,
                                    onTap: () async {
                                      likeCommentCubit.change(status);
                                    },
                                  ),
                                ),
                                const SizedBox(width: 2.0),
                                Text(
                                  likeCommentCubit.numOfLikeComment != 0
                                      ? likeCommentCubit.numOfLikeComment
                                          .toString()
                                      : '',
                                  style: theme.textTheme.headline6,
                                ),
                              ],
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        } else if (state.status == DetailCommentStatus.failure) {
          return Center(
            child: AppButton.text(
              label: 'Tải lại',
              onTap: () {},
            ),
          );
        } else if (state.status == DetailCommentStatus.loading) {
          return AppLoading.threeBounce(size: 20.0);
        }
        return AppLoading.threeBounce(size: 20.0);
      },
    );
  }
}
