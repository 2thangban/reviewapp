import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';

class BottomTabCubit extends Cubit<int> {
  final PageController pageController;
  BottomTabCubit(int currentTab, this.pageController) : super(currentTab);

  void onChangeTab(int indexSelected) {
    pageController.jumpToPage(indexSelected);
    emit(indexSelected);
  }
}
