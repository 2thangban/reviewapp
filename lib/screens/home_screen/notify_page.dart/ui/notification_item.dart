import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../models/notification/notification.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../bloc/read_all_notification_cubit/read_all_notification_cubit.dart';
import '../bloc/read_notification_cubit/read_notification_cubit.dart';

// ignore: must_be_immutable
class NotificationItem extends StatefulWidget {
  final NotificationApp notification;
  const NotificationItem({Key key, this.notification}) : super(key: key);

  @override
  _NotificationItemState createState() => _NotificationItemState();
}

class _NotificationItemState extends State<NotificationItem> {
  final sizeScreen = SizedConfig.heightMultiplier;
  @override
  Widget build(BuildContext context) {
    final ReadNotificationCubit readNotificationCubit =
        ReadNotificationCubit(widget.notification.click);
    final ReadAllNotificationCubit readAllNotificationCubit =
        BlocProvider.of<ReadAllNotificationCubit>(context);
    final theme = Theme.of(context);

    return InkWell(
      splashColor: AppColors.primaryColor.withOpacity(0.2),
      highlightColor: AppColors.primaryColor.withOpacity(0.2),
      onTap: () {
        readNotificationCubit.readNotification(widget.notification.id);
        switch (widget.notification.type) {
          case 1:
            return Navigator.of(context).pushNamed(
              RouteName.profile,
              arguments: widget.notification.account.id,
            );
          case 2:
            return Navigator.of(context).pushNamed(
              RouteName.detailComment,
              arguments: <String, dynamic>{
                'postId': widget.notification.postId,
                'commentId': widget.notification.commentId,
              },
            );
          case 3:
            return Navigator.of(context).pushNamed(
              RouteName.detailComment,
              arguments: <String, dynamic>{
                'postId': widget.notification.postId,
                'commentId': widget.notification.commentId,
              },
            );
          case 4:
            return Navigator.of(context).pushNamed(
              RouteName.postDetail,
              arguments: widget.notification.postId,
            );
          case 5:
            return Navigator.of(context).pushNamed(
              RouteName.postDetail,
              arguments: widget.notification.postId,
            );
          case 6:
            return Navigator.of(context).pushNamed(
              RouteName.detailComment,
              arguments: <String, dynamic>{
                "postId": widget.notification.postId,
                "commentId": widget.notification.commentId,
              },
            );
          default:
        }
      },
      child: BlocBuilder<ReadNotificationCubit, bool>(
        bloc: readNotificationCubit,
        builder: (context, state) {
          return Container(
            padding: const EdgeInsets.symmetric(vertical: 1.0),
            color:
                (readNotificationCubit.read || readAllNotificationCubit.readAll)
                    ? Colors.transparent
                    : theme.primaryColor.withOpacity(0.15),
            child: ListTile(
              horizontalTitleGap: 10.0,
              minLeadingWidth: 1,
              leading: Stack(
                alignment: Alignment.bottomRight,
                children: [
                  Container(
                    margin: const EdgeInsets.only(
                      bottom: 5.0,
                      right: 5.0,
                    ),
                    alignment: Alignment.bottomRight,
                    height: sizeScreen * 6,
                    width: sizeScreen * 6,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: NetworkImage(
                          AppAssets.baseUrl +
                              widget.notification.account.avatar,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.amber[200],
                      shape: BoxShape.circle,
                      border: Border.all(
                        color: theme.backgroundColor,
                        width: 2.0,
                      ),
                    ),
                    child: Icon(
                      AppIcon.notifyTab,
                      color: theme.primaryColor,
                      size: sizeScreen * 2.5,
                    ),
                  ),
                ],
              ),
              title: Text.rich(
                TextSpan(
                    text: '${widget.notification.account.userName} ',
                    children: [
                      TextSpan(
                        text: widget.notification.title,
                        style: theme.textTheme.bodyText1
                            .copyWith(fontWeight: AppFontWeight.regular),
                      ),
                    ]),
                style: theme.textTheme.headline6,
              ),
              subtitle: Text(
                widget.notification.customdate,
                style: theme.textTheme.bodyText2,
              ),
            ),
          );
        },
      ),
    );
  }
}
