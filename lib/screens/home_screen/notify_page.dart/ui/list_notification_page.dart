import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/num_of_new_notify_cubit/num_of_new_notify_cubit.dart';
import '../bloc/now_notification_bloc/now_notification_bloc.dart';
import '../bloc/old_notification_bloc/old_notification_bloc.dart';
import '../bloc/read_all_notification_cubit/read_all_notification_cubit.dart';
import '../bloc/seen_notification_cubit/seen_notification_cubit.dart';
import 'new_notify.dart';
import 'notification_item.dart';

class NotifyPage extends StatefulWidget {
  const NotifyPage({Key key}) : super(key: key);
  @override
  _NotifyPageState createState() => _NotifyPageState();
}

class _NotifyPageState extends State<NotifyPage>
    with AutomaticKeepAliveClientMixin<NotifyPage> {
  final ScrollController scrollController = ScrollController();
  final sizeText = SizedConfig.textMultiplier;
  final sizeScreen = SizedConfig.heightMultiplier;
  final OldNotificationBloc oldNotificationBloc = OldNotificationBloc();
  final NowNotificationBloc nowNotificationBloc = NowNotificationBloc();
  final ReadAllNotificationCubit readAllNotificationCubit =
      ReadAllNotificationCubit(false);
  AuthBloc authBloc;
  SeenNotificationCubit seenNotificationCubit;
  NumOfNewNotifyCubit newNotifyCubit;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    newNotifyCubit = BlocProvider.of<NumOfNewNotifyCubit>(context);
    seenNotificationCubit =
        SeenNotificationCubit(newNotifyCubit.numOfNotify != 0);
    seenNotificationCubit.seenAllNotification();
    newNotifyCubit.updateNumOfNotify();

    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (!oldNotificationBloc.state.isLastPage) {
          oldNotificationBloc.add(GetListOldNotification());
        }
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    seenNotificationCubit.close();
    oldNotificationBloc.close();
    readAllNotificationCubit.close();
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return authBloc.isChecked
        ? MultiBlocProvider(
            providers: [
              BlocProvider(create: (context) => readAllNotificationCubit),
              BlocProvider(create: (context) => nowNotificationBloc),
            ],
            child: Scaffold(
              appBar: CommonAppBar(
                leadingWidth: 0.0,
                titleWidget: Padding(
                  padding: const EdgeInsets.only(left: 15.0),
                  child: Text(
                    lang.locaized('titleNofication'),
                    style: theme.textTheme.headline3,
                  ),
                ),
                centerTitle: false,
                rightBarButtonItems: [
                  AppButton.icon(
                    icon: AppIcon.more_vert,
                    iconSize: 30.0,
                    onTap: () => showModalBottomSheet(
                      barrierColor: AppColors.darkThemeColor.withOpacity(0.4),
                      clipBehavior: Clip.antiAlias,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: const Radius.circular(15.0),
                          topRight: const Radius.circular(15.0),
                        ),
                      ),
                      context: context,
                      builder: (context) => Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Center(
                              child: Container(
                                margin: const EdgeInsets.only(
                                    top: 10, bottom: 20.0),
                                decoration: BoxDecoration(
                                  color:
                                      AppColors.subLightColor.withOpacity(0.3),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                height: 5,
                                width: 70,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: AppButton.text(
                                label: lang.locaized('readAllNofication'),
                                labelStyle: theme.textTheme.headline6.copyWith(
                                  fontWeight: AppFontWeight.regular,
                                ),
                                onTap: () {
                                  readAllNotificationCubit.readAllNotify();
                                  Navigator.pop(context);
                                },
                              ),
                            ),
                            const Divider(thickness: 1),
                            Padding(
                              padding: const EdgeInsets.only(left: 10.0),
                              child: AppButton.text(
                                label: lang.locaized('cancelNofication'),
                                labelStyle: theme.textTheme.headline6.copyWith(
                                  fontWeight: AppFontWeight.regular,
                                ),
                                onTap: () => Navigator.pop(context),
                              ),
                            ),
                            const SizedBox(height: 30.0),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              body: Container(
                child: RefreshIndicator(
                  onRefresh: () async {
                    oldNotificationBloc.add(
                      GetListOldNotification(isReFresh: true),
                    );
                    nowNotificationBloc.add(
                      GetListNowNotification(isReFresh: true),
                    );
                  },
                  child: SingleChildScrollView(
                    controller: scrollController,
                    child: BlocBuilder<ReadAllNotificationCubit, bool>(
                      builder: (context, state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const NewNotify(),
                            const Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 5,
                              ),
                              child: Divider(thickness: 0.8),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 15.0),
                              child: Text(
                                lang.locaized('mommentNofication'),
                                style: theme.textTheme.headline4,
                              ),
                            ),
                            BlocBuilder<OldNotificationBloc,
                                OldNotificationState>(
                              bloc: oldNotificationBloc,
                              builder: (_, state) {
                                if (state.status ==
                                    NotificationStatus.initial) {
                                  oldNotificationBloc
                                      .add(GetListOldNotification());
                                } else if (state.status ==
                                    NotificationStatus.failure) {
                                  return AppButton.custom(
                                    icon: AppIcon.reload,
                                    iconColor: AppColors.subLightColor,
                                    label: lang.locaized('reConnect'),
                                    isLeftIcon: true,
                                    onPress: () {
                                      oldNotificationBloc
                                          .add(GetListOldNotification());
                                    },
                                  );
                                } else if (state.status ==
                                    NotificationStatus.success) {
                                  if (state.list.isEmpty) {
                                    return const SizedBox.shrink();
                                  }
                                  return ListView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    itemCount: state.list.length,
                                    itemBuilder: (_, index) => NotificationItem(
                                      notification: state.list[index],
                                    ),
                                  );
                                }
                                return const SizedBox.shrink();
                              },
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
              ),
            ),
          )
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 70.0),
                  child: AppButton.common(
                    labelText: lang.locaized('labelBtnLogin'),
                    labelStyle: theme.textTheme.headline5.copyWith(
                      color: theme.backgroundColor,
                    ),
                    backgroundColor: theme.primaryColor,
                    shadowColor: theme.backgroundColor,
                    contentPadding: 10.0,
                    radius: 30.0,
                    onPressed: () async {
                      await Navigator.pushNamed(context, RouteName.login).then(
                        (check) {
                          if (check == false) return;
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            RouteName.home,
                            (route) => false,
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          );
  }

  @override
  bool get wantKeepAlive => true;
}
