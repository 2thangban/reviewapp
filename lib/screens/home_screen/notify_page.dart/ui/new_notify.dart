import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../localizations/app_localization.dart';
import '../bloc/now_notification_bloc/now_notification_bloc.dart';
import 'notification_item.dart';

// ignore: must_be_immutable
class NewNotify extends StatefulWidget {
  const NewNotify({Key key}) : super(key: key);
  @override
  _NewNotifyState createState() => _NewNotifyState();
}

class _NewNotifyState extends State<NewNotify> {
  final sizeScreen = SizedConfig.heightMultiplier;
  NowNotificationBloc notificationBloc;

  @override
  void initState() {
    super.initState();
    notificationBloc = BlocProvider.of<NowNotificationBloc>(context);
    notificationBloc.add(GetListNowNotification());
  }

  @override
  void dispose() {
    super.dispose();
    notificationBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15.0),
          child: Text(
            lang.locaized('nowNofication'),
            style: theme.textTheme.headline4,
          ),
        ),
        BlocBuilder<NowNotificationBloc, NowNotificationState>(
          bloc: notificationBloc,
          builder: (_, state) {
            if (state.status == NotificationStatus.failure) {
              return AppButton.custom(
                icon: AppIcon.reload,
                iconColor: AppColors.subLightColor,
                label: lang.locaized('reConnect'),
                isLeftIcon: true,
                onPress: () {
                  notificationBloc.add(GetListNowNotification());
                },
              );
            } else if (state.status == NotificationStatus.success) {
              if (state.list.isEmpty) {
                return const SizedBox.shrink();
              }
              return ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: state.list.length,
                itemBuilder: (_, index) => NotificationItem(
                  notification: state.list[index],
                ),
              );
            }
            return const SizedBox.shrink();
          },
        ),
        BlocBuilder<NowNotificationBloc, NowNotificationState>(
          bloc: notificationBloc,
          buildWhen: (previous, current) =>
              current.status == NotificationStatus.loading ||
              current.status == NotificationStatus.success,
          builder: (_, state) {
            if (state.status == NotificationStatus.loading) {
              return AppLoading.doubleBounce(size: 20.0);
            } else if (state.status == NotificationStatus.success) {
              if (!state.isLastPage) {
                return Center(
                  child: AppButton.text(
                    contentPadding: 0.0,
                    label: lang.locaized('more'),
                    labelStyle: theme.textTheme.bodyText1.copyWith(
                      color: theme.primaryColor,
                    ),
                    onTap: () => notificationBloc.add(GetListNowNotification()),
                  ),
                );
              }
            }
            return const SizedBox.shrink();
          },
        ),
      ],
    );
  }
}
