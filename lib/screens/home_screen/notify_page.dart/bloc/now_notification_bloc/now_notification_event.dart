part of 'now_notification_bloc.dart';

abstract class NowNotificationEvent {
  const NowNotificationEvent();
}

class GetListNowNotification extends NowNotificationEvent {
  final bool isReFresh;
  GetListNowNotification({this.isReFresh = false});
}
