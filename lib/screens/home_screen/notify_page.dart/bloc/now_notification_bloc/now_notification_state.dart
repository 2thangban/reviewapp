part of 'now_notification_bloc.dart';

enum NotificationStatus { initial, loading, success, failure }

class NowNotificationState extends Equatable {
  final NotificationStatus status;
  final List<NotificationApp> list;
  final bool isLastPage;
  const NowNotificationState({
    this.status = NotificationStatus.initial,
    this.list = const <NotificationApp>[],
    this.isLastPage = false,
  });

  NowNotificationState cloneWith({
    NotificationStatus status,
    List<NotificationApp> list,
    bool isLastPage,
  }) =>
      NowNotificationState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );
  @override
  List<Object> get props => [status, list];
}
