import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/notification/notification.dart';
import '../../../../../models/notification/notification_repo.dart';
import '../../../../../models/token/token_repo.dart';

part 'now_notification_event.dart';
part 'now_notification_state.dart';

class NowNotificationBloc
    extends Bloc<NowNotificationEvent, NowNotificationState> {
  NowNotificationBloc() : super(const NowNotificationState());
  final int limit = 10;
  int page = 0;
  @override
  Stream<NowNotificationState> mapEventToState(
    NowNotificationEvent event,
  ) async* {
    if (event is GetListNowNotification) {
      if (event.isReFresh) {
        page = 0;
      }
      yield state.cloneWith(status: NotificationStatus.loading);
      try {
        final result = await NotificationRepo.getNowList(
          page: page++,
          limit: limit,
        );
        yield state.cloneWith(
          status: NotificationStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } on ErrorResponse catch (e) {
        page--;
        if (e.staticCode == 401) {
          List<NotificationApp> list;
          await TokenRepo.refreshToken().then((token) async {
            await TokenRepo.write(token).then((value) async {
              list = await NotificationRepo.getNowList(
                page: page++,
                limit: limit,
              );
            });
          });
          yield state.cloneWith(
            status: NotificationStatus.success,
            list: List.of(state.list)..addAll(list),
            isLastPage: list.length < limit,
          );
        } else {
          page--;
          yield state.cloneWith(status: NotificationStatus.failure);
        }
      }
    } else {
      page--;
      yield state.cloneWith(status: NotificationStatus.failure);
    }
  }
}
