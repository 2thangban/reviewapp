part of 'old_notification_bloc.dart';

abstract class OldNotificationEvent {
  const OldNotificationEvent();
}

class GetListOldNotification extends OldNotificationEvent {
  final bool isReFresh;
  GetListOldNotification({this.isReFresh = false});
}
