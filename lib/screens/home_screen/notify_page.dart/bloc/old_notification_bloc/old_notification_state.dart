part of 'old_notification_bloc.dart';

class OldNotificationState extends Equatable {
  final NotificationStatus status;
  final List<NotificationApp> list;
  final bool isLastPage;
  const OldNotificationState({
    this.status = NotificationStatus.initial,
    this.list = const <NotificationApp>[],
    this.isLastPage = false,
  });

  OldNotificationState cloneWith({
    NotificationStatus status,
    List<NotificationApp> list,
    bool isLastPage,
  }) =>
      OldNotificationState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );
  @override
  List<Object> get props => [status, list];
}
