import 'package:bloc/bloc.dart';

import '../../../../../models/notification/notification_repo.dart';

class ReadNotificationCubit extends Cubit<bool> {
  bool read;
  ReadNotificationCubit(this.read) : super(read);

  void readNotification(int notifyId) async {
    try {
      if (read == false) {
        final result = await NotificationRepo.realdNotification(notifyId);
        emit(this.read = result.status);
      }
    } catch (e) {
      ///
    }
  }
}
