part of 'list_notification_bloc.dart';

abstract class ListNotificationEvent {}

class GetListNotificationEvent extends ListNotificationEvent {
  final int page;
  final int limit;
  GetListNotificationEvent(this.page, this.limit);
}
