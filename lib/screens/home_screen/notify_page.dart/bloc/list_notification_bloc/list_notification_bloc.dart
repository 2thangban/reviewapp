import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/notification/notification.dart';
import '../../../../../models/notification/notification_repo.dart';
import '../../../../../models/token/token_repo.dart';

part 'list_notification_event.dart';
part 'list_notification_state.dart';

class ListNotificationBloc
    extends Bloc<ListNotificationEvent, ListNotificationState> {
  ListNotificationBloc() : super(ListNotificationInitial());

  @override
  Stream<ListNotificationState> mapEventToState(
      ListNotificationEvent event) async* {
    if (event is GetListNotificationEvent) {
      try {
        yield ListNotificationLoaing();
        final result = await NotificationRepo.list(
          page: event.page,
          limit: event.limit,
        );
        yield ListNotificationSuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<NotificationApp> listNotification;
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              listNotification = await NotificationRepo.list(
                page: event.page,
                limit: event.limit,
              );
            });
          });
          yield ListNotificationSuccess(listNotification);
        } else {
          yield ListNotificationFailed();
        }
      } catch (e) {
        ///
      }
    }
  }
}
