part of 'list_notification_bloc.dart';

abstract class ListNotificationState {}

class ListNotificationInitial extends ListNotificationState {}

class ListNotificationLoaing extends ListNotificationState {}

class ListNotificationSuccess extends ListNotificationState {
  final List<NotificationApp> listNotification;
  ListNotificationSuccess(this.listNotification);
}

class ListNotificationFailed extends ListNotificationState {}
