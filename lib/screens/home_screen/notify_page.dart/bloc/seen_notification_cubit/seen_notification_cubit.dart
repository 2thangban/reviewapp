import 'package:bloc/bloc.dart';

import '../../../../../models/notification/notification_repo.dart';

class SeenNotificationCubit extends Cubit<bool> {
  final bool seen;
  SeenNotificationCubit(this.seen) : super(seen);

  void seenAllNotification() async {
    try {
      if (seen == true) {
        final result = await NotificationRepo.seenAll();
        emit(result.status);
      }
    } catch (e) {
      ///
    }
  }
}
