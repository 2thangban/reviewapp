import 'package:bloc/bloc.dart';

import '../../../../../models/notification/notification_repo.dart';

class ReadAllNotificationCubit extends Cubit<bool> {
  bool readAll;
  ReadAllNotificationCubit(this.readAll) : super(readAll);

  void readAllNotify() async {
    try {
      final result = await NotificationRepo.realdAllNotification();
      emit(this.readAll = result.status);
    } catch (e) {
      ///
    }
  }
}
