import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/post/post.dart';
import '../../../../../../models/post/post_repo.dart';
import '../../../../../../models/token/token_repo.dart';

part 'post_detail_event.dart';
part 'post_detail_state.dart';

class PostDetailBloc extends Bloc<PostDetailEvent, PostDetailState> {
  Post post;
  PostDetailBloc() : super(PostDetailInitial());

  @override
  Stream<PostDetailState> mapEventToState(PostDetailEvent event) async* {
    if (event is GetPostDetail) {
      try {
        final result = await PostRepo.detailPost(
          event.postId,
          isAuth: event.isAuth,
        );
        yield PostDetailSuccess(this.post = result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Post post;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            post =
                await PostRepo.detailPost(event.postId, isAuth: event.isAuth);
          });
          yield PostDetailSuccess(this.post = post);
        } else {
          yield PostDetailFailed();
        }
      } catch (e) {
        ///
      }
    }
  }
}
