part of 'post_detail_bloc.dart';

@immutable
abstract class PostDetailState {}

class PostDetailInitial extends PostDetailState {}

class PostDetailSuccess extends PostDetailState {
  final Post post;
  PostDetailSuccess(this.post);
}

class PostDetailFailed extends PostDetailState {}
