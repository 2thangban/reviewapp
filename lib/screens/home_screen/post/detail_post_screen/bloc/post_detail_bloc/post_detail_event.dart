part of 'post_detail_bloc.dart';

@immutable
abstract class PostDetailEvent {}

class GetPostDetail extends PostDetailEvent {
  final int postId;
  final bool isAuth;
  GetPostDetail(this.postId, {this.isAuth});
}
