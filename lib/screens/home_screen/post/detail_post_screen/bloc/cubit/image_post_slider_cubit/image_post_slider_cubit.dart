import 'package:bloc/bloc.dart';

class ImagePostSliderCubit extends Cubit<int> {
  final List<String> listImage;
  ImagePostSliderCubit(this.listImage) : super(0);

  void jumpToIndex(int newIndex) => emit(newIndex);

  void updateList(List<String> newList) {
    this.listImage.clear();
    this.listImage.addAll(newList);
    emit(0);
  }
}
