part of 'detail_post_cubit.dart';

class DetailPostState extends Equatable {
  final String title;
  final String content;
  final int rating;
  final List<Hashtag> listHashTag;
  const DetailPostState({
    this.title = '',
    this.content = '',
    this.rating = 0,
    this.listHashTag = const <Hashtag>[],
  });

  DetailPostState cloneWith(DetailPostState detailPostCubit) => DetailPostState(
        title: detailPostCubit.title ?? this.title,
        content: detailPostCubit.content ?? this.content,
        rating: detailPostCubit.rating ?? this.rating,
        listHashTag: detailPostCubit.listHashTag ?? this.listHashTag,
      );
  @override
  List<Object> get props => [
        this.title,
        this.content,
        this.rating,
        this.listHashTag,
      ];
}
