import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../../models/hashtag/hashtag.dart';

part 'detail_post_state.dart';

class DetailPostCubit extends Cubit<DetailPostState> {
  DetailPostCubit(
    String title,
    String content,
    int rating,
    List<Hashtag> listHashtag,
  ) : super(DetailPostState(
          title: title,
          content: content,
          rating: rating,
          listHashTag: listHashtag,
        ));

  void update(
    String title,
    String content,
    int rating,
    List<Hashtag> listHashtag,
  ) =>
      emit(DetailPostState(
        title: title,
        content: content,
        rating: rating,
        listHashTag: listHashtag,
      ));
}
