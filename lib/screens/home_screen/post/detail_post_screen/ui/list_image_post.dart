import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../routes/route_name.dart';
import '../bloc/cubit/image_post_slider_cubit/image_post_slider_cubit.dart';
import '../bloc/post_detail_bloc/post_detail_bloc.dart';

// ignore: must_be_immutable
class ListImagePost extends StatefulWidget {
  const ListImagePost({Key key}) : super(key: key);

  @override
  _ListImagePostState createState() => _ListImagePostState();
}

class _ListImagePostState extends State<ListImagePost> {
  PostDetailBloc postDetailBloc;
  ImagePostSliderCubit imagePostSliderCubit;
  @override
  void initState() {
    super.initState();
    postDetailBloc = BlocProvider.of<PostDetailBloc>(context);
    imagePostSliderCubit = BlocProvider.of<ImagePostSliderCubit>(context);
  }

  @override
  void dispose() {
    super.dispose();
    imagePostSliderCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    return GestureDetector(
      onTap: () => Navigator.pushNamed(
        context,
        RouteName.image,
        arguments: postDetailBloc.post.listImage,
      ),
      child: BlocBuilder<ImagePostSliderCubit, int>(
        builder: (_, index) => Stack(
          alignment: Alignment.bottomRight,
          children: [
            AnimatedContainer(
              duration: const Duration(milliseconds: 400),
              curve: Curves.ease,
              height: sizeScreen * (index.isEven ? 45 : 60),
              child: PageView.builder(
                itemCount: imagePostSliderCubit.listImage.length,
                onPageChanged: (selectedIndex) =>
                    imagePostSliderCubit.jumpToIndex(selectedIndex),
                scrollDirection: Axis.horizontal,
                itemBuilder: (_, index) => Container(
                  child: CachedNetworkImage(
                    imageUrl: AppAssets.baseUrl +
                        imagePostSliderCubit.listImage[index],
                    fit: BoxFit.cover,
                    fadeOutDuration: const Duration(milliseconds: 100),
                    fadeInDuration: const Duration(milliseconds: 300),
                    fadeInCurve: Curves.easeInCirc,
                    placeholder: (_, url) => const Center(
                      child: const SizedBox(
                        height: 30.0,
                        width: 30.0,
                        child: const CircularProgressIndicator(
                          strokeWidth: 3.0,
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10.0),
              padding: const EdgeInsets.symmetric(
                vertical: 3.0,
                horizontal: 10.0,
              ),
              decoration: BoxDecoration(
                color: AppColors.darkThemeColor.withOpacity(0.7),
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Text(
                '${++index}/${imagePostSliderCubit.listImage.length}',
                style: TextStyle(
                  color: AppColors.lightThemeColor,
                  fontSize: sizeScreen * 1.6,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
