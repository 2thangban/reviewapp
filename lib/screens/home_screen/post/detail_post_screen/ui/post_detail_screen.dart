import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/animation/fade_anim.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../commons/widgets/circle_avt.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/post/post.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../app_common_bloc/favorite_cubit/favorite_cubit.dart';
import '../../../../app_common_bloc/follow_cubit/follow_user_cubit.dart';
import '../../../../app_common_bloc/like_cubit/like_post_cubit.dart';
import '../../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import '../../../../app_common_bloc/view_cubit/view_cubit.dart';
import '../bloc/cubit/detail_post_cubit/detail_post_cubit.dart';
import '../bloc/cubit/image_post_slider_cubit/image_post_slider_cubit.dart';
import '../bloc/post_detail_bloc/post_detail_bloc.dart';
import 'footer_detail_post.dart';
import 'list_image_post.dart';
import 'post_description.dart';
import 'related_post.dart';

class DetailPostScreen extends StatefulWidget {
  final int postId;
  final int commentId;
  DetailPostScreen(this.postId, {this.commentId});

  @override
  _DetailPostScreenState createState() => _DetailPostScreenState();
}

class _DetailPostScreenState extends State<DetailPostScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final SignUpBloc signUpBloc = SignUpBloc();
  final ScrollController scrollCommentController = new ScrollController();
  final PostDetailBloc postDetailBloc = PostDetailBloc();
  final ViewCubit viewCubit = ViewCubit();
  bool isPostOwner = false;
  FavoriteCubit favoriteCubit;

  AuthBloc authBloc;
  LikePostCubit likeCubit;
  FollowUserCubit followUserCubit;
  ImagePostSliderCubit imagePostSliderCubit;
  DetailPostCubit detailPostCubit;
  Map<String, dynamic> postMap;
  @override
  void initState() {
    super.initState();

    authBloc = BlocProvider.of<AuthBloc>(context);
    Future.delayed(
      Duration(seconds: 4),
      () => authBloc.isChecked
          ? viewCubit.increamentViewOfPost(widget.postId)
          : () {},
    );
  }

  @override
  void dispose() {
    super.dispose();

    signUpBloc.close();
    postDetailBloc.close();
    favoriteCubit.close();
    likeCubit.close();
    followUserCubit.close();
    imagePostSliderCubit.close();
    detailPostCubit.close();
    scrollCommentController.dispose();
    viewCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final navigate = Navigator.of(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => postDetailBloc),
        BlocProvider(create: (_) => followUserCubit),
        BlocProvider(create: (_) => likeCubit),
        BlocProvider(create: (_) => signUpBloc),
        BlocProvider(create: (_) => favoriteCubit),
        BlocProvider(create: (_) => imagePostSliderCubit),
        BlocProvider(create: (_) => detailPostCubit),
      ],
      child: BlocConsumer<PostDetailBloc, PostDetailState>(
        bloc: postDetailBloc,
        listener: (_, state) {
          if (state is PostDetailSuccess) {
            if (state.post.isLocked == 1) {
              showLockPostMessage(theme);
            }
          }
        },
        builder: (context, state) {
          if (state is PostDetailInitial) {
            postDetailBloc.add(
              GetPostDetail(widget.postId, isAuth: authBloc.isChecked),
            );
          }
          if (state is PostDetailSuccess) {
            postMap = <String, dynamic>{
              'post_id': postDetailBloc.post.id,
              'post_title': postDetailBloc.post.titlePost,
              'post_content': postDetailBloc.post.content,
              'post_date': postDetailBloc.post.dateCreatePost,
              'post_image': postDetailBloc.post.listImage,
              'category_id': postDetailBloc.post.category,
              'hashtag': postDetailBloc.post
                  .toHashtagJsonList(postDetailBloc.post.listHashTag),
              'post_rating': postDetailBloc.post.rating,
            };
            isPostOwner = authBloc.account.id == state.post.account.id;
            favoriteCubit = FavoriteCubit(state.post.isFavorite, state.post.id);
            likeCubit = LikePostCubit(state.post.isLiked, state.post.like);
            followUserCubit = FollowUserCubit(
              postDetailBloc.post.account.id,
              postDetailBloc.post.account.isFollowed,
            );
            imagePostSliderCubit = ImagePostSliderCubit(state.post.listImage);

            detailPostCubit = DetailPostCubit(
              state.post.titlePost,
              state.post.content,
              state.post.rating,
              state.post.listHashTag,
            );
            return Scaffold(
              appBar: CommonAppBar(
                leftBarButtonItem: AppButton.icon(
                  icon: AppIcon.popNavigate,
                  iconSize: sizeScreen * 3.3,
                  iconColor: theme.iconTheme.color,
                  onTap: () => Navigator.pop(context, likeCubit.status),
                ),
                titleWidget: FadeAnim(
                  duration: const Duration(milliseconds: 400),
                  child: ListTile(
                    horizontalTitleGap: 10.0,
                    dense: true,
                    minLeadingWidth: 1,
                    contentPadding: EdgeInsets.zero,
                    leading: BlocBuilder<FollowUserCubit, bool>(
                      bloc: followUserCubit,
                      builder: (_, status) {
                        return CircleAvt(
                          image: AppAssets.baseUrl + state.post.account.avatar,
                          radius: 18.0,
                          isIcon: authBloc.account.id != state.post.account.id,
                          iconBackground: status ? AppColors.sussess : null,
                          icon: status ? AppIcon.followed : AppIcon.follow,
                          iconSize: 15.0,
                          onTap: () async {
                            await Navigator.pushNamed(
                              context,
                              RouteName.profile,
                              arguments: state.post.account.id,
                            ).then((newStatus) => newStatus != null
                                ? followUserCubit.update(newStatus)
                                : null);
                          },
                          onIconTap: () async {
                            if (authBloc.isChecked) {
                              followUserCubit.change(status);
                            } else {
                              await Future.delayed(
                                  Duration(milliseconds: 200),
                                  () => Navigator.pushNamed(
                                      context, RouteName.login)).then(
                                (isLogin) {
                                  if (isLogin) {
                                    Navigator.pushNamedAndRemoveUntil(
                                      context,
                                      RouteName.home,
                                      (route) => false,
                                    );
                                  }
                                },
                              );
                            }
                          },
                        );
                      },
                    ),
                    title: Text(
                      state.post.account.userName,
                      style: theme.textTheme.bodyText1.copyWith(
                        fontSize: sizeText * 1.4,
                      ),
                    ),
                    subtitle: Text(
                      AppLocalization.of(context).locaized('datePost') +
                          ' ' +
                          postDetailBloc.post.customeDate,
                      style: theme.textTheme.bodyText2.copyWith(
                        fontSize: sizeText * 1.3,
                      ),
                    ),
                  ),
                ),
                rightBarButtonItems: [
                  PopupMenuButton<int>(
                    padding: const EdgeInsets.all(0.0),
                    icon: Icon(
                      AppIcon.more_hori,
                      color: theme.textTheme.bodyText1.color,
                    ),
                    offset: Offset(30, 20),
                    elevation: 5.0,
                    iconSize: sizeScreen * 3.5,
                    onSelected: (int indexSeleted) async {
                      switch (indexSeleted) {
                        case 0:
                          if (isPostOwner) {
                            await navigate
                                .pushNamed(
                              RouteName.createPost,
                              arguments: new Post.fromJson(postMap),
                            )
                                .then((result) {
                              if (result != null) {
                                final Post post = result;
                                postDetailBloc.post = post;
                                postMap = <String, dynamic>{
                                  'post_id': post.id,
                                  'post_title': post.titlePost,
                                  'post_content': post.content,
                                  'post_date': post.dateCreatePost,
                                  'post_image': post.listImage,
                                  'category_id': post.category,
                                  'hashtag':
                                      post.toHashtagJsonList(post.listHashTag),
                                  'post_rating': post.rating,
                                };
                                detailPostCubit.update(
                                  post.titlePost,
                                  post.content,
                                  post.rating,
                                  post.listHashTag,
                                );
                                imagePostSliderCubit.updateList(post.listImage);
                              }
                            });
                          } else {
                            navigate.pushNamed(
                              RouteName.reportPost,
                              arguments: postDetailBloc.post.id,
                            );
                          }
                          break;

                        default:
                      }
                    },
                    itemBuilder: (_) => [
                      PopupMenuItem(
                        value: 0,
                        child: SizedBox(
                          width: sizeScreen * 15,
                          child: Text(
                            isPostOwner ? 'Chỉnh sửa' : 'Báo cáo',
                            style: theme.textTheme.headline6.copyWith(
                              fontWeight: AppFontWeight.regular,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              body: FadeAnim(
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    Container(
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            const ListImagePost(),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 10.0,
                              ),
                              child: Column(
                                children: [
                                  const PostDescription(),
                                  const RelatedPost(),
                                  const SizedBox(height: 20.0),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              bottomNavigationBar: postDetailBloc.post.isLocked == 0
                  ? const FooterDetailPost()
                  : const SizedBox(),
            );
          }
          return AppShimmer.home();
        },
      ),
    );
  }

  Future showLockPostMessage(ThemeData theme) async {
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        backgroundColor: Colors.transparent,
        child: Center(
          child: Container(
            padding: const EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              color: theme.backgroundColor,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Bài viết bị hạn chế',
                  style: theme.textTheme.headline4,
                ),
                const SizedBox(height: 20.0),
                Text(
                  'Bài viết của bạn vẫn sẽ hiểm thị ở trang cá nhân.' +
                      ' Tuy nhiên, sẽ không được hiển thị ở trang chủ,' +
                      ' cũng như hạn chế lượt tìm kiếm.' +
                      'Vì một số lý do:\n\n* Bài Spam, không ý nghĩa hoặc chứa nội dung, hình ảnh không rõ ràng.\n\n ' +
                      'Bạn có thể chỉnh sửa để bài viết không mắc những lỗi trên.',
                  style: theme.textTheme.headline6.copyWith(
                    color: AppColors.subLightColor,
                    fontWeight: AppFontWeight.regular,
                  ),
                ),
                const SizedBox(height: 20.0),
                Padding(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 20.0,
                  ),
                  child: Row(
                    children: [
                      AppButton.common(
                        width: sizeScreen * 15.0,
                        contentPadding: 10.0,
                        radius: 15.0,
                        labelText: 'Đóng',
                        labelStyle: theme.textTheme.headline6,
                        backgroundColor: AppColors.backgroundSearchColor,
                        shadowColor: theme.backgroundColor,
                        onPressed: () => Navigator.pop(context),
                      ),
                      const SizedBox(width: 10.0),
                      Expanded(
                        child: AppButton.common(
                          contentPadding: 10.0,
                          shadowColor: theme.primaryColor,
                          backgroundColor: theme.primaryColor,
                          labelText: 'Sửa bài viết',
                          radius: 15.0,
                          labelStyle: theme.textTheme.headline6.copyWith(
                            color: theme.backgroundColor,
                          ),
                          onPressed: () async {
                            await Navigator.pushNamed(
                              context,
                              RouteName.createPost,
                              arguments: new Post.fromJson(postMap),
                            ).then((result) {
                              if (result != null) {
                                final Post post = result;
                                postDetailBloc.post = post;
                                postMap = <String, dynamic>{
                                  'post_id': post.id,
                                  'post_title': post.titlePost,
                                  'post_content': post.content,
                                  'post_date': post.dateCreatePost,
                                  'post_image': post.listImage,
                                  'category_id': post.category,
                                  'hashtag':
                                      post.toHashtagJsonList(post.listHashTag),
                                  'post_rating': post.rating,
                                };
                                detailPostCubit.update(
                                  post.titlePost,
                                  post.content,
                                  post.rating,
                                  post.listHashTag,
                                );
                                imagePostSliderCubit.updateList(post.listImage);
                              }
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
