import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../../commons/animation/icon_button_anim.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../app_common_bloc/like_cubit/like_post_cubit.dart';
import '../../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import '../../../../comment/list_all_comment_screen/ui/list_comment_screen.dart';
import '../bloc/post_detail_bloc/post_detail_bloc.dart';

class RelatedPost extends StatefulWidget {
  const RelatedPost({Key key}) : super(key: key);
  @override
  _RelatedPostState createState() => _RelatedPostState();
}

class _RelatedPostState extends State<RelatedPost> {
  final sizeScreen = SizedConfig.heightMultiplier;
  LikePostCubit likeCubit;
  AuthBloc authBloc;
  SignUpBloc signUpBloc;
  PostDetailBloc postDetailBloc;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    signUpBloc = BlocProvider.of<SignUpBloc>(context);
    likeCubit = BlocProvider.of<LikePostCubit>(context);
    postDetailBloc = BlocProvider.of<PostDetailBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              BlocBuilder<LikePostCubit, bool>(
                bloc: likeCubit,
                builder: (_, status) => Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 2.5),
                      child: IconButtonAnim(
                        status: status,
                        child: AppButton.icon(
                          iconSize: sizeScreen * 2.8,
                          icon: status ? AppIcon.liked : AppIcon.unLike,
                          iconColor: status
                              ? AppColors.likeColor
                              : AppColors.darkThemeColor,
                          onTap: () async {
                            if (authBloc.isChecked) {
                              likeCubit.change(
                                status,
                                postId: postDetailBloc.post.id,
                              );
                            } else {
                              await Navigator.pushNamed(
                                context,
                                RouteName.login,
                              ).then(
                                (isLogin) {
                                  if (!isLogin) return;
                                  Navigator.pushNamedAndRemoveUntil(
                                    context,
                                    RouteName.home,
                                    (route) => false,
                                  );
                                },
                              );
                            }
                          },
                        ),
                      ),
                    ),
                    Text(
                      '${likeCubit.numOfLike}',
                      style: theme.textTheme.headline5.copyWith(
                        fontWeight: AppFontWeight.regular,
                      ),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              GestureDetector(
                onTap: () => Future.delayed(
                  Duration(milliseconds: 300),
                  () => Navigator.push(
                    context,
                    PageRouteBuilder(
                      pageBuilder: (context, anim, seconAnim) =>
                          ListCommentScreen(
                        params: ParamListComment(postDetailBloc.post.id),
                      ),
                      transitionsBuilder: (context, anim, seconAnim, child) =>
                          SlideTransition(
                        position: Tween<Offset>(
                          begin: const Offset(0.0, 1.0),
                          end: Offset.zero,
                        ).animate(anim),
                        child: SlideTransition(
                          position: Tween<Offset>(
                            begin: Offset.zero,
                            end: const Offset(0.0, 1.0),
                          ).animate(seconAnim),
                          child: child,
                        ),
                      ),
                      transitionDuration: Duration(milliseconds: 400),
                    ),
                  ),
                ),
                child: Row(
                  children: [
                    Icon(
                      FontAwesomeIcons.commentAlt,
                      size: sizeScreen * 2.5,
                    ),
                    const SizedBox(width: 5.0),
                    Text(
                      'Bình luận',
                      style: theme.textTheme.bodyText1.copyWith(
                        fontWeight: AppFontWeight.ultraBold,
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
        SizedBox(height: sizeScreen * 5.0),
      ],
    );
  }
}
