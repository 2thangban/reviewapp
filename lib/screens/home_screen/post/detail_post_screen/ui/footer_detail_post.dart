import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/animation/icon_button_anim.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/list_gallery_widget.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/gallery/gellery.dart';
import '../../../../../routes/route_name.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../app_common_bloc/favorite_cubit/favorite_cubit.dart';
import '../../../../app_common_bloc/like_cubit/like_post_cubit.dart';
import '../../../../comment/list_all_comment_screen/ui/list_comment_screen.dart';
import '../bloc/post_detail_bloc/post_detail_bloc.dart';

class FooterDetailPost extends StatefulWidget {
  const FooterDetailPost({Key key}) : super(key: key);
  @override
  _FooterDetailPostState createState() => _FooterDetailPostState();
}

class _FooterDetailPostState extends State<FooterDetailPost> {
  LikePostCubit likePostCubit;
  FavoriteCubit favoriteCubit;
  PostDetailBloc postDetailBloc;
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    postDetailBloc = BlocProvider.of<PostDetailBloc>(context);
    likePostCubit = BlocProvider.of<LikePostCubit>(context);
    favoriteCubit = FavoriteCubit(
      postDetailBloc.post.isFavorite,
      postDetailBloc.post.id,
    );
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    favoriteCubit.close();
  }

  Future<Gallery> _showListGallery() => showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: AppColors.darkThemeColor.withOpacity(0.3),
        isDismissible: false,
        builder: (_) => ListGalleryWidget(),
      );
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Container(
      child: Row(
        children: [
          Expanded(
            child: GestureDetector(
              onTap: () async {
                if (authBloc.isChecked) {
                  Future.delayed(
                    Duration(milliseconds: 300),
                    () => Navigator.push(
                      context,
                      PageRouteBuilder(
                        pageBuilder: (context, anim, seconAnim) =>
                            ListCommentScreen(
                          params: ParamListComment(
                            postDetailBloc.post.id,
                            isComment: true,
                          ),
                        ),
                        transitionsBuilder: (context, anim, seconAnim, child) =>
                            SlideTransition(
                          position: Tween<Offset>(
                            begin: const Offset(0.0, 1.0),
                            end: Offset.zero,
                          ).animate(anim),
                          child: SlideTransition(
                            position: Tween<Offset>(
                              begin: Offset.zero,
                              end: const Offset(0.0, 1.0),
                            ).animate(seconAnim),
                            child: child,
                          ),
                        ),
                        transitionDuration: Duration(milliseconds: 400),
                      ),
                    ),
                  );
                } else {
                  await Future.delayed(
                    Duration(milliseconds: 200),
                    () => Navigator.pushNamed(context, RouteName.login),
                  ).then(
                    (isLoginSuccess) {
                      if (isLoginSuccess) {
                        Navigator.pushNamedAndRemoveUntil(
                          context,
                          RouteName.home,
                          (route) => false,
                        );
                      }
                    },
                  );
                }
              },
              child: Container(
                padding: const EdgeInsets.all(14.0),
                margin: const EdgeInsets.symmetric(
                  horizontal: 10.0,
                  vertical: 5.0,
                ),
                decoration: BoxDecoration(
                  color: AppColors.backgroundSearchColor,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Text(
                  lang.locaized('comment'),
                ),
              ),
            ),
          ),
          BlocBuilder<LikePostCubit, bool>(
            bloc: likePostCubit,
            buildWhen: (previous, current) => current != previous,
            builder: (context, status) {
              return IconButtonAnim(
                status: status,
                child: AppButton.icon(
                  icon: status ? AppIcon.liked : AppIcon.unLike,
                  iconColor:
                      status ? AppColors.likeColor : theme.iconTheme.color,
                  onTap: () async {
                    if (authBloc.isChecked) {
                      likePostCubit.change(
                        status,
                        postId: postDetailBloc.post.id,
                      );
                    } else {
                      await Future.delayed(
                        Duration(milliseconds: 200),
                        () => Navigator.pushNamed(context, RouteName.login),
                      ).then(
                        (isLoginSuccess) {
                          if (isLoginSuccess) {
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              RouteName.home,
                              (route) => false,
                            );
                          }
                        },
                      );
                    }
                  },
                ),
              );
            },
          ),
          const SizedBox(width: 5.0),
          BlocConsumer<FavoriteCubit, FavoriteState>(
            bloc: favoriteCubit,
            listener: (context, state) {
              if (state.isAddSuccess) {
                AppShower.showSnackBar(
                  context,
                  iconLeading: Icon(
                    AppIcon.saveGallery,
                    color: theme.primaryColor,
                  ),
                  label: lang.locaized('isSavedPost'),
                  labelAction: state.gallery.name,
                );
              } else if (state.isAddSuccess == false) {
                AppShower.showSnackBar(
                  context,
                  iconLeading: Icon(AppIcon.unsaveGallery),
                  label: lang.locaized('isUnSavedPost'),
                  labelAction: '',
                );
              }
            },
            builder: (context, state) {
              return IconButtonAnim(
                status: state.status,
                child: AppButton.icon(
                  icon: state.status
                      ? AppIcon.saveGallery
                      : AppIcon.unsaveGallery,
                  iconColor:
                      state.status ? theme.primaryColor : theme.iconTheme.color,
                  onTap: () async {
                    if (authBloc.isChecked) {
                      if (state.status) {
                        favoriteCubit.unFavoritePost();
                      } else {
                        await Future.delayed(
                          const Duration(milliseconds: 200),
                          () => _showListGallery(),
                        ).then(
                          (gallery) {
                            if (gallery != null) {
                              favoriteCubit.favoritePost(gallery);
                            }
                          },
                        );
                      }
                    } else {
                      await Future.delayed(
                        Duration(milliseconds: 200),
                        () => Navigator.pushNamed(context, RouteName.login),
                      ).then(
                        (isLoginSuccess) {
                          if (isLoginSuccess) {
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              RouteName.home,
                              (route) => false,
                            );
                          }
                        },
                      );
                    }
                  },
                ),
              );
            },
          ),
          const SizedBox(width: 5.0),
        ],
      ),
    );
  }
}
