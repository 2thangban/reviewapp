import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../routes/route_name.dart';
import '../bloc/cubit/detail_post_cubit/detail_post_cubit.dart';
import '../bloc/post_detail_bloc/post_detail_bloc.dart';

class PostDescription extends StatefulWidget {
  const PostDescription({Key key}) : super(key: key);

  @override
  _PostDescriptionState createState() => _PostDescriptionState();
}

class _PostDescriptionState extends State<PostDescription> {
  DetailPostCubit detailPostCubit;
  PostDetailBloc postDetailBloc;
  @override
  void initState() {
    super.initState();
    detailPostCubit = BlocProvider.of<DetailPostCubit>(context);
    postDetailBloc = BlocProvider.of<PostDetailBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;

    final theme = Theme.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10.0),
      child: BlocBuilder<DetailPostCubit, DetailPostState>(
        bloc: detailPostCubit,
        builder: (_, state) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SvgPicture.asset(
                    AppAssets.ratingIcon,
                    height: sizeScreen * 2,
                  ),
                  const SizedBox(width: 5.0),
                  Text.rich(
                    TextSpan(
                      text: state.rating.toString(),
                      style: theme.textTheme.headline6,
                      children: [
                        TextSpan(
                          text: " /5 điểm",
                          style: theme.textTheme.bodyText2,
                        ),
                      ],
                    ),
                  ),
                  const Spacer(),
                  SvgPicture.asset(AppAssets.viewIcon,
                      height: sizeScreen * 2.5),
                  Padding(
                    padding: const EdgeInsets.only(left: 5.0),
                    child: Text(
                      postDetailBloc.post.customeView,
                      style: theme.textTheme.headline6.copyWith(
                        color: AppColors.subLightColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: SizedBox(
                width: sizeScreen * 50.0,
                child: Text(
                  state.title.toUpperCase(),
                  style: theme.textTheme.headline4,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ),
            /////
            Padding(
              padding: const EdgeInsets.only(bottom: 15.0),
              child: Html(
                data: """${state.content}""",
                style: {
                  "p": Style(color: theme.textTheme.bodyText1.color),
                },
              ),
            ),
            ////
            Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: Wrap(
                spacing: 10.0,
                runSpacing: 5.0,
                children: state.listHashTag
                    .map(
                      (e) => Container(
                        child: Text(
                          '#${e.title}',
                          style: theme.textTheme.bodyText1.copyWith(
                            color: theme.primaryColor,
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
            Card(
              elevation: 0.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(7.0),
              ),
              color: AppColors.backgroundSearchColor.withOpacity(0.5),
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteName.detailLocation,
                  arguments: postDetailBloc.post.location.id,
                ),
                child: Row(
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(
                        vertical: 5.0,
                        horizontal: 10.0,
                      ),
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: Image.network(
                        AppAssets.baseUrl +
                            postDetailBloc.post.location.image.first,
                        fit: BoxFit.cover,
                        width: sizeScreen * 10,
                        height: sizeScreen * 10,
                      ),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: sizeScreen * 25,
                          child: Text(
                            postDetailBloc.post.location.name,
                            style: theme.textTheme.headline6,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 5.0),
                          child: SizedBox(
                            width: sizeScreen * 25,
                            child: Text(
                              postDetailBloc.post.location.address,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: SvgPicture.asset(
                                AppAssets.ratingIcon,
                                height: sizeScreen * 1.8,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 5.0),
                              child: Text(
                                postDetailBloc.post.location.rating.toString(),
                                style: theme.textTheme.bodyText1,
                              ),
                            ),
                            Text(
                              '(${postDetailBloc.post.location.postByLocation} Review)',
                              style: theme.textTheme.bodyText2,
                            ),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
