import 'dart:async';

import 'package:flutter/material.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/untils/app_color.dart';
import '../../../../../../commons/untils/app_icon.dart';
import '../../../../../../commons/untils/app_image.dart';
import '../../../../../../localizations/app_localization.dart';
import '../../../../../../routes/route_name.dart';
import '../../../../../../theme.dart';

class SlideTopMonth extends StatefulWidget {
  const SlideTopMonth({Key key}) : super(key: key);
  @override
  _SlideTopMonthState createState() => _SlideTopMonthState();
}

class _SlideTopMonthState extends State<SlideTopMonth> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final lstIconItem = [AppIcon.topPlaces, AppIcon.topUser];
  Timer timer;
  int _currentIndex = 0;
  PageController _pageController = PageController(initialPage: 0);

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(seconds: 3), (timer) {
      if (_currentIndex < 1)
        _currentIndex++;
      else
        _currentIndex = 0;

      _pageController.animateToPage(
        _currentIndex,
        duration: Duration(milliseconds: 500),
        curve: Curves.ease,
      );
    });
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
    timer.cancel();
  }

  int getLastMonth() {
    final currentTime = DateTime.now();
    final previousMounth = currentTime.month - 1;
    return previousMounth == 0 ? 12 : previousMounth;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 5.0),
      color: theme.backgroundColor,
      child: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 10.0),
            height: sizeScreen * 17.0,
            child: PageView.builder(
              onPageChanged: (int selectedIndex) =>
                  setState(() => _currentIndex = selectedIndex),
              controller: _pageController,
              itemCount: 2,
              itemBuilder: (context, index) => InkWell(
                onTap: () => Future.delayed(
                  Duration(milliseconds: 300),
                  () => _currentIndex == 0
                      ? Navigator.pushNamed(context, RouteName.topLocation)
                      : Navigator.pushNamed(context, RouteName.topUser),
                ),
                child: Stack(
                  alignment: AlignmentDirectional.centerStart,
                  children: [
                    Container(
                      clipBehavior: Clip.antiAlias,
                      margin: const EdgeInsets.only(right: 5.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: AppColors.listBgColor[index],
                        image: DecorationImage(
                          image: AssetImage(
                            AppAssets.imgBanner1,
                          ),
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    Positioned(
                      left: 20.0,
                      bottom: 20,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Icon(
                            lstIconItem[index],
                            color: AppColors.lightThemeColor,
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(vertical: 5.0),
                            child: Text(
                              index == 0
                                  ? lang.locaized('topLocation')
                                  : lang.locaized('topUser'),
                              style: theme.textTheme.headline6.copyWith(
                                color: AppColors.lightThemeColor,
                                fontWeight: AppFontWeight.light,
                              ),
                            ),
                          ),
                          Text(
                            "${lang.locaized('month')} ${getLastMonth().toString()}",
                            style: theme.textTheme.headline4.copyWith(
                              color: AppColors.lightThemeColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(height: 5.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              for (var i = 0; i < 2; i++)
                if (i == _currentIndex)
                  DotsIndicator(isActive: true)
                else
                  DotsIndicator(isActive: false)
            ],
          )
        ],
      ),
    );
  }
}

class DotsIndicator extends StatelessWidget {
  final bool isActive;
  DotsIndicator({this.isActive});
  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: const EdgeInsets.symmetric(horizontal: 2.0),
      height: 6,
      width: isActive ? 15 : 6,
      decoration: BoxDecoration(
        color: isActive ? AppColors.primaryColor : AppColors.subLightColor,
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }
}
