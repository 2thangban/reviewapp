import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../commons/widgets/app_shimmer.dart';
import 'item_list_follow.dart';
import 'list_user_bloc/list_user_bloc.dart';

class ListFollow extends StatefulWidget {
  const ListFollow({Key key}) : super(key: key);
  @override
  _ListFollowState createState() => _ListFollowState();
}

class _ListFollowState extends State<ListFollow>
    with AutomaticKeepAliveClientMixin<ListFollow> {
  ListUserBloc listUserBloc;

  @override
  void initState() {
    super.initState();
    listUserBloc = BlocProvider.of<ListUserBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return RefreshIndicator(
      onRefresh: () async {
        listUserBloc.isRefresh = true;
        await Future.delayed(
          Duration(seconds: 1),
          () => listUserBloc.add(GetListUserEvent()),
        );
      },
      child: BlocBuilder<ListUserBloc, ListUserState>(
        bloc: listUserBloc,
        builder: (context, state) {
          if (listUserBloc.isRefresh && (state is ListUserLoading)) {
            listUserBloc.isRefresh = false;
            return AppShimmer.list();
          } else if (state is ListUserInitial) {
            listUserBloc.add(GetListUserEvent());
            return AppShimmer.list();
          } else if (state is ListUserFailed) {
            ///
          } else if (state is ListUserSuccess) {}
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: listUserBloc.list.length,
                itemBuilder: (context, index) =>
                    ItemListFollow(listUserBloc.list[index]),
              ),
              Visibility(
                visible: (state is ListUserLoading),
                child: AppLoading.threeBounce(size: 20.0),
              ),
            ],
          );
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
