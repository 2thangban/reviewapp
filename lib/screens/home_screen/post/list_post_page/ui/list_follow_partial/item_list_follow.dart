import 'package:flutter/material.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/untils/app_color.dart';
import '../../../../../../commons/untils/app_image.dart';
import '../../../../../../commons/widgets/circle_avt.dart';
import '../../../../../../commons/widgets/level_user_widget.dart';
import '../../../../../../models/user/user.dart';
import '../../../../../../routes/route_name.dart';

class ItemListFollow extends StatelessWidget {
  final Account account;
  ItemListFollow(this.account);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return Column(
      children: [
        ListTile(
          onTap: () => Navigator.pushNamed(
            context,
            RouteName.profile,
            arguments: account.id,
          ),
          dense: true,
          horizontalTitleGap: 10,
          contentPadding: const EdgeInsets.only(left: 10.0),
          leading: CircleAvt(
            radius: 19.0,
            image: '${AppAssets.baseUrl + account.avatar}',
            isIcon: true,
          ),
          title: Row(
            children: [
              Container(
                constraints: BoxConstraints(
                  maxWidth: sizeScreen * 20,
                ),
                child: InkWell(
                  child: Text(
                    account.userName,
                    style: theme.textTheme.bodyText1,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              const SizedBox(width: 5.0),
              LevelUser(account.rank),
            ],
          ),
          subtitle: Text(
            "${account.numOfFollower} người theo dõi",
            style: theme.textTheme.bodyText2,
          ),
        ),
        Container(
          child: SingleChildScrollView(
            padding: const EdgeInsets.only(left: 10.0),
            scrollDirection: Axis.horizontal,
            child: Row(
              children: account.postbyUser
                  .map(
                    (e) => Container(
                      margin: const EdgeInsets.only(right: 5.0),
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10.0),
                        color: AppColors.listBgColor[0],
                      ),
                      child: InkWell(
                        child: Image.network(
                          '${AppAssets.baseUrl + e.listImage.first}',
                          fit: BoxFit.cover,
                          height: 100,
                          width: 100,
                        ),
                        onTap: () => Future.delayed(
                          Duration(milliseconds: 500),
                          () => Navigator.pushNamed(
                            context,
                            RouteName.postDetail,
                            arguments: e.id,
                          ),
                        ),
                      ),
                    ),
                  )
                  .toList(),
            ),
          ),
        ),
        const Divider(
          color: AppColors.backgroundSearchColor,
          thickness: 2.0,
        ),
      ],
    );
  }
}
