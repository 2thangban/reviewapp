part of 'list_user_bloc.dart';

abstract class ListUserState {}

/// initialization
class ListUserInitial extends ListUserState {}

/// In case the user is getting the list
class ListUserLoading extends ListUserState {}

/// In case the user get the list successfully
class ListUserSuccess extends ListUserState {}

/// In case the user get the list fail
class ListUserFailed extends ListUserState {}
