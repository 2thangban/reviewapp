part of 'list_user_bloc.dart';

abstract class ListUserEvent {}

/// In case the user get the user of list.
class GetListUserEvent extends ListUserEvent {}
