import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../../models/user/user.dart';
import '../../../../../../../models/user/user_repo.dart';

part 'list_user_event.dart';
part 'list_user_state.dart';

class ListUserBloc extends Bloc<ListUserEvent, ListUserState> {
  final List<Account> list = [];
  bool isLastPage = false;
  bool isRefresh = false;
  int page = 0;
  int limit = 10;
  ListUserBloc() : super(ListUserInitial());

  @override
  Stream<ListUserState> mapEventToState(ListUserEvent event) async* {
    try {
      if (event is GetListUserEvent) {
        if (isRefresh || (state is ListUserInitial)) {
          yield ListUserLoading();
          page = 0;
          list.clear();
          await AccountRepo.getTopUserList(
            page: page++,
            limit: limit,
          ).then((value) {
            this.list.addAll(value);
            isLastPage = value.length < limit;
          });
          yield ListUserSuccess();
        } else {
          yield ListUserLoading();
          await AccountRepo.getTopUserList(
            page: page++,
            limit: limit,
          ).then((value) {
            this.list.addAll(value);
            isLastPage = value.length < limit;
          });
          yield ListUserSuccess();
        }
      }
    } catch (e) {
      ///
    }
  }
}
