import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/post/post.dart';
import '../../../../../../../../models/post/post_repo.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_food_event.dart';
part 'list_food_state.dart';

class ListFoodBloc extends Bloc<ListFoodEvent, ListFoodState> {
  bool isRefresh = false;
  bool isLastPage = false;
  int page = 0;
  bool isActive = false;
  final int limit = 10;
  final List<Post> listPost = [];
  ListFoodBloc() : super(ListFoodInitial());

  @override
  Stream<ListFoodState> mapEventToState(
    ListFoodEvent event,
  ) async* {
    if (event is GetListFoodEvent) {
      try {
        ///initial then get list page = 0
        if (isRefresh || (state is ListFoodInitial)) {
          yield ListFoodRefresh();
          page = 0;
          listPost.clear();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          isLastPage = result.length < limit;
          listPost.addAll(result);
          yield ListFoodSuccess();
        } else {
          /// else then handle get list (load more or refresh)
          yield ListFoodLoading();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListFoodSuccess();
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Post> result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            result = await PostRepo.getAllPost(
              page: page++,
              limit: limit,
              isAuth: event.isAuth,
              provinceId: event.pronvinceId,
              categoryId: event.categoryId,
            );
          });
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListFoodSuccess();
        } else {
          yield ListFoodFailed(msg: e.toString());
        }
      } catch (e) {
        ///
      }
    }
  }
}
