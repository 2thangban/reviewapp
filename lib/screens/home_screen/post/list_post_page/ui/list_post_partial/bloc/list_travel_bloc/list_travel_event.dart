part of 'list_travel_bloc.dart';

abstract class ListTravelEvent {}

class GetListTravelEvent extends ListTravelEvent {
  final bool isAuth;
  final int pronvinceId;
  final int categoryId;

  GetListTravelEvent({
    this.isAuth = false,
    this.pronvinceId,
    this.categoryId,
  });
}
