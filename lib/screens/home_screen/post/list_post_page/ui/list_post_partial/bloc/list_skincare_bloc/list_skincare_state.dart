part of 'list_skincare_bloc.dart';

abstract class ListSkinCareState {}

class ListSkinCareInitial extends ListSkinCareState {}

class ListSkinCareRefresh extends ListSkinCareState {}

class ListSkinCareLoading extends ListSkinCareState {}

class ListSkinCareSuccess extends ListSkinCareState {}

class ListSkinCareFailed extends ListSkinCareState {
  final String msg;
  ListSkinCareFailed({this.msg});
}
