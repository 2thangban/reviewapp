part of 'list_post_bloc.dart';

@immutable
abstract class ListPostEvent {}

class GetListPostEvent extends ListPostEvent {
  final bool isAuth;
  final int pronvinceId;
  final int categoryId;
  GetListPostEvent({
    this.isAuth = false,
    this.pronvinceId,
    this.categoryId,
  });
}
