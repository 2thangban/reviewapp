part of 'list_beverage_bloc.dart';

abstract class ListBeverageEvent {}

class GetListBeverageEvent extends ListBeverageEvent {
  final bool isAuth;
  final int pronvinceId;
  final int categoryId;

  GetListBeverageEvent({
    this.isAuth = false,
    this.pronvinceId,
    this.categoryId,
  });
}
