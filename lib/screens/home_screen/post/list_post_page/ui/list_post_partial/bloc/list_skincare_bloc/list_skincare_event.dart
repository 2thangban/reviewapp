part of 'list_skincare_bloc.dart';

abstract class ListSkinCareEvent {}

class GetListSkinCareEvent extends ListSkinCareEvent {
  final bool isAuth;
  final int pronvinceId;
  final int categoryId;

  GetListSkinCareEvent({
    this.isAuth = false,
    this.pronvinceId,
    this.categoryId,
  });
}
