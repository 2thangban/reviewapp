part of 'list_beverage_bloc.dart';

abstract class ListBeverageState {}

class ListBeverageInitial extends ListBeverageState {}

class ListBeverageRefresh extends ListBeverageState {}

class ListBeverageLoading extends ListBeverageState {}

class ListBeverageSuccess extends ListBeverageState {}

class ListBeverageFailed extends ListBeverageState {
  final String msg;
  ListBeverageFailed({this.msg});
}
