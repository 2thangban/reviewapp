import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/post/post.dart';
import '../../../../../../../../models/post/post_repo.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_beverage_event.dart';
part 'list_beverage_state.dart';

class ListBeverageBloc extends Bloc<ListBeverageEvent, ListBeverageState> {
  bool isRefresh = false;
  bool isLastPage = false;
  int page = 0;
  bool isActive = false;
  final int limit = 10;
  final List<Post> listPost = [];
  ListBeverageBloc() : super(ListBeverageInitial());

  @override
  Stream<ListBeverageState> mapEventToState(
    ListBeverageEvent event,
  ) async* {
    if (event is GetListBeverageEvent) {
      try {
        ///initial then get list page = 0
        if (isRefresh || state is ListBeverageInitial) {
          yield ListBeverageRefresh();
          page = 0;
          listPost.clear();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          isLastPage = result.length < limit;
          listPost.addAll(result);
          yield ListBeverageSuccess();
        } else {
          /// else then handle get list (load more or refresh)
          yield ListBeverageLoading();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListBeverageSuccess();
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Post> result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            result = await PostRepo.getAllPost(
              page: page++,
              limit: limit,
              isAuth: event.isAuth,
              provinceId: event.pronvinceId,
              categoryId: event.categoryId,
            );
          });
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListBeverageSuccess();
        } else {
          yield ListBeverageFailed(msg: e.toString());
        }
      } catch (e) {
        ///
      }
    }
  }
}
