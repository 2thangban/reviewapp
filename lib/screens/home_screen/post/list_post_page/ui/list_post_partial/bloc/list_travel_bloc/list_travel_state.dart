part of 'list_travel_bloc.dart';

abstract class ListTravelState {}

class ListTravelInitial extends ListTravelState {}

class ListTravelRefresh extends ListTravelState {}

class ListTravelLoading extends ListTravelState {}

class ListTravelSuccess extends ListTravelState {}

class ListTravelFailed extends ListTravelState {
  final String msg;
  ListTravelFailed({this.msg});
}
