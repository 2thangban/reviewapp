import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../../../models/category/category.dart';
import '../../../../../../../../models/category/category_repo.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc() : super(CategoryInitial());

  @override
  Stream<CategoryState> mapEventToState(CategoryEvent event) async* {
    if (event is GetListCategoriesEvent) {
      try {
        yield CategoryLoading();
        final result = await CategoryRepo.getAll();
        yield CategorySuccess(result);
      } catch (e) {
        ///
      }
    }
  }
}
