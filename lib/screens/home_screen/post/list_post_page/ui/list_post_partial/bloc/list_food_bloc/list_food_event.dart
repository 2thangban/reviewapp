part of 'list_food_bloc.dart';

abstract class ListFoodEvent {}

class GetListFoodEvent extends ListFoodEvent {
  final bool isAuth;
  final int pronvinceId;
  final int categoryId;

  GetListFoodEvent({
    this.isAuth = false,
    this.pronvinceId,
    this.categoryId,
  });
}
