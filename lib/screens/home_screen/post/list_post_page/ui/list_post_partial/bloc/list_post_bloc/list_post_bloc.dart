import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/post/post.dart';
import '../../../../../../../../models/post/post_repo.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_post_event.dart';
part 'list_post_state.dart';

class ListPostBloc extends Bloc<ListPostEvent, ListPostState> {
  bool isRefresh = false;
  bool isLastPage = false;
  int page = 0;
  final int limit = 6;
  final List<Post> listPost = [];
  ListPostBloc() : super(GetPostInitial());

  @override
  Stream<ListPostState> mapEventToState(ListPostEvent event) async* {
    if (event is GetListPostEvent) {
      try {
        ///initial then get list page = 0
        if ((state is GetPostInitial) || isRefresh) {
          yield GetPostRefresh();
          page = 0;
          listPost.clear();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          isLastPage = result.length < limit;
          listPost.addAll(result);
          yield GetPostSuccess();
        } else {
          /// else then handle get list (load more or refresh)
          yield GetPostLoading();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield GetPostSuccess();
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Post> result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value).whenComplete(() async {
              result = await PostRepo.getAllPost(
                page: page++,
                limit: limit,
                isAuth: event.isAuth,
                provinceId: event.pronvinceId,
                categoryId: event.categoryId,
              );
            });
          });
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield GetPostSuccess();
        } else {
          yield GetPostFailed(msg: e.toString());
        }
      } catch (e) {
        ///
      }
    }
  }
}
