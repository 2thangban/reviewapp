part of 'list_food_bloc.dart';

abstract class ListFoodState {}

class ListFoodInitial extends ListFoodState {}

class ListFoodRefresh extends ListFoodState {}

class ListFoodLoading extends ListFoodState {}

class ListFoodSuccess extends ListFoodState {}

class ListFoodFailed extends ListFoodState {
  final String msg;
  ListFoodFailed({this.msg});
}
