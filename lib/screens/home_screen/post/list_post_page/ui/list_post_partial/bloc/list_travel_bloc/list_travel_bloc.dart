import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/post/post.dart';
import '../../../../../../../../models/post/post_repo.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_travel_event.dart';
part 'list_travel_state.dart';

class ListTravelBloc extends Bloc<ListTravelEvent, ListTravelState> {
  bool isRefresh = false;
  bool isLastPage = false;
  int page = 0;
  bool isActive = false;
  final int limit = 10;
  final List<Post> listPost = [];
  ListTravelBloc() : super(ListTravelInitial());

  @override
  Stream<ListTravelState> mapEventToState(
    ListTravelEvent event,
  ) async* {
    if (event is GetListTravelEvent) {
      try {
        ///initial then get list page = 0
        if (isRefresh || state is ListTravelInitial) {
          yield ListTravelRefresh();
          page = 0;
          listPost.clear();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          isLastPage = result.length < limit;
          listPost.addAll(result);
          yield ListTravelSuccess();
        } else {
          /// else then handle get list (load more or refresh)
          yield ListTravelLoading();
          final result = await PostRepo.getAllPost(
            page: page++,
            limit: limit,
            isAuth: event.isAuth,
            provinceId: event.pronvinceId,
            categoryId: event.categoryId,
          );
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListTravelSuccess();
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Post> result;
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            result = await PostRepo.getAllPost(
              page: page++,
              limit: limit,
              isAuth: event.isAuth,
              provinceId: event.pronvinceId,
              categoryId: event.categoryId,
            );
          });
          listPost.addAll(result);
          isLastPage = result.length < limit;
          yield ListTravelSuccess();
        } else {
          yield ListTravelFailed(msg: e.toString());
        }
      } catch (e) {
        ///
      }
    }
  }
}
