part of 'list_post_bloc.dart';

@immutable
abstract class ListPostState {}

class GetPostInitial extends ListPostState {}

class GetPostRefresh extends ListPostState {}

class GetPostLoading extends ListPostState {}

class GetPostSuccess extends ListPostState {
  GetPostSuccess();
}

class GetPostFailed extends ListPostState {
  final String msg;
  GetPostFailed({this.msg});
}
