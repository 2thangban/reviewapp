import 'package:bloc/bloc.dart';

import '../../../../../../../../models/category/category.dart';

class ListcategoryCubit extends Cubit<int> {
  final List<Category> list = [Category(id: 0, name: 'Tất cả')];
  int index = 0;
  ListcategoryCubit() : super(0);

  void addList(List<Category> newlist) {
    this.list.addAll(newlist);
    emit(0);
  }
}
