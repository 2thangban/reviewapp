import 'package:app_review/commons/helper/size_config.dart';
import 'package:app_review/commons/widgets/app_button.dart';
import 'package:app_review/commons/widgets/list_empty.dart';
import 'package:app_review/localizations/app_localization.dart';

import '../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../commons/widgets/sliver_grid_list.dart';
import '../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../../app_common_bloc/province_bloc/province_bloc.dart';
import 'bloc/list_beverage_bloc/list_beverage_bloc.dart';
import 'bloc/listcategory_cubit/listcategory_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/list_food_bloc/list_food_bloc.dart';

class ListFoodPost extends StatefulWidget {
  const ListFoodPost({Key key}) : super(key: key);
  @override
  _ListFoodPostState createState() => _ListFoodPostState();
}

class _ListFoodPostState extends State<ListFoodPost>
    with AutomaticKeepAliveClientMixin<ListFoodPost> {
  final sizeScreen = SizedConfig.heightMultiplier;
  ListFoodBloc listFoodBloc;
  AuthBloc authBloc;
  ListcategoryCubit listcategoryCubit;
  ProvinceBloc provinceBloc;
  @override
  void initState() {
    super.initState();
    listFoodBloc = BlocProvider.of<ListFoodBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    listcategoryCubit = BlocProvider.of<ListcategoryCubit>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ListFoodBloc, ListFoodState>(
      builder: (context, state) {
        if (state is ListFoodRefresh) {
          listFoodBloc.isRefresh = false;
          return AppShimmer.list();
        } else if (state is ListFoodInitial) {
          listFoodBloc.isActive = true;
          listFoodBloc.add(
            GetListFoodEvent(
              isAuth: authBloc.isChecked,
              pronvinceId: provinceBloc.currentProvince.id,
              categoryId: 1,
            ),
          );
        } else if (state is ListFoodFailed) {
          return SizedBox(
            height: sizeScreen * 40.0,
            child: SizedBox(
              height: sizeScreen * 5,
              width: sizeScreen * 15,
              child: AppButton.common(
                labelText: AppLocalization.of(context).locaized('reConnect'),
                onPressed: () {
                  listFoodBloc.add(
                    GetListFoodEvent(
                      isAuth: authBloc.isChecked,
                      pronvinceId: provinceBloc.currentProvince.id,
                      categoryId: 2,
                    ),
                  );
                },
              ),
            ),
          );
        }
        if (state is ListFoodSuccess) {
          if (listFoodBloc.listPost.isEmpty) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 40.0,
                width: sizeScreen * 40.0,
                child: AppObjectEmpty.list(),
              ),
            );
          }
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              PostList(
                listFoodBloc.listPost,
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 5,
                ),
              ),
              Visibility(
                visible: (state is ListFoodLoading),
                child: SizedBox(
                  height: 30.0,
                  child: AppLoading.threeBounce(size: 20.0),
                ),
              )
            ],
          );
        }
        return AppShimmer.list();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
