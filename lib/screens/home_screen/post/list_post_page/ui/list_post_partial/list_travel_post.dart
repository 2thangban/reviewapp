import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/widgets/app_button.dart';
import '../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../commons/widgets/list_empty.dart';
import '../../../../../../commons/widgets/sliver_grid_list.dart';
import '../../../../../../localizations/app_localization.dart';
import '../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../../app_common_bloc/province_bloc/province_bloc.dart';
import 'bloc/list_travel_bloc/list_travel_bloc.dart';
import 'bloc/listcategory_cubit/listcategory_cubit.dart';

class ListTravelPost extends StatefulWidget {
  const ListTravelPost({Key key}) : super(key: key);
  @override
  _ListTravelPostState createState() => _ListTravelPostState();
}

class _ListTravelPostState extends State<ListTravelPost>
    with AutomaticKeepAliveClientMixin<ListTravelPost> {
  final sizeScreen = SizedConfig.heightMultiplier;
  ListTravelBloc listTravelPostBloc;
  AuthBloc authBloc;
  ListcategoryCubit listcategoryCubit;
  ProvinceBloc provinceBloc;
  @override
  void initState() {
    super.initState();
    listTravelPostBloc = BlocProvider.of<ListTravelBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    listcategoryCubit = BlocProvider.of<ListcategoryCubit>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ListTravelBloc, ListTravelState>(
      builder: (context, state) {
        if (state is ListTravelRefresh) {
          listTravelPostBloc.isRefresh = false;
          return AppShimmer.list();
        } else if (state is ListTravelInitial) {
          listTravelPostBloc.isActive = true;
          listTravelPostBloc.add(
            GetListTravelEvent(
              isAuth: authBloc.isChecked,
              pronvinceId: provinceBloc.currentProvince.id,
              categoryId: 3,
            ),
          );
        } else if (state is ListTravelFailed) {
          return SizedBox(
            height: sizeScreen * 40.0,
            child: SizedBox(
              height: sizeScreen * 5,
              width: sizeScreen * 15,
              child: AppButton.common(
                labelText: AppLocalization.of(context).locaized('reConnect'),
                onPressed: () {
                  listTravelPostBloc.add(
                    GetListTravelEvent(
                      isAuth: authBloc.isChecked,
                      pronvinceId: provinceBloc.currentProvince.id,
                      categoryId: 2,
                    ),
                  );
                },
              ),
            ),
          );
        }
        if (state is ListTravelSuccess) {
          if (listTravelPostBloc.listPost.isEmpty) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 40.0,
                width: sizeScreen * 40.0,
                child: AppObjectEmpty.list(),
              ),
            );
          }
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              PostList(
                listTravelPostBloc.listPost,
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 5.0,
                ),
              ),
              Visibility(
                visible: (state is ListTravelLoading),
                child: SizedBox(
                  height: 30.0,
                  child: AppLoading.threeBounce(size: 20.0),
                ),
              )
            ],
          );
        }
        return AppShimmer.list();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
