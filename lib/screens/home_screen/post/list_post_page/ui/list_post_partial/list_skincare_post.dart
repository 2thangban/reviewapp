import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/widgets/app_button.dart';
import '../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../commons/widgets/list_empty.dart';
import '../../../../../../commons/widgets/sliver_grid_list.dart';
import '../../../../../../localizations/app_localization.dart';
import '../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../../app_common_bloc/province_bloc/province_bloc.dart';
import 'bloc/list_skincare_bloc/list_skincare_bloc.dart';
import 'bloc/listcategory_cubit/listcategory_cubit.dart';

class ListSkinCarePost extends StatefulWidget {
  const ListSkinCarePost({Key key}) : super(key: key);
  @override
  _ListSkinCarePostState createState() => _ListSkinCarePostState();
}

class _ListSkinCarePostState extends State<ListSkinCarePost>
    with AutomaticKeepAliveClientMixin<ListSkinCarePost> {
  final sizeScreen = SizedConfig.heightMultiplier;
  ListSkinCareBloc listSkinCareBloc;
  AuthBloc authBloc;
  ListcategoryCubit listcategoryCubit;
  ProvinceBloc provinceBloc;
  @override
  void initState() {
    super.initState();
    listSkinCareBloc = BlocProvider.of<ListSkinCareBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    listcategoryCubit = BlocProvider.of<ListcategoryCubit>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ListSkinCareBloc, ListSkinCareState>(
      builder: (context, state) {
        if (state is ListSkinCareRefresh) {
          listSkinCareBloc.isRefresh = false;
          return AppShimmer.list();
        } else if (state is ListSkinCareInitial) {
          listSkinCareBloc.isActive = true;
          listSkinCareBloc.add(
            GetListSkinCareEvent(
              isAuth: authBloc.isChecked,
              pronvinceId: provinceBloc.currentProvince.id,
              categoryId: 4,
            ),
          );
        } else if (state is ListSkinCareFailed) {
          return SizedBox(
            height: sizeScreen * 40.0,
            child: SizedBox(
              height: sizeScreen * 5,
              width: sizeScreen * 15,
              child: AppButton.common(
                labelText: AppLocalization.of(context).locaized('reConnect'),
                onPressed: () {
                  listSkinCareBloc.add(
                    GetListSkinCareEvent(
                      isAuth: authBloc.isChecked,
                      pronvinceId: provinceBloc.currentProvince.id,
                      categoryId: 2,
                    ),
                  );
                },
              ),
            ),
          );
        }
        if (state is ListSkinCareSuccess) {
          if (listSkinCareBloc.listPost.isEmpty) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 40.0,
                width: sizeScreen * 40.0,
                child: AppObjectEmpty.list(),
              ),
            );
          }
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              PostList(
                listSkinCareBloc.listPost,
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 5.0,
                ),
              ),
              Visibility(
                visible: (state is ListSkinCareLoading),
                child: SizedBox(
                  height: 30.0,
                  child: AppLoading.threeBounce(size: 20.0),
                ),
              )
            ],
          );
        }
        return AppShimmer.list();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
