import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/widgets/app_button.dart';
import '../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../commons/widgets/list_empty.dart';
import '../../../../../../commons/widgets/sliver_grid_list.dart';
import '../../../../../../localizations/app_localization.dart';
import '../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../../app_common_bloc/province_bloc/province_bloc.dart';
import 'bloc/list_post_bloc/list_post_bloc.dart';
import 'bloc/listcategory_cubit/listcategory_cubit.dart';

class ListPostAll extends StatefulWidget {
  const ListPostAll({Key key}) : super(key: key);
  @override
  _ListPostAllState createState() => _ListPostAllState();
}

class _ListPostAllState extends State<ListPostAll>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  ListPostBloc listPostAllBloc;
  AuthBloc authBloc;
  ListcategoryCubit listcategoryCubit;
  ProvinceBloc provinceBloc;
  @override
  void initState() {
    super.initState();
    listPostAllBloc = BlocProvider.of<ListPostBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    listcategoryCubit = BlocProvider.of<ListcategoryCubit>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);

    listPostAllBloc.add(
      GetListPostEvent(
        isAuth: authBloc.isChecked,
        pronvinceId: provinceBloc.currentProvince.id,
        categoryId: listcategoryCubit.index,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocBuilder<ListPostBloc, ListPostState>(
      bloc: listPostAllBloc,
      builder: (context, state) {
        if (state is GetPostRefresh) {
          listPostAllBloc.isRefresh = false;
          return AppShimmer.list();
        } else if (state is GetPostFailed) {
          return SizedBox(
            height: sizeScreen * 40.0,
            child: SizedBox(
              height: sizeScreen * 5,
              width: sizeScreen * 15,
              child: AppButton.common(
                labelText: AppLocalization.of(context).locaized('reConnect'),
                onPressed: () {
                  listPostAllBloc.add(
                    GetListPostEvent(
                      isAuth: authBloc.isChecked,
                      pronvinceId: provinceBloc.currentProvince.id,
                      categoryId: listcategoryCubit.index,
                    ),
                  );
                },
              ),
            ),
          );
        }
        if (state is GetPostSuccess) {
          if (listPostAllBloc.listPost.isEmpty) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 40.0,
                width: sizeScreen * 40.0,
                child: AppObjectEmpty.list(),
              ),
            );
          }
          return ListView(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            children: [
              PostList(
                listPostAllBloc.listPost,
                physics: NeverScrollableScrollPhysics(),
                padding: const EdgeInsets.symmetric(
                  horizontal: 15.0,
                  vertical: 5.0,
                ),
              ),
              Visibility(
                visible: (state is GetPostLoading),
                child: SizedBox(
                  height: 30.0,
                  child: AppLoading.threeBounce(size: 20.0),
                ),
              )
            ],
          );
        }
        return AppShimmer.list();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
