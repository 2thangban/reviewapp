import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/untils/app_color.dart';
import '../../../../../../commons/widgets/app_bar.dart';
import 'bloc/category_bloc/category_bloc.dart';
import 'bloc/listcategory_cubit/listcategory_cubit.dart';
import 'list_beverage_post.dart';
import 'list_food_post.dart';
import 'list_post_all.dart';
import 'list_skincare_post.dart';
import 'list_travel_post.dart';
import '../../../../../../theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListPostPartial extends StatefulWidget {
  @override
  _ListPostPartialState createState() => _ListPostPartialState();
}

class _ListPostPartialState extends State<ListPostPartial>
    with AutomaticKeepAliveClientMixin<ListPostPartial> {
  final CategoryBloc categoryBloc = CategoryBloc();
  ListcategoryCubit listcategoryCubit;

  @override
  void initState() {
    super.initState();
    listcategoryCubit = BlocProvider.of<ListcategoryCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        appBar: CommonAppBar(
          height: sizeScreen * 3.5,
          bottomWidget: PreferredSize(
            preferredSize: Size.fromHeight(sizeScreen * 10),
            child: Container(
              child: BlocBuilder<CategoryBloc, CategoryState>(
                bloc: categoryBloc,
                builder: (context, state) {
                  if (state is CategoryInitial) {
                    categoryBloc.add(GetListCategoriesEvent());
                    return const SizedBox();
                  } else if (state is CategoryLoading) {
                    return const SizedBox();
                  } else if (listcategoryCubit.list.length == 1 &&
                      state is CategorySuccess) {
                    listcategoryCubit
                        .addList(state.categories.reversed.toList());
                  }
                  return BlocBuilder<ListcategoryCubit, int>(
                    bloc: listcategoryCubit,
                    builder: (context, listCate) {
                      return TabBar(
                        onTap: (int selectedIndex) =>
                            listcategoryCubit.index = selectedIndex,
                        isScrollable: true,
                        labelStyle: theme.textTheme.bodyText1.copyWith(
                          fontWeight: AppFontWeight.regular,
                        ),
                        labelColor: AppColors.primaryColor,
                        indicatorColor: AppColors.primaryColor,
                        indicatorSize: TabBarIndicatorSize.label,
                        tabs: listcategoryCubit.list
                            .map((e) => Text(e.name))
                            .toList(),
                      );
                    },
                  );
                },
              ),
            ),
          ),
        ),
        body: TabBarView(
          children: [
            const ListPostAll(),
            const ListFoodPost(),
            const ListBeveragePost(),
            const ListTravelPost(),
            const ListSkinCarePost(),
          ],
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
