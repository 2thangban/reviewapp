import 'package:bloc/bloc.dart';

class PageCubit extends Cubit<int> {
  int currentPage = 0;
  PageCubit() : super(0);

  void change(int newPage) => emit(this.currentPage = newPage);
}
