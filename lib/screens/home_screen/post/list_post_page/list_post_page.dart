import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/animation/fade_anim.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_shimmer.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/category/category.dart';
import '../../../../models/province/province.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../../app_common_bloc/province_bloc/province_bloc.dart';
import '../../../show_image_screen/bloc/page_cubit/page_cubit.dart';
import 'ui/list_follow_partial/list_follwer.dart';
import 'ui/list_follow_partial/list_user_bloc/list_user_bloc.dart';
import 'ui/list_post_partial/bloc/list_beverage_bloc/list_beverage_bloc.dart';
import 'ui/list_post_partial/bloc/list_food_bloc/list_food_bloc.dart';
import 'ui/list_post_partial/bloc/list_post_bloc/list_post_bloc.dart';
import 'ui/list_post_partial/bloc/list_skincare_bloc/list_skincare_bloc.dart';
import 'ui/list_post_partial/bloc/list_travel_bloc/list_travel_bloc.dart';
import 'ui/list_post_partial/bloc/listcategory_cubit/listcategory_cubit.dart';
import 'ui/list_post_partial/list_post_partial.dart';
import 'ui/silde_home/top_slide.dart';

class ListPostPage extends StatefulWidget {
  const ListPostPage({Key key}) : super(key: key);
  @override
  _ListPostPageState createState() => _ListPostPageState();
}

class _ListPostPageState extends State<ListPostPage>
    with AutomaticKeepAliveClientMixin<ListPostPage> {
  final ScrollController _scrollController = ScrollController();

  AppLocationBloc locationBloc;
  final ListPostBloc listPostAllBloc = ListPostBloc();
  final ListFoodBloc listFoodBloc = ListFoodBloc();
  final ListBeverageBloc listBeverageBloc = ListBeverageBloc();
  final ListTravelBloc listTravelBloc = ListTravelBloc();
  final ListSkinCareBloc listSkincareBloc = ListSkinCareBloc();
  final ListUserBloc listUserBloc = ListUserBloc();
  AuthBloc authBloc;

  bool isLoadingLocation = false;

  ProvinceBloc provinceBloc;

  //Province currentProvince;
  final ListcategoryCubit listcategoryCubit = ListcategoryCubit();
  final PageCubit pageCubit = PageCubit();
  final List<Category> listCategories = [];

  @override
  void initState() {
    super.initState();

    /// initial location bloc
    locationBloc = BlocProvider.of<AppLocationBloc>(context);
    locationBloc.add(GetLocationEvent());

    /// initial auth bloc
    authBloc = BlocProvider.of(context);

    provinceBloc = BlocProvider.of(context);

    /// This function called while user scroll list to last position.
    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          log(listcategoryCubit.index.toString());
        }
      },
    );
  }

  bool checkExistProvince(Province province) {
    provinceBloc.boxProvince.values.forEach((element) {
      if (province.id == element.id) {
        return true;
      }
    });
    return false;
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
    listPostAllBloc.close();
    listBeverageBloc.close();
    listFoodBloc.close();
    listSkincareBloc.close();
    listTravelBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final double sizeScreen = SizedConfig.heightMultiplier;
    final lang = AppLocalization.of(context);
    final theme = Theme.of(context);

    return BlocBuilder<AppLocationBloc, AppLocationState>(
      bloc: locationBloc,
      builder: (context, state) {
        if (state is ChangeLocationSuccess) {
          /// refresh the all post list with new province
          listPostAllBloc.add(
            GetListPostEvent(
              isAuth: authBloc.isChecked,
              pronvinceId: state.province.id,
              categoryId: listcategoryCubit.index,
            ),
          );

          /// refresh the food list with new province
          if (listFoodBloc.isActive) {
            listFoodBloc.add(
              GetListFoodEvent(
                isAuth: authBloc.isChecked,
                pronvinceId: state.province.id,
                categoryId: 1,
              ),
            );
          }

          /// refresh the beverage list with new province
          if (listBeverageBloc.isActive) {
            listBeverageBloc.add(
              GetListBeverageEvent(
                isAuth: authBloc.isChecked,
                pronvinceId: state.province.id,
                categoryId: 2,
              ),
            );
          }

          /// refresh the travel list with new province
          if (listTravelBloc.isActive) {
            listTravelBloc.add(
              GetListTravelEvent(
                isAuth: authBloc.isChecked,
                pronvinceId: state.province.id,
                categoryId: 3,
              ),
            );
          }

          /// refresh the skincare list with new province
          if (listSkincareBloc.isActive) {
            listSkincareBloc.add(
              GetListSkinCareEvent(
                isAuth: authBloc.isChecked,
                pronvinceId: state.province.id,
                categoryId: 4,
              ),
            );
          }
          provinceBloc.add(AddNewProvinceEvent(state.province));
        } else if (state is AppLocationAllow) {
          if (provinceBloc.currentProvince == null) {
            if (checkExistProvince(state.province) == false) {
              provinceBloc.add(AddNewProvinceEvent(state.province));
              provinceBloc.currentProvince = state.province;
            }
          }
          isLoadingLocation = false;
        } else if (state is AppLocationDenied) {
          if (checkExistProvince(state.province) == false) {
            provinceBloc.add(AddNewProvinceEvent(state.province));
            provinceBloc.currentProvince = state.province;
          }

          isLoadingLocation = false;
        } else if (state is AppLocationLoading) {
          isLoadingLocation = true;
        } else {
          isLoadingLocation = true;
        }
        return isLoadingLocation
            ? AppShimmer.home()
            : MultiBlocProvider(
                providers: [
                  BlocProvider(create: (context) => listUserBloc),
                  BlocProvider(create: (context) => listFoodBloc),
                  BlocProvider(create: (context) => listcategoryCubit),
                  BlocProvider(create: (context) => listPostAllBloc),
                  BlocProvider(create: (context) => listBeverageBloc),
                  BlocProvider(create: (context) => listTravelBloc),
                  BlocProvider(create: (context) => listSkincareBloc),
                ],
                child: RefreshIndicator(
                  onRefresh: () async => await Future.delayed(
                    Duration(seconds: 1),
                    () => locationBloc.add(GetLocationEvent()),
                  ),
                  child: Scaffold(
                    appBar: CommonAppBar(
                      leadingWidth: 0.0,
                      statusBarColor: theme.backgroundColor,
                      height: sizeScreen * 11,
                      titleWidget: Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: InkWell(
                            child: SizedBox(
                              height: sizeScreen * 3,
                              child: FittedBox(
                                fit: BoxFit.cover,
                                child: Row(
                                  children: [
                                    Icon(
                                      AppIcon.pin,
                                      color: AppColors.darkThemeColor,
                                    ),
                                    const SizedBox(width: 2.5),
                                    BlocBuilder<ProvinceBloc, ProvinceState>(
                                      bloc: provinceBloc,
                                      builder: (_, state) {
                                        if (state is ProvinceSuccess) {
                                          return Text(
                                            state.province.name,
                                            style: theme.textTheme.headline4,
                                          );
                                        }
                                        return Text(
                                          provinceBloc.currentProvince.name,
                                          style: theme.textTheme.headline4,
                                        );
                                      },
                                    )
                                  ],
                                ),
                              ),
                            ),
                            onTap: () async {
                              final result = await Future.delayed(
                                Duration(milliseconds: 200),
                                () => Navigator.pushNamed(
                                  context,
                                  RouteName.changeAddress,
                                  arguments: provinceBloc.boxProvince,
                                ),
                              );
                              if (result != null) {
                                final Province newProvince =
                                    (result as Province);
                                if (newProvince.id !=
                                    provinceBloc.currentProvince.id) {
                                  listPostAllBloc.isRefresh = true;
                                  listFoodBloc.isRefresh = true;
                                  listBeverageBloc.isRefresh = true;
                                  listSkincareBloc.isRefresh = true;
                                  listTravelBloc.isRefresh = true;
                                  listUserBloc.isRefresh = true;
                                  locationBloc.add(
                                    ChangeLocationEvent(newProvince),
                                  );
                                }
                              }
                            },
                          ),
                        ),
                      ),
                      bottomWidget: PreferredSize(
                        preferredSize: Size.fromHeight(sizeScreen * 13),
                        child: GestureDetector(
                          onTap: () => Future.delayed(
                            Duration(milliseconds: 200),
                            () => Navigator.pushNamed(
                              context,
                              RouteName.search,
                            ),
                          ),
                          child: Container(
                            margin: const EdgeInsets.symmetric(
                              horizontal: 10.0,
                              vertical: 5.0,
                            ),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(7.0),
                              color: AppColors.backgroundSearchColor
                                  .withOpacity(0.5),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 12.0,
                              ),
                              child: Row(
                                children: [
                                  const SizedBox(width: 20.0),
                                  Image.asset(
                                    AppAssets.searchIcon,
                                    height: sizeScreen * 2.0,
                                  ),
                                  const SizedBox(width: 10.0),
                                  Text(
                                    lang.locaized('hintSearch'),
                                    style: theme.textTheme.bodyText1.copyWith(
                                      fontWeight: AppFontWeight.regular,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    body: FadeAnim(
                      child: DefaultTabController(
                        length: 2,
                        child: NestedScrollView(
                          controller: _scrollController,
                          headerSliverBuilder: (context, value) => [
                            SliverAppBar(
                              pinned: true,
                              floating: true,
                              toolbarHeight: 0.0,
                              collapsedHeight: 0.1,
                              elevation: 0.0,
                              backgroundColor: theme.backgroundColor,
                              expandedHeight: sizeScreen * 27,
                              flexibleSpace: FlexibleSpaceBar(
                                background: Column(
                                  children: [
                                    const SlideTopMonth(),
                                    //const SlideTotalPost(),
                                  ],
                                ),
                              ),
                              bottom: PreferredSize(
                                preferredSize:
                                    Size.fromHeight(sizeScreen * 6.5),
                                child: Container(
                                  color: theme.backgroundColor,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      TabBar(
                                        isScrollable: true,
                                        labelStyle: theme.textTheme.headline6,
                                        labelColor: AppColors.primaryColor,
                                        indicatorColor: theme.backgroundColor,
                                        tabs: [
                                          Tab(
                                            child: Text(
                                              lang.locaized('discovery'),
                                            ),
                                          ),
                                          Tab(
                                            child: Text(
                                              lang.locaized('follow'),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                          body: TabBarView(
                            children: [
                              ListPostPartial(),
                              ListFollow(),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              );
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
