import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/report/report_repo.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/reponse_action_user.dart';

part 'report_post_event.dart';
part 'report_post_state.dart';

class ReportPostBloc extends Bloc<ReportPostEvent, ReportPostState> {
  ReportPostBloc() : super(ReportPostState());

  @override
  Stream<ReportPostState> mapEventToState(
    ReportPostEvent event,
  ) async* {
    if (event is ReportPost) {
      yield state.copyWith(status: ReportStatus.loading);
      try {
        final result = await ReportPostRepo.report(
          event.postId,
          event.reportId,
          content: event.moreContent,
        );
        if (result.status.contains('Success')) {
          yield state.copyWith(status: ReportStatus.success);
        }
        yield state.copyWith(status: ReportStatus.failure);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          ResponseActionUser result;
          await TokenRepo.refreshToken().then((value) async {
            await ReportPostRepo.report(
              event.postId,
              event.reportId,
              content: event.moreContent,
            ).then((value) => result = value);
          });
          if (result?.status != null) {
            yield state.copyWith(status: ReportStatus.success);
          }
          yield state.copyWith(status: ReportStatus.failure);
        }
        yield state.copyWith(status: ReportStatus.failure);
      }
    } else if (event is RefreshFormReport) {
      yield state.copyWith(status: ReportStatus.initial);
    }
  }
}
