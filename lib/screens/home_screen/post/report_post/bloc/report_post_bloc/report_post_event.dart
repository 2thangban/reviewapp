part of 'report_post_bloc.dart';

abstract class ReportPostEvent {
  const ReportPostEvent();
}

class ReportPost extends ReportPostEvent {
  final int reportId;
  final int postId;
  final String moreContent;
  ReportPost(this.postId, this.reportId, {this.moreContent = ''});
}

class RefreshFormReport extends ReportPostEvent {}
