part of 'report_post_bloc.dart';

enum ReportStatus { initial, loading, success, failure }

class ReportPostState {
  final ReportStatus status;
  const ReportPostState({this.status = ReportStatus.initial});

  ReportPostState copyWith({ReportStatus status}) => ReportPostState(
        status: status ?? this.status,
      );
}
