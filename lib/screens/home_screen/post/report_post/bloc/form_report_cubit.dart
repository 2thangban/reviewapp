import 'package:bloc/bloc.dart';

class FormReportCubit extends Cubit<bool> {
  FormReportCubit() : super(false);

  void checkFormSelected(bool isCheck) => emit(isCheck);
}
