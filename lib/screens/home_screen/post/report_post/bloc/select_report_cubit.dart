import 'package:bloc/bloc.dart';

class SelectReportCubit extends Cubit<int> {
  SelectReportCubit() : super(-1);

  void select(int index) => emit(index);
}
