part of 'get_list_report_title_bloc.dart';

enum ListReportStatus { initial, success, failure }

class GetListReportTitleState extends Equatable {
  final ListReportStatus status;
  final List<ReportPost> list;
  const GetListReportTitleState({
    this.status = ListReportStatus.initial,
    this.list = const <ReportPost>[],
  });

  GetListReportTitleState cloneWith({
    ListReportStatus status,
    List<ReportPost> list,
  }) =>
      GetListReportTitleState(
        status: status ?? this.status,
        list: list ?? this.list,
      );
  @override
  List<Object> get props => [this.list, this.status];
}
