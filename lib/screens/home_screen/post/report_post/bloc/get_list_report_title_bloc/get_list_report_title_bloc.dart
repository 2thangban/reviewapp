import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/report/report.dart';
import '../../../../../../models/report/report_repo.dart';
import '../../../../../../models/token/token_repo.dart';

part 'get_list_report_title_event.dart';
part 'get_list_report_title_state.dart';

class GetListReportTitleBloc
    extends Bloc<GetListReportTitleEvent, GetListReportTitleState> {
  GetListReportTitleBloc() : super(GetListReportTitleState());

  @override
  Stream<GetListReportTitleState> mapEventToState(
    GetListReportTitleEvent event,
  ) async* {
    if (event is GetListReport) {
      try {
        final result = await ReportPostRepo.getList();
        yield state.cloneWith(
          status: ListReportStatus.success,
          list: List.of(state.list)..addAll(result),
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          final List<ReportPost> list = [];
          await TokenRepo.refreshToken().then((value) async {
            await ReportPostRepo.getList().then((value) => list.addAll(value));
          });
          yield state.cloneWith(
            status: ListReportStatus.success,
            list: List.of(state.list)..addAll(list),
          );
        }
        yield state.cloneWith(status: ListReportStatus.success);
      }
    }
  }
}
