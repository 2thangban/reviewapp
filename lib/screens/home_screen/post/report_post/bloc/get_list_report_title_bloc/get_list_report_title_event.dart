part of 'get_list_report_title_bloc.dart';

abstract class GetListReportTitleEvent {
  const GetListReportTitleEvent();
}

class GetListReport extends GetListReportTitleEvent {}
