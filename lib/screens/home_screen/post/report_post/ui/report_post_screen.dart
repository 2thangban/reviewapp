import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/animation/fade_anim.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/helper/validator.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../theme.dart';
import '../bloc/form_report_cubit.dart';
import '../bloc/get_list_report_title_bloc/get_list_report_title_bloc.dart';
import '../bloc/report_post_bloc/report_post_bloc.dart';
import '../bloc/select_report_cubit.dart';

class ReportPostScreen extends StatefulWidget {
  final int postId;
  const ReportPostScreen({Key key, this.postId})
      : assert(postId != null),
        super(key: key);

  @override
  _ReportPostScreenState createState() => _ReportPostScreenState();
}

class _ReportPostScreenState extends State<ReportPostScreen> {
  final SelectReportCubit selectReport = SelectReportCubit();
  final FocusNode focusNode = FocusNode();
  final GetListReportTitleBloc getListBloc = GetListReportTitleBloc();
  final FormReportCubit formCubit = FormReportCubit();
  final TextEditingController reportController = TextEditingController();
  final ReportPostBloc reportPostBloc = ReportPostBloc();
  @override
  void initState() {
    super.initState();
    getListBloc.add(GetListReport());
  }

  @override
  void dispose() {
    super.dispose();
    selectReport.close();
    reportPostBloc.close();
    focusNode.dispose();
    getListBloc.close();
    formCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context),
        ),
        rightBarButtonItems: [
          BlocBuilder<FormReportCubit, bool>(
            bloc: formCubit,
            buildWhen: (previous, current) => previous != current,
            builder: (context, isCheck) {
              return AppButton.common(
                labelText: lang.locaized('reportBtn'),
                labelStyle: theme.textTheme.bodyText1.copyWith(
                  color:
                      isCheck ? theme.backgroundColor : AppColors.subLightColor,
                ),
                radius: 10.0,
                backgroundColor: isCheck
                    ? AppColors.primaryColor
                    : AppColors.backgroundSearchColor,
                shadowColor: theme.backgroundColor,
                onPressed: () {
                  if (!isCheck ||
                      reportPostBloc.state.status == ReportStatus.loading)
                    return;
                  reportPostBloc.add(
                    ReportPost(
                      widget.postId,
                      selectReport.state,
                      moreContent: reportController.text,
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
      body: BlocConsumer<ReportPostBloc, ReportPostState>(
        bloc: reportPostBloc,
        listener: (context, state) {
          if (state.status == ReportStatus.success) {
            Future.delayed(
              Duration(seconds: 1),
              () => Navigator.pop(context),
            );
          } else if (state.status == ReportStatus.failure) {
            Future.delayed(
              Duration(seconds: 1),
              () => reportPostBloc.add(RefreshFormReport()),
            );
          }
        },
        builder: (context, state) {
          return Stack(
            children: [
              SingleChildScrollView(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(lang.locaized('reportTitle'),
                        style: theme.textTheme.headline5),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0, bottom: 20.0),
                      child: BlocBuilder<SelectReportCubit, int>(
                        bloc: selectReport,
                        builder: (_, indexSelected) => BlocBuilder<
                            GetListReportTitleBloc, GetListReportTitleState>(
                          bloc: getListBloc,
                          builder: (_, state) {
                            return FadeAnim(
                              child: state.status == ListReportStatus.success
                                  ? Wrap(
                                      spacing: 5.0,
                                      children: state.list
                                          .map(
                                            (e) => ActionChip(
                                              backgroundColor:
                                                  indexSelected == e.id
                                                      ? AppColors.listBgColor[0]
                                                          .withOpacity(0.2)
                                                      : AppColors
                                                          .backgroundSearchColor
                                                          .withOpacity(0.4),
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                vertical: 10.0,
                                                horizontal: 5.0,
                                              ),
                                              label: Text(e.title),
                                              labelStyle: theme
                                                  .textTheme.bodyText1
                                                  .copyWith(
                                                fontWeight:
                                                    AppFontWeight.regular,
                                                color: indexSelected == e.id
                                                    ? AppColors.primaryColor
                                                    : AppColors.darkThemeColor,
                                              ),
                                              onPressed: () {
                                                focusNode.unfocus();
                                                if (e.id == indexSelected)
                                                  return;
                                                selectReport.select(e.id);
                                                formCubit
                                                    .checkFormSelected(true);
                                              },
                                            ),
                                          )
                                          .toList(),
                                    )
                                  : const SizedBox(),
                            );
                          },
                        ),
                      ),
                    ),
                    Text(
                      lang.locaized('reportText'),
                      style: theme.textTheme.headline5,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: AppTextField.common(
                        focusNode: focusNode,
                        controller: reportController,
                        hintText: lang.locaized('reportField'),
                        backgroundColor:
                            AppColors.backgroundSearchColor.withOpacity(0.5),
                        radius: 10.0,
                        maxLines: 5,
                        onChange: (String textReport) {
                          formCubit.checkFormSelected(
                            !Validator.isEmptyString(textReport),
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
              state.status != ReportStatus.initial
                  ? Container(
                      color: AppColors.darkThemeColor.withOpacity(0.2),
                    )
                  : const SizedBox(),
              state.status != ReportStatus.loading &&
                      state.status != ReportStatus.initial
                  ? Center(
                      child: Container(
                        padding:
                            const EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 30.0),
                        decoration: BoxDecoration(
                          color: theme.backgroundColor,
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        child: state.status != ReportStatus.success
                            ? resultReportWidget(
                                Icon(
                                  AppIcon.successCheck,
                                  color: AppColors.sussess,
                                  size: sizeScreen * 15.0,
                                ),
                                'Thành công!!',
                                AppColors.sussess,
                                theme)
                            : resultReportWidget(
                                Icon(
                                  AppIcon.failedCheck,
                                  color: AppColors.errorColor,
                                  size: sizeScreen * 15.0,
                                ),
                                'Thất bại',
                                AppColors.errorColor,
                                theme,
                              ),
                      ),
                    )
                  : const SizedBox(),
            ],
          );
        },
      ),
    );
  }

  Widget resultReportWidget(
    final Icon icon,
    final String title,
    final Color color,
    final ThemeData theme,
  ) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        icon,
        Text(title, style: theme.textTheme.headline5.copyWith(color: color)),
      ],
    );
  }
}
