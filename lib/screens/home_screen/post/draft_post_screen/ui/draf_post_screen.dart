import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:hive/hive.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_loading.dart';
import '../../../../../models/post/post.dart';
import '../../../../../theme.dart';
import '../bloc/draft_post_bloc/draft_post_bloc.dart';

class DrafPostScreen extends StatefulWidget {
  const DrafPostScreen({Key key}) : super(key: key);

  @override
  _DrafPostScreenState createState() => _DrafPostScreenState();
}

class _DrafPostScreenState extends State<DrafPostScreen> {
  final double sizeScreen = SizedConfig.heightMultiplier;

  final double sizeImage = SizedConfig.imageSizeMultiplier;

  final double sizeText = SizedConfig.textMultiplier;
  final DraftPostBloc draftPostBloc = DraftPostBloc();
  Box<Post> draftPostbox;
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: IconButton(
          icon: Icon(
            AppIcon.popNavigate,
            color: AppColors.darkThemeColor,
            size: sizeScreen * 3.5,
          ),
          onPressed: () => Navigator.pop(context),
        ),
        titleWidget: Text(
          "Riviu nháp",
          style: theme.textTheme.headline3,
        ),
        centerTitle: true,
      ),
      backgroundColor: theme.backgroundColor,
      body: Container(
        color: AppColors.backgroundSearchColor.withOpacity(0.8),
        child: BlocBuilder<DraftPostBloc, DraftPostState>(
          bloc: draftPostBloc,
          builder: (_, state) {
            if (state is DraftPostInitial) {
              Future.delayed(
                Duration(milliseconds: 500),
                () => draftPostBloc.add(OpenDraftPostEvent()),
              );
              return Center(
                child: AppLoading.threeBounce(size: 20.0),
              );
            } else if (state is OpenDraftPostSuccess) {
              draftPostbox = state.box;
            }
            return draftPostbox.isNotEmpty
                ? Container(
                    child: ListView.builder(
                      itemCount: draftPostbox.length,
                      itemBuilder: (context, index) {
                        /// get post at position index
                        final Post post = draftPostbox.getAt(index);
                        return InkWell(
                          /// back create post screen with post selected
                          onTap: () => Navigator.pop(context, [index, post]),
                          child: Card(
                            margin: const EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                post.listImage != null
                                    ? Row(
                                        children: post.imageUpload
                                            .map(
                                              (e) => Container(
                                                padding:
                                                    const EdgeInsets.all(10.0),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10.0),
                                                  child: Image.file(
                                                    e,
                                                    fit: BoxFit.fill,
                                                    height: sizeScreen * 10,
                                                    width: sizeScreen * 10,
                                                  ),
                                                ),
                                              ),
                                            )
                                            .toList(),
                                      )
                                    : const SizedBox(),
                                post.location != null
                                    ? ListTile(
                                        horizontalTitleGap: 10.0,
                                        contentPadding:
                                            EdgeInsets.symmetric(vertical: 2.0),
                                        leading: Container(
                                          margin:
                                              const EdgeInsets.only(left: 10.0),
                                          clipBehavior: Clip.antiAlias,
                                          decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(8.0),
                                          ),
                                          child: Image.network(
                                            '${AppAssets.baseUrl + post.location.image.first}',
                                            fit: BoxFit.fill,
                                            width: sizeScreen * 8,
                                          ),
                                        ),
                                        title: Text(
                                          post.location.name,
                                          style: theme.textTheme.headline5,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        subtitle: Text(
                                          post.location.address,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      )
                                    : const SizedBox(),
                                Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: Align(
                                    alignment: Alignment.centerLeft,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        RichText(
                                          overflow: TextOverflow.ellipsis,
                                          maxLines: 1,
                                          softWrap: true,
                                          text: TextSpan(
                                              text: "Tiêu đề: ",
                                              style: theme.textTheme.headline6,
                                              children: [
                                                TextSpan(
                                                  text: post.titlePost !=
                                                              null &&
                                                          post.titlePost != ''
                                                      ? post.titlePost
                                                      : 'Bài Rivew này chưa có tiêu đề',
                                                  style: theme
                                                      .textTheme.bodyText1
                                                      .copyWith(
                                                    fontWeight:
                                                        AppFontWeight.regular,
                                                  ),
                                                ),
                                              ]),
                                        ),
                                        const SizedBox(height: 5.0),
                                        Container(
                                          child: Text(
                                            post.content,
                                            style: theme.textTheme.bodyText1
                                                .copyWith(
                                              fontWeight: AppFontWeight.regular,
                                            ),
                                            maxLines: 5,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                const Divider(
                                  thickness: 0.3,
                                  color: AppColors.darkThemeColor,
                                ),
                                Padding(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0,
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                          "Ngày viết bài:   ${post.dafaultDate}"),
                                      IconButton(
                                        icon: FaIcon(
                                          AppIcon.remove,
                                          size: sizeScreen * 2.7,
                                          color: AppColors.errorColor,
                                        ),
                                        onPressed: () {
                                          draftPostbox.deleteAt(index);
                                          draftPostBloc.add(
                                            RemoveDraftPostEvent(index),
                                          );
                                        },
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  )
                : Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Icon(
                            AppIcon.box,
                            size: sizeScreen * 15,
                            color: AppColors.darkThemeColor.withOpacity(0.3),
                          ),
                        ),
                        Text(
                          "Không có bài viết nào được lưu",
                          style: theme.textTheme.headline4.copyWith(
                            color: AppColors.darkThemeColor.withOpacity(0.4),
                          ),
                        ),
                      ],
                    ),
                  );
          },
        ),
      ),
    );
  }
}
