import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../../../../../models/post/post.dart';

part 'draft_post_event.dart';
part 'draft_post_state.dart';

class DraftPostBloc extends Bloc<DraftPostEvent, DraftPostState> {
  Box<Post> box;
  DraftPostBloc() : super(DraftPostInitial());

  @override
  Stream<DraftPostState> mapEventToState(
    DraftPostEvent event,
  ) async* {
    if (event is OpenDraftPostEvent) {
      try {
        box = await Hive.openBox<Post>('draftPost');
        yield OpenDraftPostSuccess(box);
      } catch (e) {
        yield OpenDraftPostFailed();
      }
    } else if (event is AddNewDraftPostEvent) {
      if (event.key == null) {
        box.add(event.post);
      } else {
        box.put(event.key, event.post);
      }
      yield AddNewPostToBoxSuccess();
    } else if (event is RemoveDraftPostEvent) {
      box.delete(event.index);
      yield RemoveDraftPostFromBox();
    }
  }
}
