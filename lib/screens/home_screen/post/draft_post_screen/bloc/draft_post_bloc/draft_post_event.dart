part of 'draft_post_bloc.dart';

abstract class DraftPostEvent {}

class OpenDraftPostEvent extends DraftPostEvent {}

class AddNewDraftPostEvent extends DraftPostEvent {
  final Post post;
  final int key;
  AddNewDraftPostEvent(this.key, this.post);
}

class RemoveDraftPostEvent extends DraftPostEvent {
  final int index;
  RemoveDraftPostEvent(this.index);
}
