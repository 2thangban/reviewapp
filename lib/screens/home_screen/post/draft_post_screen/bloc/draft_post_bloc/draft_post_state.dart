part of 'draft_post_bloc.dart';

abstract class DraftPostState {}

class DraftPostInitial extends DraftPostState {}

class OpenDraftPostSuccess extends DraftPostState {
  final Box<Post> box;
  OpenDraftPostSuccess(this.box);
}

class OpenDraftPostFailed extends DraftPostState {}

class AddNewPostToBoxSuccess extends DraftPostState {}

class RemoveDraftPostFromBox extends DraftPostState {}
