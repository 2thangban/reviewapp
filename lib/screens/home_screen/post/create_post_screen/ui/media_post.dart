import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:video_player/video_player.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/untils/app_picker.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../localizations/app_localization.dart';
import '../bloc/cubit/post_edit_cubit.dart';
import '../bloc/file_form_create_bloc/file_form_create_bloc.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';

class AddImageVideoPost extends StatefulWidget {
  const AddImageVideoPost({Key key}) : super(key: key);
  @override
  _AddImageVideoPostState createState() => _AddImageVideoPostState();
}

class _AddImageVideoPostState extends State<AddImageVideoPost> {
  FileFormCreateBloc fileFormCreateBloc;
  FormCreateBloc formCreateBloc;
  PostEditCubit postEditCubit;
  final sizeScreen = SizedConfig.heightMultiplier;

  VideoPlayerController _videoPlayerController;

  @override
  void initState() {
    super.initState();
    formCreateBloc = BlocProvider.of<FormCreateBloc>(context);
    postEditCubit = BlocProvider.of<PostEditCubit>(context);
    fileFormCreateBloc =
        FileFormCreateBloc(listUrlImage: formCreateBloc.post.listImage);
  }

  @override
  void dispose() {
    super.dispose();
    fileFormCreateBloc.close();
    _videoPlayerController?.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      padding: EdgeInsets.symmetric(vertical: 10.0),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: BlocBuilder<FileFormCreateBloc, FileFormCreateState>(
          bloc: fileFormCreateBloc,
          builder: (_, __) {
            if (fileFormCreateBloc.video?.path != null &&
                fileFormCreateBloc.video.path != '') {
              _videoPlayerController =
                  VideoPlayerController.file(fileFormCreateBloc.video)
                    ..setLooping(false)
                    ..initialize().then((_) => _videoPlayerController.pause());
            }
            return Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  children: [
                    _itemCreate(
                      title: AppLocalization.of(context).locaized('addImage'),
                      icon: AppAssets.addImageIcon,
                      theme: theme,
                      onTap: () async {
                        await AppPicker.getImageFromGallery().then((lstImage) {
                          if (lstImage.isNotEmpty) {
                            if (postEditCubit.isEdit) {
                              postEditCubit.editPost.imageUpload
                                  ?.addAll(lstImage);
                              formCreateBloc.add(ValidationFormEditPostEvent(
                                  postEditCubit.editPost));
                              fileFormCreateBloc.add(AddImageEvent(lstImage));
                            } else {
                              formCreateBloc.post.imageUpload?.addAll(lstImage);
                              formCreateBloc
                                  .add(ValidationFormCreatePostEvent());
                              fileFormCreateBloc.add(AddImageEvent(lstImage));
                            }
                          }
                        });
                      },
                    ),
                    fileFormCreateBloc.video != null
                        ? Stack(
                            alignment: Alignment.center,
                            children: [
                              Container(
                                width: sizeScreen * 14,
                                height: sizeScreen * 14,
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(20.0)),
                                child: Stack(
                                  children: [
                                    VideoPlayer(_videoPlayerController),
                                    Positioned(
                                      top: 10,
                                      right: 5,
                                      child: AppButton.icon(
                                          icon: AppIcon.delete,
                                          iconSize: sizeScreen * 3,
                                          iconColor: AppColors.lightThemeColor,
                                          onTap: () {
                                            if (postEditCubit.isEdit) {
                                              postEditCubit
                                                  .editPost.videoUpload = null;
                                              formCreateBloc.add(
                                                ValidationFormEditPostEvent(
                                                    postEditCubit.editPost),
                                              );
                                            }
                                            fileFormCreateBloc.add(
                                              RemoveVideoEvent(),
                                            );
                                          }),
                                    ),
                                  ],
                                ),
                              ),
                              Icon(
                                AppIcon.playVideo,
                                color: AppColors.subLightColor.withOpacity(0.7),
                                size: sizeScreen * 6,
                              ),
                            ],
                          )
                        : _itemCreate(
                            title: AppLocalization.of(context)
                                .locaized('addVideo'),
                            icon: AppAssets.addVideoIcon,
                            theme: theme,
                            onTap: () async {
                              final video = await AppPicker.pickVideoFile();
                              if (video != null) {
                                formCreateBloc.post.videoUpload = video;
                                fileFormCreateBloc.add(AddVideoEvent(video));
                              }
                            },
                          ),
                  ],
                ),
                Row(
                  children: [
                    /// In case the user add file image form local app
                    ...fileFormCreateBloc.listImage.reversed
                        .map((e) => itemImageCreate(file: e))
                        .toList(),

                    /// In case the user edit post then show the image list
                    /// from api
                    ...fileFormCreateBloc.listUrlImage.reversed
                        .map((e) => itemImageCreate(urlImage: e))
                        .toList(),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Widget _itemCreate({
    @required String title,
    @required Function onTap,
    String icon,
    ThemeData theme,
  }) =>
      Container(
        width: sizeScreen * 14,
        margin: EdgeInsets.symmetric(horizontal: 5.0),
        decoration: BoxDecoration(
          color: AppColors.subLightColor.withOpacity(0.15),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: InkWell(
          borderRadius: BorderRadius.circular(20.0),
          splashColor: theme.primaryColor.withOpacity(0.2),
          highlightColor: theme.primaryColor.withOpacity(0.3),
          onTap: onTap,
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: sizeScreen * 3),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: EdgeInsets.all(sizeScreen * 0.4),
                  child: SvgPicture.asset(icon, height: sizeScreen * 5),
                ),
                Text(title, style: theme.textTheme.bodyText2),
              ],
            ),
          ),
        ),
      );

  Widget itemImageCreate({String urlImage, File file}) => Container(
        width: sizeScreen * 14,
        height: sizeScreen * 14,
        margin: const EdgeInsets.symmetric(horizontal: 5.0),
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: urlImage != null
                ? NetworkImage(AppAssets.baseUrl + urlImage)
                : FileImage(file),
          ),
          borderRadius: BorderRadius.circular(20.0),
        ),
        child: Stack(
          children: [
            Positioned(
              top: 10,
              right: 5,
              child: AppButton.icon(
                  icon: AppIcon.delete,
                  iconSize: sizeScreen * 3,
                  iconColor: AppColors.lightThemeColor,
                  onTap: () {
                    /// In case the user edit post
                    if (urlImage != null) {
                      postEditCubit.editPost.listImage.remove(urlImage);
                      formCreateBloc.add(
                        ValidationFormEditPostEvent(postEditCubit.editPost),
                      );
                      fileFormCreateBloc.add(
                        RemoveImageEvent(urlImage: urlImage),
                      );
                    }

                    /// In case the user create post
                    else {
                      formCreateBloc.post.imageUpload.remove(file);
                      formCreateBloc.add(ValidationFormCreatePostEvent());
                      fileFormCreateBloc.add(
                        RemoveImageEvent(image: file),
                      );
                    }
                  }),
            ),
          ],
        ),
      );
}
