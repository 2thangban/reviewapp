import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/location/location.dart';
import '../../../../../routes/route_name.dart';
import '../bloc/cubit/post_edit_cubit.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';
import '../bloc/location_form_create_cubit/location_form_create_cubit.dart';
import '../bloc/rating_cubit/rating_cubit.dart';

// ignore: must_be_immutable
class AddLocationRating extends StatefulWidget {
  const AddLocationRating({Key key}) : super(key: key);
  @override
  _AddLocationRatingState createState() => _AddLocationRatingState();
}

class _AddLocationRatingState extends State<AddLocationRating> {
  final double sizeScreen = SizedConfig.heightMultiplier;
  final double sizeText = SizedConfig.textMultiplier;
  RatingCubit ratingCubit;
  LocationFormCreateCubit locationFormCreateCubit;
  PostEditCubit postEditCubit;
  FormCreateBloc formCreateBloc;

  @override
  void initState() {
    super.initState();
    formCreateBloc = BlocProvider.of<FormCreateBloc>(context);
    locationFormCreateCubit = BlocProvider.of<LocationFormCreateCubit>(context);
    postEditCubit = BlocProvider.of<PostEditCubit>(context);
    ratingCubit = BlocProvider.of<RatingCubit>(context);
  }

  @override
  void dispose() {
    super.dispose();
    locationFormCreateCubit.close();
    ratingCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final List<String> listRatingTitle = [
      "",
      AppLocalization.of(context).locaized('badRating'),
      AppLocalization.of(context).locaized('improveRating'),
      AppLocalization.of(context).locaized('normalRating'),
      AppLocalization.of(context).locaized('goodRating'),
      AppLocalization.of(context).locaized('excellentRating'),
    ];

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 30.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          postEditCubit.isEdit == false
              ? Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Text(
                        AppLocalization.of(context).locaized('titleLocation'),
                        style: theme.textTheme.headline6
                            .copyWith(fontSize: sizeText * 2.8),
                      ),
                    ),
                    BlocBuilder<LocationFormCreateCubit, Location>(
                      bloc: locationFormCreateCubit,
                      builder: (_, location) => location != null
                          ? InkWell(
                              onTap: () async {
                                final location = await Navigator.of(context)
                                    .pushNamed(RouteName.searchLocation);
                                if (location != null) {
                                  formCreateBloc.post.location = location;
                                  locationFormCreateCubit.add(location);
                                }
                              },
                              highlightColor:
                                  AppColors.primaryColor.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 10.0, vertical: 10.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AppColors.primaryColor,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 10.0),
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        child: location.image != null
                                            ? Image.network(
                                                '${AppAssets.baseUrl}${location.image.first}',
                                                height: sizeScreen * 10,
                                                width: sizeScreen * 10,
                                                fit: BoxFit.cover,
                                              )
                                            : Image.asset(
                                                AppAssets.img1,
                                                height: sizeScreen * 10,
                                                width: sizeScreen * 10,
                                                fit: BoxFit.cover,
                                              ),
                                      ),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          width: sizeScreen * 30,
                                          child: Text(
                                            location.name,
                                            style: theme.textTheme.headline5,
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        Container(
                                          width: sizeScreen * 32,
                                          padding: const EdgeInsets.symmetric(
                                            vertical: 5.0,
                                          ),
                                          child: Text(
                                            location.address,
                                            style: theme.textTheme.bodyText2
                                                .copyWith(
                                                    fontSize: sizeText * 2.1),
                                            overflow: TextOverflow.ellipsis,
                                          ),
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            SvgPicture.asset(
                                              AppAssets.ratingIcon,
                                              height: sizeScreen * 2.5,
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 10.0),
                                              child: Text(
                                                location.rating.toString(),
                                                style:
                                                    theme.textTheme.headline5,
                                              ),
                                            ),
                                            Text(
                                              '(${location.postByLocation} Riviu)',
                                              style: theme.textTheme.bodyText2
                                                  .copyWith(
                                                      fontSize: sizeText * 2.2),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : InkWell(
                              onTap: () async {
                                final location = await Navigator.of(context)
                                    .pushNamed(RouteName.searchLocation);
                                if (location != null) {
                                  formCreateBloc.post.location = location;
                                  locationFormCreateCubit.add(location);
                                }
                              },
                              highlightColor:
                                  AppColors.primaryColor.withOpacity(0.2),
                              borderRadius: BorderRadius.circular(10.0),
                              child: Container(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0, vertical: 20.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: AppColors.subLightColor,
                                  ),
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20.0, right: 10.0),
                                      child: SvgPicture.asset(
                                        AppAssets.pinIcon,
                                        height: sizeScreen * 3.2,
                                      ),
                                    ),
                                    Text(
                                      AppLocalization.of(context)
                                          .locaized('addLocation'),
                                      style: theme.textTheme.headline5.copyWith(
                                        color: AppColors.subLightColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                    )
                  ],
                )
              : const SizedBox(),

          ///
          Padding(
            padding: const EdgeInsets.only(top: 20.0, bottom: 10.0),
            child: Text(
              AppLocalization.of(context).locaized('titleRating'),
              style:
                  theme.textTheme.headline6.copyWith(fontSize: sizeText * 2.8),
            ),
          ),
          BlocBuilder<RatingCubit, int>(
            bloc: ratingCubit,
            builder: (context, value) {
              return Row(
                children: [
                  SizedBox(
                    width: sizeScreen * 13,
                    child: Text(
                      AppLocalization.of(context).locaized('totalRating'),
                      style: theme.textTheme.headline5,
                    ),
                  ),
                  Expanded(
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: RatingBar.builder(
                        initialRating: value.toDouble(),
                        itemSize: 30.0,
                        unratedColor: AppColors.subLightColor.withOpacity(0.5),
                        tapOnlyMode: true,
                        glow: false,
                        minRating: 0,
                        itemBuilder: (context, index) => Icon(
                          Icons.star_rounded,
                          color: theme.primaryColor,
                        ),
                        onRatingUpdate: (value) {
                          if (postEditCubit.isEdit) {
                            postEditCubit.editPost.rating = value.toInt();
                            formCreateBloc.add(
                              ValidationFormEditPostEvent(
                                  postEditCubit.editPost),
                            );
                          } else {
                            if (formCreateBloc.post.rating == 0 && value > 0) {
                              formCreateBloc.post.rating = value.toInt();
                              formCreateBloc
                                  .add(ValidationFormCreatePostEvent());
                            } else if (formCreateBloc.post.rating != 0 &&
                                value == 0) {
                              formCreateBloc.post.rating = value.toInt();
                              formCreateBloc
                                  .add(ValidationFormCreatePostEvent());
                            }
                          }
                          ratingCubit.rate(value.toInt());
                        },
                      ),
                    ),
                  ),
                  Text(
                    listRatingTitle[value.toInt()],
                    style: theme.textTheme.bodyText2.copyWith(
                      fontSize: sizeText * 2.1,
                    ),
                  ),
                ],
              );
            },
          ),
          const SizedBox(height: 30.0),
        ],
      ),
    );
  }
}
