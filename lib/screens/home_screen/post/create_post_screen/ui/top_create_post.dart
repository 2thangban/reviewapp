import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/helper/validator.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/post/post.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../draft_post_screen/bloc/draft_post_bloc/draft_post_bloc.dart';
import '../bloc/create_post_bloc/create_post_bloc.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';

class TopCreatePost extends StatefulWidget {
  const TopCreatePost({Key key}) : super(key: key);
  @override
  _TopCreatePostState createState() => _TopCreatePostState();
}

class _TopCreatePostState extends State<TopCreatePost> {
  FormCreateBloc formCreateBloc;
  CreatePostBloc createPostBloc;
  DraftPostBloc draftPostBloc;
  DraftBox draftBox;
  @override
  void initState() {
    super.initState();
    formCreateBloc = BlocProvider.of<FormCreateBloc>(context);
    draftPostBloc = BlocProvider.of<DraftPostBloc>(context);
    createPostBloc = BlocProvider.of<CreatePostBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    final navigate = Navigator.of(context);
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Row(
        children: [
          AppButton.icon(
            icon: AppIcon.popNavigate,
            onTap: () async {
              /// if content field and location filed is not
              /// null then show bottom sheet save draft post
              if (!Validator.isEmptyString(formCreateBloc.post.titlePost) &&
                  !Validator.isEmptyString(formCreateBloc.post.content) &&
                  formCreateBloc.post.location != null) {
                await showCupertinoModalPopup(
                  context: context,
                  builder: (context) => CupertinoActionSheet(
                    title: Text(
                      'Lưu bài viết này làm bản nháp?',
                      style: theme.textTheme.headline6.copyWith(
                        fontWeight: AppFontWeight.regular,
                        color: AppColors.subLightColor,
                      ),
                    ),
                    actions: [
                      CupertinoActionSheetAction(
                        child: Text(
                          'Lưu bản nháp',
                          style: theme.textTheme.headline4.copyWith(
                            color: Colors.blue,
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                        onPressed: () => navigate.pop(1),
                      ),
                      CupertinoActionSheetAction(
                        isDestructiveAction: true,
                        child: Text(
                          'Không lưu',
                        ),
                        onPressed: () => navigate.pop(0),
                      ),
                    ],
                    cancelButton: CupertinoActionSheetAction(
                      isDefaultAction: true,
                      child: Text(
                        'Quay lại',
                      ),
                      onPressed: () => navigate.pop(),
                    ),
                  ),
                ).then(
                  (value) {
                    /// result:
                    /// - null : cancel bottom sheet by user.
                    /// - 0 : user select save post.
                    /// - 1 : user select unsave post.

                    if (value != null) {
                      switch (value) {
                        case 0:
                          return navigate.pop();
                        case 1:
                          draftPostBloc.add(
                            AddNewDraftPostEvent(
                              draftBox != null ? draftBox.key : null,
                              formCreateBloc.post,
                            ),
                          );
                          return navigate.pop();
                        default:
                      }
                    }
                  },
                );
              } else {
                // await Hive.box('post')?.close();
                navigate.pop();
              }
            },
          ),
          const Spacer(),
          formCreateBloc.post.id == null
              ? BlocBuilder<DraftPostBloc, DraftPostState>(
                  bloc: draftPostBloc,
                  builder: (_, state) {
                    if (state is DraftPostInitial) {
                      draftPostBloc.add(OpenDraftPostEvent());
                    }
                    return Align(
                      alignment: Alignment.centerRight,
                      child: GestureDetector(
                        onTap: () async {
                          /// receive post from draft post screen
                          final result = await navigate.pushNamed(
                            RouteName.drafPost,
                          );
                          if (result != null) {
                            List<dynamic> list = (result as List<dynamic>);
                            draftBox = new DraftBox(list[0], list[1]);
                            formCreateBloc.add(
                              UpdatePostFromDraftEvent(draftBox.value),
                            );
                          }
                        },
                        child: Stack(
                          alignment: Alignment.topRight,
                          children: [
                            Container(
                              margin: const EdgeInsets.all(5.0),
                              padding: const EdgeInsets.all(5.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                color: AppColors.subLightColor.withOpacity(0.2),
                              ),
                              child: const FaIcon(
                                AppIcon.box,
                                color: AppColors.darkThemeColor,
                              ),
                            ),
                            state is OpenDraftPostSuccess
                                ? CircleAvatar(
                                    radius: 8.0,
                                    backgroundColor: AppColors.errorColor,
                                    child: Text(
                                      state.box.length.toString(),
                                      style: theme.textTheme.bodyText1.copyWith(
                                        color: theme.backgroundColor,
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  )
                                : const SizedBox(),
                          ],
                        ),
                      ),
                    );
                  },
                )
              : const SizedBox(),
          const SizedBox(width: 10.0),
          BlocBuilder<FormCreateBloc, FormCreateState>(
            bloc: formCreateBloc,
            builder: (_, state) => Padding(
              padding: const EdgeInsets.only(bottom: 10.0),
              child: AppButton.common(
                radius: 10.0,
                width: sizeScreen * 12,
                contentPadding: 10.0,
                labelStyle: theme.textTheme.bodyText1.copyWith(
                  color: (state is ValidFormSuccess)
                      ? theme.backgroundColor
                      : AppColors.subLightColor,
                ),
                shadowColor: theme.backgroundColor,
                backgroundColor: (state is ValidFormSuccess)
                    ? AppColors.primaryColor
                    : AppColors.backgroundSearchColor,
                labelText: formCreateBloc.post.id != null
                    ? 'Chỉnh sửa'
                    : AppLocalization.of(context).locaized('btnPost'),
                onPressed: () => state is ValidFormSuccess
                    ? (formCreateBloc.post.id != null)
                        ? createPostBloc.add(EditPost(state.post))
                        : createPostBloc.add(CreatePost(formCreateBloc.post))
                    : null,
              ),
            ),
          )
        ],
      ),
    );
  }
}

class DraftBox {
  final int key;
  final Post value;
  DraftBox(this.key, this.value);
}
