import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../theme.dart';
import '../bloc/categoties_cubit/categories_cubit.dart';
import '../bloc/cubit/post_edit_cubit.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';

class CategoriesPost extends StatefulWidget {
  const CategoriesPost({Key key}) : super(key: key);

  @override
  _CategoriesPostState createState() => _CategoriesPostState();
}

class _CategoriesPostState extends State<CategoriesPost> {
  final listCategories = ['Đồ ăn', 'Đồ uống', 'Du lịch', 'Làm đẹp'];
  final sizeScreen = SizedConfig.heightMultiplier;
  CategoriesCubit categoriesCubit;
  FormCreateBloc formCreateBloc;
  PostEditCubit postEditCubit;

  @override
  void initState() {
    super.initState();
    formCreateBloc = BlocProvider.of<FormCreateBloc>(context);
    categoriesCubit = BlocProvider.of<CategoriesCubit>(context);
    postEditCubit = BlocProvider.of<PostEditCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      height: sizeScreen * 5.5,
      child: BlocBuilder<CategoriesCubit, int>(
        bloc: categoriesCubit,
        builder: (_, indexCategory) => ListView(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          scrollDirection: Axis.horizontal,
          children: List.generate(
            listCategories.length,
            (index) => GestureDetector(
              onTap: () {
                if (postEditCubit.isEdit) {
                  postEditCubit.editPost.category = index + 1;
                  formCreateBloc.add(
                    ValidationFormEditPostEvent(postEditCubit.editPost),
                  );
                } else {
                  formCreateBloc.post.category = index + 1;
                }
                categoriesCubit.select(index);
              },
              child: Container(
                margin:
                    const EdgeInsets.symmetric(vertical: 5.0, horizontal: 3.0),
                padding: const EdgeInsets.symmetric(horizontal: 15.0),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: indexCategory == index
                      ? theme.primaryColor.withOpacity(0.8)
                      : AppColors.backgroundSearchColor,
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Text(
                  listCategories[index],
                  style: theme.textTheme.headline6.copyWith(
                    color: indexCategory == index
                        ? theme.backgroundColor
                        : AppColors.subLightColor,
                    fontWeight: AppFontWeight.medium,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
