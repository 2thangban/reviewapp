import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/helper/validator.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_toast.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../bloc/content_form_create_cubit/content_cubit.dart';
import '../bloc/cubit/post_edit_cubit.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';
import '../bloc/hashtag_form_create_bloc/hashtag_form_create_bloc.dart';
import '../bloc/title_form_create_cubit/title_form_create_cubit.dart';

class AddContentPost extends StatefulWidget {
  const AddContentPost({Key key}) : super(key: key);

  @override
  _AddContentPostState createState() => _AddContentPostState();
}

class _AddContentPostState extends State<AddContentPost> {
  final _hashTagController = TextEditingController();
  TextEditingController _titlePostController = new TextEditingController();
  TextEditingController _contentPostController = new TextEditingController();
  HashtagFormCreateBloc hashtagFormCreateBloc;
  FormCreateBloc formCreateBloc;
  TitleFormCreateCubit titleFormCreateCubit;
  ContentFormCreateCubit contentFormCreateCubit;
  PostEditCubit postCubit;
  @override
  void initState() {
    super.initState();
    formCreateBloc = BlocProvider.of<FormCreateBloc>(context);
    postCubit = BlocProvider.of<PostEditCubit>(context);
    hashtagFormCreateBloc = BlocProvider.of<HashtagFormCreateBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _titlePostController.dispose();
    _contentPostController.dispose();
    _hashTagController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return Column(
      children: [
        BlocBuilder<TitleFormCreateCubit, String>(
          bloc: titleFormCreateCubit,
          builder: (context, title) {
            _titlePostController.text = title;
            return AppTextField.create(
              controller: _titlePostController,
              hindText: AppLocalization.of(context).locaized('addTitlePost'),
              hintStyle: Theme.of(context).textTheme.headline6.copyWith(
                    color: AppColors.subLightColor,
                  ),
              isTop: true,
              onChange: (String input) {
                if (postCubit.isEdit) {
                  postCubit.editPost.titlePost = input;
                  formCreateBloc.add(
                    ValidationFormEditPostEvent(postCubit.editPost),
                  );
                } else {
                  if (Validator.isEmptyString(formCreateBloc.post.titlePost) &&
                      !Validator.isEmptyString(input.trim())) {
                    formCreateBloc.add(ValidationFormCreatePostEvent());
                  } else if (!Validator.isEmptyString(
                          formCreateBloc.post.titlePost) &&
                      Validator.isEmptyString(input.trim())) {
                    formCreateBloc.add(ValidationFormCreatePostEvent());
                  }
                  formCreateBloc.post.titlePost = input.trim();
                }
              },
            );
          },
        ),
        BlocBuilder<ContentFormCreateCubit, String>(
          bloc: contentFormCreateCubit,
          builder: (context, content) {
            _contentPostController.text = content;
            return AppTextField.create(
              controller: _contentPostController,
              hindText: AppLocalization.of(context).locaized('addContentPost'),
              hintStyle: Theme.of(context).textTheme.bodyText2,
              isContent: true,
              onChange: (String input) {
                if (postCubit.isEdit) {
                  postCubit.editPost.content = input;
                  formCreateBloc.add(
                    ValidationFormEditPostEvent(postCubit.editPost),
                  );
                } else {
                  if (Validator.isEmptyString(formCreateBloc.post.content) &&
                      !Validator.isEmptyString(input.trim())) {
                    formCreateBloc.add(ValidationFormCreatePostEvent());
                  } else if (!Validator.isEmptyString(
                          formCreateBloc.post.content) &&
                      Validator.isEmptyString(input.trim())) {
                    formCreateBloc.add(ValidationFormCreatePostEvent());
                  }
                  formCreateBloc.post.content = input.trim();
                }
              },
            );
          },
        ),
        Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.subLightColor.withOpacity(0.7),
              width: 0.5,
            ),
            borderRadius: BorderRadius.only(
              bottomLeft: const Radius.circular(20.0),
              bottomRight: const Radius.circular(20.0),
            ),
          ),
          child: Container(
            color: AppColors.subLightColor.withOpacity(0.05),
            child: Wrap(
              children: [
                BlocConsumer<HashtagFormCreateBloc, HashtagFormCreateState>(
                  bloc: hashtagFormCreateBloc,
                  listener: (context, state) {
                    if (state is AddHashTagFailed) {
                      AppToast.show(state.mgs, context);
                    }
                  },
                  builder: (context, state) {
                    return Wrap(
                      children: [
                        ...List.generate(
                          hashtagFormCreateBloc.listHashTag?.length,
                          (index) => Container(
                            padding: const EdgeInsets.all(10.0),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                GestureDetector(
                                  onTap: () => _hashTagController.text =
                                      hashtagFormCreateBloc.listHashTag[index],
                                  child: Text(
                                    hashtagFormCreateBloc.listHashTag[index],
                                    style: theme.textTheme.bodyText1.copyWith(
                                      color: AppColors.primaryColor,
                                    ),
                                  ),
                                ),
                                AppButton.icon(
                                  padding: const EdgeInsets.only(left: 3.0),
                                  icon: AppIcon.exitNavigate,
                                  iconSize: 15.0,
                                  iconColor: AppColors.subLightColor,
                                  onTap: () {
                                    if (postCubit.isEdit) {
                                      postCubit.editPost.hashTag
                                          .removeAt(index);
                                      formCreateBloc.add(
                                        ValidationFormEditPostEvent(
                                            postCubit.editPost),
                                      );
                                    } else {
                                      formCreateBloc.post.hashTag
                                          .removeAt(index);
                                    }
                                    hashtagFormCreateBloc.add(
                                      RemoveHashTagEvent(index),
                                    );
                                  },
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                ),
                Container(
                  constraints: BoxConstraints(
                    minWidth: sizeScreen * 10,
                  ),
                  child: TextFormField(
                    controller: _hashTagController,
                    minLines: 1,
                    maxLines: 5,
                    cursorColor: AppColors.primaryColor,
                    cursorHeight: 25.0,
                    decoration: InputDecoration(
                      hintText:
                          AppLocalization.of(context).locaized('addHashtag'),
                      hintStyle: Theme.of(context).textTheme.headline6.copyWith(
                            color: AppColors.subLightColor,
                          ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide.none,
                      ),
                    ),
                    onChanged: (String input) {
                      if (input.endsWith(' ')) {
                        hashtagFormCreateBloc
                            .add(AddHashTagEvent(input.trim()));
                        if (postCubit.isEdit) {
                          if (!postCubit.editPost.hashTag
                              .contains(input.trim())) {
                            postCubit.editPost.hashTag.add(input.trim());
                            formCreateBloc.add(
                              ValidationFormEditPostEvent(postCubit.editPost),
                            );
                          }
                        } else {
                          if (!formCreateBloc.post.hashTag
                              .contains(input.trim())) {
                            formCreateBloc.post.hashTag.add(input.trim());
                          }
                        }
                        _hashTagController.clear();
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
