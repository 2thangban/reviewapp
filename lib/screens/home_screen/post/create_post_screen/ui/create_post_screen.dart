import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_loading.dart';
import '../../../../../models/post/post.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../draft_post_screen/bloc/draft_post_bloc/draft_post_bloc.dart';
import '../bloc/categoties_cubit/categories_cubit.dart';
import '../bloc/content_form_create_cubit/content_cubit.dart';
import '../bloc/create_post_bloc/create_post_bloc.dart';
import '../bloc/cubit/post_edit_cubit.dart';
import '../bloc/form_create_bloc/form_create_bloc.dart';
import '../bloc/hashtag_form_create_bloc/hashtag_form_create_bloc.dart';
import '../bloc/location_form_create_cubit/location_form_create_cubit.dart';
import '../bloc/rating_cubit/rating_cubit.dart';
import '../bloc/title_form_create_cubit/title_form_create_cubit.dart';
import 'add_content_post.dart';
import 'add_location_rating.dart';
import 'categories_post.dart';
import 'media_post.dart';
import 'top_create_post.dart';

class CreaterPostScreen extends StatefulWidget {
  final Post currentPost;
  CreaterPostScreen(this.currentPost, {Key key}) : super(key: key);
  @override
  _CreaterPostScreenState createState() => _CreaterPostScreenState();
}

class _CreaterPostScreenState extends State<CreaterPostScreen> {
  Post post;

  bool isLoading = false;
  final double sizeScreen = SizedConfig.heightMultiplier;
  final double sizeText = SizedConfig.textMultiplier;

  final CreatePostBloc createPostBloc = CreatePostBloc();
  final DraftPostBloc draftPostBloc = DraftPostBloc();
  FormCreateBloc formCreateBloc;
  PostEditCubit postEditCubit;
  CategoriesCubit categoriesCubit;
  TitleFormCreateCubit titleFormCreateCubit;
  ContentFormCreateCubit contentFormCreateCubit;
  HashtagFormCreateBloc hashtagFormCreateBloc;
  LocationFormCreateCubit locationFormCreateCubit;
  RatingCubit ratingCubit;
  AuthBloc authBloc;

  Widget loadingWidget = const SizedBox();

  @override
  void initState() {
    super.initState();

    post = widget.currentPost;
    final Map postMap = <String, dynamic>{
      'post_id': post.id,
      'post_title': post.titlePost,
      'post_content': post.content,
      'post_date': post.dateCreatePost,
      'post_image': post.listImage,
      'category_id': post.category,
      'hashtag': post.toHashtagJsonList(post.listHashTag),
      'post_rating': post.rating,
    };
    postEditCubit = PostEditCubit(new Post.fromJson(postMap));
    formCreateBloc = FormCreateBloc(post);
    categoriesCubit = CategoriesCubit(post.category - 1);
    titleFormCreateCubit = TitleFormCreateCubit(post.titlePost);
    contentFormCreateCubit = ContentFormCreateCubit(post.content);
    hashtagFormCreateBloc = HashtagFormCreateBloc(post.hashTag);
    locationFormCreateCubit = LocationFormCreateCubit(post.location);
    ratingCubit = RatingCubit(post.rating);

    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    formCreateBloc.close();
    postEditCubit.close();
    categoriesCubit.close();
    titleFormCreateCubit.close();
    contentFormCreateCubit.close();
    hashtagFormCreateBloc.close();
    locationFormCreateCubit.close();
    ratingCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => formCreateBloc),
        BlocProvider(create: (_) => categoriesCubit),
        BlocProvider(create: (_) => titleFormCreateCubit),
        BlocProvider(create: (_) => contentFormCreateCubit),
        BlocProvider(create: (_) => hashtagFormCreateBloc),
        BlocProvider(create: (_) => locationFormCreateCubit),
        BlocProvider(create: (_) => ratingCubit),
        BlocProvider(create: (_) => postEditCubit),
        BlocProvider(create: (_) => draftPostBloc),
        BlocProvider(create: (_) => createPostBloc),
      ],
      child: Scaffold(
        body: SafeArea(
          child: BlocConsumer<CreatePostBloc, PostState>(
            bloc: createPostBloc,
            listener: (context, state) {
              if (state.status == PostStatus.failure) {
                isLoading = false;
                AppShower.showFlushBar(
                  AppIcon.failedCheck,
                  context,
                  message: state.mgsError,
                  theme: theme,
                  iconColor: AppColors.errorColor,
                );
              } else if (state.status == PostStatus.createSuccess) {
                isLoading = false;
                Future.delayed(
                  const Duration(seconds: 2),
                  () => Navigator.pushReplacementNamed(
                    context,
                    RouteName.postDetail,
                    arguments: state.post.id,
                  ),
                );
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  message: 'Đăng bài thành công!',
                  theme: theme,
                  iconColor: AppColors.sussess,
                );
              } else if (state.status == PostStatus.editSuccess) {
                Future.delayed(const Duration(seconds: 2),
                    () => Navigator.pop(context, state.post));
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  message: 'Chỉnh sửa thành công!',
                  theme: theme,
                  iconColor: AppColors.sussess,
                );
              } else if (state.status == PostStatus.loading) {
                isLoading = true;
              }
            },
            builder: (_, state) => Stack(
              children: [
                Column(
                  children: [
                    const TopCreatePost(),
                    Flexible(
                      flex: 1,
                      child: BlocBuilder<FormCreateBloc, FormCreateState>(
                        buildWhen: (previous, current) =>
                            (current is UpdatePostFromDraftSuccess ||
                                    current is FormCreateInitial)
                                ? true
                                : false,
                        builder: (context, state) {
                          if (state is UpdatePostFromDraftSuccess) {
                            categoriesCubit
                                .select(formCreateBloc.post.category);
                            titleFormCreateCubit
                                .update(formCreateBloc.post.titlePost);
                            contentFormCreateCubit
                                .update(formCreateBloc.post.content);
                            hashtagFormCreateBloc.add(
                              UpdateHashTagEvent(formCreateBloc.post.hashTag),
                            );
                            locationFormCreateCubit
                                .add(formCreateBloc.post.location);
                            ratingCubit.rate(formCreateBloc.post.rating);
                          }
                          return SingleChildScrollView(
                            padding: const EdgeInsets.only(top: 10.0),
                            physics: RangeMaintainingScrollPhysics(),
                            child: Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Visibility(
                                    visible: !authBloc.isChecked,
                                    child: GestureDetector(
                                      onTap: () async {
                                        await Navigator.pushNamed(
                                          context,
                                          RouteName.login,
                                        ).then(
                                          (isLogin) {
                                            if (!isLogin) return;
                                            Navigator.pushNamedAndRemoveUntil(
                                              context,
                                              RouteName.home,
                                              (route) => false,
                                            );
                                          },
                                        );
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(
                                          color: AppColors.primaryColor
                                              .withOpacity(0.15),
                                          borderRadius:
                                              BorderRadius.circular(10.0),
                                        ),
                                        padding: const EdgeInsets.all(10.0),
                                        child: Row(
                                          children: [
                                            RichText(
                                              text: TextSpan(
                                                text: "Bạn cần ",
                                                style: theme.textTheme.bodyText1
                                                    .copyWith(
                                                  color: AppColors.primaryColor,
                                                  fontWeight:
                                                      AppFontWeight.regular,
                                                ),
                                                children: [
                                                  TextSpan(
                                                    text: "đăng nhập ",
                                                    style: theme
                                                        .textTheme.headline6
                                                        .copyWith(
                                                      color: AppColors
                                                          .primaryColor,
                                                    ),
                                                  ),
                                                  const TextSpan(
                                                    text:
                                                        "để có thể đăng bài reviu !",
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  const CategoriesPost(),
                                  const AddImageVideoPost(),
                                  const AddContentPost(),
                                  const AddLocationRating(),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ],
                ),
                isLoading
                    ? Center(
                        child: Container(
                          color: AppColors.darkThemeColor.withOpacity(0.6),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              AppLoading.doubleBounce(size: 20.0),
                            ],
                          ),
                        ),
                      )
                    : const SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
