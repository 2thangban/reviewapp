part of 'create_post_bloc.dart';

@immutable
abstract class CreatePostEvent {}

class CreatePost extends CreatePostEvent {
  final Post post;
  CreatePost(this.post);
}

class EditPost extends CreatePostEvent {
  final Post post;
  EditPost(this.post);
}
