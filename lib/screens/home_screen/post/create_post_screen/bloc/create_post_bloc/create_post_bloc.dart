import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/post/post.dart';
import '../../../../../../models/post/post_repo.dart';
import '../../../../../../models/token/token_repo.dart';

part 'create_post_event.dart';
part 'create_post_state.dart';

class CreatePostBloc extends Bloc<CreatePostEvent, PostState> {
  CreatePostBloc() : super(const PostState());

  @override
  Stream<PostState> mapEventToState(CreatePostEvent event) async* {
    /// handles event the user create new the post
    if (event is CreatePost) {
      yield state.cloneWith(status: PostStatus.loading);
      yield await mapCreateToState(event);
    }

    /// handles event the useredit the post
    else if (event is EditPost) {
      yield state.cloneWith(status: PostStatus.loading);
      yield await mapEditToState(event);
    }
  }

  Future<PostState> mapCreateToState(CreatePost event) async {
    try {
      return await PostRepo.createPost(event.post).then(
        (value) =>
            state.cloneWith(post: value, status: PostStatus.createSuccess),
      );
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        Post post;
        await TokenRepo.refreshToken().then((value) async {
          await TokenRepo.write(value);
          post = await PostRepo.createPost(event.post);
        });
        return state.cloneWith(post: post, status: PostStatus.createSuccess);
      } else {
        return state.cloneWith(status: PostStatus.failure);
      }
    } catch (e) {
      return state.cloneWith(status: PostStatus.failure);
    }
  }

  Future<PostState> mapEditToState(EditPost event) async {
    try {
      return await PostRepo.editPost(event.post).then(
        (value) => state.cloneWith(post: value, status: PostStatus.editSuccess),
      );
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        Post post;
        await TokenRepo.refreshToken().then((value) async {
          await TokenRepo.write(value);
          post = await PostRepo.editPost(event.post);
        });
        return state.cloneWith(post: post, status: PostStatus.editSuccess);
      } else {
        return state.cloneWith(status: PostStatus.failure);
      }
    } catch (e) {
      return state.cloneWith(status: PostStatus.failure);
    }
  }
}
