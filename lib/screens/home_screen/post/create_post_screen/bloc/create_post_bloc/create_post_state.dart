part of 'create_post_bloc.dart';

enum PostStatus { initial, loading, createSuccess, editSuccess, failure }

class PostState {
  final Post post;
  final PostStatus status; // create post success return the post.
  final String mgsError; // status of post.

  const PostState({
    this.post,
    this.mgsError,
    this.status = PostStatus.initial,
  });

  PostState cloneWith({Post post, PostStatus status, String mgsError}) =>
      PostState(
        post: post ?? this.post,
        status: status ?? this.status,
        mgsError: mgsError ?? this.mgsError,
      );
}
