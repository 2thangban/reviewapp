import 'package:bloc/bloc.dart';

class CategoriesCubit extends Cubit<int> {
  int value;
  CategoriesCubit(this.value) : super(value);

  void select(int newIndex) => emit(this.value = newIndex);
}
