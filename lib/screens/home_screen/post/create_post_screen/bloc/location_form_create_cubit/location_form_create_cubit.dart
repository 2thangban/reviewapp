import 'package:bloc/bloc.dart';

import '../../../../../../models/location/location.dart';

class LocationFormCreateCubit extends Cubit<Location> {
  Location location;
  LocationFormCreateCubit(this.location) : super(location);

  void add(Location newLocation) => emit(this.location = newLocation);
}
