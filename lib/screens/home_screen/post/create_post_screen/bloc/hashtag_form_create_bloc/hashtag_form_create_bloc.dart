import 'dart:async';

import 'package:bloc/bloc.dart';

part 'hashtag_form_create_event.dart';
part 'hashtag_form_create_state.dart';

class HashtagFormCreateBloc
    extends Bloc<HashtagFormCreateEvent, HashtagFormCreateState> {
  final List<String> listHashTag = [];

  HashtagFormCreateBloc(List<String> listHashTag)
      : assert(listHashTag != null),
        super(HashtagFormCreateInitial()) {
    this.listHashTag.addAll(listHashTag ?? []);
  }
  @override
  Stream<HashtagFormCreateState> mapEventToState(
      HashtagFormCreateEvent event) async* {
    if (event is AddHashTagEvent) {
      if (this.listHashTag.length != 20) {
        if (!this.listHashTag.contains(event.newHashTag)) {
          this.listHashTag.add(event.newHashTag);
          yield AddHashTagSuccess();
        } else
          yield AddHashTagFailed('Hashtag đã tồn tại !');
      } else
        yield AddHashTagFailed('Không thể thêm quá 20 Hashtag !');
    } else if (event is EditHashTagEvent) {
      this.listHashTag[event.index] = event.newHashTag;
      yield EditHashTagSuccess();
    } else if (event is RemoveHashTagEvent) {
      this.listHashTag.removeAt(event.index);
      yield RemoveHashTagSuccess();
    } else if (event is UpdateHashTagEvent) {
      this.listHashTag.clear();
      this.listHashTag.addAll(event.listHashtag);
      yield UpdateHashTagSuccess();
    }
  }
}
