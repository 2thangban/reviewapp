part of 'hashtag_form_create_bloc.dart';

abstract class HashtagFormCreateEvent {}

class AddHashTagEvent extends HashtagFormCreateEvent {
  final String newHashTag;
  AddHashTagEvent(this.newHashTag);
}

class EditHashTagEvent extends HashtagFormCreateEvent {
  final String newHashTag;
  final int index;
  EditHashTagEvent(this.newHashTag, this.index);
}

class RemoveHashTagEvent extends HashtagFormCreateEvent {
  final int index;
  RemoveHashTagEvent(this.index);
}

class UpdateHashTagEvent extends HashtagFormCreateEvent {
  final List<String> listHashtag;
  UpdateHashTagEvent(this.listHashtag);
}
