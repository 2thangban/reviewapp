part of 'hashtag_form_create_bloc.dart';

abstract class HashtagFormCreateState {}

class HashtagFormCreateInitial extends HashtagFormCreateState {}

class AddHashTagSuccess extends HashtagFormCreateState {}

class AddHashTagFailed extends HashtagFormCreateState {
  final String mgs;
  AddHashTagFailed(this.mgs);
}

class EditHashTagSuccess extends HashtagFormCreateState {}

class RemoveHashTagSuccess extends HashtagFormCreateState {}

class UpdateHashTagSuccess extends HashtagFormCreateState {}
