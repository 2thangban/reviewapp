import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';

part 'file_form_create_event.dart';
part 'file_form_create_state.dart';

class FileFormCreateBloc
    extends Bloc<FileFormCreateEvent, FileFormCreateState> {
  List<File> listImage = [];
  List<String> listUrlImage = [];
  File video;
  FileFormCreateBloc({List<String> listUrlImage})
      : super(FileFormCreateInitial()) {
    this.listUrlImage.addAll(listUrlImage ?? []);
  }

  @override
  Stream<FileFormCreateState> mapEventToState(
      FileFormCreateEvent event) async* {
    /// handle add image/ video
    if (event is AddImageEvent) {
      listImage.addAll(event.listImage);
      yield AddImageSuccess();
    } else if (event is AddVideoEvent) {
      video = event.video;
      yield AddVideoSuccess();
    }

    /// handle remove image/ video
    else if (event is RemoveVideoEvent) {
      video = null;
      yield RemoveVideoSuccess();
    } else if (event is RemoveImageEvent) {
      if (event.image != null) {
        listImage.remove(event.image);
      } else {
        listUrlImage.remove(event.urlImage);
      }
      yield RemoveImageSuccess();
    }
  }
}
