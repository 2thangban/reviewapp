part of 'file_form_create_bloc.dart';

abstract class FileFormCreateState {}

class FileFormCreateInitial extends FileFormCreateState {}

class AddImageSuccess extends FileFormCreateState {}

class AddVideoSuccess extends FileFormCreateState {}

class RemoveImageSuccess extends FileFormCreateState {}

class RemoveVideoSuccess extends FileFormCreateState {}
