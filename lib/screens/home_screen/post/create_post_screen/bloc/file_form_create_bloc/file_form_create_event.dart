part of 'file_form_create_bloc.dart';

abstract class FileFormCreateEvent {}

class AddImageEvent extends FileFormCreateEvent {
  final List<File> listImage;
  AddImageEvent(this.listImage);
}

class AddVideoEvent extends FileFormCreateEvent {
  final File video;
  AddVideoEvent(this.video);
}

class RemoveVideoEvent extends FileFormCreateEvent {}

class RemoveImageEvent extends FileFormCreateEvent {
  final File image;
  final String urlImage;
  RemoveImageEvent({
    this.urlImage,
    this.image,
  });
}
