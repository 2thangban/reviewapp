import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../commons/helper/validator.dart';
import '../../../../../../models/post/post.dart';

part 'form_create_event.dart';
part 'form_create_state.dart';

class FormCreateBloc extends Bloc<FormCreateEvent, FormCreateState> {
  Post post;
  FormCreateBloc(this.post) : super(FormCreateInitial(post));

  @override
  Stream<FormCreateState> mapEventToState(
    FormCreateEvent event,
  ) async* {
    if (event is ValidationFormCreatePostEvent) {
      yield (!Validator.isEmptyString(this.post.titlePost) &&
              !Validator.isEmptyString(this.post.content) &&
              this.post.imageUpload.isNotEmpty &&
              this.post.location != null &&
              this.post.rating != 0)
          ? ValidFormSuccess()
          : ValidFormFailed();
    } else if (event is UpdatePostFromDraftEvent) {
      this.post = event.post;
      yield UpdatePostFromDraftSuccess(event.post);
    } else if (event is ValidationFormEditPostEvent) {
      yield (this.post == event.post)
          ? ValidFormFailed()
          : ValidFormSuccess(post: event.post);
    }
  }
}
