part of 'form_create_bloc.dart';

abstract class FormCreateState {}

class FormCreateInitial extends FormCreateState {
  final Post post;
  FormCreateInitial(this.post);
}

class ValidFormSuccess extends FormCreateState {
  final Post post;
  ValidFormSuccess({this.post});
}

class ValidFormFailed extends FormCreateState {}

class UpdatePostFromDraftSuccess extends FormCreateState {
  final Post post;
  UpdatePostFromDraftSuccess(this.post);
}
