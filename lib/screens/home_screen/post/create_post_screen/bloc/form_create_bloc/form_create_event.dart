part of 'form_create_bloc.dart';

abstract class FormCreateEvent {}

class ValidationFormCreatePostEvent extends FormCreateEvent {}

class ValidationFormEditPostEvent extends FormCreateEvent {
  final Post post;
  ValidationFormEditPostEvent(this.post);
}

class UpdatePostFromDraftEvent extends FormCreateEvent {
  final Post post;
  UpdatePostFromDraftEvent(this.post);
}
