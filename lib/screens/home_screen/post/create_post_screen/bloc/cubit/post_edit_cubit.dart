import 'package:bloc/bloc.dart';

import '../../../../../../models/post/post.dart';

class PostEditCubit extends Cubit<Post> {
  Post editPost;
  bool isEdit = false;
  PostEditCubit(this.editPost) : super(editPost) {
    this.isEdit = this.editPost?.id != null;
  }
}
