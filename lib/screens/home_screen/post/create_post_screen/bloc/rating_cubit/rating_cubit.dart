import 'package:bloc/bloc.dart';

class RatingCubit extends Cubit<int> {
  int value;
  RatingCubit(this.value) : super(value);

  void rate(int value) => emit(this.value = value);
}
