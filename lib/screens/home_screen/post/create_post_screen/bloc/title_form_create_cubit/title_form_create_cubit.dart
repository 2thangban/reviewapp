import 'package:bloc/bloc.dart';

class TitleFormCreateCubit extends Cubit<String> {
  String title;
  TitleFormCreateCubit(this.title) : super(title);

  void update(String title) => emit(this.title = title);
}
