import 'package:bloc/bloc.dart';

class ContentFormCreateCubit extends Cubit<String> {
  String content;
  ContentFormCreateCubit(this.content) : super(content);

  void update(String newContent) => emit(this.content = newContent);
}
