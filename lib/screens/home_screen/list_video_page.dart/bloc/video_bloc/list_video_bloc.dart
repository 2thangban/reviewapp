import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/post/post.dart';
import '../../../../../models/post/post_repo.dart';
import '../../../../../models/token/token_repo.dart';

part 'list_video_event.dart';
part 'list_video_state.dart';

class ListVideoBloc extends Bloc<ListVideoEvent, ListVideoState> {
  ListVideoBloc() : super(const ListVideoState());
  final int limit = 5;
  int page = 0;
  @override
  Stream<ListVideoState> mapEventToState(ListVideoEvent event) async* {
    if (event is GetListVideoEvent) {
      if (event.isRefresh) {
        page = 0;
      }
      yield state.cloneWith(status: ListVideoStatus.loading);
      try {
        final result = await PostRepo.getAllVideo(
          page: page++,
          limit: limit,
          isAuth: event.isAuth,
        );
        yield state.cloneWith(
          status: ListVideoStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } on ErrorResponse catch (e) {
        page--;
        if (e.staticCode == 401) {
          List<Post> list;
          await TokenRepo.refreshToken().then(
            (token) async {
              await TokenRepo.write(token).then((value) async {
                list = await PostRepo.getAllVideo(
                  page: page++,
                  limit: limit,
                  isAuth: true,
                );
              });
            },
          );
          yield state.cloneWith(
            status: ListVideoStatus.success,
            list: List.of(state.list)..addAll(list),
            isLastPage: list.length < limit,
          );
        }
      } catch (e) {
        page--;
        yield state.cloneWith(status: ListVideoStatus.failure);
      }
    } else {
      page--;
      yield state.cloneWith(status: ListVideoStatus.failure);
    }
  }
}
