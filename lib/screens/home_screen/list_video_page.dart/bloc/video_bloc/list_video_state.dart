part of 'list_video_bloc.dart';

enum ListVideoStatus { initial, loading, success, failure }

class ListVideoState extends Equatable {
  final ListVideoStatus status;
  final List<Post> list;
  final bool isLastPage;

  const ListVideoState({
    this.status = ListVideoStatus.initial,
    this.list = const <Post>[],
    this.isLastPage = false,
  });

  ListVideoState cloneWith({
    final ListVideoStatus status,
    final List<Post> list,
    final bool isLastPage,
  }) =>
      ListVideoState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );

  @override
  List<Object> get props => [status, list, isLastPage];
}
