part of 'list_video_bloc.dart';

@immutable
abstract class ListVideoEvent {}

/// event get list video
class GetListVideoEvent extends ListVideoEvent {
  final bool isAuth;
  final bool isRefresh;
  GetListVideoEvent({
    this.isRefresh = false,
    this.isAuth = false,
  });
}
