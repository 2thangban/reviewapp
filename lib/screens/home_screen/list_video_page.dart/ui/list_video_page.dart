import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_shimmer.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/post/post.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../bloc/video_bloc/list_video_bloc.dart';
import 'content_item_video.dart';
import 'reac_item_video.dart';
import 'video_by_post.dart';

class ListVideoPage extends StatefulWidget {
  const ListVideoPage({Key key}) : super(key: key);
  @override
  _ListVideoPageState createState() => _ListVideoPageState();
}

class _ListVideoPageState extends State<ListVideoPage>
    with AutomaticKeepAliveClientMixin<ListVideoPage> {
  final sizedText = SizedConfig.textMultiplier;
  final sizedScreen = SizedConfig.heightMultiplier;
  AuthBloc authBloc;

  final ListVideoBloc listVideoBloc = ListVideoBloc();

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of(context);
    listVideoBloc.add(
      GetListVideoEvent(isAuth: authBloc.isChecked),
    );
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        titleWidget: Text("Video", style: theme.textTheme.headline3),
      ),
      body: RefreshIndicator(
        onRefresh: () async => await Future.delayed(
          Duration(milliseconds: 500),
          () {
            /// item show loading button
            listVideoBloc.add(
              GetListVideoEvent(
                isAuth: authBloc.isChecked,
                isRefresh: true,
              ),
            );
          },
        ),
        child: BlocBuilder<ListVideoBloc, ListVideoState>(
          bloc: listVideoBloc,
          builder: (_, state) {
            if (state.status == ListVideoStatus.success) {
              if (state.list.isEmpty) return const SizedBox.shrink();
              return Container(
                child: ListView.separated(
                  itemCount: state.list.length,
                  itemBuilder: (context, index) {
                    Post video = state.list[index];
                    return Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          VideoByPost(
                            video.account,
                            video.urlVideo,
                            video.customeDate,
                          ),
                          ReacItemVideo(video),
                          ContentItemVideo(video),
                        ],
                      ),
                    );
                  },
                  separatorBuilder: (context, index) => Divider(
                    thickness: 5.0,
                    color: AppColors.subLightColor.withOpacity(0.3),
                  ),
                ),
              );
            }
            if (state.status == ListVideoStatus.failure) {
              return Center(
                child: SizedBox(
                  height: sizedScreen * 5,
                  width: sizedScreen * 15,
                  child: AppButton.common(
                    labelText: lang.locaized('reConnect'),
                    labelStyle: theme.textTheme.bodyText1.copyWith(
                      color: theme.backgroundColor,
                    ),
                    radius: 5.0,
                    backgroundColor: theme.primaryColor,
                    shadowColor: theme.primaryColor,
                    onPressed: () => listVideoBloc.add(GetListVideoEvent()),
                  ),
                ),
              );
            }
            return AppShimmer.video();
          },
        ),
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
