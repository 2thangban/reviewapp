import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../commons/widgets/circle_avt.dart';
import '../../../../models/user/user.dart';
import '../../../../routes/route_name.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/follow_cubit/follow_user_cubit.dart';
import '../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import 'overlay_video.dart';

class VideoByPost extends StatefulWidget {
  final Account account;
  final String urlVideo;
  final String datePost;
  const VideoByPost(this.account, this.urlVideo, this.datePost);
  @override
  _VideoByPostState createState() => _VideoByPostState();
}

class _VideoByPostState extends State<VideoByPost> {
  VideoPlayerController _videoController;
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  AuthBloc authBloc;
  final SignUpBloc signUpBloc = SignUpBloc();
  FollowUserCubit followUserCubit;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of(context);
    _videoController =
        VideoPlayerController.network(AppAssets.baseUrl + widget.urlVideo)
          ..addListener(() => setState(() {}))
          ..setLooping(true)
          ..initialize().then(
            (_) => _videoController.pause(),
          );
  }

  @override
  void dispose() {
    super.dispose();
    _videoController.dispose();
    signUpBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final Account account = widget.account;
    final theme = Theme.of(context);

    return Column(
      children: [
        ListTile(
          onTap: () => Navigator.pushNamed(
            context,
            RouteName.profile,
            arguments: account.id,
          ),
          horizontalTitleGap: 10.0,
          minLeadingWidth: 1,
          leading: CircleAvt(
            image: AppAssets.baseUrl + account.avatar,
            radius: 25.0,
          ),
          title: Container(
            constraints: BoxConstraints(
              maxWidth: sizeScreen * 13,
            ),
            child: Text(
              widget.account.userName,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              style: theme.textTheme.headline6,
            ),
          ),
          subtitle: Text(
            widget.datePost,
            style: theme.textTheme.bodyText2.copyWith(
              fontSize: sizeText * 1.7,
            ),
          ),
        ),
        Container(
          color: AppColors.darkThemeColor,
          height: sizeScreen * 50,
          width: double.infinity,
          child:
              _videoController != null && _videoController.value.isInitialized
                  ? Stack(
                      children: [
                        Center(
                          child: AspectRatio(
                            aspectRatio: _videoController.value.aspectRatio,
                            child: VideoPlayer(_videoController),
                          ),
                        ),
                        Positioned.fill(
                          child: OverlayWidget(controller: _videoController),
                        ),
                      ],
                    )
                  : Center(child: AppLoading.doubleBounce(size: 20.0)),
        ),
      ],
    );
  }
}
