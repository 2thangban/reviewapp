import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';

class OverlayWidget extends StatelessWidget {
  final VideoPlayerController controller;

  const OverlayWidget({
    Key key,
    @required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () =>
          controller.value.isPlaying ? controller.pause() : controller.play(),
      child: Stack(
        children: <Widget>[
          buildPlay(),
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: buildIndicator(),
          ),
        ],
      ),
    );
  }

  Widget buildIndicator() => VideoProgressIndicator(
        controller,
        colors: VideoProgressColors(
          playedColor: AppColors.primaryColor,
          bufferedColor: AppColors.primaryColor.withOpacity(0.5),
          backgroundColor: Colors.grey.withOpacity(0.2),
        ),
        allowScrubbing: true,
        padding: const EdgeInsets.only(bottom: 10.0),
      );

  Widget buildPlay() => controller.value.isPlaying
      ? Container()
      : Container(
          alignment: Alignment.center,
          color: Colors.black26,
          child: Icon(
            AppIcon.playVideo,
            color: AppColors.primaryColor,
            size: 80,
          ),
        );
}
