import 'package:flutter/material.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/post/post.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';

class ContentItemVideo extends StatefulWidget {
  final Post post;
  const ContentItemVideo(this.post, {Key key}) : super(key: key);

  @override
  _ContentItemVideoState createState() => _ContentItemVideoState();
}

class _ContentItemVideoState extends State<ContentItemVideo> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text(
                  widget.post.titlePost,
                  style: theme.textTheme.bodyText1.copyWith(
                    fontSize: sizeText * 2.2,
                  ),
                ),
              ),
              Visibility(
                visible: widget.post.listHashTag.isNotEmpty,
                child: Wrap(
                  spacing: 3,
                  runSpacing: -20,
                  children: widget.post.listHashTag
                      .map(
                        (e) => ActionChip(
                          onPressed: () => Navigator.pushNamed(
                            context,
                            RouteName.detailHashtag,
                            arguments: e.id,
                          ),
                          labelPadding: EdgeInsets.symmetric(
                            vertical: -5,
                            horizontal: 2.0,
                          ),
                          backgroundColor:
                              AppColors.primaryColor.withOpacity(0.2),
                          avatar: Container(
                            decoration: BoxDecoration(
                              color: AppColors.primaryColor.withOpacity(0.8),
                              borderRadius: BorderRadius.circular(20),
                            ),
                            child: Text(
                              ' # ',
                              style: TextStyle(
                                color: AppColors.lightThemeColor,
                                fontSize: sizeText * 1.5,
                              ),
                            ),
                          ),
                          label: Text(
                            e.title,
                            style: theme.textTheme.bodyText1.copyWith(
                              fontSize: sizeText * 1.6,
                              color: AppColors.primaryColor,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ),
              InkWell(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteName.detailLocation,
                  arguments: widget.post.locationId,
                ),
                child: Container(
                  margin: const EdgeInsets.only(top: 10.0),
                  padding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 7.0),
                  decoration: BoxDecoration(
                    color: AppColors.sussess.withOpacity(0.7),
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Icon(
                        AppIcon.pin,
                        size: sizeScreen * 2.5,
                        color: AppColors.lightThemeColor,
                      ),
                      const SizedBox(width: 5.0),
                      Text(
                        lang.locaized('labelBtnInfoLocation'),
                        style: theme.textTheme.bodyText1.copyWith(
                            color: AppColors.lightThemeColor,
                            fontWeight: AppFontWeight.regular),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 10.0),
      ],
    );
  }
}
