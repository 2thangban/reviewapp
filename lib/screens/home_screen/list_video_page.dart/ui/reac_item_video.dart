import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/animation/icon_button_anim.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/list_gallery_widget.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/gallery/gellery.dart';
import '../../../../models/post/post.dart';
import '../../../../routes/route_name.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/favorite_cubit/favorite_cubit.dart';
import '../../../app_common_bloc/like_cubit/like_post_cubit.dart';
import '../../../comment/list_all_comment_screen/ui/list_comment_screen.dart';

// ignore: must_be_immutable
class ReacItemVideo extends StatefulWidget {
  final Post post;

  ReacItemVideo(this.post);

  @override
  _ReacItemVideoState createState() => _ReacItemVideoState();
}

class _ReacItemVideoState extends State<ReacItemVideo> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  LikePostCubit likeCubit;
  FavoriteCubit favoriteCubit;
  AuthBloc authBloc;

  Future<Gallery> _showListGallery() => showModalBottomSheet(
        context: context,
        backgroundColor: Colors.transparent,
        barrierColor: AppColors.darkThemeColor.withOpacity(0.3),
        isDismissible: false,
        builder: (_) => ListGalleryWidget(),
      );

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    likeCubit = LikePostCubit(widget.post.isLiked, widget.post.like);
    favoriteCubit = FavoriteCubit(widget.post.isFavorite, widget.post.id);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Row(
        children: [
          BlocBuilder<LikePostCubit, bool>(
            bloc: likeCubit,
            builder: (_, status) => Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                IconButtonAnim(
                  status: status,
                  child: AppButton.icon(
                    icon: status ? AppIcon.liked : AppIcon.unLike,
                    iconColor:
                        status ? AppColors.likeColor : AppColors.subLightColor,
                    onTap: () async {
                      if (authBloc.isChecked) {
                        likeCubit.change(
                          status,
                          postId: widget.post.id,
                        );
                      } else {
                        await Navigator.pushNamed(context, RouteName.login)
                            .then((isLogin) {
                          if (!isLogin) return;
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            RouteName.home,
                            (route) => false,
                          );
                        });
                      }
                    },
                  ),
                ),
                const SizedBox(width: 5.0),
                Text(
                  likeCubit.numOfLike.toString(),
                  style: theme.textTheme.headline5,
                ),
              ],
            ),
          ),
          const Spacer(),
          GestureDetector(
            onTap: () => Future.delayed(
              Duration(milliseconds: 300),
              () => Navigator.push(
                context,
                PageRouteBuilder(
                  pageBuilder: (context, anim, seconAnim) => ListCommentScreen(
                    params: ParamListComment(widget.post.id),
                  ),
                  transitionsBuilder: (context, anim, seconAnim, child) =>
                      SlideTransition(
                    position: Tween<Offset>(
                      begin: const Offset(0.0, 1.0),
                      end: Offset.zero,
                    ).animate(anim),
                    child: SlideTransition(
                      position: Tween<Offset>(
                        begin: Offset.zero,
                        end: const Offset(0.0, 1.0),
                      ).animate(seconAnim),
                      child: child,
                    ),
                  ),
                  transitionDuration: Duration(milliseconds: 400),
                ),
              ),
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Icon(AppIcon.comment),
                const SizedBox(width: 5.0),
                Text(
                  widget.post.numOfComment.toString(),
                  style: theme.textTheme.headline5,
                ),
              ],
            ),
          ),
          const SizedBox(width: 15.0),
          BlocConsumer<FavoriteCubit, FavoriteState>(
            bloc: favoriteCubit,
            listener: (context, state) {
              if (state.isAddSuccess) {
                AppShower.showSnackBar(
                  context,
                  iconLeading: Icon(
                    AppIcon.saveGallery,
                    color: theme.primaryColor,
                  ),
                  label: lang.locaized('isSavedPost'),
                  labelAction: state.gallery.name,
                );
              } else if (state.isAddSuccess == false) {
                AppShower.showSnackBar(
                  context,
                  iconLeading: Icon(AppIcon.unsaveGallery),
                  label: lang.locaized('isUnSavedPost'),
                  labelAction: '',
                );
              }
            },
            builder: (context, state) {
              return IconButtonAnim(
                status: state.status,
                child: AppButton.icon(
                  icon: state.status
                      ? AppIcon.saveGallery
                      : AppIcon.unsaveGallery,
                  iconColor:
                      state.status ? theme.primaryColor : theme.iconTheme.color,
                  onTap: () async {
                    if (authBloc.isChecked) {
                      if (state.status) {
                        favoriteCubit.unFavoritePost();
                      } else {
                        await Future.delayed(
                          const Duration(milliseconds: 200),
                          () => _showListGallery(),
                        ).then(
                          (gallery) {
                            if (gallery != null) {
                              favoriteCubit.favoritePost(gallery);
                            }
                          },
                        );
                      }
                    } else {
                      await Future.delayed(
                        Duration(milliseconds: 200),
                        () => Navigator.pushNamed(context, RouteName.login),
                      ).then(
                        (isLoginSuccess) {
                          if (isLoginSuccess) {
                            Navigator.pushNamedAndRemoveUntil(
                              context,
                              RouteName.home,
                              (route) => false,
                            );
                          }
                        },
                      );
                    }
                  },
                ),
              );
            },
          ),
          const SizedBox(width: 10.0),
        ],
      ),
    );
  }
}
