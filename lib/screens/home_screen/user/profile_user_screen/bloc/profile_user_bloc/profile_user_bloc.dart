import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/user.dart';
import '../../../../../../models/user/user_repo.dart';

part 'profile_user_event.dart';
part 'profile_user_state.dart';

class ProfileUserBloc extends Bloc<ProfileUserEvent, ProfileUserState> {
  Account account;
  ProfileUserBloc() : super(ProfileUserInitial());

  @override
  Stream<ProfileUserState> mapEventToState(ProfileUserEvent event) async* {
    if (event is GetProfileUserEvent) {
      try {
        yield ProfileUserLoading();
        final result = await AccountRepo.anotherUser(
          event.userId,
          isAuth: event.isAuth,
        );
        yield ProfileUserSuccess(account = result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Account account;
          await TokenRepo.refreshToken().then((value) async {
            TokenRepo.write(value);
            account = await AccountRepo.anotherUser(
              event.userId,
              isAuth: event.isAuth,
            );
          });
          yield ProfileUserSuccess(account);
        } else {
          yield ProfileUserFailed();
        }
      }
    }
  }
}
