part of 'profile_user_bloc.dart';

abstract class ProfileUserEvent {}

class GetProfileUserEvent extends ProfileUserEvent {
  final int userId;
  final bool isAuth;
  GetProfileUserEvent(this.userId, {this.isAuth});
}
