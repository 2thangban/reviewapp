part of 'profile_user_bloc.dart';

abstract class ProfileUserState {}

class ProfileUserInitial extends ProfileUserState {}

class ProfileUserLoading extends ProfileUserState {}

class ProfileUserSuccess extends ProfileUserState {
  final Account account;
  ProfileUserSuccess(this.account);
}

class ProfileUserFailed extends ProfileUserState {}
