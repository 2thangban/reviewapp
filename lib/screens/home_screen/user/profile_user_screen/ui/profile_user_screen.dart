import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../app_common_bloc/follow_cubit/follow_user_cubit.dart';
import '../bloc/profile_user_bloc/profile_user_bloc.dart';
import 'gallery_by_account/ui/gallery_by_account.dart';
import 'header_info.dart';
import 'list_post_by_account/ui/list_post_by_user.dart';

class ProfileUserScreen extends StatefulWidget {
  final int userId;
  ProfileUserScreen(this.userId);
  @override
  _ProfileUserScreenState createState() => _ProfileUserScreenState(this.userId);
}

class _ProfileUserScreenState extends State<ProfileUserScreen> {
  final int userId;
  _ProfileUserScreenState(this.userId);

  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  final ProfileUserBloc profileUserBloc = ProfileUserBloc();
  FollowUserCubit followUserCubit;
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    profileUserBloc.add(
      GetProfileUserEvent(
        userId,
        isAuth: authBloc.isChecked,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    profileUserBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final lang = AppLocalization.of(context);
    final theme = Theme.of(context);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => profileUserBloc),
        BlocProvider(create: (_) => followUserCubit),
      ],
      child: Scaffold(
        body: BlocBuilder<ProfileUserBloc, ProfileUserState>(
          bloc: profileUserBloc,
          builder: (context, state) {
            if (state is ProfileUserFailed) {
              return Center(
                child: SizedBox(
                  height: sizeScreen * 5.0,
                  width: sizeScreen * 15.0,
                  child: AppButton.common(
                    radius: 5.0,
                    labelText: lang.locaized('reConnect'),
                    labelStyle: theme.textTheme.headline6.copyWith(
                      color: theme.backgroundColor,
                    ),
                    backgroundColor: theme.primaryColor,
                    shadowColor: theme.primaryColor,
                    onPressed: () => profileUserBloc.add(
                      GetProfileUserEvent(
                        userId,
                        isAuth: authBloc.isChecked,
                      ),
                    ),
                  ),
                ),
              );
            } else if (state is ProfileUserSuccess) {
              final Account account = state.account;
              followUserCubit = FollowUserCubit(userId, account.isFollowed);
              return DefaultTabController(
                length: 2,
                child: Scaffold(
                  body: NestedScrollView(
                    physics: NeverScrollableScrollPhysics(),
                    floatHeaderSlivers: true,
                    headerSliverBuilder: (_, value) => [
                      SliverAppBar(
                        pinned: true,
                        floating: true,
                        backgroundColor: theme.backgroundColor,
                        expandedHeight: sizeScreen * 40,
                        toolbarHeight: 40,
                        collapsedHeight: 60,
                        elevation: 0.0,
                        flexibleSpace: FlexibleSpaceBar(
                          collapseMode: CollapseMode.none,
                          background: HeaderInfo(),
                        ),
                        bottom: PreferredSize(
                          preferredSize: Size.fromHeight(20.0),
                          child: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 5.0),
                            decoration: BoxDecoration(
                              color: theme.backgroundColor,
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                color:
                                    AppColors.darkThemeColor.withOpacity(0.1),
                              ),
                            ),
                            child: TabBar(
                              indicatorWeight: 1.5,
                              indicatorPadding:
                                  const EdgeInsets.symmetric(horizontal: 30.0),
                              indicatorColor: AppColors.darkThemeColor,
                              tabs: [
                                tabItem(AppIcon.post, sizeScreen),
                                tabItem(AppIcon.unsaveGallery, sizeScreen),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                    body: TabBarView(
                      children: [
                        ListPostByAccount(widget.userId),
                        const GalleryByAccount(),
                      ],
                    ),
                  ),
                ),
              );
            } else if (state is ProfileUserLoading) {
              return AppShimmer.user();
            }
            return AppShimmer.user();
          },
        ),
      ),
    );
  }

  Tab tabItem(IconData icon, double sizeScreen) => Tab(
        iconMargin: const EdgeInsets.all(0.0),
        icon: Icon(
          icon,
          color: AppColors.darkThemeColor,
          size: sizeScreen * 4,
        ),
      );
}
