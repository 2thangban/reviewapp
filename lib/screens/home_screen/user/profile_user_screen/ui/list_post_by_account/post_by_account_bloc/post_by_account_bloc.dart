import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../../models/post/post.dart';
import '../../../../../../../models/user/user_repo.dart';

part 'post_by_account_event.dart';
part 'post_by_account_state.dart';

class PostByAccountBloc extends Bloc<PostByAccountEvent, PostByAccountState> {
  final int accountId;
  final int limit = 10;
  int page = 0;
  PostByAccountBloc(this.accountId) : super(const PostByAccountState());
  @override
  Stream<PostByAccountState> mapEventToState(PostByAccountEvent event) async* {
    if (event is GetPostByAccountEvent) {
      yield state.cloneWith(status: PostAccountStatus.loading);
      if (event.isRefresh) {
        page = 0;
      }
      try {
        final result = await AccountRepo.getAllPost(
          page: page++,
          limit: limit,
          userId: accountId,
        );
        yield state.cloneWith(
          status: PostAccountStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } catch (e) {
        page--;
        yield state.cloneWith(status: PostAccountStatus.failure);
      }
    } else {
      yield state.cloneWith(status: PostAccountStatus.failure);
    }
  }
}
