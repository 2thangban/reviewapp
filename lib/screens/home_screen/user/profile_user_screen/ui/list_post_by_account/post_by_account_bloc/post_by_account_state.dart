part of 'post_by_account_bloc.dart';

enum PostAccountStatus { initial, loading, failure, success }

class PostByAccountState extends Equatable {
  final PostAccountStatus status;
  final List<Post> list;
  final bool isLastPage;

  const PostByAccountState({
    this.status = PostAccountStatus.initial,
    this.list = const <Post>[],
    this.isLastPage = false,
  });

  PostByAccountState cloneWith({
    PostAccountStatus status,
    List<Post> list,
    bool isLastPage,
  }) =>
      PostByAccountState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );

  @override
  List<Object> get props => [status, list, isLastPage];
}
