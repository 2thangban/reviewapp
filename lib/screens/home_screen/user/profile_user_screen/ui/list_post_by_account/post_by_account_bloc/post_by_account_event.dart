part of 'post_by_account_bloc.dart';

abstract class PostByAccountEvent {
  const PostByAccountEvent();
}

class GetPostByAccountEvent extends PostByAccountEvent {
  final bool isRefresh;
  GetPostByAccountEvent({this.isRefresh = false});
}
