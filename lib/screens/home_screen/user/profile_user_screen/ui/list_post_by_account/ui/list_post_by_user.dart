import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../../../../commons/helper/size_config.dart';
import '../../../../../../../commons/widgets/app_button.dart';
import '../../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../../commons/widgets/list_empty.dart';
import '../../../../../../../localizations/app_localization.dart';
import '../../../../profile_page.dart/ui/list_post_by_user/item_post_by_user.dart';
import '../post_by_account_bloc/post_by_account_bloc.dart';

class ListPostByAccount extends StatefulWidget {
  final int userId;
  const ListPostByAccount(this.userId, {Key key}) : super(key: key);
  @override
  _ListPostByAccountState createState() => _ListPostByAccountState();
}

class _ListPostByAccountState extends State<ListPostByAccount>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  PostByAccountBloc postByAccountBloc;

  @override
  void initState() {
    super.initState();
    postByAccountBloc = PostByAccountBloc(widget.userId);
    postByAccountBloc.add(GetPostByAccountEvent());
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final lang = AppLocalization.of(context);
    final theme = Theme.of(context);
    return BlocBuilder<PostByAccountBloc, PostByAccountState>(
      bloc: postByAccountBloc,
      builder: (_, state) {
        if (state.status == PostAccountStatus.failure) {
          return Center(
            child: SizedBox(
              height: sizeScreen * 5.0,
              width: sizeScreen * 15.0,
              child: AppButton.common(
                labelText: lang.locaized('reConnect'),
                labelStyle: theme.textTheme.bodyText1.copyWith(
                  color: theme.backgroundColor,
                ),
                backgroundColor: theme.primaryColor,
                shadowColor: theme.primaryColor,
                radius: 5.0,
                onPressed: () => postByAccountBloc.add(GetPostByAccountEvent()),
              ),
            ),
          );
        }
        if (state.status == PostAccountStatus.success) {
          if (state.list.isEmpty) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 40.0,
                width: sizeScreen * 50.0,
                child: AppObjectEmpty.list(),
              ),
            );
          }
          return StaggeredGridView.extentBuilder(
            padding: const EdgeInsets.symmetric(horizontal: 15.0),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: state.list.length,
            staggeredTileBuilder: (index) =>
                StaggeredTile.extent(1, index.isEven ? 270 : 220),
            maxCrossAxisExtent: 200,
            itemBuilder: (context, index) =>
                ItemListPostByUser(state.list[index]),
          );
        }
        return AppShimmer.list();
      },
    );
  }

  @override
  bool get wantKeepAlive => true;
}
