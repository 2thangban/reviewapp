import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../../commons/helper/size_config.dart';
import '../../../../../../../commons/untils/app_color.dart';
import '../../../../../../../commons/untils/app_icon.dart';
import '../../../../../../../commons/untils/app_image.dart';
import '../../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../../localizations/app_localization.dart';
import '../../../../../../../models/gallery/gellery.dart';
import '../../../../../../../routes/route_name.dart';
import '../../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../bloc/profile_user_bloc/profile_user_bloc.dart';
import '../bloc/list_gallery_account_bloc/list_gallery_account_bloc.dart';

class GalleryByAccount extends StatefulWidget {
  const GalleryByAccount({Key key}) : super(key: key);
  @override
  _GalleryByAccountState createState() => _GalleryByAccountState();
}

class _GalleryByAccountState extends State<GalleryByAccount>
    with AutomaticKeepAliveClientMixin<GalleryByAccount> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final GalleryByAccountBloc listGalleryBloc = GalleryByAccountBloc();

  ProfileUserBloc profileUserBloc;
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    profileUserBloc = BlocProvider.of<ProfileUserBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    listGalleryBloc.add(GetListGalleryAccountEvent(profileUserBloc.account.id));
  }

  @override
  void dispose() {
    super.dispose();
    listGalleryBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        const SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('labelGallery'),
            style: theme.textTheme.headline5,
          ),
        ),
        BlocBuilder<GalleryByAccountBloc, ListGalleryAccountState>(
          bloc: listGalleryBloc,
          builder: (context, state) => AnimatedContainer(
            curve: Curves.decelerate,
            duration: const Duration(milliseconds: 500),
            height: state.status == ListGalleryAccounttatus.loading
                ? sizeScreen * 5
                : sizeScreen * 17,
            child: state.status == ListGalleryAccounttatus.loading
                ? AppLoading.threeBounce(size: 20.0)
                : state.status == ListGalleryAccounttatus.failure
                    ? const SizedBox()
                    : ListView.builder(
                        padding: const EdgeInsets.only(left: 10.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.list.length,
                        itemBuilder: (context, index) {
                          final Gallery currentGallery = state.list[index];
                          return currentGallery.private == 0
                              ? GestureDetector(
                                  onTap: () => Navigator.pushNamed(
                                    context,
                                    RouteName.detailGallery,
                                    arguments: currentGallery,
                                  ),
                                  child: Container(
                                    width: 30 * sizeScreen,
                                    clipBehavior: Clip.antiAlias,
                                    margin: const EdgeInsets.only(right: 10.0),
                                    decoration: BoxDecoration(
                                      color: AppColors.darkThemeColor,
                                      borderRadius: BorderRadius.circular(10.0),
                                    ),
                                    child: Stack(
                                      children: [
                                        Opacity(
                                          opacity: 0.5,
                                          child: currentGallery.image != ''
                                              ? Image.network(
                                                  AppAssets.baseUrl +
                                                      currentGallery.image,
                                                  width: 32 * sizeScreen,
                                                  fit: BoxFit.fill,
                                                )
                                              : Image.asset(
                                                  AppAssets.img1,
                                                  width: 32 * sizeScreen,
                                                  fit: BoxFit.fill,
                                                ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.all(10.0),
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.end,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Container(
                                                width: 28 * sizeScreen,
                                                child: Text(
                                                  currentGallery.name,
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  maxLines: 2,
                                                  style: theme
                                                      .textTheme.headline5
                                                      .copyWith(
                                                    color: AppColors
                                                        .lightThemeColor,
                                                  ),
                                                ),
                                              ),
                                              Visibility(
                                                visible:
                                                    currentGallery.bio != '',
                                                child: Container(
                                                  width: 28 * sizeScreen,
                                                  child: Text(
                                                    currentGallery.bio,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                    maxLines: 2,
                                                    style: theme
                                                        .textTheme.bodyText1
                                                        .copyWith(
                                                      color: AppColors
                                                          .lightThemeColor,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                child: Text(
                                                  currentGallery.numOfPost == 0
                                                      ? lang.locaized(
                                                          'emptyListOfGallery')
                                                      : '${currentGallery.numOfPost} ${lang.locaized('post')}',
                                                  style: theme
                                                      .textTheme.bodyText1
                                                      .copyWith(
                                                    color: AppColors
                                                        .lightThemeColor,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Visibility(
                                          visible: currentGallery.private == 1,
                                          child: Positioned(
                                            right: 5.0,
                                            top: 5.0,
                                            child: Icon(
                                              AppIcon.block,
                                              size: 20,
                                              color: AppColors.lightThemeColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              : const SizedBox.shrink();
                        },
                      ),
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
