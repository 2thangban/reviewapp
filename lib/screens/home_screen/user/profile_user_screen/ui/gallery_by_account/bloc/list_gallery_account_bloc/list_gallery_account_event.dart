part of 'list_gallery_account_bloc.dart';

abstract class ListGalleryEvent {}

class GetListGalleryAccountEvent extends ListGalleryEvent {
  final int userId;
  GetListGalleryAccountEvent(this.userId);
}
