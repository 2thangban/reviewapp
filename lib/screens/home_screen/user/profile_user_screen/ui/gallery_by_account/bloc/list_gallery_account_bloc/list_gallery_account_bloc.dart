import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/gallery/gellary_repo.dart';
import '../../../../../../../../models/gallery/gellery.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_gallery_account_event.dart';
part 'list_gallery_account_state.dart';

class GalleryByAccountBloc
    extends Bloc<ListGalleryEvent, ListGalleryAccountState> {
  int _page = 0;
  final int _limit = 5;
  bool _isLastPage = false;
  Gallery _firstGallery;

  GalleryByAccountBloc() : super(const ListGalleryAccountState());

  get firstGallery => this._firstGallery;
  get isLastPage => this._isLastPage;

  @override
  Stream<ListGalleryAccountState> mapEventToState(
      ListGalleryEvent event) async* {
    if (event is GetListGalleryAccountEvent) {
      try {
        yield state.cloneWith(status: ListGalleryAccounttatus.loading);
        final result = await GalleryRepo.getAll(
          event.userId,
          page: _page++,
          limit: _limit,
          isAuth: false,
        );
        if (this._firstGallery == null) this._firstGallery = result.first;
        yield state.cloneWith(
          list: List.of(state.list)..addAll(result),
          hasReachedEnd: _isLastPage = result.length < _limit,
          status: ListGalleryAccounttatus.success,
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Gallery> result;
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              result = await GalleryRepo.getAll(
                event.userId,
                page: _page,
                limit: _limit,
              );
            });
          });
          if (this._firstGallery == null) this._firstGallery = result.first;
          yield state.cloneWith(
            list: List.of(state.list)..addAll(result),
            hasReachedEnd: _isLastPage = result.length < _limit,
            status: ListGalleryAccounttatus.success,
          );
        } else {
          yield state.cloneWith(status: ListGalleryAccounttatus.failure);
        }
      } catch (e) {
        yield state.cloneWith(status: ListGalleryAccounttatus.failure);
      }
    }
  }
}
