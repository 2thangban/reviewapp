part of 'list_gallery_account_bloc.dart';

enum ListGalleryAccounttatus { initial, loading, failure, success }

class ListGalleryAccountState extends Equatable {
  final List<Gallery> list;
  final bool hasReachedEnd;
  final ListGalleryAccounttatus status;

  const ListGalleryAccountState({
    this.list = const <Gallery>[],
    this.hasReachedEnd = false,
    this.status = ListGalleryAccounttatus.initial,
  });

  ListGalleryAccountState cloneWith({
    List<Gallery> list,
    bool hasReachedEnd,
    ListGalleryAccounttatus status,
  }) =>
      ListGalleryAccountState(
        list: list ?? this.list,
        hasReachedEnd: hasReachedEnd ?? this.hasReachedEnd,
        status: status ?? this.status,
      );
  @override
  List<Object> get props => [this.list, this.hasReachedEnd, this.status];
}
