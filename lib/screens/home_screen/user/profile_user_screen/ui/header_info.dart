import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/circle_avt.dart';
import '../../../../../commons/widgets/follow_user_widget.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/follow_cubit/follow_user_cubit.dart';
import '../bloc/profile_user_bloc/profile_user_bloc.dart';

class HeaderInfo extends StatefulWidget {
  const HeaderInfo();

  @override
  _HeaderInfoState createState() => _HeaderInfoState();
}

class _HeaderInfoState extends State<HeaderInfo> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  FollowUserCubit followCubit;
  ProfileUserBloc profileBloc;
  @override
  void initState() {
    super.initState();
    followCubit = BlocProvider.of<FollowUserCubit>(context);
    profileBloc = BlocProvider.of<ProfileUserBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Container(
      color: theme.backgroundColor,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            const SizedBox(height: 10.0),
            Row(
              children: [
                AppButton.icon(
                  icon: AppIcon.popNavigate,
                  onTap: () => Navigator.pop(context, followCubit.state),
                ),
                const Spacer(),
                PopupMenuButton<int>(
                  offset: Offset(0, 10),
                  padding: EdgeInsets.zero,
                  icon: Icon(
                    AppIcon.more_hori,
                    color: AppColors.darkThemeColor,
                  ),
                  onSelected: (int newValue) => print(newValue),
                  itemBuilder: (context) => [
                    PopupMenuItem<int>(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10.0),
                        child: Text(
                          lang.locaized('report'),
                          style: theme.textTheme.bodyText1,
                        ),
                      ),
                      value: 0,
                    ),
                  ],
                ),
              ],
            ),
            const SizedBox(height: 10.0),
            Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10.0),
                    child: CircleAvt(
                      image: AppAssets.baseUrl + profileBloc.account.avatar,
                      radius: 35.0,
                      onTap: () => Navigator.pushNamed(
                        context,
                        RouteName.image,
                        arguments: [profileBloc.account.avatar],
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          profileBloc.account.userName,
                          style: theme.textTheme.headline6,
                        ),
                        Text(
                          profileBloc.account.firstName +
                              profileBloc.account.lastName,
                          style: theme.textTheme.bodyText2,
                        ),
                        Row(
                          children: [
                            BlocBuilder<FollowUserCubit, bool>(
                              bloc: followCubit,
                              builder: (_, status) => Expanded(
                                flex: 3,
                                child: FollowByUser(
                                  widthFollow: sizeScreen * 10,
                                  widthUnFollow: sizeScreen * 12,
                                  padding: 10,
                                  contentPadding: 7,
                                  isFollow: status,
                                  onTap: () => followCubit.change(status),
                                ),
                              ),
                            ),
                            const Spacer(),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 35.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        profileBloc.account.numOfPostByUser.toString(),
                        style: theme.textTheme.headline5,
                      ),
                      Text(lang.locaized('post')),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        profileBloc.account.numOfFollowing.toString(),
                        style: theme.textTheme.headline5,
                      ),
                      Text(lang.locaized('following')),
                    ],
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        profileBloc.account.numOfFollower.toString(),
                        style: theme.textTheme.headline5,
                      ),
                      Text(lang.locaized('follower')),
                    ],
                  ),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: sizeScreen * 2.0,
                vertical: sizeScreen * 2.0,
              ),
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  profileBloc.account.bio,
                  style: theme.textTheme.bodyText1.copyWith(
                    fontWeight: AppFontWeight.regular,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
