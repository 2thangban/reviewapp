import 'package:bloc/bloc.dart';

import '../../../../../../models/province/province.dart';

class AddressCubit extends Cubit<Province> {
  AddressCubit(Province province) : super(province);

  void change(Province newProvince) => emit(newProvince);
}
