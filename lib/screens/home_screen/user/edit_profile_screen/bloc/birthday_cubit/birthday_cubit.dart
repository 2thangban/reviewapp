import 'package:bloc/bloc.dart';

import '../../../../../../commons/helper/app_format.dart';

class BirthdayCubit extends Cubit<String> {
  BirthdayCubit(String date) : super(date);

  void change(String newDate) => emit(AppFormat.dateToString(
        AppFormat.stringToDate(newDate, isTime: false),
        showTime: false,
      ));
}
