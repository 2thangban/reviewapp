import 'package:bloc/bloc.dart';

import '../../../../../../models/user/user.dart';

class EditFormCubit extends Cubit<bool> {
  Account account;
  EditFormCubit(this.account) : super(false);

  void check(Account newAccount) => emit(
        (this.account != newAccount) &&
            newAccount.lastName.isNotEmpty &&
            newAccount.firstName.isNotEmpty &&
            newAccount.userName.isNotEmpty &&
            newAccount.lastName.isNotEmpty,
      );
}
