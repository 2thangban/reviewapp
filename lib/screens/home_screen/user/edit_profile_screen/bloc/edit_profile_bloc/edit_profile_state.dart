part of 'edit_profile_bloc.dart';

abstract class EditProfileState {}

class EditProfileInitial extends EditProfileState {}

class EditProfileLoading extends EditProfileState {}

class EditProfileSuccess extends EditProfileState {
  final Account account;
  EditProfileSuccess(this.account);
}

class EditProfileFailed extends EditProfileState {
  final String mgs;
  EditProfileFailed(this.mgs);
}
