import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/user.dart';
import '../../../../../../models/user/user_repo.dart';

part 'edit_profile_event.dart';
part 'edit_profile_state.dart';

class EditProfileBloc extends Bloc<EditProfileEvent, EditProfileState> {
  Account account;
  EditProfileBloc(this.account) : super(EditProfileInitial());

  @override
  Stream<EditProfileState> mapEventToState(EditProfileEvent event) async* {
    if (event is EditProfile) {
      try {
        yield EditProfileLoading();
        final result = await AccountRepo.editprofile(
          event.account,
          image: event.image,
        );
        yield EditProfileSuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Account account;
          await TokenRepo.refreshToken().then((value) async {
            TokenRepo.write(value);
            account = await AccountRepo.editprofile(
              event.account,
              image: event.image,
            );
          });
          yield EditProfileSuccess(account);
        } else {
          yield EditProfileFailed(e.toString());
        }
      }
    }
  }
}
