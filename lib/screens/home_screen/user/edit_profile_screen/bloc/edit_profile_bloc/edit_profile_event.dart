part of 'edit_profile_bloc.dart';

abstract class EditProfileEvent {}

class EditProfile extends EditProfileEvent {
  final Account account;
  final File image;
  EditProfile(this.account, {this.image});
}
