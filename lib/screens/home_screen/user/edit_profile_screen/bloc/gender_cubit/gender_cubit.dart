import 'package:bloc/bloc.dart';

class GenderCubit extends Cubit<String> {
  GenderCubit(String gender) : super(gender);

  void change(String newGender) => emit(newGender);
}
