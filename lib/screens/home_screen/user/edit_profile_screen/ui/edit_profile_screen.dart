import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/helper/validator.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../../../app_common_bloc/province_bloc/province_bloc.dart';
import '../../edit_pass_screen.dart/bloc/pasword_bloc/password_bloc.dart';
import '../bloc/address_cubit/address_cubit.dart';
import '../bloc/birthday_cubit/birthday_cubit.dart';
import '../bloc/edit_form_cubit/edit_form_cubit.dart';
import '../bloc/edit_profile_bloc/edit_profile_bloc.dart';
import '../bloc/gender_cubit/gender_cubit.dart';
import 'more_info.dart';

class EditProfileScreen extends StatefulWidget {
  final Account account;
  EditProfileScreen(this.account);
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  Account account;
  TextEditingController _firstNameController;
  TextEditingController _lastNameController;
  TextEditingController _userNameController;

  EditProfileBloc editProfileBloc;
  EditFormCubit editFormCubit;

  File newImage;
  bool isLoadingForm = false;
  bool isLoadingAvatar = false;
  AppLocationBloc appLocationBloc;
  ProvinceBloc provinceBloc;
  @override
  void initState() {
    super.initState();
    account = widget.account.clone();
    _firstNameController = new TextEditingController(text: account.firstName);
    _lastNameController = new TextEditingController(text: account.lastName);
    _userNameController = new TextEditingController(text: account.userName);
    editFormCubit = EditFormCubit(widget.account);
    editProfileBloc = EditProfileBloc(account);
    appLocationBloc = BlocProvider.of<AppLocationBloc>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final PasswordBloc passwordBloc = PasswordBloc();
    final GenderCubit genderCubit = GenderCubit(
        editProfileBloc.account.gender == 0
            ? lang.locaized('male')
            : lang.locaized('feMale'));
    final BirthdayCubit birthdayCubit =
        BirthdayCubit(editProfileBloc.account.customDate);
    final AddressCubit addressCubit =
        AddressCubit(editProfileBloc.account.province);
    final sizeScreen = SizedConfig.heightMultiplier;
    final sizeText = SizedConfig.textMultiplier;
    return MultiBlocProvider(
      providers: [
        BlocProvider<GenderCubit>(create: (_) => genderCubit),
        BlocProvider<BirthdayCubit>(create: (_) => birthdayCubit),
        BlocProvider<AddressCubit>(create: (_) => addressCubit),
        BlocProvider<EditFormCubit>(create: (_) => editFormCubit),
        BlocProvider<EditProfileBloc>(create: (_) => editProfileBloc),
      ],
      child: Scaffold(
        appBar: CommonAppBar(
          leftBarButtonItem: AppButton.icon(
            icon: AppIcon.popNavigate,
            onTap: () => Navigator.pop(context, null),
          ),
          rightBarButtonItems: [
            BlocConsumer<EditProfileBloc, EditProfileState>(
              listener: (context, state) {
                if (state is EditProfileLoading) {
                  isLoadingForm = !isLoadingForm;
                } else if (state is EditProfileSuccess) {
                  isLoadingForm = !isLoadingForm;
                  AppShower.showFlushBar(
                    AppIcon.successCheck,
                    context,
                    message: lang.locaized('successUpdateInfoUser'),
                    theme: theme,
                    iconColor: AppColors.sussess,
                  );
                  Future.delayed(
                    Duration(milliseconds: 2200),
                    () => Navigator.pop(context, state.account),
                  );
                } else if (state is EditProfileFailed) {
                  isLoadingForm = !isLoadingForm;
                  AppShower.showFlushBar(
                    AppIcon.failedCheck,
                    context,
                    message: lang.locaized('errorUpdateInfoUser'),
                    theme: theme,
                    iconColor: AppColors.errorColor,
                  );
                }
              },
              builder: (_, __) => BlocBuilder<EditFormCubit, bool>(
                bloc: editFormCubit,
                builder: (_, check) => Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: AppButton.common(
                    width: sizeScreen * 8,
                    contentPadding: sizeScreen * 0.5,
                    labelStyle: theme.textTheme.bodyText1.copyWith(
                      color: check
                          ? theme.backgroundColor
                          : AppColors.subLightColor.withOpacity(0.7),
                    ),
                    shadowColor: theme.backgroundColor,
                    backgroundColor: check
                        ? theme.primaryColor
                        : AppColors.backgroundSearchColor,
                    loadingWidget: isLoadingForm
                        ? SizedBox(
                            height: sizeScreen * 2.5,
                            width: sizeScreen * 2.5,
                            child: CircularProgressIndicator(
                              strokeWidth: 3,
                              backgroundColor: theme.backgroundColor,
                            ),
                          )
                        : null,
                    labelText: lang.locaized('saveButton'),
                    radius: 10.0,
                    onPressed: () => check
                        ? editProfileBloc.add(
                            EditProfile(
                              account,
                              image: newImage,
                            ),
                          )
                        : null,
                  ),
                ),
              ),
            ),
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    lang.locaized('lastName'),
                    style: theme.textTheme.bodyText1
                        .copyWith(fontSize: sizeText * 2.1),
                  ),
                ),
                AppTextField.common(
                  controller: _firstNameController,
                  hintText: lang.locaized('isNotUpdated'),
                  hintStyle: theme.textTheme.bodyText2.copyWith(
                    fontSize: sizeText * 2.0,
                    fontWeight: AppFontWeight.regular,
                  ),
                  backgroundColor: AppColors.backgroundSearchColor,
                  radius: 10.0,
                  onChange: (String input) {
                    editProfileBloc.account.firstName = input;
                    editFormCubit.check(editProfileBloc.account);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    lang.locaized('firstName'),
                    style: theme.textTheme.bodyText1
                        .copyWith(fontSize: sizeText * 2.1),
                  ),
                ),
                AppTextField.common(
                  controller: _lastNameController,
                  hintText: Validator.isEmptyString(_lastNameController.text)
                      ? lang.locaized('isNotUpdated')
                      : null,
                  hintStyle: theme.textTheme.bodyText2.copyWith(
                    fontSize: sizeText * 2.0,
                    fontWeight: AppFontWeight.regular,
                  ),
                  backgroundColor: AppColors.backgroundSearchColor,
                  radius: 10.0,
                  onChange: (String input) {
                    editProfileBloc.account.lastName = input;
                    editFormCubit.check(editProfileBloc.account);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    "User Name",
                    style: theme.textTheme.bodyText1
                        .copyWith(fontSize: sizeText * 2.1),
                  ),
                ),
                AppTextField.common(
                  hintText: Validator.isEmptyString(_userNameController.text)
                      ? lang.locaized('isNotUpdated')
                      : null,
                  controller: _userNameController,
                  hintStyle: theme.textTheme.bodyText1.copyWith(
                    fontSize: sizeText * 2.0,
                    fontWeight: AppFontWeight.regular,
                  ),
                  backgroundColor: AppColors.backgroundSearchColor,
                  radius: 10.0,
                  onChange: (String input) {
                    editProfileBloc.account.userName = input;
                    editFormCubit.check(editProfileBloc.account);
                  },
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 10.0),
                  child: Text(
                    lang.locaized('password'),
                    style: theme.textTheme.bodyText1
                        .copyWith(fontSize: sizeText * 2.1),
                  ),
                ),
                BlocBuilder<PasswordBloc, PasswordState>(
                  bloc: passwordBloc,
                  builder: (_, __) => GestureDetector(
                    onTap: () async {
                      final isChangePass = await Navigator.pushNamed(
                          context, RouteName.editPass);
                      if ((isChangePass as bool)) {
                        Future.delayed(
                          Duration(milliseconds: 300),
                          () {
                            widget.account.passIsNull = false;
                            passwordBloc.add(UpDatePassFieldEvent());
                            AppShower.showFlushBar(
                              AppIcon.successCheck,
                              context,
                              message:
                                  lang.locaized('messageChangePassSuccess'),
                              theme: theme,
                              iconColor: AppColors.sussess,
                            );
                          },
                        );
                      }
                    },
                    child: Container(
                      alignment: Alignment.centerLeft,
                      padding: const EdgeInsets.all(15.0),
                      decoration: BoxDecoration(
                        color: AppColors.backgroundSearchColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: Text(
                        widget.account.passIsNull == false
                            ? '********'
                            : lang.locaized('isNotUpdated'),
                        style: widget.account.passIsNull == false
                            ? theme.textTheme.bodyText1.copyWith(
                                fontSize: sizeText * 2.0,
                                fontWeight: AppFontWeight.regular,
                              )
                            : theme.textTheme.bodyText2.copyWith(
                                fontSize: sizeText * 2.0,
                                fontWeight: AppFontWeight.regular,
                              ),
                      ),
                    ),
                  ),
                ),
                const MoreInfoAccount(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
