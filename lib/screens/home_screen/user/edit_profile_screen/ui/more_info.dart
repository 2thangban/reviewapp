import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/animation/transaction_anim.dart';
import '../../../../../commons/helper/app_date_picker.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/province/province.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../../../app_common_bloc/province_bloc/province_bloc.dart';
import '../bloc/address_cubit/address_cubit.dart';
import '../bloc/birthday_cubit/birthday_cubit.dart';
import '../bloc/edit_form_cubit/edit_form_cubit.dart';
import '../bloc/edit_profile_bloc/edit_profile_bloc.dart';
import '../bloc/gender_cubit/gender_cubit.dart';

class MoreInfoAccount extends StatefulWidget {
  const MoreInfoAccount({Key key}) : super(key: key);
  @override
  _MoreInfoAccountState createState() => _MoreInfoAccountState();
}

class _MoreInfoAccountState extends State<MoreInfoAccount> {
  final sizeScreen = SizedConfig.heightMultiplier;
  GenderCubit genderCubit;
  AddressCubit addressCubit;
  BirthdayCubit birthdayCubit;
  EditFormCubit editFormCubit;
  EditProfileBloc editProfileBloc;
  ProvinceBloc provinceBloc;
  AppLocationBloc appLocationBloc;
  @override
  void initState() {
    super.initState();
    genderCubit = BlocProvider.of<GenderCubit>(context);
    birthdayCubit = BlocProvider.of<BirthdayCubit>(context);
    addressCubit = BlocProvider.of<AddressCubit>(context);
    editFormCubit = BlocProvider.of<EditFormCubit>(context);
    editProfileBloc = BlocProvider.of<EditProfileBloc>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
    appLocationBloc = BlocProvider.of<AppLocationBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ExpansionTile(
        expandedCrossAxisAlignment: CrossAxisAlignment.start,
        tilePadding: EdgeInsets.zero,
        title: Text(
          lang.locaized('labelMoreInfo'),
          style: theme.textTheme.headline4,
        ),
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              lang.locaized('titleGender'),
              style: theme.textTheme.headline6,
            ),
          ),
          BlocBuilder<GenderCubit, String>(
            bloc: genderCubit,
            builder: (_, gender) => GestureDetector(
              child: GestureDetector(
                onTap: () async => showGeneralDialog(
                  barrierDismissible: true,
                  barrierLabel: '',
                  context: context,
                  transitionDuration: Duration(milliseconds: 200),
                  transitionBuilder:
                      (_, _animation, _secondaryAnimation, child) =>
                          TransactionAnima.shink(
                              _animation, _secondaryAnimation, child),
                  pageBuilder: (_, _animation, _secondaryAnimation) =>
                      SimpleDialog(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15.0)),
                    title: Text(
                      lang.locaized('titleGender'),
                      style: theme.textTheme.headline5,
                    ),
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                          horizontal: 50.0,
                          vertical: 10.0,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: List.generate(
                            2,
                            (index) => GestureDetector(
                              onTap: () {
                                editProfileBloc.account.gender = index;
                                editFormCubit.check(editProfileBloc.account);
                                if (index == 0) {
                                  genderCubit.change(lang.locaized('male'));
                                  Navigator.pop(context);
                                } else {
                                  genderCubit.change('feMale');
                                  Navigator.pop(context);
                                }
                              },
                              child: Container(
                                alignment: Alignment.center,
                                width: sizeScreen * 9,
                                padding: const EdgeInsets.symmetric(
                                  vertical: 10.0,
                                  horizontal: 15.0,
                                ),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(7.0),
                                  border: Border.all(
                                    color: AppColors.subLightColor,
                                  ),
                                ),
                                child: Text(
                                  index == 0
                                      ? lang.locaized('male')
                                      : lang.locaized('feMale'),
                                  style: theme.textTheme.bodyText1.copyWith(
                                    fontWeight: AppFontWeight.regular,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                child: Container(
                  alignment: Alignment.centerLeft,
                  padding: const EdgeInsets.all(15.0),
                  decoration: BoxDecoration(
                    color: AppColors.backgroundSearchColor,
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Text(
                    gender,
                    style: theme.textTheme.headline6.copyWith(
                      fontWeight: AppFontWeight.regular,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              lang.locaized('birthDay'),
              style: theme.textTheme.headline6,
            ),
          ),
          BlocBuilder<BirthdayCubit, String>(
            bloc: birthdayCubit,
            builder: (_, birthday) => GestureDetector(
              onTap: () async {
                final newDate = await AppDatePicker.getDate(context);
                if (newDate != '') {
                  editProfileBloc.account.birthDay = newDate;
                  editFormCubit.check(editProfileBloc.account);
                  birthdayCubit.change(newDate);
                }
              },
              child: Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  color: AppColors.backgroundSearchColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  birthday != '' ? birthday : lang.locaized('isNotUpdated'),
                  style: birthday != ''
                      ? theme.textTheme.bodyText1.copyWith(
                          fontWeight: AppFontWeight.regular,
                        )
                      : theme.textTheme.bodyText2.copyWith(
                          fontWeight: AppFontWeight.regular,
                        ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 10.0),
            child: Text(
              lang.locaized('area'),
              style: theme.textTheme.headline6,
            ),
          ),
          BlocBuilder<AddressCubit, Province>(
            bloc: addressCubit,
            builder: (_, province) => GestureDetector(
              onTap: () async => showGeneralDialog(
                context: context,
                barrierDismissible: true,
                barrierLabel: '',
                transitionDuration: Duration(milliseconds: 300),
                transitionBuilder:
                    (_, _animation, _secondaryAnimation, child) =>
                        TransactionAnima.fromRight(
                            _animation, _secondaryAnimation, child),
                pageBuilder: (_, _animation, _secondaryAnimation) => Dialog(
                  elevation: 0.0,
                  insetAnimationDuration: Duration(milliseconds: 300),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  insetPadding: const EdgeInsets.all(0.0),
                  child: Scaffold(
                    appBar: CommonAppBar(
                      leftBarButtonItem: AppButton.icon(
                        icon: AppIcon.popNavigate,
                        onTap: () => Navigator.pop(context, null),
                      ),
                      titleWidget: Text(
                        lang.locaized('chooseArea'),
                        style: theme.textTheme.headline5,
                      ),
                      centerTitle: true,
                    ),
                    body: SingleChildScrollView(
                      padding: const EdgeInsets.symmetric(
                        vertical: 10.0,
                        horizontal: 10.0,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              lang.locaized('TopArea'),
                              style: theme.textTheme.headline6,
                            ),
                          ),
                          GridView.count(
                            mainAxisSpacing: 10.0,
                            crossAxisSpacing: 10.0,
                            crossAxisCount: 3,
                            childAspectRatio: 5 / 2,
                            padding: const EdgeInsets.only(top: 10.0),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: provinceBloc.boxProvince.values
                                .map(
                                  (e) => MaterialButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      side: BorderSide(
                                        color: AppColors.subLightColor,
                                      ),
                                    ),
                                    elevation: 0.0,
                                    color: theme.backgroundColor,
                                    onPressed: () {
                                      editProfileBloc.account.province = e;
                                      editFormCubit
                                          .check(editProfileBloc.account);
                                      addressCubit.change(e);
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      e.name,
                                      style: theme.textTheme.bodyText1.copyWith(
                                        fontWeight: AppFontWeight.regular,
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10.0),
                            child: Text(
                              lang.locaized('AllArea'),
                              style: theme.textTheme.headline6.copyWith(),
                            ),
                          ),
                          GridView.count(
                            mainAxisSpacing: 10.0,
                            crossAxisSpacing: 10.0,
                            crossAxisCount: 3,
                            childAspectRatio: 5 / 2,
                            padding: const EdgeInsets.only(top: 10.0),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            children: appLocationBloc.listProvince
                                .map(
                                  (e) => MaterialButton(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      side: BorderSide(
                                        color: AppColors.subLightColor,
                                      ),
                                    ),
                                    elevation: 0.0,
                                    color: theme.backgroundColor,
                                    onPressed: () {
                                      editProfileBloc.account.province = e;
                                      editFormCubit
                                          .check(editProfileBloc.account);
                                      addressCubit.change(e);
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      e.name,
                                      style: theme.textTheme.bodyText1.copyWith(
                                        fontWeight: AppFontWeight.regular,
                                      ),
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              child: Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  color: AppColors.backgroundSearchColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  province?.name ?? lang.locaized('isNotUpdated'),
                  style: province?.name != null
                      ? theme.textTheme.bodyText1.copyWith(
                          fontWeight: AppFontWeight.regular,
                        )
                      : theme.textTheme.bodyText2.copyWith(
                          fontWeight: AppFontWeight.regular,
                        ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 5),
        ],
      ),
    );
  }
}
