import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../commons/helper/validator.dart';
import '../../../../../../models/token/token.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/user_repo.dart';

part 'password_event.dart';
part 'password_state.dart';

class PasswordBloc extends Bloc<PasswordEvent, PasswordState> {
  PasswordBloc() : super(PasswordInitial());

  @override
  Stream<PasswordState> mapEventToState(PasswordEvent event) async* {
    if (event is ChangePasswordEvent) {
      try {
        yield ChangePasswordLoading();
        final result = await AccountRepo.changePassword(event.body);
        final Token token = Token(
          accessToken: result.accessToken,
          refreshToken: result.refreshToken,
        );
        TokenRepo.write(token);
        yield ChangePasswordSuccess();
      } catch (e) {
        yield ChangePasswordFailed(mgs: e.message);
      }
    } else if (event is UpDatePassFieldEvent) {
      yield ChangePasswordSuccess();
    } else if (event is ValidPassFormEvent) {
      if (Validator.isValidPass(event.currentPass)) {
        yield (Validator.isEmptyString(event.currentPass))
            ? ChangePasswordFailed(mgs: "Mật khẩu cũ không để trống !")
            : ChangePasswordFailed(mgs: "Mật khẩu cũ phải đúng 8 ký tự !");
      } else if (Validator.isValidPass(event.newPass)) {
        yield (Validator.isEmptyString(event.newPass))
            ? ChangePasswordFailed(mgs: "Mật khẩu mới không để trống !")
            : ChangePasswordFailed(mgs: "Mật khẩu mới phải đúng 8 ký tự !");
      } else if (Validator.isValidPass(event.confirmPass)) {
        yield (Validator.isEmptyString(event.confirmPass))
            ? ChangePasswordFailed(mgs: "Nhập lại mật khẩu không để trống !")
            : ChangePasswordFailed(
                mgs: "Nhập lại mật khẩu phải đúng 8 ký tự !");
      } else if (!Validator.isConfirmString(
        event.newPass,
        event.confirmPass,
      )) {
        yield ChangePasswordFailed(mgs: "2 mật khẩu mới không giống nhau !");
      } else
        yield ValidPassFormSuccess(<String, String>{
          'old_pass': event.currentPass,
          'new_pass': event.newPass,
          'confirm_pass': event.confirmPass,
        });
    }
  }
}
