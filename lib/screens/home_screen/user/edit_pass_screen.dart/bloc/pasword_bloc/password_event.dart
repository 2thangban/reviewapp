part of 'password_bloc.dart';

@immutable
abstract class PasswordEvent {}

class ChangePasswordEvent extends PasswordEvent {
  final Map<String, String> body;
  ChangePasswordEvent(this.body);
}

class UpDatePassFieldEvent extends PasswordEvent {}

class ValidPassFormEvent extends PasswordEvent {
  final String currentPass;
  final String newPass;
  final String confirmPass;

  ValidPassFormEvent({
    @required this.currentPass,
    @required this.newPass,
    @required this.confirmPass,
  });
}
