part of 'password_bloc.dart';

@immutable
abstract class PasswordState {}

class PasswordInitial extends PasswordState {}

class ChangePasswordLoading extends PasswordState {}

class ChangePasswordSuccess extends PasswordState {}

class ValidPassFormSuccess extends PasswordState {
  final Map<String, String> passJson;
  ValidPassFormSuccess(this.passJson);
}

class ChangePasswordFailed extends PasswordState {
  final String mgs;
  ChangePasswordFailed({this.mgs});
}
