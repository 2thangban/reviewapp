import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../theme.dart';
import '../bloc/pasword_bloc/password_bloc.dart';

class EditPassScreen extends StatefulWidget {
  const EditPassScreen({Key key}) : super(key: key);

  @override
  _EditPassScreenState createState() => _EditPassScreenState();
}

class _EditPassScreenState extends State<EditPassScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  final TextEditingController _oldPassController = new TextEditingController();
  final TextEditingController _newPassController = new TextEditingController();
  final TextEditingController _confirmPassController =
      new TextEditingController();

  final PasswordBloc passwordBloc = PasswordBloc();

  bool isValid = false;
  bool isLoading = false;

  @override
  void dispose() {
    super.dispose();
    passwordBloc.close();
    _oldPassController.dispose();
    _newPassController.dispose();
    _confirmPassController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final focusScope = FocusScope.of(context);
    return Scaffold(
      backgroundColor: AppColors.backgroundSearchColor,
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () {
            focusScope.unfocus();
            Future.delayed(
              const Duration(milliseconds: 200),
              () => Navigator.pop(context, false),
            );
          },
        ),
        titleWidget: Text(
          'Đổi mật khẩu',
          style: theme.textTheme.headline4,
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          children: [
            AppTextField.common(
              hintText: 'Mật khẩu cũ',
              controller: _oldPassController,
              hintStyle: theme.textTheme.bodyText2.copyWith(
                fontSize: sizeText * 2.0,
                fontWeight: AppFontWeight.regular,
              ),
              isPassWord: true,
              backgroundColor: theme.backgroundColor,
              radius: 10.0,
            ),
            const SizedBox(height: 2.0),
            AppTextField.common(
              hintText: 'Mật khẩu mới',
              isPassWord: true,
              controller: _newPassController,
              hintStyle: theme.textTheme.bodyText2.copyWith(
                fontSize: sizeText * 2.0,
                fontWeight: AppFontWeight.regular,
              ),
              backgroundColor: theme.backgroundColor,
              radius: 10.0,
            ),
            const SizedBox(height: 2.0),
            AppTextField.common(
              hintText: 'Nhập lại mật khẩu mới',
              isPassWord: true,
              controller: _confirmPassController,
              hintStyle: theme.textTheme.bodyText2.copyWith(
                fontSize: sizeText * 2.0,
                fontWeight: AppFontWeight.regular,
              ),
              backgroundColor: theme.backgroundColor,
              radius: 10.0,
            ),
            const SizedBox(height: 20.0),
            BlocConsumer<PasswordBloc, PasswordState>(
              bloc: passwordBloc,
              listener: (context, state) {
                if (state is ChangePasswordSuccess) {
                  isLoading = false;
                  Future.delayed(
                    Duration(milliseconds: 300),
                    () => Navigator.pop(context, true),
                  );
                } else if (state is ValidPassFormSuccess) {
                  passwordBloc.add(ChangePasswordEvent(state.passJson));
                } else if (state is ChangePasswordFailed) {
                  isLoading = false;
                  AppShower.showFlushBar(
                    AppIcon.failedCheck,
                    context,
                    message: state.mgs,
                    theme: theme,
                    iconColor: AppColors.errorColor,
                  );
                } else if (state is ChangePasswordLoading) {
                  isLoading = true;
                }
              },
              builder: (_, __) => AppButton.common(
                labelText: 'Hoàn tất',
                labelStyle: theme.textTheme.headline6.copyWith(
                  color: theme.backgroundColor,
                ),
                loadingWidget: isLoading
                    ? SizedBox(
                        height: sizeScreen * 3,
                        width: sizeScreen * 3,
                        child: CircularProgressIndicator(
                          backgroundColor: theme.backgroundColor,
                          strokeWidth: 2.5,
                        ),
                      )
                    : null,
                contentPadding: 10.0,
                radius: 5.0,
                backgroundColor:
                    isLoading ? AppColors.disibleColor : AppColors.primaryColor,
                shadowColor: theme.backgroundColor,
                onPressed: () {
                  if (!isValid) {
                    focusScope.unfocus();
                    isValid = !isValid;
                    passwordBloc.add(
                      ValidPassFormEvent(
                          currentPass: _oldPassController.text,
                          newPass: _newPassController.text,
                          confirmPass: _confirmPassController.text),
                    );
                    Future.delayed(
                      Duration(milliseconds: 1800),
                      () => isValid = !isValid,
                    );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
