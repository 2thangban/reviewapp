part of 'get_list_top_user_bloc.dart';

enum ListTopUserStatus { initial, loading, success, failure }

class GetListTopUserState extends Equatable {
  final ListTopUserStatus status;
  final List<Account> list;
  final bool isLastPage;
  const GetListTopUserState({
    this.status = ListTopUserStatus.initial,
    this.list = const <Account>[],
    this.isLastPage = false,
  });

  @override
  List<Object> get props => [status, list, isLastPage];

  GetListTopUserState cloneWith({
    ListTopUserStatus status,
    List<Account> list,
    bool isLastPage,
  }) =>
      GetListTopUserState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );
}
