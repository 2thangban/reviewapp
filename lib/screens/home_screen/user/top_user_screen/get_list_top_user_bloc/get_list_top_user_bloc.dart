import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/user/user.dart';
import '../../../../../models/user/user_repo.dart';

part 'get_list_top_user_event.dart';
part 'get_list_top_user_state.dart';

class GetListTopUserBloc
    extends Bloc<GetListTopUserEvent, GetListTopUserState> {
  GetListTopUserBloc() : super(GetListTopUserState());
  int page = 0;
  final int limit = 10;
  @override
  Stream<GetListTopUserState> mapEventToState(
      GetListTopUserEvent event) async* {
    if (event is GetListTopUser) {
      if (event.isRefresh) {
        page = 0;
        state.list.clear();
      }
      yield state.cloneWith(status: ListTopUserStatus.loading);
      try {
        final result = await AccountRepo.getTopUser(page: page++, limit: limit);
        sleep(Duration(milliseconds: 200));
        yield state.cloneWith(
          status: ListTopUserStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } catch (e) {
        yield state.cloneWith(status: ListTopUserStatus.failure);
      }
    }
  }
}
