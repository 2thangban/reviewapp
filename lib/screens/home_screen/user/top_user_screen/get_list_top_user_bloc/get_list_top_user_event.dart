part of 'get_list_top_user_bloc.dart';

abstract class GetListTopUserEvent {
  const GetListTopUserEvent();
}

class GetListTopUser extends GetListTopUserEvent {
  final bool isRefresh;
  GetListTopUser({this.isRefresh = false});
}
