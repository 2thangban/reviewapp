import 'package:flutter/material.dart';

import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/widgets/circle_avt.dart';
import '../../../../../commons/widgets/follow_user_widget.dart';
import '../../../../../models/user/user.dart';

class ItemTopUser extends StatelessWidget {
  final Account account;
  const ItemTopUser({Key key, this.account})
      : assert(account != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              dense: true,
              horizontalTitleGap: 10.0,
              minLeadingWidth: 1.0,
              leading: CircleAvt(
                image: AppAssets.baseUrl + account.avatar,
                radius: 25.0,
              ),
              title: Text(account.userName, style: theme.textTheme.bodyText1),
              subtitle: SizedBox(
                width: MediaQuery.of(context).size.width / 3,
                child: FollowByUser(
                  widthFollow: 10,
                  widthUnFollow: 10,
                  contentPadding: 10,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: account.listImagePost
                    .map(
                      (e) => Container(
                        margin: EdgeInsets.only(right: 5.0),
                        height: 80.0,
                        width: 80.0,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        clipBehavior: Clip.antiAlias,
                        child: Image.network(
                          AppAssets.baseUrl + e,
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                    .toList(),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
