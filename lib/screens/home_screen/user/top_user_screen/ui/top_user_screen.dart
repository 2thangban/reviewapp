import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_bar.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_loading.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../theme.dart';
import '../get_list_top_user_bloc/get_list_top_user_bloc.dart';
import 'item_top_user.dart';

class TopUserScreen extends StatefulWidget {
  const TopUserScreen({Key key}) : super(key: key);
  @override
  _TopUserScreenState createState() => _TopUserScreenState();
}

class _TopUserScreenState extends State<TopUserScreen> {
  final double sizeScreen = SizedConfig.heightMultiplier;
  final GetListTopUserBloc topUserBloc = GetListTopUserBloc();
  final ScrollController _listUserController = ScrollController();
  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration(milliseconds: 250),
      () => topUserBloc.add(GetListTopUser()),
    );
    _listUserController.addListener(() {
      if (_listUserController.position.pixels ==
              _listUserController.position.maxScrollExtent &&
          !topUserBloc.state.isLastPage) {
        topUserBloc.add(GetListTopUser());
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    topUserBloc.close();
    _listUserController.dispose();
  }

  int getLastMonth() {
    final currentTime = DateTime.now();
    final previousMounth = currentTime.month - 1;
    return previousMounth == 0 ? 12 : previousMounth;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      backgroundColor: theme.primaryColor,
      appBar: CommonAppBar(
        statusBarColor: theme.primaryColor,
        backgroundColor: Colors.transparent,
        leftBarButtonItem: null,
        rightBarButtonItems: [
          AppButton.icon(
            icon: AppIcon.exitNavigate,
            iconColor: theme.backgroundColor,
            iconSize: sizeScreen * 3,
            onTap: () => Navigator.pop(context),
          ),
        ],
      ),
      body: Container(
        color: theme.primaryColor,
        child: RefreshIndicator(
          onRefresh: () async {
            topUserBloc.add(GetListTopUser(isRefresh: true));
          },
          child: SingleChildScrollView(
            controller: _listUserController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 50.0, bottom: 10.0),
                        child: Icon(
                          AppIcon.topUser,
                          size: 30.0,
                          color: AppColors.lightThemeColor,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          lang.locaized('topUser'),
                          style: theme.textTheme.headline5.copyWith(
                            color: AppColors.lightThemeColor,
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: Text(
                          lang.locaized('month') +
                              " " +
                              getLastMonth().toString(),
                          style: theme.textTheme.headline3.copyWith(
                            color: AppColors.lightThemeColor,
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 15.0,
                          horizontal: 6.0,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.orange[400],
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Text(
                          lang.locaized('descriptionTopUser'),
                          style: theme.textTheme.bodyText1.copyWith(
                            color: AppColors.lightThemeColor,
                            fontWeight: AppFontWeight.regular,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                BlocBuilder<GetListTopUserBloc, GetListTopUserState>(
                  bloc: topUserBloc,
                  buildWhen: (previous, current) =>
                      current.status != ListTopUserStatus.loading,
                  builder: (_, state) {
                    if (state.status == ListTopUserStatus.initial) {
                      return Column(
                        children: [
                          AppLoading.threeBounce(
                            size: 20.0,
                            color: theme.backgroundColor,
                          ),
                          SizedBox(height: sizeScreen * 60),
                        ],
                      );
                    } else if (state.status == ListTopUserStatus.failure) {
                      ///
                    }
                    return ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.list.length,
                      itemBuilder: (context, index) =>
                          ItemTopUser(account: state.list[index]),
                    );
                  },
                ),
                BlocBuilder<GetListTopUserBloc, GetListTopUserState>(
                  bloc: topUserBloc,
                  builder: (_, state) =>
                      state.status == ListTopUserStatus.loading
                          ? AppLoading.threeBounce(
                              size: 20.0,
                              color: theme.backgroundColor,
                            )
                          : const SizedBox(),
                ),
                const SizedBox(height: 20),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
