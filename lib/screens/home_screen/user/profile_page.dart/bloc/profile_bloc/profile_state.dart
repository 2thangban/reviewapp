part of 'profile_bloc.dart';

@immutable
abstract class ProfileState {}

class GetProfileInitial extends ProfileState {}

class GetProfileLoading extends ProfileState {}

class GetProfileSuccess extends ProfileState {
  final Account account;
  GetProfileSuccess(this.account);
}

class GetProfileFailed extends ProfileState {}
