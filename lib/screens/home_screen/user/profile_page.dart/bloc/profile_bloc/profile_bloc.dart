import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/user.dart';
import '../../../../../../models/user/user_repo.dart';

part 'profile_event.dart';
part 'profile_state.dart';

class ProfileBloc extends Bloc<ProfileEvent, ProfileState> {
  ProfileBloc() : super(GetProfileInitial());

  @override
  Stream<ProfileState> mapEventToState(
    ProfileEvent event,
  ) async* {
    if (event is GetProfileEvent) {
      try {
        yield GetProfileLoading();
        final account = await AccountRepo.profile();
        yield GetProfileSuccess(account);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Account account;
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              account = await AccountRepo.profile();
            });
          });
          yield GetProfileSuccess(account);
        } else {
          yield GetProfileFailed();
        }
      }
    }
  }
}
