part of 'post_by_user_bloc.dart';

@immutable
abstract class PostByUserState {}

class PostByUserInitial extends PostByUserState {}

class PostByUserLoading extends PostByUserState {}

class PostByUserFailed extends PostByUserState {
  final String msg;
  PostByUserFailed({this.msg});
}

class PostByUserSuccess extends PostByUserState {
  final List<Post> posts;
  PostByUserSuccess(this.posts);
}
