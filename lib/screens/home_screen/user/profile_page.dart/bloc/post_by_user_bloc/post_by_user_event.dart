part of 'post_by_user_bloc.dart';

abstract class PostByUserEvent {}

class GetPostByUserEvent extends PostByUserEvent {
  final int page;
  final int limit;
  final int userId;
  GetPostByUserEvent({
    this.page,
    this.limit,
    this.userId,
  });
}
