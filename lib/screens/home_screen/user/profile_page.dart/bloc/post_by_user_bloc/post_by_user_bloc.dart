import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/post/post.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/user_repo.dart';

part 'post_by_user_event.dart';
part 'post_by_user_state.dart';

class PostByUserBloc extends Bloc<PostByUserEvent, PostByUserState> {
  bool isLastPage = false;
  PostByUserBloc() : super(PostByUserInitial());

  @override
  Stream<PostByUserState> mapEventToState(PostByUserEvent event) async* {
    if (event is GetPostByUserEvent) {
      try {
        if (state is PostByUserInitial) {
          final listPost = await AccountRepo.getAllPost(
            page: event.page,
            limit: event.limit,
            userId: event.userId,
            isAuth: true,
          );
          this.isLastPage = listPost.length < event.limit;
          yield PostByUserSuccess(listPost);
        } else {
          yield PostByUserLoading();
          final listPost = await AccountRepo.getAllPost(
            page: event.page,
            limit: event.limit,
            userId: event.userId,
            isAuth: true,
          );
          this.isLastPage = listPost.length < event.limit;
          yield PostByUserSuccess(listPost);
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Post> listPost;
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              listPost = await AccountRepo.getAllPost(
                page: event.page,
                limit: event.limit,
                userId: event.userId,
                isAuth: true,
              );
            });
          });
          this.isLastPage = listPost.length < event.limit;
          yield PostByUserSuccess(listPost);
        } else {
          yield PostByUserFailed(msg: e.toString());
        }
      }
    }
  }
}
