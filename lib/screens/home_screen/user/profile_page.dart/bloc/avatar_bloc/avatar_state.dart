part of 'avatar_bloc.dart';

@immutable
abstract class AvatarState {}

class AvatarInitial extends AvatarState {}

class AvatarLoading extends AvatarState {}

class AvatarSuccess extends AvatarState {
  final newURLImage;
  AvatarSuccess(this.newURLImage);
}

class AvatarLocalSuccess extends AvatarState {
  final String newURLImage;
  final File newImageUpload;
  AvatarLocalSuccess({this.newURLImage, this.newImageUpload});
}

class AvatarFailed extends AvatarState {
  final String msg;
  AvatarFailed({this.msg});
}
