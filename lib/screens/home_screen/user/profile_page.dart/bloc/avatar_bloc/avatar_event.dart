part of 'avatar_bloc.dart';

@immutable
abstract class AvatarEvent {}

class ChangeAvatarToAPIEvent extends AvatarEvent {
  final File imageUpload;
  ChangeAvatarToAPIEvent(this.imageUpload);
}

class ChangeAvatarFromLocalEvent extends AvatarEvent {
  final String newUrlImage;
  final File newImageUpload;
  ChangeAvatarFromLocalEvent({this.newImageUpload, this.newUrlImage});
}
