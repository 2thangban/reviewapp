import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../../api/api_response.dart';
import '../../../../../../models/token/token_repo.dart';
import '../../../../../../models/user/reponse_action_user.dart';
import '../../../../../../models/user/user_repo.dart';

part 'avatar_event.dart';
part 'avatar_state.dart';

class AvatarBloc extends Bloc<AvatarEvent, AvatarState> {
  String currentUrlImage;
  AvatarBloc(this.currentUrlImage) : super(AvatarInitial());

  @override
  Stream<AvatarState> mapEventToState(AvatarEvent event) async* {
    if (event is ChangeAvatarToAPIEvent) {
      try {
        final result = await AccountRepo.changeAvatar(event.imageUpload);
        yield AvatarSuccess(result.avatar);
      } on ErrorResponse catch (e) {
        ResponseActionUser result;
        if (e.staticCode == 401) {
          await TokenRepo.refreshToken().then((value) async {
            await TokenRepo.write(value);
            result = await AccountRepo.changeAvatar(event.imageUpload);
          });
          yield AvatarSuccess(result.avatar);
        } else {
          yield AvatarFailed(msg: e.toString());
        }
      }
    } else if (event is ChangeAvatarFromLocalEvent) {
      yield event.newImageUpload != null
          ? AvatarLocalSuccess(newImageUpload: event.newImageUpload)
          : AvatarLocalSuccess(newURLImage: event.newUrlImage);
    }
  }
}
