import 'package:bloc/bloc.dart';

import '../../../../../../models/user/user.dart';

class UpdateProfileCubit extends Cubit<Account> {
  Account account;
  UpdateProfileCubit(this.account) : super(account);

  void update(Account newAccount) => emit(this.account = newAccount);
}
