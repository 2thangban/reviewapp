import 'package:bloc/bloc.dart';

class BioCubit extends Cubit<String> {
  String bio;
  BioCubit(this.bio) : super(bio);

  void change(String newString) => emit(this.bio = newString);
}
