import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../../localizations/app_localization.dart';
import '../../../../../../models/post/post.dart';
import '../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../bloc/post_by_user_bloc/post_by_user_bloc.dart';
import 'item_post_by_user.dart';

class ListPostByUser extends StatefulWidget {
  final List<Post> listPostByUser;

  ListPostByUser(
    this.listPostByUser,
  );

  @override
  _ListPostByUserState createState() => _ListPostByUserState();
}

class _ListPostByUserState extends State<ListPostByUser>
    with AutomaticKeepAliveClientMixin {
  final PostByUserBloc _postByUserBloc = PostByUserBloc();
  final ScrollController _scrollController = ScrollController();
  final int _limit = 10;
  int _page = 0;
  AuthBloc _authBloc;
  bool _isLastPage = false;
  int _userId;
  @override
  void initState() {
    super.initState();
    _authBloc = BlocProvider.of<AuthBloc>(context);
    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent &&
            _isLastPage == false) {
          ///
        }
      },
    );
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    _userId = _authBloc.account.id;
    return RefreshIndicator(
      onRefresh: () async {
        _page = 0;
        Future.delayed(Duration(seconds: 2), () {
          _postByUserBloc.add(
            GetPostByUserEvent(
              page: _page++,
              limit: _limit,
              userId: _userId,
            ),
          );
        });
      },
      child: BlocBuilder<PostByUserBloc, PostByUserState>(
        bloc: _postByUserBloc,
        builder: (_, state) {
          if (state is PostByUserInitial) {
            _postByUserBloc.add(
              GetPostByUserEvent(
                page: _page++,
                limit: _limit,
                userId: _authBloc.account.id,
              ),
            );
          } else if (state is PostByUserFailed) {
            return Center(
              child: Text(AppLocalization.of(context).locaized('errorConnect')),
            );
          } else if (state is PostByUserSuccess) {
            widget.listPostByUser.addAll(state.posts);
            return ListView(
              controller: _scrollController,
              shrinkWrap: true,
              children: [
                Container(
                  child: StaggeredGridView.extentBuilder(
                    padding: const EdgeInsets.symmetric(horizontal: 15.0),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: widget.listPostByUser.length,
                    staggeredTileBuilder: (index) =>
                        StaggeredTile.extent(1, index.isEven ? 270 : 220),
                    maxCrossAxisExtent: 200,
                    itemBuilder: (context, index) =>
                        ItemListPostByUser(widget.listPostByUser[index]),
                  ),
                ),
              ],
            );
          }
          return AppShimmer.list();
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
