import 'dart:math';

import 'package:flutter/material.dart';

import '../../../../../../commons/helper/size_config.dart';
import '../../../../../../commons/untils/app_color.dart';
import '../../../../../../commons/untils/app_image.dart';
import '../../../../../../models/post/post.dart';
import '../../../../../../routes/route_name.dart';

class ItemListPostByUser extends StatelessWidget {
  final Post post;
  ItemListPostByUser(this.post);
  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    final ran = Random();
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Column(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: AppColors.listBgColor[ran.nextInt(4)],
                image: DecorationImage(
                  image: NetworkImage(
                    AppAssets.baseUrl + post.listImage.first,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: InkWell(
                onTap: () => Navigator.of(context).pushNamed(
                  RouteName.postDetail,
                  arguments: post.id,
                ),
                highlightColor: AppColors.primaryColor.withOpacity(0.1),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(sizeScreen * 0.7),
            child: Column(
              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    post.titlePost,
                    style: Theme.of(context).textTheme.bodyText1,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
