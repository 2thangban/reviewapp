import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/animation/fade_anim.dart';
import '../../../../../commons/animation/transaction_anim.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../../follow_user_screen/ui/follow_user_screen.dart';
import '../bloc/avatar_bloc/avatar_bloc.dart';
import '../bloc/bio_cubit/bio_cubit.dart';
import '../bloc/update_profile_cubit/update_profile_cubit.dart';

class HeaderInfoUser extends StatefulWidget {
  const HeaderInfoUser({Key key}) : super(key: key);
  @override
  _HeaderInfoUserState createState() => _HeaderInfoUserState();
}

class _HeaderInfoUserState extends State<HeaderInfoUser> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  BioCubit bioCubit;
  TextEditingController _bioController;
  // final AvatarBloc avatarBloc = AvatarBloc();
  AuthBloc authBloc;
  bool isLoading = false;

  /// check loading avatar

  AvatarBloc avatarBloc;
  UpdateProfileCubit updateProfileCubit;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    avatarBloc = BlocProvider.of<AvatarBloc>(context);
    updateProfileCubit = BlocProvider.of<UpdateProfileCubit>(context);
    bioCubit = BioCubit(updateProfileCubit.account.bio);
    _bioController = new TextEditingController(
      text: updateProfileCubit.account.bio,
    );
  }

  @override
  void dispose() {
    super.dispose();
    _bioController.dispose();
    bioCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return FadeAnim(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          BlocConsumer<AvatarBloc, AvatarState>(
            listener: (context, state) {
              if (state is AvatarLocalSuccess) {
                avatarBloc.currentUrlImage = state.newURLImage;
              } else if (state is AvatarLocalSuccess) {
                avatarBloc.currentUrlImage = state.newURLImage;
              } else if (state is AvatarSuccess) {
                isLoading = false;
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  message: lang.locaized('successUpdateAvatar'),
                  theme: theme,
                  iconColor: AppColors.sussess,
                );
                avatarBloc.currentUrlImage = state.newURLImage;
              } else if (state is AvatarLoading) {
                isLoading = true;
              } else if (state is AvatarFailed) {
                isLoading = false;
                AppShower.showFlushBar(
                  AppIcon.failedCheck,
                  context,
                  message: lang.locaized('errorUpdateAvatar'),
                  theme: theme,
                  iconColor: AppColors.errorColor,
                );
              }
            },
            builder: (_, state) => Stack(
              alignment: Alignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    top: 70.0,
                  ),
                  child: GestureDetector(
                    onTap: () async {
                      if (!isLoading) {
                        final File newImage = await AppShower.showOptionAvatar(
                          theme,
                          context,
                          showAvatar: [updateProfileCubit.account.avatar],
                        );
                        if (newImage?.path != null &&
                            newImage.path.isNotEmpty) {
                          avatarBloc.add(ChangeAvatarToAPIEvent(newImage));
                        }
                      }
                    },
                    child: CircleAvatar(
                      radius: sizeScreen * 5,
                      foregroundImage: NetworkImage(
                        AppAssets.baseUrl + avatarBloc.currentUrlImage,
                      ),
                      backgroundImage: AssetImage(AppAssets.img1),
                    ),
                  ),
                ),
                Visibility(
                  visible: isLoading,
                  child: Container(
                    height: sizeScreen * 3,
                    width: sizeScreen * 3,
                    child: CircularProgressIndicator(strokeWidth: 2.5),
                  ),
                ),
              ],
            ),
          ),
          BlocBuilder<UpdateProfileCubit, Account>(
            bloc: updateProfileCubit,
            builder: (context, state) {
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 15.0),
                child: Text(
                  updateProfileCubit.account.userName.toUpperCase(),
                  style: theme.textTheme.headline6,
                ),
              );
            },
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      updateProfileCubit.account.numOfPostByUser.toString(),
                      style: theme.textTheme.headline5,
                    ),
                    Text(
                      lang.locaized('post'),
                      style: theme.textTheme.bodyText2,
                    ),
                  ],
                ),
                GestureDetector(
                  onTap: () => Navigator.pushNamed(
                    context,
                    RouteName.follow,
                    arguments: FollowUserData(
                      FollowType.following,
                      updateProfileCubit.account.numOfFollowing,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        updateProfileCubit.account.numOfFollowing.toString(),
                        style: theme.textTheme.headline5,
                      ),
                      Text(
                        lang.locaized('following'),
                        style: theme.textTheme.bodyText2,
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () => Navigator.pushNamed(
                    context,
                    RouteName.follow,
                    arguments: FollowUserData(
                      FollowType.follower,
                      updateProfileCubit.account.numOfFollower,
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        updateProfileCubit.account.numOfFollower.toString(),
                        style: theme.textTheme.headline5,
                      ),
                      Text(
                        lang.locaized('follower'),
                        style: theme.textTheme.bodyText2,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: BlocBuilder<BioCubit, String>(
              bloc: bioCubit,
              builder: (_, bio) => bio != '' && bio != null
                  ? GestureDetector(
                      onTap: () async {
                        var result = await _addDescriptionUser();
                        if (result != null) {
                          bioCubit.change(result);
                        }
                      },
                      child: Container(
                        alignment: Alignment.center,
                        margin: const EdgeInsets.all(10.0),
                        child: Text(
                          bio,
                          style: theme.textTheme.bodyText1
                              .copyWith(fontWeight: AppFontWeight.regular),
                          maxLines: 3,
                          textAlign: TextAlign.center,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    )
                  : GestureDetector(
                      onTap: () async {
                        var result = await _addDescriptionUser();
                        if (result != null) {
                          bioCubit.change(result);
                        }
                      },
                      child: Container(
                        width: sizeScreen * 30,
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        margin: const EdgeInsets.only(top: 15.0),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(
                            color: AppColors.darkThemeColor.withOpacity(0.3),
                          ),
                        ),
                        child: Text(
                          lang.locaized('moreDescript'),
                          style: theme.textTheme.headline6.copyWith(
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                      ),
                    ),
            ),
          ),
          SizedBox(height: sizeScreen * 7.0),
        ],
      ),
    );
  }

  /// show dialog add description infomation of user
  Future<String> _addDescriptionUser() async => showGeneralDialog(
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        transitionDuration: Duration(milliseconds: 600),
        transitionBuilder: (_, _animation, _secondaryAnimation, child) =>
            TransactionAnima.grow(_animation, _secondaryAnimation, child),
        pageBuilder: (_, _animation, _secondaryAnimation) {
          final lang = AppLocalization.of(context);
          final theme = Theme.of(context);
          return Dialog(
            elevation: 0.0,
            insetAnimationDuration: Duration(milliseconds: 200),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: [
                      Text(
                        lang.locaized('bio'),
                        style: theme.textTheme.headline6,
                      ),
                      const Spacer(),
                      AppButton.icon(
                        icon: AppIcon.exitNavigate,
                        onTap: () => Navigator.pop(context),
                      )
                    ],
                  ),
                ),
                AppTextField.create(
                  isContent: true,
                  hindText: lang.locaized('hintBio'),
                  controller: _bioController,
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: AppButton.common(
                      width: sizeScreen * 13,
                      labelText: lang.locaized('saveButton'),
                      labelStyle: theme.textTheme.bodyText1.copyWith(
                        color: theme.backgroundColor,
                      ),
                      contentPadding: 10,
                      radius: 10,
                      backgroundColor: AppColors.primaryColor,
                      shadowColor: theme.backgroundColor,
                      onPressed: () =>
                          Navigator.pop(context, _bioController.text),
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      );
}
