import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_shimmer.dart';
import '../../../../../models/post/post.dart';
import '../../../../../models/token/token_repo.dart';
import '../../../../../models/user/user.dart';
import '../../../../../routes/route_name.dart';
import '../../../../../theme.dart';
import '../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../bloc/avatar_bloc/avatar_bloc.dart';
import '../bloc/post_by_user_bloc/post_by_user_bloc.dart';
import '../bloc/profile_bloc/profile_bloc.dart';
import '../bloc/update_profile_cubit/update_profile_cubit.dart';
import 'gallery_by_user/ui/gallery_by_user.dart';
import 'header_info_user.dart';
import 'list_post_by_user/list_post_by_user.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage>
    with AutomaticKeepAliveClientMixin<ProfilePage> {
  Widget loadingWidget = const SizedBox();

  Account account;
  AuthBloc authBloc;
  final List<Post> listPostByUser = [];

  final ProfileBloc profileBloc = ProfileBloc();
  final PostByUserBloc postByUserBloc = PostByUserBloc();

  UpdateProfileCubit updateProfileCubit;
  AvatarBloc avatarBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    super.build(context);

    return authBloc.isChecked
        ? MultiBlocProvider(
            providers: [
              BlocProvider<ProfileBloc>(create: (_) => profileBloc),
              BlocProvider<AvatarBloc>(create: (_) => avatarBloc),
              BlocProvider<UpdateProfileCubit>(
                create: (_) => updateProfileCubit,
              ),
              BlocProvider(create: (_) => postByUserBloc),
              BlocProvider<PostByUserBloc>(
                create: (_) => postByUserBloc,
              ),
            ],
            child: BlocBuilder<ProfileBloc, ProfileState>(
              builder: (_, state) {
                if (state is GetProfileInitial) {
                  profileBloc.add(GetProfileEvent());
                }
                if (state is GetProfileLoading) {
                  return AppShimmer.user();
                }
                if (state is GetProfileSuccess) {
                  account = state.account;
                  updateProfileCubit = UpdateProfileCubit(state.account);
                  avatarBloc = AvatarBloc(state.account.avatar);

                  return DefaultTabController(
                    length: 2,
                    child: Scaffold(
                      body: NestedScrollView(
                        physics: NeverScrollableScrollPhysics(),
                        floatHeaderSlivers: true,
                        headerSliverBuilder: (_, value) => [
                          SliverAppBar(
                            pinned: true,
                            floating: true,
                            backgroundColor: theme.backgroundColor,
                            expandedHeight: sizeScreen * 45,
                            toolbarHeight: 40,
                            collapsedHeight: 60,
                            elevation: 0.0,
                            actions: [
                              Padding(
                                padding: const EdgeInsets.only(
                                  right: 10.0,
                                ),
                                child: AppButton.icon(
                                  icon: AppIcon.menu,
                                  iconColor: AppColors.darkThemeColor,
                                  onTap: () =>
                                      _showMenu(context, theme, sizeScreen),
                                ),
                              ),
                            ],
                            flexibleSpace: FlexibleSpaceBar(
                              collapseMode: CollapseMode.none,
                              background: HeaderInfoUser(),
                            ),
                            bottom: PreferredSize(
                              preferredSize: Size.fromHeight(20.0),
                              child: Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 5.0),
                                decoration: BoxDecoration(
                                  color: theme.backgroundColor,
                                  borderRadius: BorderRadius.circular(5.0),
                                  border: Border.all(
                                    color: AppColors.darkThemeColor
                                        .withOpacity(0.1),
                                  ),
                                ),
                                child: TabBar(
                                  indicatorWeight: 1.5,
                                  indicatorPadding: const EdgeInsets.symmetric(
                                      horizontal: 30.0),
                                  indicatorColor: AppColors.darkThemeColor,
                                  tabs: [
                                    tabItem(AppIcon.post, sizeScreen),
                                    tabItem(AppIcon.unsaveGallery, sizeScreen),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                        body: TabBarView(
                          children: [
                            ListPostByUser(listPostByUser),
                            const GalleryByUser(),
                          ],
                        ),
                      ),
                    ),
                  );
                }
                return AppShimmer.user();
              },
            ),
          )
        : Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 70.0),
                  child: AppButton.common(
                    labelText: "Đăng nhập",
                    labelStyle: theme.textTheme.headline6.copyWith(
                      color: theme.backgroundColor,
                    ),
                    backgroundColor: theme.primaryColor,
                    shadowColor: theme.primaryColor,
                    contentPadding: 10.0,
                    radius: 30.0,
                    onPressed: () async {
                      await Navigator.pushNamed(context, RouteName.login).then(
                        (isLogin) {
                          if (isLogin == false) return;
                          Navigator.pushNamedAndRemoveUntil(
                            context,
                            RouteName.home,
                            (route) => false,
                          );
                        },
                      );
                    },
                  ),
                ),
              ],
            ),
          );
  }

  void _showMenu(BuildContext context, ThemeData theme, double sizeScreen) {
    final List<String> lstMenu = ["Thông tin cá nhân", "Cài đặt", "Đăng xuất"];
    showModalBottomSheet(
      barrierColor: AppColors.darkThemeColor.withOpacity(0.4),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      context: context,
      builder: (context) => Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Align(
              alignment: Alignment.topRight,
              child: AppButton.icon(
                icon: AppIcon.exitNavigate,
                iconSize: sizeScreen * 3,
                iconColor: AppColors.darkThemeColor.withOpacity(0.7),
                onTap: () => Navigator.pop(context),
              ),
            ),
          ),
          const Divider(thickness: 1),
          Container(
            child: ListView.separated(
              itemCount: lstMenu.length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              separatorBuilder: (context, index) => Divider(thickness: 1),
              itemBuilder: (context, index) => GestureDetector(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 7.0),
                  child: Text(
                    lstMenu[index],
                    style: theme.textTheme.headline6.copyWith(
                      fontWeight: AppFontWeight.regular,
                    ),
                  ),
                ),
                onTap: () async {
                  Navigator.pop(context);
                  switch (index) {
                    case 0:
                      final result = await Navigator.pushNamed(
                          context, RouteName.editProfile,
                          arguments: updateProfileCubit.account);
                      if (result != null) {
                        Account account = (result as Account);
                        updateProfileCubit.update(account);
                        avatarBloc.add(
                          ChangeAvatarFromLocalEvent(
                            newUrlImage: account.avatar,
                          ),
                        );
                      }
                      break;
                    default:
                      _showLogout();
                  }
                },
              ),
            ),
          ),
          const SizedBox(height: 30.0),
        ],
      ),
    );
  }

  void _showLogout() => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10.0),
          ),
          title: Text(
            "Đăng xuất",
            style: Theme.of(context).textTheme.headline4,
          ),
          actions: [
            AppButton.text(
              label: "KHÔNG",
              labelStyle: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: AppColors.primaryColor,
                  ),
              onTap: () => Navigator.pop(context),
            ),
            AppButton.text(
              label: "CÓ",
              labelStyle: Theme.of(context).textTheme.bodyText1.copyWith(
                    color: AppColors.primaryColor,
                  ),
              onTap: () async {
                await TokenRepo.deleteALl();
                authBloc.isChecked = false;
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  RouteName.home,
                  (route) => false,
                );
              },
            ),
          ],
        ),
      );

  Tab tabItem(IconData icon, double sizeScreen) => Tab(
        iconMargin: const EdgeInsets.all(0.0),
        icon: Icon(
          icon,
          color: AppColors.darkThemeColor,
          size: sizeScreen * 4,
        ),
      );

  @override
  bool get wantKeepAlive => true;
}
