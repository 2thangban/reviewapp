part of 'list_like_post_bloc.dart';

abstract class ListLikePostEvent {
  const ListLikePostEvent();
}

class GetListLikePostEvent extends ListLikePostEvent {
  final bool isRefresh;
  GetListLikePostEvent({this.isRefresh = false});
}
