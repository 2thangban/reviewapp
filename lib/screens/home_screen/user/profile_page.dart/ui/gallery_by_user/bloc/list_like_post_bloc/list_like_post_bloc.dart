import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../../../../models/post/post.dart';
import '../../../../../../../../models/post/post_repo.dart';

part 'list_like_post_event.dart';
part 'list_like_post_state.dart';

class ListLikePostBloc extends Bloc<ListLikePostEvent, ListLikePostState> {
  ListLikePostBloc() : super(const ListLikePostState());
  final int limit = 10;
  int page = 0;
  @override
  Stream<ListLikePostState> mapEventToState(ListLikePostEvent event) async* {
    if (event is GetListLikePostEvent) {
      yield state.cloneWith(status: ListLikePostStatus.loading);
      if (event.isRefresh) {
        page = 0;
      }
      try {
        final result = await PostRepo.getAllLikePost(
          page: page++,
          limit: limit,
        );

        yield state.cloneWith(
          status: ListLikePostStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } catch (e) {}
    } else {
      yield state.cloneWith(status: ListLikePostStatus.failure);
    }
  }
}
