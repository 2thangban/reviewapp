import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../../../../../api/api_response.dart';
import '../../../../../../../../models/gallery/gellary_repo.dart';
import '../../../../../../../../models/gallery/gellery.dart';
import '../../../../../../../../models/token/token_repo.dart';

part 'list_gallery_event.dart';
part 'list_gallery_state.dart';

class ListGalleryBloc extends Bloc<ListGalleryEvent, ListGalleryState> {
  int _page = 0;
  final int _limit = 5;
  bool _isLastPage = false;
  Gallery _firstGallery;

  ListGalleryBloc() : super(const ListGalleryState());

  get firstGallery => this._firstGallery;
  get isLastPage => this._isLastPage;

  @override
  Stream<Transition<ListGalleryEvent, ListGalleryState>> transformEvents(
    Stream<ListGalleryEvent> events,
    transitionFn,
  ) {
    return super.transformEvents(
      events.throttleTime(const Duration(milliseconds: 300)),
      transitionFn,
    );
  }

  @override
  Stream<ListGalleryState> mapEventToState(ListGalleryEvent event) async* {
    if (event is GetListGalleryEvent) {
      try {
        yield state.cloneWith(status: ListGalleryStatus.loading);
        final result = await GalleryRepo.getAll(
          event.userId,
          page: _page++,
          limit: _limit,
        );
        if (this._firstGallery == null) this._firstGallery = result.first;
        yield state.cloneWith(
          list: List.of(state.list)..addAll(result),
          hasReachedEnd: _isLastPage = result.length < _limit,
          status: ListGalleryStatus.success,
        );
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          List<Gallery> result;
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              result = await GalleryRepo.getAll(
                event.userId,
                page: _page,
                limit: _limit,
              );
            });
          });
          if (this._firstGallery == null) this._firstGallery = result.first;
          yield state.cloneWith(
            list: List.of(state.list)..addAll(result),
            hasReachedEnd: _isLastPage = result.length < _limit,
            status: ListGalleryStatus.success,
          );
        } else {
          yield state.cloneWith(status: ListGalleryStatus.failure);
        }
      } catch (e) {
        yield state.cloneWith(status: ListGalleryStatus.failure);
      }
    } else if (event is AddNewGalleryEvent) {
      yield state.cloneWith(
        list: List.of(state.list)..add(event.gallery),
        status: ListGalleryStatus.success,
      );
    } else if (event is DeleteGalleryEvent) {
      yield state.cloneWith(
        list: List.of(state.list)..removeAt(event.index),
        status: ListGalleryStatus.success,
      );
    } else if (event is UpdateGalleryEvent) {
      yield state.cloneWith(
        list: List.of(state.list)
          ..removeAt(event.index)
          ..insert(event.index, event.gallery),
        status: ListGalleryStatus.success,
      );
    }
  }
}
