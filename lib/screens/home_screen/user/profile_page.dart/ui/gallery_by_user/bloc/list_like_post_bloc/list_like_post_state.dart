part of 'list_like_post_bloc.dart';

enum ListLikePostStatus { initial, loading, success, failure }

class ListLikePostState extends Equatable {
  final ListLikePostStatus status;
  final List<Post> list;
  final bool isLastPage;
  const ListLikePostState({
    this.status = ListLikePostStatus.initial,
    this.list = const <Post>[],
    this.isLastPage = false,
  });

  ListLikePostState cloneWith({
    ListLikePostStatus status,
    List<Post> list,
    bool isLastPage,
  }) =>
      ListLikePostState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );

  @override
  List<Object> get props => [status, list, isLastPage];
}
