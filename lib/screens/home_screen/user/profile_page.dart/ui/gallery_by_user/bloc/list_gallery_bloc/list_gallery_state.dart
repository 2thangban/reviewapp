part of 'list_gallery_bloc.dart';

enum ListGalleryStatus { initial, loading, failure, success }

class ListGalleryState extends Equatable {
  final List<Gallery> list;
  final bool hasReachedEnd;
  final ListGalleryStatus status;

  const ListGalleryState({
    this.list = const <Gallery>[],
    this.hasReachedEnd = false,
    this.status = ListGalleryStatus.initial,
  });

  ListGalleryState cloneWith({
    List<Gallery> list,
    bool hasReachedEnd,
    ListGalleryStatus status,
  }) =>
      ListGalleryState(
        list: list ?? this.list,
        hasReachedEnd: hasReachedEnd ?? this.hasReachedEnd,
        status: status ?? this.status,
      );
  @override
  List<Object> get props => [this.list, this.hasReachedEnd, this.status];
}
