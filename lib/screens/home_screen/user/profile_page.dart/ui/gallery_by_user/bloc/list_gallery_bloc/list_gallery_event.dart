part of 'list_gallery_bloc.dart';

abstract class ListGalleryEvent {}

class GetListGalleryEvent extends ListGalleryEvent {
  final int userId;
  GetListGalleryEvent(this.userId);
}

class AddNewGalleryEvent extends ListGalleryEvent {
  final Gallery gallery;
  AddNewGalleryEvent(this.gallery);
}

class DeleteGalleryEvent extends ListGalleryEvent {
  final int index;
  DeleteGalleryEvent(this.index);
}

class UpdateGalleryEvent extends ListGalleryEvent {
  final int index;
  final Gallery gallery;
  UpdateGalleryEvent(this.index, this.gallery);
}
