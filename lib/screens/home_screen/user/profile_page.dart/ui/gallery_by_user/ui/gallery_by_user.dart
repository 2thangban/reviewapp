import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../../../../commons/helper/size_config.dart';
import '../../../../../../../commons/untils/app_color.dart';
import '../../../../../../../commons/untils/app_icon.dart';
import '../../../../../../../commons/untils/app_image.dart';
import '../../../../../../../commons/widgets/app_button.dart';
import '../../../../../../../commons/widgets/app_loading.dart';
import '../../../../../../../localizations/app_localization.dart';
import '../../../../../../../models/gallery/gellery.dart';
import '../../../../../../../routes/route_name.dart';
import '../../../../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../list_post_by_user/item_post_by_user.dart';
import '../bloc/list_gallery_bloc/list_gallery_bloc.dart';
import '../bloc/list_like_post_bloc/list_like_post_bloc.dart';

class ResultGalley {
  int status;
  Gallery gallery;
  int index;
  ResultGalley(this.status, {this.gallery, this.index});
}

class GalleryByUser extends StatefulWidget {
  const GalleryByUser({Key key}) : super(key: key);
  @override
  _GalleryByUserState createState() => _GalleryByUserState();
}

class _GalleryByUserState extends State<GalleryByUser>
    with AutomaticKeepAliveClientMixin<GalleryByUser> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final ListGalleryBloc listGalleryBloc = ListGalleryBloc();
  final ListLikePostBloc listlikePostBloc = ListLikePostBloc();
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    listGalleryBloc.add(GetListGalleryEvent(authBloc.account.id));
    listlikePostBloc.add(GetListLikePostEvent());
  }

  @override
  void dispose() {
    super.dispose();
    listGalleryBloc.close();
    listlikePostBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return ListView(
      physics: NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: AppButton.common(
            labelText: lang.locaized('CreateGalleryBtn'),
            labelStyle: theme.textTheme.headline5.copyWith(
              color: theme.backgroundColor,
            ),
            radius: 7,
            backgroundColor: theme.primaryColor,
            shadowColor: theme.backgroundColor,
            contentPadding: 12.0,
            onPressed: () async {
              final newGallery = await Navigator.pushNamed(
                context,
                RouteName.createGallery,
                arguments: null,
              );
              if (newGallery != null) {
                listGalleryBloc.add(AddNewGalleryEvent(newGallery));
              }
            },
          ),
        ),
        const SizedBox(height: 10.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('labelGallery'),
            style: theme.textTheme.headline5,
          ),
        ),
        BlocBuilder<ListGalleryBloc, ListGalleryState>(
          bloc: listGalleryBloc,
          buildWhen: (previous, current) =>
              current.status != ListGalleryStatus.initial,
          builder: (context, state) => AnimatedContainer(
            curve: Curves.decelerate,
            duration: const Duration(milliseconds: 500),
            height: state.status == ListGalleryStatus.loading
                ? sizeScreen * 5
                : sizeScreen * 17,
            child: state.status == ListGalleryStatus.loading
                ? AppLoading.threeBounce(size: 20.0)
                : state.status == ListGalleryStatus.failure
                    ? const SizedBox()
                    : ListView.builder(
                        padding: const EdgeInsets.only(left: 10.0),
                        scrollDirection: Axis.horizontal,
                        itemCount: state.list.length,
                        itemBuilder: (context, index) {
                          final Gallery currentGallery = state.list[index];
                          return GestureDetector(
                            onTap: () async {
                              await Navigator.pushNamed(
                                context,
                                RouteName.detailGallery,
                                arguments: currentGallery,
                              ).then((value) {
                                if (value != null) {
                                  final params = (value as ResultGalley);
                                  switch (params.status) {
                                    case 1:
                                      listGalleryBloc.add(
                                        DeleteGalleryEvent(index),
                                      );
                                      break;
                                    case 2:
                                      listGalleryBloc.add(
                                        UpdateGalleryEvent(
                                          params.index,
                                          params.gallery,
                                        ),
                                      );
                                      break;
                                    default:
                                  }
                                }
                              });
                            },
                            child: Container(
                              width: 30 * sizeScreen,
                              clipBehavior: Clip.antiAlias,
                              margin: const EdgeInsets.only(right: 10.0),
                              decoration: BoxDecoration(
                                color: AppColors.darkThemeColor,
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              child: Stack(
                                children: [
                                  Opacity(
                                    opacity: 0.5,
                                    child: currentGallery.image != ''
                                        ? Image.network(
                                            AppAssets.baseUrl +
                                                currentGallery.image,
                                            width: 32 * sizeScreen,
                                            fit: BoxFit.fill,
                                          )
                                        : Image.asset(
                                            AppAssets.img1,
                                            width: 32 * sizeScreen,
                                            fit: BoxFit.fill,
                                          ),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.all(10.0),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container(
                                          width: 28 * sizeScreen,
                                          child: Text(
                                            currentGallery.name,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 2,
                                            style: theme.textTheme.headline5
                                                .copyWith(
                                              color: AppColors.lightThemeColor,
                                            ),
                                          ),
                                        ),
                                        Visibility(
                                          visible: currentGallery.bio != '',
                                          child: Container(
                                            width: 28 * sizeScreen,
                                            child: Text(
                                              currentGallery.bio,
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 2,
                                              style: theme.textTheme.bodyText1
                                                  .copyWith(
                                                color:
                                                    AppColors.lightThemeColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        SizedBox(
                                          child: Text(
                                            currentGallery.numOfPost == 0
                                                ? lang.locaized(
                                                    'emptyListOfGallery')
                                                : '${currentGallery.numOfPost} ${lang.locaized('post')}',
                                            style: theme.textTheme.bodyText1
                                                .copyWith(
                                              color: AppColors.lightThemeColor,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Visibility(
                                    visible: currentGallery.private == 1,
                                    child: Positioned(
                                      right: 5.0,
                                      top: 5.0,
                                      child: Icon(
                                        AppIcon.block,
                                        size: 20,
                                        color: AppColors.lightThemeColor,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Text(lang.locaized('like'), style: theme.textTheme.headline5),
        ),
        BlocBuilder<ListLikePostBloc, ListLikePostState>(
          bloc: listlikePostBloc,
          builder: (context, state) {
            if (state.status == ListLikePostStatus.failure) {
              return SizedBox(
                height: sizeScreen * 40,
                child: Center(
                  child: Text('Is Empty'),
                ),
              );
            }
            if (state.status == ListLikePostStatus.success) {
              if (state.list.isEmpty) {
                return SizedBox(
                  height: sizeScreen * 40,
                  child: Center(
                    child: Text('Is Empty'),
                  ),
                );
              }
              return Container(
                child: StaggeredGridView.extentBuilder(
                  physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  shrinkWrap: true,
                  itemCount: state.list.length,
                  maxCrossAxisExtent: 200,
                  crossAxisSpacing: 5.0,
                  mainAxisSpacing: 5.0,
                  staggeredTileBuilder: (index) =>
                      StaggeredTile.extent(1, index.isEven ? 259 : 200),
                  itemBuilder: (context, index) => ItemListPostByUser(
                    state.list[index],
                  ),
                ),
              );
            }
            return const SizedBox.shrink();
          },
        )
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
