import 'dart:async';
import 'dart:developer';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../commons/helper/size_config.dart';
import '../../commons/untils/app_color.dart';
import '../../commons/untils/app_icon.dart';
import '../../commons/untils/app_image.dart';
import '../../commons/untils/app_toast.dart';
import '../../models/post/post.dart';
import '../../routes/route_name.dart';
import '../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../app_common_bloc/num_of_new_notify_cubit/num_of_new_notify_cubit.dart';
import '../app_common_bloc/push_notification_bloc/push_notification_bloc.dart';
import 'bloc/cubit/bottom_tab_cubit.dart';
import 'list_video_page.dart/ui/list_video_page.dart';
import 'notify_page.dart/ui/list_notification_page.dart';
import 'post/list_post_page/list_post_page.dart';
import 'user/profile_page.dart/ui/profile_screen.dart';

const AndroidNotificationChannel _channel = AndroidNotificationChannel(
  'This is message id',
  'This is message name',
  'This is message description',
  importance: Importance.high,
  playSound: true,
);

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
}

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  bool existAppPermission = true;
  final PageController _pageController = PageController(initialPage: 0);
  final NumOfNewNotifyCubit numOfNewNotifyCubit = NumOfNewNotifyCubit();
  BottomTabCubit tabCubit;
  AuthBloc authBloc;
  PushNotificationBloc pushNotificationBloc;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    if (authBloc.isChecked) {
      numOfNewNotifyCubit.getNumOfNotify();
    }

    pushNotificationBloc = BlocProvider.of<PushNotificationBloc>(context);
    tabCubit = BottomTabCubit(0, _pageController);
    init();

    ///
    /// Listen event the user app running.
    FirebaseMessaging.instance.getInitialMessage().then(
      (RemoteMessage message) {
        if (message != null) {
          RemoteNotification notification = message.notification;
          AndroidNotification android = message.notification?.android;
          if (notification != null && android != null) {
            navigatorScreenFromMessage(message);
          }
        }
      },
    );

    /// Listen event receive notification from foreground.
    FirebaseMessaging.onMessage.listen(
      (RemoteMessage message) {
        RemoteNotification notification = message.notification;
        AndroidNotification android = message.notification?.android;
        if (notification != null && android != null) {
          /// show notification in app
          flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
              android: AndroidNotificationDetails(
                _channel.id,
                _channel.name,
                _channel.description,
                color: Colors.blue,
                playSound: true,
                icon: '@mipmap/ic_launcher',
              ),
            ),
          );
          pushNotificationBloc.add(ReceiveLocalNotificationEvent(message));
        }
      },
    );

    /// Listen event receive notification from background
    FirebaseMessaging.onMessageOpenedApp.listen(
      (RemoteMessage message) {
        RemoteNotification notification = message.notification;
        AndroidNotification android = message.notification?.android;
        if (notification != null && android != null) {
          navigatorScreenFromMessage(message);
        }
      },
    );
  }

  Future<void> init() async {
    /// setup firebase messaging
    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(_channel);

    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      sound: true,
      badge: true,
      alert: true,
    );
  }

  void navigatorScreenFromMessage(RemoteMessage message) {
    switch (int.parse(message.data['type'])) {
      case 1:
        Navigator.pushNamed(
          context,
          RouteName.profile,
          arguments: message.data['userid'],
        );

        break;
      case 2:
        Navigator.pushNamed(
          context,
          RouteName.postDetail,
          arguments: int.parse(message.data['postId']),
        );
        break;
      default:
    }
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return WillPopScope(
      onWillPop: () {
        AppToast.show("Nhấn back lần nữa để thoát !!", context);
        existAppPermission = !existAppPermission;
        Timer(Duration(seconds: 3), () => existAppPermission = true);
        return Future.value(existAppPermission);
      },
      child: BlocProvider<NumOfNewNotifyCubit>(
        create: (context) => numOfNewNotifyCubit,
        child: BlocBuilder<NumOfNewNotifyCubit, int>(
          bloc: numOfNewNotifyCubit,
          builder: (context, count) =>
              BlocBuilder<PushNotificationBloc, PushNotificationState>(
            builder: (context, state) {
              if (state is ReceiveLocalNotificationSuccess) {
                numOfNewNotifyCubit.numOfNotify++;
              }
              return BlocBuilder<BottomTabCubit, int>(
                bloc: tabCubit,
                builder: (_, index) {
                  return Scaffold(
                    body: PageView(
                      physics: NeverScrollableScrollPhysics(),
                      controller: _pageController,
                      children: [
                        const ListPostPage(),
                        const ListVideoPage(),
                        const SizedBox(),
                        const NotifyPage(),
                        const ProfilePage(),
                      ],
                    ),
                    bottomNavigationBar: Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 8.0,
                      ),
                      decoration: BoxDecoration(
                        border: Border(
                          top: BorderSide(
                            color: AppColors.backgroundEditColor,
                          ),
                        ),
                      ),
                      child: BlocBuilder<BottomTabCubit, int>(
                        bloc: tabCubit,
                        builder: (_, index) => Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            _itemTab(
                              icon: index == 0
                                  ? AppAssets.homeActiveTab
                                  : AppAssets.homeTab,
                              height: sizeScreen * 3.5,
                              onTap: () => tabCubit.onChangeTab(0),
                            ),
                            _itemTab(
                              icon: index == 1
                                  ? AppAssets.videoActiveTab
                                  : AppAssets.videoTab,
                              height: sizeScreen * 3.5,
                              onTap: () => tabCubit.onChangeTab(1),
                            ),
                            GestureDetector(
                              onTap: () => Future.delayed(
                                const Duration(milliseconds: 200),
                                () => Navigator.pushNamed(
                                  context,
                                  RouteName.createPost,
                                  arguments: Post(
                                    titlePost: '',
                                    content: '',
                                    category: 1,
                                    imageUpload: [],
                                    videoUpload: File(''),
                                    hashTag: [],
                                    dateCreatePost: DateTime.now().toString(),
                                    location: null,
                                    rating: 0,
                                  ),
                                ),
                              ),
                              child: Container(
                                decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  gradient: LinearGradient(
                                    begin: Alignment.bottomLeft,
                                    end: Alignment.topRight,
                                    colors: [
                                      AppColors.primaryColor,
                                      AppColors.secondaryColor,
                                    ],
                                  ),
                                ),
                                child: Icon(
                                  AppIcon.add,
                                  size: sizeScreen * 5.7,
                                  color: theme.backgroundColor,
                                ),
                              ),
                            ),
                            _itemTab(
                              icon: index == 3
                                  ? AppAssets.notifyActiveTab
                                  : AppAssets.notifyTab,
                              height: sizeScreen * 3.5,
                              notify: numOfNewNotifyCubit.numOfNotify != 0,
                              countNotify: numOfNewNotifyCubit.numOfNotify,
                              onTap: () => tabCubit.onChangeTab(3),
                            ),
                            _itemTab(
                              icon: index == 4
                                  ? AppAssets.accountActiveTab
                                  : AppAssets.accountTab,
                              height: sizeScreen * 3.5,
                              onTap: () => tabCubit.onChangeTab(4),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _itemTab({
    String icon,
    double height,
    bool notify = false,
    int countNotify,
    VoidCallback onTap,
  }) =>
      GestureDetector(
        onTap: onTap,
        child: Stack(
          alignment: Alignment.topRight,
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 2.0),
              child: SvgPicture.asset(
                icon,
                height: height ?? 28,
              ),
            ),
            Visibility(
              visible: notify,
              child: Container(
                alignment: Alignment.center,
                width: 16.0,
                height: 16.0,
                padding: const EdgeInsets.all(2.0),
                decoration: BoxDecoration(
                    color: AppColors.errorColor, shape: BoxShape.circle),
                child: Text(
                  countNotify.toString(),
                  style: Theme.of(context).textTheme.bodyText1.copyWith(
                        fontSize: sizeScreen * 1.3,
                        color: Theme.of(context).backgroundColor,
                      ),
                ),
              ),
            ),
          ],
        ),
      );
}
