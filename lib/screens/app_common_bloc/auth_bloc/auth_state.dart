part of 'auth_bloc.dart';

@immutable
abstract class AuthState {}

class AuthInitial extends AuthState {}

class AuthSuccess extends AuthState {
  final String accessToken;
  final String refreshToken;
  AuthSuccess(this.accessToken, this.refreshToken);
}

class AuthFailed extends AuthState {}
