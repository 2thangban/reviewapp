import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../models/token/token_repo.dart';
import '../../../models/user/user.dart';
import '../../../models/user/user_repo.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  /// result Check
  bool isChecked = false;
  Account account;
  AuthBloc() : super(AuthInitial());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is CheckLogin) {
      isChecked = await TokenRepo.check();
      if (isChecked) {
        final token = await TokenRepo.read();
        final account = await AccountRepo.read();
        this.account = account;
        yield AuthSuccess(token.accessToken, token.refreshToken);
      } else {
        account = Account();
        yield AuthFailed();
      }
    }
  }
}
