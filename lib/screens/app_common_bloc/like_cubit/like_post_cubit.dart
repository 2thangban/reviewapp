import 'package:bloc/bloc.dart';

import '../../../api/api_response.dart';
import '../../../models/token/token_repo.dart';
import '../../../models/user/user_repo.dart';

class LikePostCubit extends Cubit<bool> {
  int numOfLike;
  bool status;

  LikePostCubit(this.status, this.numOfLike) : super(status);

  void change(bool status, {int postId, int commentId}) async {
    if (postId != null) {
      try {
        if (status) {
          await AccountRepo.unLikePost(postId).whenComplete(() {
            numOfLike--;
            this.status = false;
            emit(false);
          });
        } else {
          await AccountRepo.likePost(postId).whenComplete(() {
            numOfLike++;
            emit(true);
            this.status = true;
          });
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          await TokenRepo.refreshToken().then((newToken) async {
            await TokenRepo.write(newToken).whenComplete(() async {
              if (status) {
                await AccountRepo.unLikePost(postId).whenComplete(() {
                  numOfLike--;
                  this.status = false;
                  emit(false);
                });
              } else {
                await AccountRepo.likePost(postId).whenComplete(() {
                  numOfLike++;
                  this.status = true;
                  emit(true);
                });
              }
            });
          });
        }
      }
    } else {
      try {
        if (status) {
          await AccountRepo.unlikeComment(commentId).whenComplete(() {
            numOfLike--;
            emit(false);
          });
        } else {
          await AccountRepo.likeComment(commentId).whenComplete(() {
            numOfLike++;
            emit(true);
          });
        }
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          await TokenRepo.refreshToken().then(
            (newToken) async {
              await TokenRepo.write(newToken).whenComplete(
                () async {
                  if (status) {
                    await AccountRepo.unlikeComment(commentId).whenComplete(() {
                      numOfLike--;
                      emit(false);
                    });
                  } else {
                    await AccountRepo.likeComment(commentId).whenComplete(
                      () {
                        numOfLike++;
                        emit(true);
                      },
                    );
                  }
                },
              );
            },
          );
        }
      }
    }
  }

  void update(bool status) {
    if (status) {
      numOfLike++;
    } else {
      numOfLike--;
    }
    emit(status);
  }
}
