part of 'province_bloc.dart';

abstract class ProvinceEvent {}

class InitProvinceEvent extends ProvinceEvent {}

class GetCurrentProvinceEvent extends ProvinceEvent {}

class GetListProvinceEvent extends ProvinceEvent {}

class AddNewProvinceEvent extends ProvinceEvent {
  final Province province;
  AddNewProvinceEvent(this.province);
}
