part of 'province_bloc.dart';

abstract class ProvinceState {}

class ProvinceInitial extends ProvinceState {}

class ProvinceLoading extends ProvinceState {}

class ProvinceFailed extends ProvinceState {}

class ProvinceSuccess extends ProvinceState {
  final Province province;
  ProvinceSuccess(this.province);
}

class ListProvinceSuccess extends ProvinceState {
  final Box<Province> boxProvince;
  ListProvinceSuccess(this.boxProvince);
}
