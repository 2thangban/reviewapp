import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:hive/hive.dart';

import '../../../models/province/province.dart';

part 'province_event.dart';
part 'province_state.dart';

class ProvinceBloc extends Bloc<ProvinceEvent, ProvinceState> {
  Box<Province> boxProvince;
  Province currentProvince;
  ProvinceBloc() : super(ProvinceInitial());
  @override
  Stream<ProvinceState> mapEventToState(ProvinceEvent event) async* {
    if (event is InitProvinceEvent) {
      boxProvince = await Hive.openBox<Province>('province');
      currentProvince = (boxProvince != null && boxProvince.length > 0)
          ? boxProvince.getAt(boxProvince.length - 1)
          : null;
    } else if (event is GetCurrentProvinceEvent) {
      if (boxProvince != null && boxProvince.length > 0) {
        Province province = boxProvince.getAt(boxProvince.length - 1);
        yield ProvinceSuccess(province);
      }
      yield ProvinceSuccess(null);
    } else if (event is GetListProvinceEvent) {
      yield ListProvinceSuccess(boxProvince);
    } else if (event is AddNewProvinceEvent) {
      for (var i = 0; i < boxProvince.length - 1; i++) {
        if (boxProvince.getAt(i).id == event.province.id) {
          await boxProvince.deleteAt(i);
        }
      }
      await boxProvince.add(event.province);
      currentProvince = event.province;
      yield ProvinceSuccess(event.province);
    } else {
      ///
    }
  }
}
