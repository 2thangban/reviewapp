import 'package:bloc/bloc.dart';

import '../../../models/post/post_repo.dart';

class ViewCubit extends Cubit<bool> {
  ViewCubit() : super(false);

  void increamentViewOfPost(int postId) async {
    try {
      final result = await PostRepo.incrementView(postId);
      emit(result.status.contains('Success'));
    } catch (e) {
      ///
    }
  }
}
