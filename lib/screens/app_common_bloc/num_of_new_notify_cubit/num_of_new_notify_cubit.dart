import 'package:bloc/bloc.dart';

import '../../../api/api_response.dart';
import '../../../models/notification/notification_repo.dart';
import '../../../models/token/token_repo.dart';

class NumOfNewNotifyCubit extends Cubit<int> {
  int numOfNotify = 0;
  NumOfNewNotifyCubit() : super(0);

  void getNumOfNotify() async {
    try {
      final result = await NotificationRepo.numOfUnseen();
      emit(numOfNotify = result.numOfUnSeen);
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        await TokenRepo.refreshToken().then((newToken) async {
          await TokenRepo.write(newToken).then((complete) async {
            final result = await NotificationRepo.numOfUnseen();
            emit(numOfNotify = result.numOfUnSeen);
          });
        });
      }
    }
  }

  void updateNumOfNotify() => emit(this.numOfNotify = 0);
}
