part of 'push_notification_bloc.dart';

abstract class PushNotificationState {}

class PushNotificationInitial extends PushNotificationState {}

class ReceiveLocalNotificationSuccess extends PushNotificationState {
  RemoteMessage message;

  ReceiveLocalNotificationSuccess(this.message);
}
