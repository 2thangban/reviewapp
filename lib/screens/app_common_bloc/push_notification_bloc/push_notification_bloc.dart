import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

part 'push_notification_event.dart';
part 'push_notification_state.dart';

class PushNotificationBloc
    extends Bloc<PushNotificationEvent, PushNotificationState> {
  PushNotificationBloc() : super(PushNotificationInitial());

  @override
  Stream<PushNotificationState> mapEventToState(
      PushNotificationEvent event) async* {
    if (event is ReceiveLocalNotificationEvent) {
      yield ReceiveLocalNotificationSuccess(event.message);
    }
  }
}
