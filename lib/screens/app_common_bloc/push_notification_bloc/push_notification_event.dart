part of 'push_notification_bloc.dart';

abstract class PushNotificationEvent {}

class ReceiveLocalNotificationEvent extends PushNotificationEvent {
  RemoteMessage message;
  ReceiveLocalNotificationEvent(this.message);
}
