import 'package:bloc/bloc.dart';

import '../../../api/api_response.dart';
import '../../../models/user/user_repo.dart';

class FollowUserCubit extends Cubit<bool> {
  final int userId;
  FollowUserCubit(this.userId, bool status) : super(status);

  void change(bool status) async {
    try {
      switch (status) {
        case true:
          await AccountRepo.unFollow(this.userId)
              .whenComplete(() => emit(false));
          break;
        case false:
          await AccountRepo.follow(userId).whenComplete(() => emit(true));
          break;
        default:
      }
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        switch (status) {
          case true:
            await AccountRepo.unFollow(this.userId)
                .whenComplete(() => emit(false));
            break;
          case false:
            await AccountRepo.follow(userId).whenComplete(() => emit(true));
            break;
          default:
        }
      }
    } catch (e) {
      ///
    }
  }

  void update(bool status) => emit(status);
}
