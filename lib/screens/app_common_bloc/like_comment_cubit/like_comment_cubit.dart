import 'package:bloc/bloc.dart';

import '../../../api/api_response.dart';
import '../../../models/token/token_repo.dart';
import '../../../models/user/user_repo.dart';

class LikeCommentCubit extends Cubit<bool> {
  int numOfLikeComment;
  bool status;
  final int commentId;
  LikeCommentCubit(
    this.numOfLikeComment,
    this.status,
    this.commentId,
  ) : super(status);

  void change(bool status) async {
    try {
      if (status) {
        await AccountRepo.unlikeComment(commentId).whenComplete(() {
          emit(false);
          numOfLikeComment--;
        });
      } else {
        await AccountRepo.likeComment(commentId).whenComplete(() {
          emit(true);
          numOfLikeComment++;
        });
      }
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        await TokenRepo.refreshToken().then((newToken) async {
          await TokenRepo.write(newToken).whenComplete(() async {
            if (status) {
              await AccountRepo.unlikeComment(commentId).whenComplete(() {
                emit(false);
                numOfLikeComment--;
              });
            } else {
              await AccountRepo.likeComment(commentId).whenComplete(() {
                emit(true);
                numOfLikeComment++;
              });
            }
          });
        });
      }
    }
  }
}
