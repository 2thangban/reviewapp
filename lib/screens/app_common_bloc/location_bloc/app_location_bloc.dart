import 'dart:async';
import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../location_app/location_manager.dart';
import '../../../models/province/province.dart';
import '../../../models/province/province_repo.dart';

part 'app_location_event.dart';
part 'app_location_state.dart';

class AppLocationBloc extends Bloc<AppLocationEvent, AppLocationState> {
  List<Province> listProvince;
  Province currentProvince; // current province by user
  AppLocationBloc() : super(AppLocationInitial());

  @override
  Stream<AppLocationState> mapEventToState(AppLocationEvent event) async* {
    try {
      if (event is GetLocationEvent) {
        yield AppLocationLoading();
        final listProvince = await ProvinceRepo.getAll();
        this.listProvince = listProvince;
        // get position by user
        final position = await LocationManager.getPosition();
        if (position != null) {
          /// get address
          final address = await LocationManager.getAddress(position);
          final result =
              LocationManager.convertToProvince(address, listProvince);
          this.currentProvince = result;
          yield AppLocationAllow(result);
        } else {
          this.currentProvince = listProvince[13];
          yield AppLocationDenied(listProvince[13]);
        }
      } else if (event is ChangeLocationEvent) {
        this.currentProvince = event.newProvince;
        yield ChangeLocationSuccess(event.newProvince);
      }
    } catch (e) {
      log(e.toString());
    }
  }
}
