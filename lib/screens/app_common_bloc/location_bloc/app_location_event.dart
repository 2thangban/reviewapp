part of 'app_location_bloc.dart';

@immutable
abstract class AppLocationEvent {}

class GetLocationEvent extends AppLocationEvent {}

class ChangeLocationEvent extends AppLocationEvent {
  final Province newProvince;
  ChangeLocationEvent(this.newProvince);
}
