part of 'app_location_bloc.dart';

@immutable
abstract class AppLocationState {}

class AppLocationInitial extends AppLocationState {}

class AppLocationLoading extends AppLocationState {}

class AppLocationDenied extends AppLocationState {
  final Province province;
  AppLocationDenied(this.province);
}

class AppLocationAllow extends AppLocationState {
  final Province province;
  AppLocationAllow(this.province);
}

class ChangeLocationSuccess extends AppLocationState {
  final Province province;
  ChangeLocationSuccess(this.province);
}
