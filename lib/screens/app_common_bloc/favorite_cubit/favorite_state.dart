part of 'favorite_cubit.dart';

class FavoriteState {
  final bool status;
  final Gallery gallery;
  final bool isAddSuccess;
  FavoriteState(this.status, {this.gallery, this.isAddSuccess});
}
