import 'package:bloc/bloc.dart';

import '../../../api/api_response.dart';
import '../../../models/gallery/gellary_repo.dart';
import '../../../models/gallery/gellery.dart';
import '../../../models/token/token_repo.dart';

part 'favorite_state.dart';

class FavoriteCubit extends Cubit<FavoriteState> {
  final int postid;
  FavoriteCubit(bool status, this.postid) : super(FavoriteState(status));

  void unFavoritePost() async {
    try {
      await GalleryRepo.removePostFromGallery(postid).whenComplete(
        () => emit(FavoriteState(false, isAddSuccess: false)),
      );
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        await TokenRepo.refreshToken().then(
          (newToken) async {
            await TokenRepo.write(newToken).whenComplete(
              () async {
                await GalleryRepo.removePostFromGallery(postid).whenComplete(
                  () => emit(FavoriteState(false, isAddSuccess: false)),
                );
              },
            );
          },
        );
      }
    }
  }

  void favoritePost(Gallery gallery) async {
    try {
      await GalleryRepo.addPostToGallery(postid, gallery.id).whenComplete(
        () => emit(FavoriteState(true, gallery: gallery, isAddSuccess: true)),
      );
    } on ErrorResponse catch (e) {
      if (e.staticCode == 401) {
        await TokenRepo.refreshToken().then(
          (newToken) async {
            await TokenRepo.write(newToken).whenComplete(
              () async {
                await GalleryRepo.addPostToGallery(postid, gallery.id)
                    .whenComplete(
                  () => emit(
                    FavoriteState(true, gallery: gallery, isAddSuccess: true),
                  ),
                );
              },
            );
          },
        );
      }
    }
  }
}
