import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../models/token/token.dart';
import '../../../models/token/token_repo.dart';
import '../../../models/user/user.dart';
import '../../../models/user/user_repo.dart';

part 'sign_up_event.dart';
part 'sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpBloc() : super(SignUpInitial());

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    yield SignUpLoading();
    try {
      if (event is SignUpSocialAccount) {
        final account =
            await AccountRepo.signUp(event.account, SignUpType.social);

        /// save token to local app
        final Token newToken = Token(
          accessToken: account.accessToken,
          refreshToken: account.refreshToken,
        );
        await TokenRepo.write(newToken);
        await AccountRepo.write(account);
        yield SignUpSuccess(account);
      } else if (event is SignUpPhone) {
        final account =
            await AccountRepo.signUp(event.account, SignUpType.phone);

        /// save token to local app
        final Token newToken = Token(
          accessToken: account.accessToken,
          refreshToken: account.refreshToken,
        );
        await TokenRepo.write(newToken);
        await AccountRepo.write(account);
        yield SignUpSuccess(account);
      }
    } catch (e) {
      yield SignUpFailed(e.toString());
    }
  }
}
