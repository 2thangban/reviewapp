part of 'sign_up_bloc.dart';

@immutable
abstract class SignUpState {}

class SignUpInitial extends SignUpState {}

class SignUpLoading extends SignUpState {}

class SignUpFailed extends SignUpState {
  final String msg;
  SignUpFailed(this.msg);
}

class SignUpSuccess extends SignUpState {
  final Account account;
  SignUpSuccess(this.account);
}
