part of 'sign_up_bloc.dart';

@immutable
abstract class SignUpEvent {}

class SignUpSocialAccount extends SignUpEvent {
  final Account account;
  SignUpSocialAccount(this.account);
}

class SignUpPhone extends SignUpEvent {
  final Account account;
  SignUpPhone(this.account);
}
