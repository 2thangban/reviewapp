import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/hashtag/hashtag_repo.dart';
import '../../../../../models/post/post.dart';

part 'list_post_by_hashtag_event.dart';
part 'list_post_by_hashtag_state.dart';

class ListPostByHashtagBloc
    extends Bloc<ListPostByHashtagEvent, ListPostByHashtagState> {
  final int limit = 10;
  int page = 0;
  ListPostByHashtagBloc() : super(const ListPostByHashtagState());

  @override
  Stream<ListPostByHashtagState> mapEventToState(
    ListPostByHashtagEvent event,
  ) async* {
    if (event is GetListPostByHashTag) {
      yield state.cloneWith(status: ListPostHashtagStatus.loading);
      if (event.isRefresh) {
        page = 0;
      }
      try {
        final result = await HashtagRepo.postByHashtag(
          event.idHashtag,
          page: page++,
          limit: limit,
        );
        yield state.cloneWith(
          status: ListPostHashtagStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } catch (e) {
        page--;
        yield state.cloneWith(status: ListPostHashtagStatus.failure);
      }
    } else {
      yield state.cloneWith(status: ListPostHashtagStatus.failure);
    }
  }
}
