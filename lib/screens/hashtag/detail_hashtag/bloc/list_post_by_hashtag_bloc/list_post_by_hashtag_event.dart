part of 'list_post_by_hashtag_bloc.dart';

abstract class ListPostByHashtagEvent {
  const ListPostByHashtagEvent();
}

class GetListPostByHashTag extends ListPostByHashtagEvent {
  final bool isRefresh;
  final int idHashtag;
  GetListPostByHashTag(this.idHashtag, {this.isRefresh = false});
}
