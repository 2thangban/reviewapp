part of 'list_post_by_hashtag_bloc.dart';

enum ListPostHashtagStatus { initial, loading, success, failure }

class ListPostByHashtagState extends Equatable {
  final ListPostHashtagStatus status;
  final List<Post> list;
  final bool isLastPage;
  const ListPostByHashtagState({
    this.status = ListPostHashtagStatus.initial,
    this.list = const <Post>[],
    this.isLastPage = false,
  });

  ListPostByHashtagState cloneWith({
    ListPostHashtagStatus status,
    List<Post> list,
    bool isLastPage,
  }) =>
      ListPostByHashtagState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );

  @override
  List<Object> get props => [status, list, isLastPage];
}
