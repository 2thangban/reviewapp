part of 'detail_hashtag_bloc.dart';

enum DetailHashtagStatus { initial, loading, success, failure }

class DetailHashtagState extends Equatable {
  final DetailHashtagStatus status;
  final Hashtag hashtag;
  const DetailHashtagState({
    this.status = DetailHashtagStatus.initial,
    this.hashtag,
  });
  DetailHashtagState cloneWith({
    DetailHashtagStatus status,
    Hashtag hashtag,
  }) =>
      DetailHashtagState(
        status: status ?? this.hashtag,
        hashtag: hashtag ?? this.hashtag,
      );
  @override
  List<Object> get props => [hashtag, status];
}
