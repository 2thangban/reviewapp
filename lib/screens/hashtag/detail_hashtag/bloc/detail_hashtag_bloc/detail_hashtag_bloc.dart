import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../../models/hashtag/hashtag.dart';
import '../../../../../models/hashtag/hashtag_repo.dart';

part 'detail_hashtag_event.dart';
part 'detail_hashtag_state.dart';

class DetailHashtagBloc extends Bloc<DetailHashtagEvent, DetailHashtagState> {
  DetailHashtagBloc() : super(const DetailHashtagState());

  @override
  Stream<DetailHashtagState> mapEventToState(
    DetailHashtagEvent event,
  ) async* {
    if (event is GetDetailHashTagEvent) {
      yield state.cloneWith(status: DetailHashtagStatus.loading);
      try {
        final result = await HashtagRepo.detail(event.id);
        yield state.cloneWith(
          status: DetailHashtagStatus.success,
          hashtag: result,
        );
      } catch (e) {
        yield state.cloneWith(status: DetailHashtagStatus.failure);
      }
    }
  }
}
