part of 'detail_hashtag_bloc.dart';

abstract class DetailHashtagEvent {
  const DetailHashtagEvent();
}

class GetDetailHashTagEvent extends DetailHashtagEvent {
  final int id;
  GetDetailHashTagEvent(this.id);
}
