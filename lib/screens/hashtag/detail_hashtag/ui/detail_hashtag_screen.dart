import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_shimmer.dart';
import '../../../../commons/widgets/item_list_post.dart';
import '../../../../commons/widgets/list_empty.dart';
import '../../../../localizations/app_localization.dart';
import '../bloc/detail_hashtag_bloc/detail_hashtag_bloc.dart';
import '../bloc/list_post_by_hashtag_bloc/list_post_by_hashtag_bloc.dart';

class DetailHashtagScreen extends StatefulWidget {
  final int hashtagId;
  const DetailHashtagScreen(this.hashtagId, {Key key}) : super(key: key);
  @override
  _DetailHashtagScreenState createState() => _DetailHashtagScreenState();
}

class _DetailHashtagScreenState extends State<DetailHashtagScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final ListPostByHashtagBloc listPostByHashtagBloc = ListPostByHashtagBloc();
  final DetailHashtagBloc detailHashtagBloc = DetailHashtagBloc();
  final ScrollController _listHashtagController = ScrollController();
  @override
  void initState() {
    super.initState();
    detailHashtagBloc.add(GetDetailHashTagEvent(widget.hashtagId));
    _listHashtagController.addListener(() {
      if (!listPostByHashtagBloc.state.isLastPage) {
        if (_listHashtagController.position.pixels ==
            _listHashtagController.position.maxScrollExtent) {
          listPostByHashtagBloc.add(GetListPostByHashTag(widget.hashtagId));
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: BlocBuilder<DetailHashtagBloc, DetailHashtagState>(
        bloc: detailHashtagBloc,
        builder: (_, state) {
          if (state.status == DetailHashtagStatus.success) {
            return Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  ListTile(
                    horizontalTitleGap: 10.0,
                    contentPadding: EdgeInsets.all(0.0),
                    leading: Container(
                      padding: const EdgeInsets.symmetric(
                        vertical: 10.0,
                        horizontal: 15.0,
                      ),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        border: Border.all(
                          color: AppColors.backgroundSearchColor,
                          width: 2.0,
                        ),
                      ),
                      child: Text('#', style: theme.textTheme.headline2),
                    ),
                    title: Text(
                      state.hashtag.title,
                      style: theme.textTheme.headline4,
                    ),
                    subtitle: Text(
                      '${state.hashtag.numOfPost} ${lang.locaized('post')}',
                      style: theme.textTheme.bodyText1,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      lang.locaized('listPostHashTag'),
                      style: theme.textTheme.headline5,
                    ),
                  ),
                  Flexible(
                    child: BlocBuilder<ListPostByHashtagBloc,
                        ListPostByHashtagState>(
                      bloc: listPostByHashtagBloc,
                      builder: (context, state) {
                        if (state.status == ListPostHashtagStatus.initial) {
                          listPostByHashtagBloc.add(
                            GetListPostByHashTag(
                              widget.hashtagId,
                            ),
                          );
                        } else if (state.status ==
                            ListPostHashtagStatus.success) {
                          if (state.list.isEmpty)
                            return Center(
                              child: SizedBox(
                                height: sizeScreen * 70,
                                width: sizeScreen * 70,
                                child: AppObjectEmpty.list(),
                              ),
                            );
                          return Scrollbar(
                            child: StaggeredGridView.extentBuilder(
                              controller: _listHashtagController,
                              padding: const EdgeInsets.fromLTRB(
                                10.0,
                                5.0,
                                10.0,
                                0.0,
                              ),
                              itemCount: state.list.length,
                              staggeredTileBuilder: (index) =>
                                  StaggeredTile.extent(
                                1,
                                index.isEven ? 270 : 230,
                              ),
                              maxCrossAxisExtent: 200,
                              itemBuilder: (context, index) => ItemListPost(
                                state.list[index],
                              ),
                            ),
                          );
                        }
                        if (state.status == ListPostHashtagStatus.failure) {
                          return Center(
                            child: SizedBox(
                              height: sizeScreen * 5.0,
                              width: sizeScreen * 15.0,
                              child: AppButton.common(
                                labelText: lang.locaized('reConnect'),
                                labelStyle: theme.textTheme.headline6.copyWith(
                                  color: theme.backgroundColor,
                                ),
                                radius: 5.0,
                                backgroundColor: theme.primaryColor,
                                shadowColor: theme.primaryColor,
                                onPressed: () => listPostByHashtagBloc.add(
                                  GetListPostByHashTag(
                                    widget.hashtagId,
                                  ),
                                ),
                              ),
                            ),
                          );
                        }
                        return AppShimmer.list();
                      },
                    ),
                  ),
                ],
              ),
            );
          }
          if (state.status == DetailHashtagStatus.failure) {
            return Center(
              child: SizedBox(
                height: sizeScreen * 6,
                width: sizeScreen * 15,
                child: AppButton.common(
                  labelText: lang.locaized('reConnect'),
                  labelStyle: theme.textTheme.headline6.copyWith(
                    color: theme.backgroundColor,
                  ),
                  radius: 5.0,
                  backgroundColor: theme.primaryColor,
                  shadowColor: theme.primaryColor,
                  onPressed: () {
                    detailHashtagBloc.add(
                      GetDetailHashTagEvent(widget.hashtagId),
                    );
                  },
                ),
              ),
            );
          }
          return AppShimmer.home();
        },
      ),
    );
  }
}
