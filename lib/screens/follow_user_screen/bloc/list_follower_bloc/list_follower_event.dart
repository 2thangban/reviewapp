part of 'list_follower_bloc.dart';

abstract class ListFollowerEvent {}

/// In case the user get the list of follow
class GetListFollowerEvent extends ListFollowerEvent {
  final int page;
  final int limit;
  final bool isAuth;
  final int userId;
  GetListFollowerEvent(
    this.page,
    this.limit, {
    this.isAuth = false,
    this.userId,
  });
}
