part of 'list_follower_bloc.dart';

abstract class ListFollowerState {}

/// Initialization
class ListFollowerInitial extends ListFollowerState {}

/// In case the user is getting the list of follower.
class ListFollowerLoading extends ListFollowerState {}

/// In case the user get the list of follower successfully.
class ListFollowerSuccess extends ListFollowerState {
  final List<Account> listUser;
  ListFollowerSuccess(this.listUser);
}

/// In case the user get the list of follower failed.
class ListFollowerFailed extends ListFollowerState {}
