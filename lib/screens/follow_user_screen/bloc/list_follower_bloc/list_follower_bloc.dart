import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';

part 'list_follower_event.dart';
part 'list_follower_state.dart';

class ListFollowerBloc extends Bloc<ListFollowerEvent, ListFollowerState> {
  ListFollowerBloc() : super(ListFollowerInitial());

  @override
  Stream<ListFollowerState> mapEventToState(ListFollowerEvent event) async* {
    try {
      if (event is GetListFollowerEvent) {
        yield ListFollowerLoading();
        final result = await AccountRepo.getFollowerList(
          page: event.page,
          limit: event.limit,
          userId: event.userId,
        );
        yield ListFollowerSuccess(result);
      }
    } catch (e) {
      yield ListFollowerFailed();
    }
  }
}
