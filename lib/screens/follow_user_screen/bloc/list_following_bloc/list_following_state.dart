part of 'list_following_bloc.dart';

abstract class ListFollowingState {}

/// Initialization
class ListFollowingInitial extends ListFollowingState {}

/// In case the user is getting the list of following
class ListFollowingLoading extends ListFollowingState {}

/// In case the user get the list of following successfully
class ListFollowingSuccess extends ListFollowingState {
  final List<Account> listAccount;
  ListFollowingSuccess(this.listAccount);
}

/// In case the user get the list of following fail
class ListFollowingFailed extends ListFollowingState {}
