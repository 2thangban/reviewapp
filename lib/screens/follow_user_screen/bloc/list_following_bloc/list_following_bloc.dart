import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';

part 'list_following_event.dart';
part 'list_following_state.dart';

class ListFollowingBloc extends Bloc<ListFollowingEvent, ListFollowingState> {
  ListFollowingBloc() : super(ListFollowingInitial());

  @override
  Stream<ListFollowingState> mapEventToState(ListFollowingEvent event) async* {
    try {
      if (event is GetListFollowingEvent) {
        yield ListFollowingLoading();
        final result = await AccountRepo.getFollowingList(
          page: event.page,
          limit: event.limit,
          userId: event.userId,
        );
        yield ListFollowingSuccess(result);
      }
    } catch (e) {
      yield ListFollowingFailed();
    }
  }
}
