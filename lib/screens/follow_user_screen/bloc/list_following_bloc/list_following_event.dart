part of 'list_following_bloc.dart';

abstract class ListFollowingEvent {}

/// In case the usef get the list of following.
class GetListFollowingEvent extends ListFollowingEvent {
  final int page;
  final int limit;
  final bool isAuth;
  final int userId;
  GetListFollowingEvent(
    this.page,
    this.limit, {
    this.isAuth = false,
    this.userId,
  });
}
