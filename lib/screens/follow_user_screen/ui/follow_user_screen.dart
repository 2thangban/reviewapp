import 'package:flutter/material.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_icon.dart';
import '../../../commons/widgets/app_bar.dart';
import '../../../commons/widgets/app_button.dart';
import 'follower_partial.dart';
import 'following_partial.dart';

enum FollowType { following, follower }

class FollowUserData {
  final int totalUser;
  final FollowType type;

  const FollowUserData(this.type, this.totalUser);
}

// ignore: must_be_immutable
class FollowUserScreen extends StatelessWidget {
  final FollowUserData followUserData;
  FollowUserScreen(this.followUserData);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;

    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context),
        ),
        titleWidget: Text(
          followUserData.type == FollowType.follower
              ? 'Follower'
              : 'Đang follow',
          style: theme.textTheme.headline5,
        ),
        height: sizeScreen * 5.0,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const Divider(thickness: 1),
          Container(
            height: sizeScreen * 70,
            child: followUserData.totalUser == 0
                ? Flexible(
                    child: Center(
                      child: Text('Chưa có ai theo dõi'),
                    ),
                  )
                : followUserData.type == FollowType.follower
                    ? FollowerPartial()
                    : FollowingPartial(),
          ),
        ],
      ),
    );
  }
}
