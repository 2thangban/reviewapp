import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_image.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../commons/widgets/circle_avt.dart';
import '../../../models/user/user.dart';
import '../../../routes/route_name.dart';
import '../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../bloc/list_follower_bloc/list_follower_bloc.dart';

// ignore: must_be_immutable
class FollowerPartial extends StatelessWidget {
  final ListFollowerBloc listFollowerBloc = ListFollowerBloc();
  final int limit = 10;
  final List<Account> listfollow = [];
  int page = 0;
  AuthBloc authBloc;
  @override
  Widget build(BuildContext context) {
    authBloc = Provider.of<AuthBloc>(context);
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    final sizeText = SizedConfig.textMultiplier;
    return BlocBuilder<ListFollowerBloc, ListFollowerState>(
      bloc: listFollowerBloc,
      builder: (_, state) {
        if (state is ListFollowerInitial) {
          Future.delayed(
            Duration(milliseconds: 500),
            () => listFollowerBloc.add(
              GetListFollowerEvent(
                page,
                limit,
                userId: authBloc.account.id,
              ),
            ),
          );
          return Center(
            child: AppLoading.threeBounce(size: 20),
          );
        } else if (state is ListFollowerSuccess) {
          listfollow.addAll(state.listUser);
        }
        return ListView.builder(
          itemCount: listfollow.length,
          itemBuilder: (context, index) {
            final Account account = listfollow[index];
            // final FollowProvider followProvider = FollowProvider(true);
            return ListTile(
              onTap: () => Navigator.pushNamed(
                context,
                RouteName.profile,
              ),
              horizontalTitleGap: 5.0,
              contentPadding: const EdgeInsets.symmetric(
                horizontal: 10.0,
              ),
              minVerticalPadding: 0.0,
              leading: CircleAvt(
                image: '${AppAssets.baseUrl + account.avatar}',
                radius: 20.0,
              ),
              title: SizedBox(
                width: sizeScreen * 5,
                child: Text(
                  account.userName,
                  style: theme.textTheme.bodyText1,
                  maxLines: 1,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              subtitle: Text(
                '${account.numOfFollower} người theo dõi',
                style: theme.textTheme.bodyText2.copyWith(
                  fontSize: sizeText * 1.5,
                ),
              ),
              // trailing: ChangeNotifierProvider<FollowProvider>(
              //   create: (_) => followProvider,
              //   child: Consumer<FollowProvider>(
              //     builder: (_, follow, __) => FollowByUser(
              //       labelSize: sizeText * 2,
              //       widthFollow: sizeScreen * 1,
              //       widthUnFollow: sizeScreen * 1,
              //       isFollow: follow.status,
              //       onTap: () => follow.check(account.id),
              //     ),
              //   ),
              // ),
            );
          },
        );
      },
    );
  }
}
