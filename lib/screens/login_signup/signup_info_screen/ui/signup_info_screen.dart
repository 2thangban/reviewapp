import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';
import '../../../../routes/route_name.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import '../bloc/signup_info_user_page_transaction_cubit/signup_info_user_page_transaction_cubit.dart';
import 'pager/password_page.dart';
import 'pager/user_info_page.dart';
import 'pager/user_name_page.dart';

class SignUpInfo extends StatefulWidget {
  final String phoneNumber;
  const SignUpInfo(this.phoneNumber, {Key key}) : super(key: key);
  @override
  _SignUpInfoState createState() => _SignUpInfoState();
}

class _SignUpInfoState extends State<SignUpInfo> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final TextEditingController userNameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController firstNameController = TextEditingController();
  final TextEditingController lastNameController = TextEditingController();
  final TextEditingController birthdayController = TextEditingController();
  File avatar;
  int gender = 0;
  final userNameFocus = FocusNode();
  final passWordFocus = FocusNode();
  final firstNameFocus = FocusNode();
  final lastNameFocus = FocusNode();
  final PageController _pageController = PageController(initialPage: 0);
  final SignUpBloc signUpBloc = SignUpBloc();
  SignupInfoUserPageTransactionCubit pageTransactionCubit;
  Account account = Account();
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    pageTransactionCubit = SignupInfoUserPageTransactionCubit();
    account.phone = widget.phoneNumber;
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    pageTransactionCubit.close();
    _pageController.dispose();
    userNameFocus.dispose();
    passWordFocus.dispose();
    firstNameFocus.dispose();
    lastNameFocus.dispose();
    signUpBloc.close();
  }

  void allUnFocus() {
    userNameFocus.unfocus();
    passWordFocus.unfocus();
    firstNameFocus.unfocus();
    lastNameFocus.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);

    return BlocProvider<SignupInfoUserPageTransactionCubit>(
      create: (_) => pageTransactionCubit,
      child: Stack(
        children: [
          Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(sizeScreen * 10),
              child: BlocBuilder<SignupInfoUserPageTransactionCubit,
                  SignupInfoUserPageTransactionState>(
                bloc: pageTransactionCubit,
                builder: (context, state) {
                  bool isValid = false;
                  if (state is PageTransaction) {
                    isValid = state.isValid;
                    _pageController.animateToPage(
                      state.currentPage,
                      duration: Duration(milliseconds: 300),
                      curve: Curves.decelerate,
                    );
                  }
                  return CommonAppBar(
                    leftBarButtonItem: AppButton.icon(
                      icon: AppIcon.popNavigate,
                      iconSize: 30.0,
                      onTap: () {
                        if (pageTransactionCubit.currentPage == 0) {
                          Navigator.pop(context);
                        } else {
                          allUnFocus();
                          Future.delayed(
                            Duration(milliseconds: 300),
                            () => pageTransactionCubit.pageChange(
                              --pageTransactionCubit.currentPage,
                            ),
                          );
                        }
                      },
                    ),
                    titleWidget: Text(
                      (state is PageTransaction)
                          ? '${lang.locaized('labelStep')}  ${state.currentPage + 1}/3'
                          : '${lang.locaized('labelStep')} 1/3',
                      style: theme.textTheme.headline5,
                    ),
                    centerTitle: true,
                    rightBarButtonItems: [
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 6.0),
                        child: AppButton.common(
                          radius: 10.0,
                          width: sizeScreen * 12,
                          labelStyle: theme.textTheme.bodyText1.copyWith(
                            color: (state is ValidFieldSuccess) || isValid
                                ? theme.backgroundColor
                                : AppColors.subLightColor,
                          ),
                          shadowColor: theme.backgroundColor,
                          backgroundColor:
                              (state is ValidFieldSuccess) || isValid
                                  ? theme.primaryColor
                                  : AppColors.backgroundSearchColor,
                          labelText: lang.locaized('btnNext'),
                          onPressed: () async {
                            if (state is ValidFieldSuccess || isValid) {
                              if (pageTransactionCubit.currentPage < 2) {
                                allUnFocus();
                                Future.delayed(Duration(milliseconds: 300), () {
                                  pageTransactionCubit.pageChange(
                                    ++pageTransactionCubit.currentPage,
                                  );
                                });
                              } else {
                                //await FirebaseMessaging.instance.deleteToken();
                                account.firebaseMessagingToken =
                                    await FirebaseMessaging.instance.getToken();
                                signUpBloc.add(SignUpPhone(account));
                              }
                            }
                          },
                        ),
                      ),
                    ],
                  );
                },
              ),
            ),
            body: PageView(
              controller: _pageController,
              physics: NeverScrollableScrollPhysics(),
              children: [
                UserNamePage(account, userNameFocus: userNameFocus),
                PasswordPage(account, passWordFocus: passWordFocus),
                UserInfoPage(
                  account,
                  firstNameFocus: firstNameFocus,
                  lastNameFocus: lastNameFocus,
                ),
              ],
            ),
          ),
          BlocConsumer<SignUpBloc, SignUpState>(
            bloc: signUpBloc,
            listenWhen: (previous, current) => current != SignUpLoading(),
            listener: (_, state) async {
              if (state is SignUpSuccess) {
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  theme: theme,
                  iconColor: AppColors.sussess,
                  message: lang.locaized('successSignUp'),
                  duration: 600,
                );
                authBloc.add(CheckLogin());
                AccountRepo.write(state.account).then((value) {
                  Future.delayed(
                    const Duration(milliseconds: 700),
                    () => Navigator.pushNamedAndRemoveUntil(
                      context,
                      RouteName.home,
                      (route) => false,
                    ),
                  );
                });
              } else if (state is SignUpFailed) {
                AppShower.showFlushBar(
                  AppIcon.successCheck,
                  context,
                  theme: theme,
                  iconColor: AppColors.errorColor,
                  message: lang.locaized('errorSignUp'),
                  duration: 500,
                );
              }
            },
            builder: (_, state) {
              if (state is SignUpLoading) {
                return Container(
                  child: Center(
                    child: AppLoading.doubleBounce(size: 30),
                  ),
                );
              }
              return const SizedBox.shrink();
            },
          ),
        ],
      ),
    );
  }
}
