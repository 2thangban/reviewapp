import 'package:flutter/material.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../localizations/app_localization.dart';

// ignore: must_be_immutable
class GenderWidget extends StatefulWidget {
  int gender;
  GenderWidget(this.gender, {Key key}) : super(key: key);
  @override
  _GenderWidgetState createState() => _GenderWidgetState();
}

class _GenderWidgetState extends State<GenderWidget> {
  bool changeGender = true;
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final List<String> listGender = [
      lang.locaized('male'),
      lang.locaized('feMale'),
    ];
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          '${lang.locaized('titleGender')} :',
          style: theme.textTheme.headline6,
        ),
        const SizedBox(height: 5.0),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15.0),
          height: sizeScreen * 5.6,
          child: Stack(
            children: [
              GestureDetector(
                onTap: () {
                  setState(() {
                    changeGender = !changeGender;
                    widget.gender = changeGender ? 0 : 1;
                  });
                },
                child: Container(
                  decoration: ShapeDecoration(
                    color: AppColors.subLightColor.withOpacity(0.2),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(sizeScreen * 15),
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 30.0),
                    child: Row(
                      children: listGender
                          .map(
                            (e) => Expanded(
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(
                                  e,
                                  style: theme.textTheme.subtitle2.copyWith(
                                    color: AppColors.subLightColor,
                                    fontSize: sizeText * 2.4,
                                  ),
                                ),
                              ),
                            ),
                          )
                          .toList(),
                    ),
                  ),
                ),
              ),
              AnimatedAlign(
                alignment:
                    changeGender ? Alignment.centerLeft : Alignment.centerRight,
                duration: Duration(milliseconds: 300),
                curve: Curves.ease,
                child: Container(
                  alignment: Alignment.center,
                  height: sizeScreen * 5.6,
                  width: sizeScreen * 23,
                  decoration: ShapeDecoration(
                    shadows: [
                      BoxShadow(
                        color: AppColors.primaryColor,
                        spreadRadius: 1.5,
                      ),
                    ],
                    color: AppColors.primaryColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(sizeScreen * 10),
                    ),
                  ),
                  child: Text(
                    changeGender ? listGender[0] : listGender[1],
                    style: theme.textTheme.headline6.copyWith(
                      color: theme.backgroundColor,
                      fontSize: sizeText * 2.4,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
