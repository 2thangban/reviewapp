import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../bloc/signup_info_user_page_transaction_cubit/signup_info_user_page_transaction_cubit.dart';

class PasswordPage extends StatefulWidget {
  final FocusNode passWordFocus;
  final Account account;
  const PasswordPage(
    this.account, {
    Key key,
    this.passWordFocus,
  }) : super(key: key);
  @override
  _PasswordPageState createState() => _PasswordPageState();
}

class _PasswordPageState extends State<PasswordPage>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  SignupInfoUserPageTransactionCubit pageTransactionCubit;

  @override
  void initState() {
    super.initState();
    pageTransactionCubit =
        BlocProvider.of<SignupInfoUserPageTransactionCubit>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          lang.locaized('labelCreatepass'),
          style: theme.textTheme.headline5.copyWith(
            fontSize: sizeText * 3,
          ),
        ),
        const SizedBox(height: 20.0),
        Text(
          lang.locaized('labelRequiredPass'),
          style: theme.textTheme.bodyText2.copyWith(
            fontSize: sizeText * 2.1,
          ),
        ),
        const SizedBox(height: 70.0),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          height: sizeScreen * 6,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: AppColors.backgroundEditColor),
          ),
          child: AppTextField.common(
            focusNode: widget.passWordFocus,
            textAlign: TextAlign.center,
            textSize: sizeText * 2.3,
            hintText: lang.locaized('labelPassHint'),
            hintStyle: theme.textTheme.bodyText2,
            isPassWord: true,
            onChange: (String str) {
              pageTransactionCubit.account.password = str;
              pageTransactionCubit.validField(2);
              widget.account.password = str.trim();
            },
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
