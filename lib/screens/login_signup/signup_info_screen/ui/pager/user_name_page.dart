import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/helper/validator.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../bloc/signup_info_user_page_transaction_cubit/signup_info_user_page_transaction_cubit.dart';

class UserNamePage extends StatefulWidget {
  final FocusNode userNameFocus;
  final Account account;
  const UserNamePage(
    this.account, {
    Key key,
    this.userNameFocus,
  }) : super(key: key);
  @override
  _UserNamePageState createState() => _UserNamePageState();
}

class _UserNamePageState extends State<UserNamePage>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final SignupInfoUserPageTransactionCubit pageTransactionCubit =
        BlocProvider.of<SignupInfoUserPageTransactionCubit>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(lang.locaized('hello'), style: theme.textTheme.headline3),
        const SizedBox(height: 15.0),
        Text(
          lang.locaized('labelQuestionUserName'),
          style: theme.textTheme.bodyText2.copyWith(
            fontSize: sizeText * 2.1,
          ),
        ),
        const SizedBox(height: 70.0),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          height: sizeScreen * 6.0,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(color: AppColors.backgroundEditColor),
          ),
          child: AppTextField.common(
            focusNode: widget.userNameFocus,
            textAlign: TextAlign.center,
            textSize: sizeText * 2.3,
            hintText: lang.locaized('labelUsernameHint'),
            hintStyle: theme.textTheme.bodyText2,
            onChange: (String str) {
              if ((!Validator.isEmptyString(str) &&
                      pageTransactionCubit.account.userName.isEmpty) ||
                  (Validator.isEmptyString(str) &&
                      pageTransactionCubit.account.userName.isNotEmpty)) {
                pageTransactionCubit.account.userName = str;
                pageTransactionCubit.validField(1);
              }
              pageTransactionCubit.account.userName = str;
              widget.account.userName = str.trim();
            },
          ),
        ),
      ],
    );
  }

  @override
  bool get wantKeepAlive => true;
}
