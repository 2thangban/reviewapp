import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../commons/helper/app_date_picker.dart';
import '../../../../../commons/helper/size_config.dart';
import '../../../../../commons/untils/app_color.dart';
import '../../../../../commons/untils/app_icon.dart';
import '../../../../../commons/untils/app_image.dart';
import '../../../../../commons/untils/app_shower.dart';
import '../../../../../commons/widgets/app_button.dart';
import '../../../../../commons/widgets/app_textfield.dart';
import '../../../../../localizations/app_localization.dart';
import '../../../../../models/user/user.dart';
import '../../../../../theme.dart';
import '../../../../home_screen/user/edit_profile_screen/bloc/birthday_cubit/birthday_cubit.dart';
import '../../bloc/avatar_cubit/avatar_cubit.dart';
import '../gender_widget.dart';

// ignore: must_be_immutable
class UserInfoPage extends StatefulWidget {
  final FocusNode lastNameFocus;
  final FocusNode firstNameFocus;
  final Account account;
  UserInfoPage(
    this.account, {
    Key key,
    this.lastNameFocus,
    this.firstNameFocus,
  }) : super(key: key);
  @override
  _UserInfoPageState createState() => _UserInfoPageState();
}

class _UserInfoPageState extends State<UserInfoPage>
    with AutomaticKeepAliveClientMixin {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final BirthdayCubit birthdayCubit = BirthdayCubit('');

  /// Show error
  String firstNameError;

  String lastNameError;

  String birthDayError;

  AvatarSignUpCubit avatarSignUpCubit = AvatarSignUpCubit();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();

    avatarSignUpCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return SingleChildScrollView(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 10),
          Center(
            child: Stack(
              children: [
                BlocBuilder<AvatarSignUpCubit, File>(
                  bloc: avatarSignUpCubit,
                  builder: (context, file) => Container(
                    clipBehavior: Clip.antiAlias,
                    height: sizeScreen * 12,
                    width: sizeScreen * 12,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                        image: AssetImage(AppAssets.img1),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: file != null
                        ? Image.file(
                            file,
                            fit: BoxFit.cover,
                          )
                        : const SizedBox.shrink(),
                  ),
                ),
                Positioned(
                  right: 2.0,
                  bottom: 2.0,
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.primaryColor,
                      borderRadius: BorderRadius.circular(30.0),
                    ),
                    child: AppButton.icon(
                      icon: AppIcon.add,
                      iconColor: theme.backgroundColor,
                      iconSize: sizeScreen * 3,
                      onTap: () async =>
                          AppShower.showOptionAvatar(theme, context).then(
                        (value) {
                          if (value != null) {
                            avatarSignUpCubit.updateAvatar(value);
                            widget.account.imageUpLoad = value;
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 20.0),
          Text(lang.locaized('firstName'), style: theme.textTheme.headline6),
          const SizedBox(height: 5.0),
          AppTextField.common(
              focusNode: widget.firstNameFocus,
              height: sizeScreen * 6,
              hintText: lang.locaized('isNotUpdated'),
              hintStyle: theme.textTheme.bodyText2,
              errorText: firstNameError,
              radius: 10.0,
              backgroundColor: AppColors.backgroundSearchColor,
              onChange: (String str) {
                widget.account.firstName = str.trim();
              }),
          const SizedBox(height: 15.0),
          Text(lang.locaized('lastName'), style: theme.textTheme.headline6),
          const SizedBox(height: 5.0),
          AppTextField.common(
              focusNode: widget.lastNameFocus,
              height: sizeScreen * 6,
              hintText: lang.locaized('isNotUpdated'),
              hintStyle: theme.textTheme.bodyText2,
              errorText: lastNameError,
              radius: 10.0,
              backgroundColor: AppColors.backgroundSearchColor,
              onChange: (String str) {
                widget.account.lastName = str.trim();
              }),
          const SizedBox(height: 15.0),
          Text(lang.locaized('birthDay'), style: theme.textTheme.headline6),
          const SizedBox(height: 5.0),
          BlocBuilder<BirthdayCubit, String>(
            bloc: birthdayCubit,
            builder: (_, birthday) => GestureDetector(
              onTap: () async {
                final newDate = await AppDatePicker.getDate(context);
                if (newDate != '') {
                  widget.account.birthDay = newDate;

                  birthdayCubit.change(newDate);
                }
              },
              child: Container(
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  color: AppColors.backgroundSearchColor,
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Text(
                  birthday != '' ? birthday : lang.locaized('isNotUpdated'),
                  style: birthday != ''
                      ? theme.textTheme.bodyText1.copyWith(
                          fontWeight: AppFontWeight.regular,
                        )
                      : theme.textTheme.bodyText2.copyWith(
                          fontWeight: AppFontWeight.regular,
                        ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 15.0),
          GenderWidget(widget.account.gender),
        ],
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;
}
