import 'dart:io';

import 'package:bloc/bloc.dart';

class AvatarSignUpCubit extends Cubit<File> {
  AvatarSignUpCubit() : super(null);

  void updateAvatar(File file) => emit(file);
}
