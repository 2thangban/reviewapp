part of 'sign_up_bloc.dart';

enum SignUpStatus { initial, loading, success, failure }

class SignUpState {
  final SignUpStatus status;
  final Account account;

  const SignUpState({
    this.status = SignUpStatus.initial,
    this.account,
  });

  SignUpState cloneWith({
    SignUpStatus status,
    Account account,
  }) =>
      SignUpState(
        status: status ?? this.status,
        account: account ?? this.account,
      );
}
