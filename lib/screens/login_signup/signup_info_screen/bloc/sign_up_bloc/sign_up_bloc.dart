import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../models/token/token.dart';
import '../../../../../models/token/token_repo.dart';
import '../../../../../models/user/user.dart';
import '../../../../../models/user/user_repo.dart';

part 'sign_up_event.dart';
part 'sign_up_state.dart';

class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  SignUpBloc() : super(const SignUpState());

  @override
  Stream<SignUpState> mapEventToState(
    SignUpEvent event,
  ) async* {
    if (event is SignUpUserEvent) {
      yield state.cloneWith(status: SignUpStatus.loading);
      try {
        final result = await AccountRepo.signUp(
          event.account,
          SignUpType.phone,
        );
        Token token = Token(
          accessToken: result.accessToken,
          refreshToken: result.refreshToken,
        );
        await TokenRepo.write(token);
        yield state.cloneWith(status: SignUpStatus.success, account: result);
      } catch (e) {
        yield state.cloneWith(status: SignUpStatus.failure);
      }
    }
  }
}
