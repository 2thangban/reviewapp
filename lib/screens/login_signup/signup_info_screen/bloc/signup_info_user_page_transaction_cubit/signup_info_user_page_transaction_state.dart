part of 'signup_info_user_page_transaction_cubit.dart';

abstract class SignupInfoUserPageTransactionState {}

class SignupInfoUserPageTransactionInitial
    extends SignupInfoUserPageTransactionState {}

class TextFieldIsNotEmpty extends SignupInfoUserPageTransactionState {
  final bool isEmptyString;
  TextFieldIsNotEmpty({this.isEmptyString = false});
}

class PageTransaction extends SignupInfoUserPageTransactionState {
  final int currentPage;
  final bool isValid;
  PageTransaction(this.currentPage, {this.isValid = false});
}

class ValidFieldSuccess extends SignupInfoUserPageTransactionState {}

class ValidFieldFailed extends SignupInfoUserPageTransactionState {}
