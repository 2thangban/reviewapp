import 'package:bloc/bloc.dart';

import '../../../../../commons/helper/validator.dart';
import '../../../../../models/user/user.dart';

part 'signup_info_user_page_transaction_state.dart';

class SignupInfoUserPageTransactionCubit
    extends Cubit<SignupInfoUserPageTransactionState> {
  Account account = Account();
  int currentPage = 0;
  SignupInfoUserPageTransactionCubit()
      : super(SignupInfoUserPageTransactionInitial());

  void pageChange(int nextPage) {
    bool isvalid = false;
    switch (nextPage) {
      case 0:
        if (!Validator.isEmptyString(account.userName)) isvalid = true;
        break;
      case 1:
        if (!Validator.isEmptyString(account.password)) isvalid = true;
        break;
      case 2:
        isvalid = true;
        break;
      default:
    }
    emit(PageTransaction(nextPage, isValid: isvalid));
  }

  void validField(int currentPage) {
    switch (currentPage) {
      case 1:
        if (Validator.isEmptyString(account.userName)) {
          emit(ValidFieldFailed());
        } else
          emit(ValidFieldSuccess());
        break;
      case 2:
        if (Validator.isValidPass(account.password)) {
          emit(ValidFieldFailed());
        } else
          emit(ValidFieldSuccess());
        break;
      case 3:
        emit(ValidFieldSuccess());
        break;
      default:
    }
  }
}
