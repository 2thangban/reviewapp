import 'package:bloc/bloc.dart';

import '../../../../commons/helper/validator.dart';

class ValidatePhoneNumberCubit extends Cubit<bool> {
  ValidatePhoneNumberCubit() : super(null);

  void validate(String phoneNumber) {
    if (Validator.isValidPhone(phoneNumber)) {
      emit(true);
    } else
      emit(false);
  }
}
