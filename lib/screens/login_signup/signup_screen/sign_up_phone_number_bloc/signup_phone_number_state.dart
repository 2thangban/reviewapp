part of 'signup_phone_number_bloc.dart';

enum RegisPhoneNumberStatus { initial, loading, success, failure }

class RegisPhoneNumberState {
  final RegisPhoneNumberStatus status;
  final String phoneNumber;
  const RegisPhoneNumberState({
    this.status = RegisPhoneNumberStatus.initial,
    this.phoneNumber,
  });

  RegisPhoneNumberState cloneWith({
    final RegisPhoneNumberStatus status,
    final String phoneNumber,
  }) =>
      RegisPhoneNumberState(
        status: status ?? this.status,
        phoneNumber: phoneNumber ?? this.phoneNumber,
      );
}
