part of 'signup_phone_number_bloc.dart';

abstract class SignupPhoneNumberEvent {}

class CheckPhoneNumber extends SignupPhoneNumberEvent {
  String phoneNumber;
  CheckPhoneNumber(this.phoneNumber);
}
