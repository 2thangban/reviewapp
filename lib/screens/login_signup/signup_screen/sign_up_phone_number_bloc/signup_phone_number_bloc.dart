import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/user/user_repo.dart';

part 'signup_phone_number_event.dart';
part 'signup_phone_number_state.dart';

class SignupPhoneNumberBloc
    extends Bloc<SignupPhoneNumberEvent, RegisPhoneNumberState> {
  SignupPhoneNumberBloc() : super(const RegisPhoneNumberState());

  @override
  Stream<RegisPhoneNumberState> mapEventToState(
      SignupPhoneNumberEvent event) async* {
    if (event is CheckPhoneNumber) {
      yield state.cloneWith(status: RegisPhoneNumberStatus.loading);
      try {
        final result = await AccountRepo.checkPhone(event.phoneNumber);
        if (!result.phoneIsExisted) {
          yield state.cloneWith(
            status: RegisPhoneNumberStatus.success,
            phoneNumber: event.phoneNumber,
          );
        } else
          yield state.cloneWith(status: RegisPhoneNumberStatus.failure);
      } catch (e) {
        yield state.cloneWith(status: RegisPhoneNumberStatus.failure);
      }
    }
  }
}
