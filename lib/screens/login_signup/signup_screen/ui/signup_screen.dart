import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../cubit/validate_phone_number_cubit.dart';
import '../sign_up_phone_number_bloc/signup_phone_number_bloc.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final TextEditingController phoneController = new TextEditingController();
  final phoneFocus = new FocusNode();
  final ValidatePhoneNumberCubit phoneNumberCubit = ValidatePhoneNumberCubit();
  final SignupPhoneNumberBloc signupPhoneNumberBloc = SignupPhoneNumberBloc();
  @override
  void dispose() {
    super.dispose();
    phoneController.dispose();
    phoneFocus.dispose();
    phoneNumberCubit.close();
    signupPhoneNumberBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: CupertinoIcons.back,
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: Column(
        children: [
          const SizedBox(height: 80.0),
          RichText(
            textAlign: TextAlign.center,
            text: TextSpan(
              style: theme.textTheme.headline5,
              text: lang.locaized('labelYourNumber'),
              children: [
                WidgetSpan(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width - 100,
                    child: Text(
                      '\n${lang.locaized('labelDescriptionNumber')}',
                      textAlign: TextAlign.center,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 50.0),
          BlocBuilder<ValidatePhoneNumberCubit, bool>(
            bloc: phoneNumberCubit,
            builder: (_, check) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10.0),
              child: TextField(
                controller: phoneController,
                focusNode: phoneFocus,
                decoration: InputDecoration(
                  hintText: 'VD: 0303 123 123',
                  errorText: check != null
                      ? check == false
                          ? lang.locaized('validPhone')
                          : null
                      : null,
                  errorStyle: theme.textTheme.subtitle1.copyWith(
                    color: AppColors.errorColor,
                  ),
                  hintStyle: theme.textTheme.headline6.copyWith(
                    color: AppColors.subLightColor,
                    fontWeight: AppFontWeight.regular,
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    gapPadding: 0.0,
                  ),
                ),
                cursorHeight: 18.0,
                cursorColor: theme.primaryColor,
                onChanged: (String input) {
                  phoneNumberCubit.validate(input);
                },
              ),
            ),
          ),
          const Spacer(),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: BlocConsumer<SignupPhoneNumberBloc, RegisPhoneNumberState>(
              bloc: signupPhoneNumberBloc,
              listener: (_, state) {
                if (state.status == RegisPhoneNumberStatus.success) {
                  Future.delayed(
                    Duration(milliseconds: 400),
                    () {
                      Navigator.pushNamed(
                        context,
                        RouteName.signUpInfo,
                        arguments: state.phoneNumber,
                      );
                    },
                  );
                } else if (state.status == RegisPhoneNumberStatus.failure) {
                  AppShower.showFlushBar(
                    AppIcon.failedCheck,
                    context,
                    message: lang.locaized('numberIsExisted'),
                    theme: theme,
                    iconColor: AppColors.errorColor,
                  );
                }
              },
              builder: (context, state) {
                return BlocBuilder<ValidatePhoneNumberCubit, bool>(
                  bloc: phoneNumberCubit,
                  builder: (context, check) {
                    return AppButton.common(
                      labelText: lang.locaized('btnNext'),
                      labelStyle: theme.textTheme.headline6.copyWith(
                        color: check != null
                            ? check
                                ? theme.backgroundColor
                                : AppColors.subLightColor
                            : AppColors.subLightColor,
                      ),
                      contentPadding: 15.0,
                      radius: 5.0,
                      loadingWidget:
                          (state.status == RegisPhoneNumberStatus.loading)
                              ? SizedBox(
                                  height: sizeScreen * 2.5,
                                  width: sizeScreen * 2.5,
                                  child: CircularProgressIndicator(
                                    backgroundColor: theme.backgroundColor,
                                    strokeWidth: 3.0,
                                  ),
                                )
                              : null,
                      shadowColor: theme.backgroundColor,
                      backgroundColor: check != null
                          ? check
                              ? theme.primaryColor
                              : AppColors.backgroundSearchColor
                          : AppColors.backgroundSearchColor,
                      onPressed: () {
                        if (check &&
                            state.status != RegisPhoneNumberStatus.loading) {
                          phoneFocus.unfocus();
                          signupPhoneNumberBloc.add(
                            CheckPhoneNumber(phoneController.text),
                          );
                        }
                      },
                    );
                  },
                );
              },
            ),
          ),
          AppButton.text(
            label: lang.locaized('labelExistedUser'),
            labelStyle: theme.textTheme.bodyText2.copyWith(
              color: AppColors.subLightColor,
            ),
            onTap: () {
              phoneFocus.unfocus();
              Future.delayed(Duration(milliseconds: 250), () {
                Navigator.pop(context);
              });
            },
          ),
        ],
      ),
    );
  }
}
