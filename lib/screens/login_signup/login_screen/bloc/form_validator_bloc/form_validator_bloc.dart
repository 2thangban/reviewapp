import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';

import '../../../../../commons/helper/validator.dart';
import '../../../../../localizations/app_localization.dart';

part 'form_validator_event.dart';
part 'form_validator_state.dart';

class FormLoginValidatorBloc
    extends Bloc<FormLoginValidatorEvent, FormLoginValidatorState> {
  final BuildContext context;
  FormLoginValidatorBloc(this.context)
      : super(FormLoginValidatorInitial(context));

  @override
  Stream<FormLoginValidatorState> mapEventToState(
      FormLoginValidatorEvent event) async* {
    final AppLocalization lang = AppLocalization.of(context);

    /// In case validate phone number
    if (event is PhoneValidatorEvent) {
      final String phoneError = Validator.isEmptyString(event.phone)
          ? '${lang.locaized('labelPhoneField')} ' +
              ' ${lang.locaized('isEmpty')}'
          : (Validator.isValidPhone(event.phone)
              ? null
              : lang.locaized('formatPhone'));
      yield (event.pass == null && phoneError == null)
          ? FormLoginValidatorSuccess()
          : ValidatorPhoneError(phoneError);
    }

    /// In case validate pass
    else if (event is PassValidatorEvent) {
      final String passError = Validator.isEmptyString(event.pass)
          ? '${lang.locaized('labelPassField')} ' +
              ' ${lang.locaized('isEmpty')}'
          : (Validator.isValidPass(event.pass)
              ? lang.locaized('formatPass')
              : null);
      yield (event.phone == null && passError == null)
          ? FormLoginValidatorSuccess()
          : ValidatorPassError(passError);
    }

    /// In case the user click login
    else if (event is CheckFormLoginEvent) {
      final String phoneError = Validator.isEmptyString(event.phone)
          ? '${lang.locaized('labelPhoneField')} ' +
              ' ${lang.locaized('isEmpty')}'
          : (Validator.isValidPhone(event.phone)
              ? null
              : lang.locaized('formatPhone'));
      final String passError = Validator.isEmptyString(event.pass)
          ? '${lang.locaized('labelPassField')} ' +
              ' ${lang.locaized('isEmpty')}'
          : (Validator.isValidPass(event.pass)
              ? lang.locaized('formatPass')
              : null);
      yield (phoneError == null && passError == null)
          ? FormLoginValidatorSuccess()
          : FormLoginValidatorError(passError, phoneError);
    }
  }
}
