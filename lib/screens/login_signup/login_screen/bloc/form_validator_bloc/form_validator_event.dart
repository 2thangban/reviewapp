part of 'form_validator_bloc.dart';

abstract class FormLoginValidatorEvent {}

class PhoneValidatorEvent extends FormLoginValidatorEvent {
  final String phone;
  final String pass;
  PhoneValidatorEvent(this.phone, this.pass);
}

class PassValidatorEvent extends FormLoginValidatorEvent {
  final String pass;
  final String phone;
  PassValidatorEvent(this.phone, this.pass);
}

class CheckFormLoginEvent extends FormLoginValidatorEvent {
  final String phone;
  final String pass;
  CheckFormLoginEvent(this.phone, this.pass);
}
