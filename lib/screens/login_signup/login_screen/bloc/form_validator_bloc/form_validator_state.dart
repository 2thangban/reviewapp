part of 'form_validator_bloc.dart';

abstract class FormLoginValidatorState {}

class FormLoginValidatorInitial extends FormLoginValidatorState {
  BuildContext context;
  FormLoginValidatorInitial(this.context);
}

class ValidatorPhoneError extends FormLoginValidatorState {
  final String errorPhone;
  ValidatorPhoneError(this.errorPhone);
}

class ValidatorPassError extends FormLoginValidatorState {
  final String errorPass;
  ValidatorPassError(this.errorPass);
}

class FormLoginValidatorError extends FormLoginValidatorState {
  final String errorPass;
  final String errorPhone;
  FormLoginValidatorError(this.errorPass, this.errorPhone);
}

class FormLoginValidatorSuccess extends FormLoginValidatorState {}
