part of 'login_bloc.dart';

@immutable
abstract class LoginState {}

class LoginInitial extends LoginState {}

class LoginSuccess extends LoginState {
  final Account account;
  LoginSuccess(this.account);
}

class LoginFailed extends LoginState {
  final String mgs;
  LoginFailed({this.mgs});
}

class LoginLoading extends LoginState {}
