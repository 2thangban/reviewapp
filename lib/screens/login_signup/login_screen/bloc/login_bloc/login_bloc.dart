import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../models/token/token.dart';
import '../../../../../models/token/token_repo.dart';
import '../../../../../models/user/user.dart';
import '../../../../../models/user/user_repo.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitial());

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginUserEvent) {
      yield LoginLoading();
      try {
        final user = await AccountRepo.login(event.account);

        /// save token to local app
        final token = Token(
          accessToken: user.accessToken,
          refreshToken: user.refreshToken,
        );
        await TokenRepo.write(token);
        yield LoginSuccess(user);
      } catch (e) {
        yield LoginFailed(mgs: e.toString());
      }
    }
  }
}
