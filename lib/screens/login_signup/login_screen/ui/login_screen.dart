import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/user/user_repo.dart';
import '../../../../routes/route_name.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import 'form_login_widget.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final size = SizedConfig.heightMultiplier;
  final SignUpBloc signUpBloc = SignUpBloc();
  AuthBloc authBloc;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    signUpBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return BlocProvider(
      create: (_) => signUpBloc,
      child: Scaffold(
        body: Stack(
          alignment: Alignment.topRight,
          children: [
            Container(
              height: size.height,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    theme.backgroundColor,
                    theme.backgroundColor,
                    Color(0xFFE6E6E6),
                  ],
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 30.0,
                        horizontal: 5.0,
                      ),
                      child: AppButton.icon(
                        icon: CupertinoIcons.back,
                        onTap: () => Navigator.pop(context, false),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 10.0),
                      child: Column(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: Image.asset(
                              AppAssets.appIcon,
                              fit: BoxFit.cover,
                              width: 100,
                              height: 100,
                            ),
                          ),
                        ],
                      ),
                    ),
                    const FormLoginWidget(),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 40.0),
                      alignment: Alignment.bottomCenter,
                      child: RichText(
                        text: TextSpan(
                          text: lang.locaized('optionSignIn'),
                          style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 16.0,
                            color: Colors.black,
                          ),
                          children: [
                            WidgetSpan(
                              child: GestureDetector(
                                onTap: () {
                                  Future.delayed(Duration(milliseconds: 300),
                                      () {
                                    Navigator.pushNamed(
                                      context,
                                      RouteName.signUp,
                                    );
                                  });
                                },
                                child: Text(
                                  lang.locaized('labelSignUp'),
                                  style: theme.textTheme.headline6.copyWith(
                                    color: theme.primaryColor,
                                    decoration: TextDecoration.underline,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            BlocConsumer<SignUpBloc, SignUpState>(
              bloc: signUpBloc,
              listener: (context, state) async {
                if (state is SignUpSuccess) {
                  sleep(Duration(milliseconds: 500));
                  authBloc.add(CheckLogin());
                  await AccountRepo.write(state.account).then(
                    (_) => Navigator.pop(context, true),
                  );
                } else if (state is SignUpFailed) {
                  AppShower.showFlushBar(
                    AppIcon.failedCheck,
                    context,
                    theme: theme,
                    message: lang.locaized('errorLogin'),
                    iconColor: AppColors.errorColor,
                  );
                }
              },
              buildWhen: (previous, current) => current is SignUpLoading,
              builder: (_, state) => state is SignUpLoading
                  ? Container(
                      color: Colors.black.withOpacity(0.2),
                      child: Center(
                        child: Container(
                          width: 60,
                          height: 60,
                          padding: const EdgeInsets.all(15.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            color: theme.backgroundColor,
                          ),
                          child: CircularProgressIndicator(strokeWidth: 3),
                        ),
                      ),
                    )
                  : const SizedBox(),
            ),
          ],
        ),
      ),
    );
  }
}
