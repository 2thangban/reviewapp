import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../../commons/helper/auth_app/auth_google.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../../app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import '../bloc/form_validator_bloc/form_validator_bloc.dart';
import '../bloc/login_bloc/login_bloc.dart';

class FormLoginWidget extends StatefulWidget {
  const FormLoginWidget({Key key}) : super(key: key);

  @override
  _FormLoginWidgetState createState() => _FormLoginWidgetState();
}

class _FormLoginWidgetState extends State<FormLoginWidget> {
  final TextEditingController _phoneController = new TextEditingController();
  final TextEditingController _passController = new TextEditingController();
  final FocusNode _focusPhoneNode = FocusNode();
  final FocusNode _focusPassNode = FocusNode();
  final LoginBloc loginBloc = LoginBloc();
  SignUpBloc signUpBloc;
  Account account = Account();
  FormLoginValidatorBloc formValidatorBloc;
  AuthBloc authBloc;
  String phoneError;
  String passError;
  bool isLoading = false;
  bool validSuccess = false;
  @override
  void initState() {
    super.initState();
    formValidatorBloc = FormLoginValidatorBloc(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
    signUpBloc = BlocProvider.of<SignUpBloc>(context);
    addNewToken();
  }

  void addNewToken() async {
    account.firebaseMessagingToken =
        await FirebaseMessaging.instance.getToken();
  }

  @override
  void dispose() {
    super.dispose();
    formValidatorBloc.close();
    loginBloc.close();
    _focusPhoneNode.dispose();
    _focusPassNode.dispose();
    _phoneController.dispose();
    _passController.dispose();
  }

  void unFocus() {
    _focusPhoneNode.unfocus();
    _focusPassNode.unfocus();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return Container(
      margin: const EdgeInsets.all(10.0),
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300],
            spreadRadius: 2.0,
            blurRadius: 10.0,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Column(
        children: [
          const SizedBox(height: 10.0),
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              lang.locaized('titleformLogin'),
              style: theme.textTheme.headline4,
            ),
          ),
          const SizedBox(height: 20.0),
          BlocBuilder<FormLoginValidatorBloc, FormLoginValidatorState>(
            bloc: formValidatorBloc,
            builder: (context, state) {
              validSuccess = (state is FormLoginValidatorSuccess);
              phoneError = (state is ValidatorPhoneError)
                  ? state.errorPhone
                  : (state is FormLoginValidatorError)
                      ? state.errorPhone
                      : (state is FormLoginValidatorInitial)
                          ? null
                          : (state is FormLoginValidatorSuccess)
                              ? null
                              : phoneError;
              passError = (state is ValidatorPassError)
                  ? state.errorPass
                  : (state is FormLoginValidatorError)
                      ? state.errorPass
                      : (state is FormLoginValidatorInitial)
                          ? null
                          : (state is FormLoginValidatorSuccess)
                              ? null
                              : passError;

              return Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextField(
                    focusNode: _focusPhoneNode,
                    controller: _phoneController,
                    textInputAction: TextInputAction.done,
                    cursorHeight: 20.0,
                    cursorColor: Color(0xFFFF6600),
                    decoration: InputDecoration(
                      hintText: lang.locaized('labelPhoneField'),
                      hintStyle: TextStyle(
                        color: Colors.grey[400],
                        fontWeight: FontWeight.bold,
                      ),
                      errorText: phoneError,
                      errorStyle: theme.textTheme.subtitle1.copyWith(
                        color: AppColors.errorColor,
                      ),
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.subLightColor,
                          width: 0.5,
                        ),
                      ),
                    ),
                    onChanged: (String phone) => formValidatorBloc.add(
                      PhoneValidatorEvent(phone, passError),
                    ),
                  ),
                  const SizedBox(height: 10.0),
                  TextField(
                    focusNode: _focusPassNode,
                    controller: _passController,
                    textInputAction: TextInputAction.done,
                    cursorColor: Color(0xFFFF6600),
                    cursorHeight: 20.0,
                    keyboardType: TextInputType.text,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: UnderlineInputBorder(
                        borderSide: BorderSide(
                          color: AppColors.subLightColor.withOpacity(0.5),
                          width: 0.3,
                        ),
                      ),
                      hintText: lang.locaized('labelPassField'),
                      hintStyle: TextStyle(
                          color: Colors.grey[400], fontWeight: FontWeight.bold),
                      errorText: passError,
                      errorStyle: theme.textTheme.subtitle1.copyWith(
                        color: AppColors.errorColor,
                      ),
                    ),
                    onChanged: (String pass) => formValidatorBloc.add(
                      PassValidatorEvent(phoneError, pass),
                    ),
                  ),
                ],
              );
            },
          ),
          const SizedBox(height: 30.0),
          BlocConsumer<LoginBloc, LoginState>(
            bloc: loginBloc,
            listener: (context, state) async {
              if (state is LoginSuccess) {
                authBloc.add(CheckLogin());
                await AccountRepo.write(state.account)
                    .then((_) => Navigator.pop(context, true));
              } else if (state is LoginFailed) {
                AppShower.showFlushBar(
                  AppIcon.failedCheck,
                  context,
                  theme: theme,
                  iconColor: AppColors.errorColor,
                  message: state.mgs,
                );
              }
            },
            builder: (context, state) => AppButton.common(
              labelText: lang.locaized('labelBtnLogin'),
              labelStyle: theme.textTheme.headline5.copyWith(
                color: theme.backgroundColor,
              ),
              loadingWidget: state is LoginLoading
                  ? SizedBox(
                      height: 21.0,
                      width: 21.0,
                      child: CircularProgressIndicator(
                        backgroundColor: theme.backgroundColor,
                        strokeWidth: 2.5,
                      ),
                    )
                  : null,
              shadowColor: theme.primaryColor,
              contentPadding: 13.0,
              backgroundColor: theme.primaryColor,
              onPressed: () {
                unFocus();
                if (validSuccess) {
                  account.phone = _phoneController.text;
                  account.password = _passController.text;

                  loginBloc.add(LoginUserEvent(account));
                } else {
                  formValidatorBloc.add(
                    CheckFormLoginEvent(
                      _phoneController.text,
                      _passController.text,
                    ),
                  );
                }
              },
            ),
          ),
          const SizedBox(height: 60.0),
          Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Text(
              lang.locaized("subTitlesignUp"),
              style: theme.textTheme.headline6,
            ),
          ),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              AppButton.icon(
                icon: FontAwesomeIcons.googlePlus,
                iconColor: Colors.red,
                iconSize: sizeScreen * 4.5,
                onTap: () async {
                  unFocus();
                  AuthGoogle authGG = AuthGoogle();
                  await authGG.login().then(
                    (account) async {
                      if (account == null) return;
                      signUpBloc.add(SignUpSocialAccount(account));
                      await authGG.logout();
                    },
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}
