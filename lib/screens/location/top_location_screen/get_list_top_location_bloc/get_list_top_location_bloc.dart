import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

import '../../../../models/location/location.dart';
import '../../../../models/location/location_repo.dart';

part 'get_list_top_location_event.dart';
part 'get_list_top_location_state.dart';

class GetListTopLocationBloc
    extends Bloc<GetListTopLocationEvent, GetListTopLocationState> {
  GetListTopLocationBloc() : super(GetListTopLocationState());
  final limit = 10;
  int page = 0;
  @override
  Stream<GetListTopLocationState> mapEventToState(
    GetListTopLocationEvent event,
  ) async* {
    if (event is GetListTopLocation) {
      if (event.isRefresh) {
        page = 0;
        state.list.clear();
      }
      yield state.cloneWith(status: ListTopLocationStatus.loading);
      try {
        final result = await LocationRepo.getTopLocation(
          page: page++,
          limit: limit,
        );
        sleep(Duration(milliseconds: 200));
        yield state.cloneWith(
          status: ListTopLocationStatus.success,
          list: List.of(state.list)..addAll(result),
          isLastPage: result.length < limit,
        );
      } catch (e) {
        yield state.cloneWith(status: ListTopLocationStatus.failure);
      }
    }
  }
}
