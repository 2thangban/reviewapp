part of 'get_list_top_location_bloc.dart';

abstract class GetListTopLocationEvent {
  const GetListTopLocationEvent();
}

class GetListTopLocation extends GetListTopLocationEvent {
  final bool isRefresh;
  GetListTopLocation({this.isRefresh = false});
}
