part of 'get_list_top_location_bloc.dart';

enum ListTopLocationStatus { initial, loading, success, failure }

class GetListTopLocationState extends Equatable {
  final ListTopLocationStatus status;
  final List<Location> list;
  final bool isLastPage;
  const GetListTopLocationState({
    this.status = ListTopLocationStatus.initial,
    this.list = const <Location>[],
    this.isLastPage = false,
  });

  GetListTopLocationState cloneWith({
    ListTopLocationStatus status,
    List<Location> list,
    bool isLastPage,
  }) =>
      GetListTopLocationState(
        status: status ?? this.status,
        list: list ?? this.list,
        isLastPage: isLastPage ?? this.isLastPage,
      );

  @override
  List<Object> get props => [status, list, isLastPage];
}
