import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../theme.dart';
import '../get_list_top_location_bloc/get_list_top_location_bloc.dart';
import 'item_top_location.dart';

class TopLocationScreen extends StatefulWidget {
  const TopLocationScreen({Key key}) : super(key: key);
  @override
  _TopLocationScreenState createState() => _TopLocationScreenState();
}

class _TopLocationScreenState extends State<TopLocationScreen> {
  final double sizeScreen = SizedConfig.heightMultiplier;
  final GetListTopLocationBloc getListTopLocationBloc =
      GetListTopLocationBloc();
  final ScrollController _listLocationController = ScrollController();
  @override
  void initState() {
    super.initState();
    Future.delayed(
      Duration(milliseconds: 250),
      () => getListTopLocationBloc.add(GetListTopLocation()),
    );

    _listLocationController.addListener(() {
      if (_listLocationController.position.pixels ==
              _listLocationController.position.maxScrollExtent &&
          !getListTopLocationBloc.state.isLastPage) {
        getListTopLocationBloc.add(GetListTopLocation());
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    getListTopLocationBloc.close();
    _listLocationController.dispose();
  }

  int getLastMonth() {
    final currentTime = DateTime.now();
    final previousMounth = currentTime.month - 1;
    return previousMounth == 0 ? 12 : previousMounth;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      backgroundColor: AppColors.darkThemeColor,
      appBar: CommonAppBar(
        statusBarColor: Colors.transparent,
        backgroundColor: AppColors.darkThemeColor,
        automaticallyImplyLeading: false,
        rightBarButtonItems: [
          AppButton.icon(
            icon: AppIcon.exitNavigate,
            iconColor: theme.primaryColor,
            iconSize: sizeScreen * 3,
            onTap: () => Future.delayed(
              Duration(milliseconds: 200),
              () => Navigator.pop(context),
            ),
          ),
        ],
      ),
      body: Container(
        color: AppColors.darkThemeColor,
        child: RefreshIndicator(
          onRefresh: () async {
            getListTopLocationBloc.add(GetListTopLocation(isRefresh: true));
          },
          child: SingleChildScrollView(
            controller: _listLocationController,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 50.0, bottom: 10.0),
                        child: Icon(
                          AppIcon.topPlaces,
                          size: 30.0,
                          color: AppColors.lightThemeColor,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          lang.locaized('topLocation'),
                          style: theme.textTheme.headline5.copyWith(
                            color: AppColors.lightThemeColor,
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5.0),
                        child: Text(
                          "${lang.locaized('month')} ${getLastMonth().toString()}",
                          style: theme.textTheme.headline3.copyWith(
                            color: AppColors.lightThemeColor,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 20.0),
                        child: Text(
                          "TP. HỒ CHÍ MINH",
                          style: theme.textTheme.bodyText1.copyWith(
                            color: AppColors.lightThemeColor,
                            fontWeight: AppFontWeight.medium,
                            fontStyle: FontStyle.italic,
                          ),
                        ),
                      ),
                      Container(
                        padding: const EdgeInsets.symmetric(
                          vertical: 15.0,
                          horizontal: 6.0,
                        ),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10.0),
                          color: theme.backgroundColor.withOpacity(0.1),
                        ),
                        child: Text(
                          lang.locaized('descriptionTopLocation'),
                          style: theme.textTheme.bodyText1.copyWith(
                            color: AppColors.lightThemeColor.withOpacity(0.8),
                            fontWeight: AppFontWeight.regular,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
                BlocBuilder<GetListTopLocationBloc, GetListTopLocationState>(
                  bloc: getListTopLocationBloc,
                  builder: (_, state) {
                    if (state.status == ListTopLocationStatus.initial) {
                      return Column(
                        children: [
                          AppLoading.threeBounce(
                            size: 20.0,
                            color: theme.backgroundColor,
                          ),
                          SizedBox(height: sizeScreen * 60),
                        ],
                      );
                    } else if (state.status == ListTopLocationStatus.failure) {
                      ///
                    }
                    return ListView.builder(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      physics: NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.list.length,
                      itemBuilder: (context, index) => ItemTopLocation(
                        location: state.list[index],
                      ),
                    );
                  },
                ),
                BlocBuilder<GetListTopLocationBloc, GetListTopLocationState>(
                  bloc: getListTopLocationBloc,
                  builder: (_, state) =>
                      state.status == ListTopLocationStatus.loading
                          ? AppLoading.threeBounce(
                              size: 20.0,
                              color: theme.backgroundColor,
                            )
                          : const SizedBox(),
                ),
                const SizedBox(height: 300),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
