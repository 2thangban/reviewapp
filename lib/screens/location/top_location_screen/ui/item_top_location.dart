import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/location/location.dart';
import '../../../../routes/route_name.dart';

class ItemTopLocation extends StatelessWidget {
  final Location location;
  const ItemTopLocation({Key key, this.location})
      : assert(location != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    final sizedImage = SizedConfig.imageSizeMultiplier;
    return InkWell(
      onTap: () => Future.delayed(
        Duration(milliseconds: 200),
        () => Navigator.pushNamed(
          context,
          RouteName.detailLocation,
          arguments: location.id,
        ),
      ),
      child: Card(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 10.0),
                clipBehavior: Clip.antiAlias,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Image.network(
                  AppAssets.baseUrl + location.image.first,
                  fit: BoxFit.fill,
                  height: sizedImage * 18,
                  width: sizedImage * 18,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width - 150,
                    child: Text(
                      location.name,
                      style: theme.textTheme.bodyText1,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: SizedBox(
                      width: sizeScreen * 30,
                      child: Text(
                        location.address,
                        maxLines: 2,
                        style: theme.textTheme.bodyText2.copyWith(
                          fontSize: sizeScreen * 1.5,
                        ),
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SvgPicture.asset(
                        AppAssets.ratingIcon,
                        height: 13.0,
                        width: 13.0,
                        fit: BoxFit.fill,
                      ),
                      const SizedBox(width: 2.5),
                      RichText(
                        text: TextSpan(
                          text: location.rating.toString(),
                          style: theme.textTheme.bodyText1.copyWith(
                            fontSize: sizeScreen * 1.6,
                          ),
                          children: [
                            TextSpan(
                              text:
                                  AppLocalization.of(context).locaized('point'),
                              style: theme.textTheme.bodyText2.copyWith(
                                fontSize: sizeScreen * 1.6,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
