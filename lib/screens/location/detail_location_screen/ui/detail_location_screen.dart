import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/animation/fade_anim.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_shimmer.dart';
import '../bloc/detail_location_bloc/detail_location_bloc.dart';
import 'header_info_location.dart';
import 'info_location.dart';
import 'list_review_location.dart';

class DetailLocationScreen extends StatelessWidget {
  final locationId;
  final DetailLocationBloc detailLocationBloc = DetailLocationBloc();
  DetailLocationScreen(this.locationId);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return BlocBuilder<DetailLocationBloc, DetailLocationState>(
      bloc: detailLocationBloc,
      builder: (context, state) {
        if (state is DetailLocationInitial) {
          detailLocationBloc.add(GetDetailLocationEvent(locationId));
        } else if (state is DetailLocationFailed) {
        } else if (state is DetailLocationSuccess) {
          return Scaffold(
            backgroundColor: AppColors.backgroundSearchColor,
            appBar: CommonAppBar(
              statusBarColor: theme.backgroundColor,
              leftBarButtonItem: AppButton.icon(
                icon: AppIcon.popNavigate,
                onTap: () => Navigator.pop(context),
              ),
            ),
            body: FadeAnim(
              duration: const Duration(milliseconds: 400),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Card(
                      margin: const EdgeInsets.only(bottom: 2.5),
                      color: theme.backgroundColor,
                      elevation: 0.0,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15.0),
                          bottomRight: Radius.circular(15.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          children: [
                            const SizedBox(height: 10.0),
                            HeaderInfoLocation(state.location),
                            InfoLocation(state.location),
                          ],
                        ),
                      ),
                    ),
                    Card(
                      elevation: 0.0,
                      margin: const EdgeInsets.only(top: 2.5),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15.0),
                          topRight: Radius.circular(15.0),
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: ListReviewByLocation(state.location),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
        return AppShimmer.home();
      },
    );
  }
}
