import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/location/location.dart';

// ignore: must_be_immutable
class HeaderInfoLocation extends StatelessWidget {
  final Location location;
  HeaderInfoLocation(this.location);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizedImage = SizedConfig.imageSizeMultiplier;
    final sizedText = SizedConfig.textMultiplier;
    final sizeScreen = SizedConfig.textMultiplier;
    final lang = AppLocalization.of(context);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Text(
            location.name.toUpperCase(),
            style: theme.textTheme.headline4,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        FittedBox(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 2.0),
                child: SvgPicture.asset(
                  AppAssets.ratingIcon,
                  height: sizeScreen * 2.1,
                ),
              ),
              const SizedBox(width: 2.0),
              RichText(
                text: TextSpan(
                  text: location.rating.toString(),
                  style: theme.textTheme.bodyText1,
                  children: [
                    TextSpan(
                      text: lang.locaized('point'),
                      style: theme.textTheme.bodyText2.copyWith(
                          color: AppColors.darkThemeColor.withOpacity(0.8)),
                    ),
                    TextSpan(
                      text:
                          " (${location.postByLocation} ${lang.locaized('labelpost')})",
                      style: theme.textTheme.bodyText2,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Container(
          height: sizedImage * 25,
          margin: EdgeInsets.symmetric(vertical: 10.0),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: (context, index) => index == 0
                ? Stack(
                    children: [
                      Opacity(
                        opacity: 0.9,
                        child: Container(
                          clipBehavior: Clip.antiAlias,
                          decoration: BoxDecoration(
                            color: AppColors.listBgColor[0],
                            borderRadius: BorderRadius.circular(5.0),
                            image: DecorationImage(
                                image: NetworkImage(
                                  AppAssets.baseUrl + location.image[index],
                                ),
                                fit: BoxFit.cover),
                          ),
                          margin: EdgeInsets.only(right: 7.0),
                          width: sizedImage * 35,
                        ),
                      ),
                      Positioned(
                        left: 5.0,
                        bottom: 5.0,
                        child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: AppColors.darkThemeColor.withOpacity(0.6),
                            borderRadius: BorderRadius.circular(15.0),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(
                                AppIcon.image,
                                color: AppColors.lightThemeColor,
                                size: sizedText * 2,
                              ),
                              Text(
                                location.image.length.toString(),
                                style: theme.textTheme.subtitle1.copyWith(
                                  color: AppColors.lightThemeColor,
                                  fontSize: sizedText * 1.6,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                : Container(
                    width: sizedImage * 35,
                    clipBehavior: Clip.antiAlias,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    margin: EdgeInsets.only(right: 7.0),
                    child: Opacity(
                      opacity: 0.8,
                      child: Image.network(
                        AppAssets.baseUrl + location.image[index],
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
            itemCount: location.image.length,
          ),
        ),
      ],
    );
  }
}
