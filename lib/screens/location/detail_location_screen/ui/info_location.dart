import 'package:flutter/material.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/location/location.dart';
import '../../../../theme.dart';

class InfoLocation extends StatelessWidget {
  final Location location;
  InfoLocation(this.location);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 5.0),
          child: Text(
            lang.locaized('labelInfo'),
            style: theme.textTheme.headline4,
            softWrap: true,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: FittedBox(
            child: Row(
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: Icon(AppIcon.pin),
                ),
                SizedBox(
                  width: sizeScreen * 45,
                  child: Text(
                    location.address,
                    style: theme.textTheme.bodyText1.copyWith(
                      fontWeight: AppFontWeight.medium,
                    ),
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                ),
              ],
            ),
          ),
        ),
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: Icon(AppIcon.clock),
            ),
            Text(
              location.mgsTime(),
              style: theme.textTheme.bodyText1.copyWith(
                color: Colors.tealAccent[700],
              ),
              softWrap: true,
              overflow: TextOverflow.ellipsis,
              maxLines: 2,
            ),
            const Spacer(),
            Text(
              "${location.open} - ${location.closed}",
              style: theme.textTheme.bodyText1,
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Icon(
                  AppIcon.phoneNumber,
                  size: sizeScreen * 3.3,
                ),
              ),
              Text(
                location.phone,
                style: theme.textTheme.bodyText1.copyWith(
                  color: Colors.amber[900],
                ),
                softWrap: true,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 5.0),
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10.0),
                child: Icon(AppIcon.money, size: sizeScreen * 3.5),
              ),
              RichText(
                text: TextSpan(
                  style: theme.textTheme.bodyText1,
                  text: '${location.lowest}đ',
                  children: [
                    TextSpan(text: '  -  '),
                    TextSpan(text: '${location.hightest}đ'),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
