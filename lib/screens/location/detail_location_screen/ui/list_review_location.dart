import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/location/location.dart';
import '../../../../models/post/post.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../bloc/post_by_location_bloc/post_by_location_bloc.dart';
import 'item_review_location.dart';

class ListReviewByLocation extends StatefulWidget {
  final Location location;
  ListReviewByLocation(this.location);

  @override
  _ListReviewByLocationState createState() => _ListReviewByLocationState();
}

class _ListReviewByLocationState extends State<ListReviewByLocation> {
  final PostByLocationBloc postByLocationBloc = PostByLocationBloc();

  final List<Post> postByLocation = [];

  final sizedScreen = SizedConfig.heightMultiplier;

  final sizedText = SizedConfig.textMultiplier;

  AuthBloc authBloc;

  final int limit = 5;
  int page = 0;
  bool isLastPage = false;

  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final int totalPost =
        widget.location.postByLocation; // total post by location
    final lang = AppLocalization.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              "${lang.locaized('labelTotalReview')} (${widget.location.postByLocation})",
              style: theme.textTheme.headline4,
              softWrap: true,
              overflow: TextOverflow.ellipsis,
            ),
          ],
        ),
        widget.location.postByLocation != 0
            ? BlocBuilder<PostByLocationBloc, PostByLocationState>(
                bloc: postByLocationBloc,
                builder: (context, state) {
                  if (state is PostByLocationInitial) {
                    postByLocationBloc.add(
                      GetPostByLocationEvent(
                        widget.location.id,
                        page: page++,
                        limit: limit,
                        isAuth: authBloc.isChecked,
                      ),
                    );
                  } else if (state is PostByLocationSuccess) {
                    postByLocation.addAll(state.posts);
                    if (state.posts.length == totalPost) {
                      isLastPage = true;
                    }
                  } else if (state is PostByLocationFailed) {}
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListView.separated(
                        padding: const EdgeInsets.only(top: 10.0),
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: postByLocation.length,
                        itemBuilder: (context, index) =>
                            ItemReviewLocation(postByLocation[index]),
                        separatorBuilder: (context, index) => const Divider(
                          thickness: 1.5,
                        ),
                      ),
                      const Divider(thickness: 1.5),
                      isLastPage
                          ? const SizedBox.shrink()
                          : state is PostByLocationLoading
                              ? Container(
                                  alignment: Alignment.topCenter,
                                  height: sizedScreen * 10,
                                  child: AppLoading.threeBounce(size: 10.0),
                                )
                              : Container(
                                  alignment: Alignment.center,
                                  height: sizedScreen * 6,
                                  child: AppButton.text(
                                    label: lang.locaized('more'),
                                    onTap: () => print("Xe thêm"),
                                  ),
                                )
                    ],
                  );
                },
              )
            : Container(
                height: sizedScreen * 31,
                margin: const EdgeInsets.only(top: 10.0),
                decoration: BoxDecoration(
                  color: AppColors.backgroundSearchColor.withOpacity(0.5),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          lang.locaized('labelListReviewEmpty'),
                          style: theme.textTheme.headline5.copyWith(
                            color: AppColors.subLightColor,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
      ],
    );
  }
}
