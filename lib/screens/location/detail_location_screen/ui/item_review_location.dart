import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/circle_avt.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/post/post.dart';
import '../../../../models/user/user.dart';
import '../../../../routes/route_name.dart';

class ItemReviewLocation extends StatelessWidget {
  final Post post;
  ItemReviewLocation(this.post);

  @override
  Widget build(BuildContext context) {
    final sizedImage = SizedConfig.imageSizeMultiplier;
    final sizedScreen = SizedConfig.heightMultiplier;
    final sizedText = SizedConfig.textMultiplier;

    final theme = Theme.of(context);
    final Account user = post.account;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ListTile(
            onTap: () => Navigator.pushNamed(
              context,
              RouteName.profile,
              arguments: user.id,
            ),
            dense: true,
            minLeadingWidth: 1.0,
            horizontalTitleGap: 10.0,
            contentPadding: EdgeInsets.zero,
            leading: CircleAvt(
              image: AppAssets.baseUrl + user.avatar,
              radius: 18.0,
            ),
            title: Text(
              user.userName,
              style: theme.textTheme.headline6,
            ),
            subtitle: Text(post.customeDate),
          ),
          const SizedBox(height: 5.0),
          InkWell(
            splashColor: AppColors.primaryColor.withOpacity(0.2),
            onTap: () => Navigator.pushNamed(
              context,
              RouteName.postDetail,
              arguments: post.id,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 5.0),
                      child: SvgPicture.asset(
                        AppAssets.ratingIcon,
                        height: sizedImage * 4.1,
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        text: post.rating.toString(),
                        style: theme.textTheme.bodyText1
                            .copyWith(fontSize: sizedText * 2.2),
                        children: [
                          TextSpan(
                            text: AppLocalization.of(context).locaized('point'),
                            style: theme.textTheme.bodyText1.copyWith(
                              fontSize: sizedText * 2,
                              color: AppColors.subLightColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 10.0),
                Text(
                  post.titlePost.toUpperCase(),
                  style: theme.textTheme.headline6,
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
                const SizedBox(height: 5.0),
                Text(
                  post.content,
                  style: theme.textTheme.subtitle1
                      .copyWith(fontSize: sizedScreen * 2),
                  softWrap: true,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 5,
                ),
                Container(
                  height: sizedImage * 20,
                  margin: const EdgeInsets.only(top: 10.0, bottom: 5.0),
                  child: Stack(
                    children: [
                      Flexible(
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: post.listImage.length,
                          itemBuilder: (_, index) => Container(
                            clipBehavior: Clip.antiAlias,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              color: AppColors.listBgColor[0],
                              image: DecorationImage(
                                image: NetworkImage(
                                  AppAssets.baseUrl + post.listImage[index],
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                            margin: EdgeInsets.only(right: 7.0),
                            width: sizedImage * 21.9,
                          ),
                        ),
                      ),
                      Positioned(
                        right: 15.0,
                        bottom: 3.0,
                        child: Container(
                          padding: const EdgeInsets.symmetric(
                            vertical: 3.0,
                            horizontal: 10.0,
                          ),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15.0),
                            color: AppColors.darkThemeColor.withOpacity(0.6),
                          ),
                          child: Text(
                            post.listImage.length.toString(),
                            style: theme.textTheme.subtitle1.copyWith(
                              color: AppColors.lightThemeColor,
                              fontSize: sizedText * 1.8,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
