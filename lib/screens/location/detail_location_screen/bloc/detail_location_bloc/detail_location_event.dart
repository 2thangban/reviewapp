part of 'detail_location_bloc.dart';

abstract class DetailLocationEvent {}

class GetDetailLocationEvent extends DetailLocationEvent {
  final int id;
  GetDetailLocationEvent(this.id);
}
