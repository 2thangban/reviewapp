import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../models/location/location.dart';
import '../../../../../models/location/location_repo.dart';

part 'detail_location_event.dart';
part 'detail_location_state.dart';

class DetailLocationBloc
    extends Bloc<DetailLocationEvent, DetailLocationState> {
  DetailLocationBloc() : super(DetailLocationInitial());

  @override
  Stream<DetailLocationState> mapEventToState(
      DetailLocationEvent event) async* {
    try {
      if (event is GetDetailLocationEvent) {
        yield DetailLocationLoading();
        final result = await LocationRepo.detail(event.id);
        yield DetailLocationSuccess(result);
      }
    } catch (e) {
      yield DetailLocationFailed(mgs: e.toString());
    }
  }
}
