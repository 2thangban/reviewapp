part of 'detail_location_bloc.dart';

abstract class DetailLocationState {}

class DetailLocationInitial extends DetailLocationState {}

class DetailLocationLoading extends DetailLocationState {}

class DetailLocationSuccess extends DetailLocationState {
  final Location location;
  DetailLocationSuccess(this.location);
}

class DetailLocationFailed extends DetailLocationState {
  final String mgs;
  DetailLocationFailed({this.mgs});
}
