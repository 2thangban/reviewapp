part of 'post_by_location_bloc.dart';

abstract class PostByLocationState {}

class PostByLocationInitial extends PostByLocationState {}

class PostByLocationLoading extends PostByLocationState {}

class PostByLocationSuccess extends PostByLocationState {
  final List<Post> posts;
  PostByLocationSuccess(this.posts);
}

class PostByLocationFailed extends PostByLocationState {}
