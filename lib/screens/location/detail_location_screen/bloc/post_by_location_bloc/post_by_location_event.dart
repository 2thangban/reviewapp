part of 'post_by_location_bloc.dart';

abstract class PostByLocationEvent {}

class GetPostByLocationEvent extends PostByLocationEvent {
  final int locationId;
  final int page;
  final int limit;
  final bool isAuth;
  GetPostByLocationEvent(this.locationId, {this.page, this.limit, this.isAuth});
}
