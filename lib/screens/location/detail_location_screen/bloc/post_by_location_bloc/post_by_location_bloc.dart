import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../models/location/location_repo.dart';
import '../../../../../models/post/post.dart';

part 'post_by_location_event.dart';
part 'post_by_location_state.dart';

class PostByLocationBloc
    extends Bloc<PostByLocationEvent, PostByLocationState> {
  PostByLocationBloc() : super(PostByLocationInitial());

  @override
  Stream<PostByLocationState> mapEventToState(
      PostByLocationEvent event) async* {
    if (event is GetPostByLocationEvent) {
      yield PostByLocationLoading();
      try {
        final result = await LocationRepo.listPost(
          event.locationId,
          page: event.page,
          limit: event.limit,
          isAuth: event.isAuth,
        );
        yield PostByLocationSuccess(result);
      } catch (e) {
        yield PostByLocationFailed();
      }
    }
  }
}
