import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/animation/transaction_anim.dart';
import '../../../../commons/helper/debouncer.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../models/province/province.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../../app_common_bloc/province_bloc/province_bloc.dart';
import '../bloc/change_province_search_cubit/change_province_search_cubit.dart';
import '../bloc/search_location_bloc/search_location_bloc.dart';
import 'location_by_user.dart';
import 'recent_view_location.dart';
import 'result_search_list.dart';

class Bodysearch extends StatefulWidget {
  const Bodysearch({Key key}) : super(key: key);
  @override
  _BodysearchState createState() => _BodysearchState();
}

class _BodysearchState extends State<Bodysearch> {
  final sizeScreen = SizedConfig.heightMultiplier;

  final sizeText = SizedConfig.textMultiplier;

  final Debouncer searchDelay = Debouncer();

  final SearchLocationBloc searchLocationBloc = SearchLocationBloc();
  ChangeProvinceSearchCubit changeProvinceSearchCubit;
  AppLocationBloc appLocationBloc;
  ProvinceBloc provinceBloc;

  String searchKey = '';
  @override
  void initState() {
    super.initState();
    appLocationBloc = BlocProvider.of<AppLocationBloc>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
    changeProvinceSearchCubit =
        ChangeProvinceSearchCubit(provinceBloc.currentProvince);
  }

  @override
  void dispose() {
    changeProvinceSearchCubit.close();
    searchDelay.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            height: sizeScreen * 8,
            child: Row(
              children: [
                Flexible(
                  child: TextFormField(
                    style: TextStyle(fontSize: sizeText * 2),
                    cursorColor: AppColors.primaryColor,
                    cursorHeight: 20,
                    decoration: InputDecoration(
                      prefixIcon: Icon(
                        AppIcon.search,
                        size: sizeScreen * 3,
                      ),
                      hintText: 'Tìm Riviu, địa điểm,...',
                      hintStyle: theme.textTheme.bodyText2
                          .copyWith(fontSize: sizeText * 2),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30.0),
                          bottomLeft: Radius.circular(30.0),
                        ),
                        borderSide: BorderSide.none,
                      ),
                      filled: true,
                      fillColor: AppColors.subLightColor.withOpacity(0.15),
                      contentPadding: const EdgeInsets.symmetric(
                        horizontal: 15.0,
                      ),
                    ),
                    onChanged: (String input) {
                      searchDelay.debounce(
                        () {
                          searchKey = input.trim();
                          searchLocationBloc.add(
                            GetListLocationEvent(
                              key: searchKey,
                              provinceId: changeProvinceSearchCubit.province.id,
                            ),
                          );
                        },
                      );
                    },
                  ),
                ),
                BlocBuilder<ChangeProvinceSearchCubit, Province>(
                  bloc: changeProvinceSearchCubit,
                  builder: (_, province) {
                    return Container(
                      padding: const EdgeInsets.fromLTRB(0.0, 5.0, 5.0, 5.0),
                      decoration: BoxDecoration(
                        color: AppColors.subLightColor.withOpacity(0.15),
                        borderRadius: BorderRadius.only(
                          topRight: Radius.circular(30.0),
                          bottomRight: Radius.circular(30.0),
                        ),
                      ),
                      child: AppButton.text(
                        label: province.name,
                        onTap: () async {
                          final newProvince = await showChangeAddress(
                            context,
                            theme,
                            appLocationBloc.listProvince,
                          );
                          if (newProvince != null && newProvince != province) {
                            Future.delayed(
                              Duration(milliseconds: defaultDuration),
                              () => searchLocationBloc.add(
                                GetListLocationEvent(
                                  key: searchKey,
                                  provinceId: newProvince.id,
                                ),
                              ),
                            );
                            changeProvinceSearchCubit.change(newProvince);
                          }
                        },
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          const SizedBox(height: 10.0),
          Flexible(
            child: BlocBuilder<SearchLocationBloc, SearchLocationState>(
              bloc: searchLocationBloc,
              builder: (_, state) {
                if (state is SearchLocationInitial) {
                  return SingleChildScrollView(
                    child: Column(
                      children: [
                        SizedBox(
                          height: sizeScreen * 30,
                          child: const Center(
                            child: Text('Search'),
                          ),
                        ),
                        const LocationByUser(),
                        const RecentViewLocaion(),
                      ],
                    ),
                  );
                } else if (state is SearchLocationSuccess) {
                  return ResultSearchList(state.listLocation);
                } else if (state is SearchLocationLoading) {
                  return Center(child: AppLoading.chasingDots(size: 20));
                } else if (state is SearchLocationFailed) {
                  ///
                }
                return const SizedBox();
              },
            ),
          ),
        ],
      ),
    );
  }

  Future<Province> showChangeAddress(
    BuildContext context,
    ThemeData theme,
    List<Province> listprovince,
  ) async {
    return showGeneralDialog(
      context: context,
      barrierDismissible: true,
      barrierLabel: '',
      transitionDuration: Duration(milliseconds: 350),
      transitionBuilder: (_, _animation, _secondaryAnimation, child) =>
          TransactionAnima.fromRight(_animation, _secondaryAnimation, child),
      pageBuilder: (_, _animation, _secondaryAnimation) => Dialog(
        elevation: 0.0,
        insetAnimationDuration: Duration(milliseconds: 200),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
        ),
        insetPadding: const EdgeInsets.all(0.0),
        child: Scaffold(
          appBar: CommonAppBar(
            leftBarButtonItem: AppButton.icon(
              icon: AppIcon.popNavigate,
              onTap: () => Navigator.pop(context, null),
            ),
            titleWidget: Text(
              'Chọn khu vực',
              style: theme.textTheme.headline4,
            ),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 10.0,
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10.0),
                  child: Text(
                    "Các địa điểm chọn gần đây",
                    style: theme.textTheme.headline5,
                  ),
                ),
                GridView.count(
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  crossAxisCount: 3,
                  childAspectRatio: 5 / 2,
                  padding: const EdgeInsets.only(top: 10.0),
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: provinceBloc.boxProvince.values
                      .map(
                        (e) => MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: BorderSide(
                              color: AppColors.subLightColor,
                            ),
                          ),
                          elevation: 0.0,
                          color: theme.backgroundColor,
                          onPressed: () => Navigator.pop(context, e),
                          child: Text(
                            e.name,
                            style: theme.textTheme.bodyText1.copyWith(
                              fontWeight: AppFontWeight.regular,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 25.0),
                  child: Text(
                    "Các tỉnh thành",
                    style: theme.textTheme.headline5,
                  ),
                ),
                GridView.count(
                  mainAxisSpacing: 10.0,
                  crossAxisSpacing: 10.0,
                  crossAxisCount: 3,
                  childAspectRatio: 5 / 2,
                  padding: const EdgeInsets.only(top: 10.0),
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  children: listprovince
                      .map(
                        (e) => MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                            side: BorderSide(
                              color: AppColors.subLightColor,
                            ),
                          ),
                          elevation: 0.0,
                          color: theme.backgroundColor,
                          onPressed: () => Navigator.pop(context, e),
                          child: Text(
                            e.name,
                            style: theme.textTheme.bodyText1.copyWith(
                              fontWeight: AppFontWeight.regular,
                            ),
                          ),
                        ),
                      )
                      .toList(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
