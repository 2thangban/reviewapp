import 'package:flutter/material.dart';

import '../../../../models/location/location.dart';
import 'result_search_list.dart';

class RecentViewLocaion extends StatelessWidget {
  const RecentViewLocaion({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final List<Location> listRecentViewLocation = [];

    return listRecentViewLocation.isNotEmpty
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Địa điểm từng xem",
                style: theme.textTheme.headline5,
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: ResultSearchList([]),
              ),
            ],
          )
        : const SizedBox();
  }
}
