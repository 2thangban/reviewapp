import 'package:flutter/material.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../routes/route_name.dart';
import 'body_search.dart';

class SearchLocationScreen extends StatefulWidget {
  const SearchLocationScreen({Key key}) : super(key: key);
  @override
  _SearchLocationScreenState createState() => _SearchLocationScreenState();
}

class _SearchLocationScreenState extends State<SearchLocationScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        statusBarColor: theme.backgroundColor,
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context),
        ),
        rightBarButtonItems: [
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: AppButton.common(
              labelText: '+Tạo địa điểm',
              labelStyle: theme.textTheme.headline6.copyWith(
                color: theme.primaryColor,
              ),
              backgroundColor: theme.primaryColor.withOpacity(0.2),
              shadowColor: theme.backgroundColor,
              radius: 10.0,
              contentPadding: 5.0,
              width: sizeScreen * 20.0,
              onPressed: () async => await Navigator.pushNamed(
                context,
                RouteName.createLocation,
              ).then(
                (location) =>
                    location != null ? Navigator.pop(context, location) : null,
              ),
            ),
          ),
        ],
      ),
      body: const Bodysearch(),
    );
  }
}
