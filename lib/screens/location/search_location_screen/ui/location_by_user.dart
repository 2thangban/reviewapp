import 'package:flutter/material.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../models/location/location.dart';
import 'result_search_list.dart';

class LocationByUser extends StatefulWidget {
  const LocationByUser({Key key}) : super(key: key);
  @override
  _LocationByUserState createState() => _LocationByUserState();
}

class _LocationByUserState extends State<LocationByUser> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.textMultiplier;
  final List<Location> listLocationByUser = [];
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return listLocationByUser.isNotEmpty
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Địa điểm gần bạn",
                style: theme.textTheme.headline5,
              ),
              Container(
                padding: const EdgeInsets.all(10.0),
                child: ResultSearchList([]),
              ),
            ],
          )
        : const SizedBox();
  }
}
