import 'package:flutter/material.dart';

import '../../../../commons/widgets/item_location.dart';
import '../../../../models/location/location.dart';

class ResultSearchList extends StatelessWidget {
  final List<Location> _listLocation;
  ResultSearchList(this._listLocation);
  @override
  Widget build(BuildContext context) {
    return _listLocation.isEmpty
        ? Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text('Không tìm thấy địa điểm'),
          )
        : ListView.separated(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            itemCount: _listLocation.length,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () => Navigator.pop(context, _listLocation[index]),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: ItemLocation(_listLocation[index]),
              ),
            ),
            separatorBuilder: (_, index) => Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: const Divider(
                height: 5.0,
                thickness: 1.0,
              ),
            ),
          );
  }
}
