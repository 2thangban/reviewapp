import 'package:bloc/bloc.dart';

import '../../../../../models/province/province.dart';

class ChangeProvinceSearchCubit extends Cubit<Province> {
  Province province;
  ChangeProvinceSearchCubit(this.province) : super(province);

  void change(Province newProvince) => emit(this.province = newProvince);
}
