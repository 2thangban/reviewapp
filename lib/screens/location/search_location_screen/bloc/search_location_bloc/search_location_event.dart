part of 'search_location_bloc.dart';

@immutable
abstract class SearchLocationEvent {}

class GetListLocationEvent extends SearchLocationEvent {
  final String key;
  final int page;
  final int limit;
  final int type;
  final int provinceId;
  GetListLocationEvent({
    this.key = '',
    this.page,
    this.limit,
    this.type,
    this.provinceId,
  });
}
