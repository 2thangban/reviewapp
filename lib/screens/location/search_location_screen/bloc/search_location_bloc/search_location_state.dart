part of 'search_location_bloc.dart';

@immutable
abstract class SearchLocationState {}

class SearchLocationInitial extends SearchLocationState {}

class SearchLocationLoading extends SearchLocationState {}

class SearchLocationFailed extends SearchLocationState {}

class SearchLocationSuccess extends SearchLocationState {
  final List<Location> listLocation;

  SearchLocationSuccess(this.listLocation);
}
