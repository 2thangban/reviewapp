import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../../commons/helper/validator.dart';
import '../../../../../models/location/location.dart';
import '../../../../../models/location/location_repo.dart';

part 'search_location_event.dart';
part 'search_location_state.dart';

class SearchLocationBloc
    extends Bloc<SearchLocationEvent, SearchLocationState> {
  SearchLocationBloc() : super(SearchLocationInitial());

  @override
  Stream<SearchLocationState> mapEventToState(
    SearchLocationEvent event,
  ) async* {
    yield SearchLocationLoading();
    try {
      if (event is GetListLocationEvent) {
        if (Validator.isEmptyString(event.key)) {
          yield SearchLocationInitial();
        } else {
          final listResult = await LocationRepo.search(
            key: event.key,
            page: event.page,
            limit: event.limit,
            provinceId: event.provinceId,
          );
          yield SearchLocationSuccess(listResult);
        }
      }
    } catch (e) {
      yield SearchLocationFailed();
    }
  }
}
