import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../../../../commons/untils/app_color.dart';
import '../../../../location_app/location_manager.dart';
import '../../../../models/province/province.dart';

/// This class handles get current position by user
class PositionProvider extends ChangeNotifier {
  bool _status = false;
  Province _province;

  get status => this._status;
  get province => this._province;

  void getPosition(List<Province> listProvince) async {
    final currentPostion = await LocationManager.getPosition();
    if (currentPostion != null) {
      /// get address
      final address = await LocationManager.getAddress(currentPostion);
      this._province = LocationManager.convertToProvince(address, listProvince);
      this._status = true;
      notifyListeners();
    }
  }

  Widget messageWidget() => status
      ? Container(
          padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
          decoration: BoxDecoration(
            color: AppColors.primaryColor.withOpacity(0.2),
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: Text(
            "Bạn đang ở ${_province.name}",
            style: TextStyle(
              color: AppColors.primaryColor,
            ),
          ),
        )
      : const SizedBox();

  Widget positionButton(
          BuildContext context, Function onTap, double sizeScreen) =>
      GestureDetector(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.only(left: 10.0),
          decoration: BoxDecoration(
            color: AppColors.primaryColor.withOpacity(0.2),
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Icon(
            Icons.gps_fixed,
            color: AppColors.primaryColor,
          ),
        ),
      );
}
