part of 'search_address_bloc.dart';

enum SearchAddressStatus { initial, loading, success, empty }

class SearchAddressState {
  final SearchAddressStatus status;
  final List<Province> listResult;
  const SearchAddressState({
    this.status = SearchAddressStatus.initial,
    this.listResult = const <Province>[],
  });

  SearchAddressState cloneWith({
    SearchAddressStatus status,
    List<Province> listResult,
  }) =>
      SearchAddressState(
        status: status ?? this.status,
        listResult: listResult ?? this.listResult,
      );
}
