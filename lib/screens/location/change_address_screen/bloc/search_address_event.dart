part of 'search_address_bloc.dart';

abstract class SearchAddressEvent {
  const SearchAddressEvent();
}

class SearchAddress extends SearchAddressEvent {
  String searchKey;

  SearchAddress({this.searchKey = ''});
}
