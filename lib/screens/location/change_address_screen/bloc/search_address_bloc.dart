import 'dart:async';

import 'package:app_review/models/province/province.dart';
import 'package:bloc/bloc.dart';

part 'search_address_event.dart';
part 'search_address_state.dart';

class SearchAddressBloc extends Bloc<SearchAddressEvent, SearchAddressState> {
  final List<Province> list;
  SearchAddressBloc(this.list) : super(SearchAddressState());

  @override
  Stream<SearchAddressState> mapEventToState(SearchAddressEvent event) async* {
    if (event is SearchAddress) {
      if (event.searchKey == '') {
        yield state.cloneWith(status: SearchAddressStatus.initial);
      } else {
        yield state.cloneWith(status: SearchAddressStatus.loading);
        List<Province> result = [];
        Future.delayed(const Duration(milliseconds: 200), () {
          for (var item in list) {
            if (item.name.contains(event.searchKey)) {
              result.add(item);
            }
          }
        });
        if (result.isEmpty) {
          yield state.cloneWith(status: SearchAddressStatus.empty);
        } else {
          yield state.cloneWith(
            status: SearchAddressStatus.success,
            listResult: result,
          );
        }
      }
    }
  }
}
