import 'package:app_review/commons/animation/fade_anim.dart';
import 'package:app_review/commons/helper/debouncer.dart';
import 'package:app_review/commons/widgets/app_loading.dart';
import 'package:app_review/screens/location/change_address_screen/bloc/search_address_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:provider/provider.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../models/province/province.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../provider/position_provider.dart';

class ChangeAddressScreen extends StatefulWidget {
  final Box<Province> boxProvince;

  const ChangeAddressScreen({Key key, this.boxProvince}) : super(key: key);
  @override
  _ChangeAddressScreenState createState() =>
      _ChangeAddressScreenState(boxProvince);
}

class _ChangeAddressScreenState extends State<ChangeAddressScreen> {
  final Box<Province> boxProvince;
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeText = SizedConfig.imageSizeMultiplier;
  final Debouncer searchDelay = Debouncer();
  SearchAddressBloc searchAddressBloc;
  Province province;
  AppLocationBloc locationBloc;
  PositionProvider positionProvider;
  List<Province> listProvince;
  TextEditingController searchController = TextEditingController();
  _ChangeAddressScreenState(this.boxProvince);
  @override
  void initState() {
    super.initState();
    locationBloc = BlocProvider.of<AppLocationBloc>(context);
    positionProvider = PositionProvider();
    listProvince = locationBloc.listProvince;
    searchAddressBloc = SearchAddressBloc(locationBloc.listProvince);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        statusBarColor: theme.backgroundColor,
        height: 0.0,
      ),
      body: Column(
        children: [
          ChangeNotifierProvider<PositionProvider>(
            create: (_) => positionProvider,
            child: Column(
              children: [
                Container(
                  height: sizeScreen * 7,
                  padding: const EdgeInsets.symmetric(
                      horizontal: 5.0, vertical: 5.0),
                  child: Row(
                    children: [
                      AppButton.icon(
                        icon: AppIcon.popNavigate,
                        onTap: () => Navigator.pop(context, null),
                      ),
                      Spacer(),
                      Consumer<PositionProvider>(
                        builder: (_, position, __) => position.messageWidget(),
                      ),
                      const SizedBox(width: 15.0),
                    ],
                  ),
                ),
                Container(
                  padding: const EdgeInsets.symmetric(
                    horizontal: 15.0,
                    vertical: 10.0,
                  ),
                  child: Row(
                    children: [
                      Flexible(
                        child: TextFormField(
                          controller: searchController,
                          style: TextStyle(fontSize: sizeText * 4),
                          cursorColor: AppColors.primaryColor,
                          cursorHeight: 20,
                          decoration: InputDecoration(
                            prefixIcon: Icon(
                              AppIcon.search,
                              color: theme.iconTheme.color,
                              size: sizeScreen * 3,
                            ),
                            hintText: 'Nhập khu vực của bạn',
                            hintStyle: theme.textTheme.bodyText1.copyWith(
                              fontWeight: AppFontWeight.regular,
                            ),
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30.0),
                              borderSide: BorderSide.none,
                            ),
                            filled: true,
                            fillColor:
                                AppColors.subLightColor.withOpacity(0.15),
                            contentPadding:
                                const EdgeInsets.symmetric(horizontal: 15.0),
                          ),
                          onChanged: (String input) => setState(
                            () => searchDelay.debounce(
                              () => {
                                searchAddressBloc.add(
                                  SearchAddress(
                                    searchKey: input.trim(),
                                  ),
                                )
                              },
                            ),
                          ),
                        ),
                      ),
                      positionProvider.positionButton(
                          context,
                          () => positionProvider.status
                              ? Navigator.pop(
                                  context,
                                  positionProvider.province,
                                )
                              : positionProvider
                                  .getPosition(locationBloc.listProvince),
                          sizeScreen),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: BlocBuilder<SearchAddressBloc, SearchAddressState>(
              bloc: searchAddressBloc,
              builder: (_, state) {
                if (state.status == SearchAddressStatus.loading) {
                  return Center(
                    child: AppLoading.threeBounce(size: 20.0),
                  );
                }
                if (state.status == SearchAddressStatus.empty) {
                  return Center(
                    child: Text(
                      'Không tìm thấy thành phố này',
                      style: theme.textTheme.headline6.copyWith(
                        color: AppColors.subLightColor,
                        fontWeight: AppFontWeight.regular,
                      ),
                    ),
                  );
                } else if (state.status == SearchAddressStatus.success) {
                  return FadeAnim(
                    child: ListView.builder(
                      itemCount: state.listResult.length,
                      itemBuilder: (_, index) => Padding(
                        padding: const EdgeInsets.only(left: 5.0),
                        child: AppButton.text(
                          label: state.listResult[index].name,
                          labelStyle: theme.textTheme.headline6.copyWith(
                            fontWeight: AppFontWeight.regular,
                          ),
                          onTap: () {
                            Navigator.pop(context, state.listResult[index]);
                          },
                        ),
                      ),
                    ),
                  );
                }
                return FadeAnim(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0),
                        child: Text(
                          'Đã chọn gần đây',
                          style: theme.textTheme.headline5,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: historyProvince(theme),
                      ),
                      Flexible(child: popularProvince(theme)),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget historyProvince(ThemeData theme) {
    if (boxProvince.length == null) {
      return const SizedBox();
    } else {
      List<Province> listProvince = [];
      for (var i = 0; i < boxProvince.length; i++) {
        listProvince.add(boxProvince.getAt(i));
      }
      return Wrap(
        spacing: 2.0,
        runSpacing: -10.0,
        children: [
          ...listProvince
              .map((e) => ActionChip(
                    label: Text(e.name,
                        style: theme.textTheme.bodyText1.copyWith(
                          fontWeight: AppFontWeight.regular,
                        )),
                    backgroundColor: AppColors.backgroundSearchColor,
                    onPressed: () => Navigator.pop(context, e),
                  ))
              .toList(),
        ],
      );
    }
  }

  Widget popularProvince(ThemeData theme) {
    return Container(
      child: Scrollbar(
        child: ListView.separated(
          itemCount: listProvince.length + 1,
          separatorBuilder: (context, index) => const SizedBox(height: 0.0),
          itemBuilder: (context, index) {
            if (index == 0)
              return Padding(
                padding: const EdgeInsets.only(left: 15.0, top: 10.0),
                child: Text('Địa điểm phổ biến',
                    style: Theme.of(context).textTheme.headline5),
              );
            return Padding(
              padding: const EdgeInsets.only(left: 5.0),
              child: AppButton.text(
                label: listProvince[index - 1].name,
                labelStyle: theme.textTheme.headline6.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
                onTap: () {
                  Navigator.pop(context, listProvince[index - 1]);
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
