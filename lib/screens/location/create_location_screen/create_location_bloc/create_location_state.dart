part of 'create_location_bloc.dart';

@immutable
abstract class CreateLocationState {}

class CreateLocationInitial extends CreateLocationState {}

class CreateLocationSuccess extends CreateLocationState {
  final Location location;
  CreateLocationSuccess(this.location);
}

class CreateLocationFailed extends CreateLocationState {}

class CreateLocationLoading extends CreateLocationState {}
