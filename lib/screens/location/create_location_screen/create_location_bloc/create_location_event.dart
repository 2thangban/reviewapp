part of 'create_location_bloc.dart';

@immutable
abstract class CreateLocationEvent {}

class CreateLocation extends CreateLocationEvent {
  final Location location;
  CreateLocation(this.location);
}
