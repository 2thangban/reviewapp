import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../models/location/location.dart';
import '../../../../models/location/location_repo.dart';

part 'create_location_event.dart';
part 'create_location_state.dart';

class CreateLocationBloc
    extends Bloc<CreateLocationEvent, CreateLocationState> {
  CreateLocationBloc() : super(CreateLocationInitial());

  @override
  Stream<CreateLocationState> mapEventToState(
    CreateLocationEvent event,
  ) async* {
    try {
      if (event is CreateLocation) {
        yield CreateLocationLoading();
        final result = await LocationRepo.create(event.location);
        yield CreateLocationSuccess(result);
      }
    } catch (e) {
      yield CreateLocationFailed();
    }
  }
}
