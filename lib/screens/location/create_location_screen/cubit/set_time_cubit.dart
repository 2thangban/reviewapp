import 'package:bloc/bloc.dart';

class SetTimeCubit extends Cubit<String> {
  SetTimeCubit() : super(null);

  void setTimeFromPicker(String time) => emit(time);
}
