import 'package:bloc/bloc.dart';

import '../../../../commons/helper/validator.dart';
import '../../../../models/location/location.dart';

class ValidateFormCreateLocationCubit extends Cubit<bool> {
  ValidateFormCreateLocationCubit() : super(false);

  void validate(Location location) {
    if (!Validator.isEmptyString(location.name) &&
        !Validator.isEmptyString(location.address) &&
        !Validator.isEmptyString(location.openTime) &&
        !Validator.isEmptyString(location.closeTime) &&
        Validator.isValidPhone(location.phone) &&
        !Validator.isEmptyString(location.lowestPrice?.toString()) &&
        !Validator.isEmptyString(location.hightgestPrice?.toString())) {
      emit(true);
    } else {
      emit(false);
    }
  }
}
