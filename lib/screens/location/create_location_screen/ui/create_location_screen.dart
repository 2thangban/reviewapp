import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../commons/helper/app_date_picker.dart';
import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_textfield.dart';
import '../../../../commons/widgets/more_func.dart';
import '../../../../models/location/location.dart';
import '../../../../models/province/province.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../../app_common_bloc/province_bloc/province_bloc.dart';
import '../../search_location_screen/bloc/change_province_search_cubit/change_province_search_cubit.dart';
import '../create_location_bloc/create_location_bloc.dart';
import '../cubit/set_time_cubit.dart';
import '../cubit/validation_from_create_location_cubit.dart';

class CreateLocationScreen extends StatefulWidget {
  const CreateLocationScreen({Key key}) : super(key: key);
  @override
  _CreateLocationScreenState createState() => _CreateLocationScreenState();
}

class _CreateLocationScreenState extends State<CreateLocationScreen> {
  Location location = Location(
    name: '',
    address: '',
    openTime: '',
    closeTime: '',
    phone: '',
    hightgestPrice: null,
    lowestPrice: null,
  );
  ProvinceBloc provinceBloc;
  AppLocationBloc appLocationBloc;

  ChangeProvinceSearchCubit changeProvinceCubit;
  final highestPriceController = TextEditingController();
  final lowestPriceController = TextEditingController();
  final CreateLocationBloc createLocationBloc = CreateLocationBloc();
  final ValidateFormCreateLocationCubit validateForm =
      ValidateFormCreateLocationCubit();
  final SetTimeCubit setOpenTimeCubit = SetTimeCubit();
  final SetTimeCubit setCloseTimeCubit = SetTimeCubit();

  @override
  void initState() {
    super.initState();
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
    appLocationBloc = BlocProvider.of<AppLocationBloc>(context);
    changeProvinceCubit =
        ChangeProvinceSearchCubit(provinceBloc.currentProvince);
  }

  @override
  void dispose() {
    super.dispose();
    createLocationBloc.close();
    changeProvinceCubit.close();
    validateForm.close();
    setOpenTimeCubit.close();
    setCloseTimeCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    location.province = changeProvinceCubit.province;
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        statusBarColor: theme.backgroundColor,
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context, null),
        ),
        rightBarButtonItems: [
          BlocListener<CreateLocationBloc, CreateLocationState>(
            bloc: createLocationBloc,
            listener: (_, state) {
              if (state is CreateLocationSuccess) {
                Navigator.pop(context, location);
              }
            },
            child: BlocBuilder<ValidateFormCreateLocationCubit, bool>(
              bloc: validateForm,
              buildWhen: (previous, current) => previous != current,
              builder: (_, check) => Padding(
                padding: EdgeInsets.symmetric(vertical: 5),
                child: AppButton.common(
                  labelText: 'Hoàn tất',
                  labelStyle: theme.textTheme.bodyText1.copyWith(
                      color: check
                          ? theme.backgroundColor
                          : AppColors.subLightColor),
                  backgroundColor: check
                      ? AppColors.primaryColor
                      : AppColors.backgroundEditColor,
                  radius: 10.0,
                  shadowColor: theme.backgroundColor,
                  contentPadding: 5.0,
                  width: sizeScreen * 11.0,
                  onPressed: () => check
                      ? createLocationBloc.add(CreateLocation(location))
                      : null,
                ),
              ),
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20.0),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text(
                  'Tên địa điểm',
                  style: theme.textTheme.headline6,
                ),
              ),
              AppTextField.common(
                hintText: 'Nhập tên địa điểm',
                hintStyle: theme.textTheme.bodyText2.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
                backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                radius: 10.0,
                onChange: (input) {
                  location.name = input;
                  validateForm.validate(location);
                },
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text('Địa chỉ', style: theme.textTheme.headline6),
              ),
              Row(
                children: [
                  Expanded(
                    child: AppTextField.common(
                      hintText: 'Chọn địa chỉ',
                      hintStyle: theme.textTheme.bodyText2.copyWith(
                        fontWeight: AppFontWeight.regular,
                      ),
                      backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                      radius: 10.0,
                      onChange: (input) {
                        location.address = input;
                        validateForm.validate(location);
                      },
                    ),
                  ),
                  GestureDetector(
                    onTap: () async => await showChangeAddress(
                            context, theme, appLocationBloc.listProvince)
                        .then(
                      (value) {
                        if (value != null &&
                            value.id != changeProvinceCubit.province.id) {
                          changeProvinceCubit.change(value);
                          location.province = value;
                        }
                      },
                    ),
                    child: Container(
                      width: MediaQuery.of(context).size.width / 4.5,
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5.0,
                        vertical: 15.0,
                      ),
                      margin: const EdgeInsets.only(left: 5.0),
                      decoration: BoxDecoration(
                        color: AppColors.subLightColor.withOpacity(0.1),
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: BlocBuilder<ChangeProvinceSearchCubit, Province>(
                        buildWhen: (previous, current) =>
                            current != null || current.id != previous.id,
                        bloc: changeProvinceCubit,
                        builder: (_, state) => Text(
                          state.name,
                          softWrap: true,
                          overflow: TextOverflow.ellipsis,
                          style: theme.textTheme.bodyText1,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text('Giờ mở cửa', style: theme.textTheme.headline6),
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text('Mở:', style: theme.textTheme.bodyText1),
                  ),
                  Expanded(
                    child: BlocBuilder<SetTimeCubit, String>(
                      buildWhen: (previous, current) => previous != current,
                      bloc: setOpenTimeCubit,
                      builder: (_, time) => AppTextField.common(
                        hintText: time ?? 'Ví dụ: 8:00',
                        hintStyle: time != null
                            ? theme.textTheme.bodyText1.copyWith(
                                fontWeight: AppFontWeight.regular,
                              )
                            : theme.textTheme.bodyText2.copyWith(
                                fontWeight: AppFontWeight.regular,
                              ),
                        readOnly: true,
                        backgroundColor:
                            AppColors.subLightColor.withOpacity(0.1),
                        radius: 10.0,
                        onTap: () async =>
                            await AppDatePicker.getTime(context).then((time) {
                          if (time != null) {
                            location.openTime = time;
                            setOpenTimeCubit.setTimeFromPicker(time);
                          }
                        }),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text('Đóng:', style: theme.textTheme.bodyText1),
                  ),
                  Expanded(
                    child: BlocBuilder<SetTimeCubit, String>(
                      bloc: setCloseTimeCubit,
                      builder: (_, time) {
                        return AppTextField.common(
                          hintText: time ?? 'Ví dụ: 22:00',
                          hintStyle: time != null
                              ? theme.textTheme.bodyText1.copyWith(
                                  fontWeight: AppFontWeight.regular,
                                )
                              : theme.textTheme.bodyText2.copyWith(
                                  fontWeight: AppFontWeight.regular,
                                ),
                          readOnly: true,
                          backgroundColor:
                              AppColors.subLightColor.withOpacity(0.1),
                          radius: 10.0,
                          onTap: () async =>
                              await AppDatePicker.getTime(context).then((time) {
                            if (time != null) {
                              location.closeTime = time;
                              setCloseTimeCubit.setTimeFromPicker(time);
                            }
                          }),
                        );
                      },
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text('Số điện thoại', style: theme.textTheme.headline6),
              ),
              AppTextField.common(
                hintText: 'Ví dụ: 0123 123 123',
                hintStyle: theme.textTheme.bodyText2.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
                type: TextInputType.numberWithOptions(),
                backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                radius: 10.0,
                onChange: (String input) {
                  location.phone = input;
                  validateForm.validate(location);
                },
              ),
              const SizedBox(height: 10),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Text('Giá trung bình', style: theme.textTheme.headline6),
              ),
              Row(
                children: [
                  Expanded(
                    child: AppTextField.common(
                      controller: lowestPriceController,
                      hintText: 'Giá thấp nhất',
                      type: TextInputType.numberWithOptions(),
                      hintStyle: theme.textTheme.bodyText2.copyWith(
                        fontWeight: AppFontWeight.regular,
                      ),
                      // textInputFormatter: [CurrencyInputFormatter()],
                      suffixText: 'VNĐ',
                      backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                      radius: 10.0,
                      onChange: (String input) {
                        location.lowestPrice = int.parse(input);
                        validateForm.validate(location);
                      },
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 5),
                    child: Text('-', style: theme.textTheme.headline5),
                  ),
                  Expanded(
                    child: AppTextField.common(
                      controller: highestPriceController,
                      hintText: 'Giá cao nhất',
                      suffixText: 'VNĐ',
                      type: TextInputType.numberWithOptions(),
                      hintStyle: theme.textTheme.bodyText2.copyWith(
                        fontWeight: AppFontWeight.regular,
                      ),
                      // textInputFormatter: [CurrencyInputFormatter()],
                      backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                      radius: 10.0,
                      onChange: (String input) {
                        if (input != null && input != '')
                          location.hightgestPrice = int.parse(input);
                        validateForm.validate(location);
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
