import 'package:bloc/bloc.dart';

import '../../../../models/province/province.dart';

class CurrentProvinceCubit extends Cubit<Province> {
  Province province;
  CurrentProvinceCubit(this.province) : super(province);

  void change(Province newProvince) => emit(this.province = newProvince);
}
