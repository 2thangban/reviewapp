part of 'search_post_bloc.dart';

abstract class SearchPostState {}

class SearchPostInitial extends SearchPostState {}

class SearchPostLoading extends SearchPostState {}

class SearchPostSuccess extends SearchPostState {
  final List<Post> listPost;
  SearchPostSuccess(this.listPost);
}

class SearchPostFailed extends SearchPostState {}
