import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/post/post.dart';
import '../../../../models/post/post_repo.dart';

part 'search_post_event.dart';
part 'search_post_state.dart';

class SearchPostBloc extends Bloc<SearchPostEvent, SearchPostState> {
  SearchPostBloc() : super(SearchPostInitial());

  @override
  Stream<SearchPostState> mapEventToState(SearchPostEvent event) async* {
    if (event is SearchingPostEvent) {
      try {
        yield SearchPostLoading();
        final result = await PostRepo.search(
          key: event.key,
          page: event.page,
          limit: event.limit,
        );
        yield SearchPostSuccess(result);
      } catch (e) {
        yield SearchPostFailed();
      }
    }
  }
}
