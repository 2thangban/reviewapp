part of 'search_post_bloc.dart';

abstract class SearchPostEvent {}

class SearchingPostEvent extends SearchPostEvent {
  final String key;
  final int page;
  final int limit;
  final int cate;
  SearchingPostEvent({this.key, this.page, this.limit, this.cate});
}
