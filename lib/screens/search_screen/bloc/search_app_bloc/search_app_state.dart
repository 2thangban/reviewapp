part of 'search_app_bloc.dart';

abstract class SearchAppState {}

class SearchAppInitial extends SearchAppState {}

class SearchAppLoading extends SearchAppState {}

class SearchAppSuccess extends SearchAppState {
  final AppSearch appSearch;
  final List<Account> listAccount;
  final List<Location> listLocation;
  final List<Post> listPost;
  final List<Hashtag> listHashtag;
  SearchAppSuccess({
    this.appSearch,
    this.listAccount,
    this.listLocation,
    this.listPost,
    this.listHashtag,
  });
}

class SearchAppFailed extends SearchAppState {}
