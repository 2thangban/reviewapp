import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../commons/helper/validator.dart';
import '../../../../models/hashtag/hashtag.dart';
import '../../../../models/hashtag/hashtag_repo.dart';
import '../../../../models/location/location.dart';
import '../../../../models/location/location_repo.dart';
import '../../../../models/post/post.dart';
import '../../../../models/post/post_repo.dart';
import '../../../../models/search/search.dart';
import '../../../../models/search/search_repo.dart';
import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';

part 'search_app_event.dart';
part 'search_app_state.dart';

class SearchAppBloc extends Bloc<SearchAppEvent, SearchAppState> {
  SearchAppBloc() : super(SearchAppInitial());

  @override
  Stream<SearchAppState> mapEventToState(SearchAppEvent event) async* {
    if (event is SearchingAppEvent) {
      if (Validator.isEmptyString(event.key)) {
        yield SearchAppInitial();
      } else {
        yield SearchAppLoading();
        try {
          switch (event.category) {

            /// In case the user seaching with category is location.
            case 1:
              final result = await LocationRepo.search(
                key: event.key,
                page: event.page,
                limit: event.limit,
                provinceId: event.provinceId,
              );
              yield SearchAppSuccess(listLocation: result);
              break;

            /// In case the user seaching with category is post.
            case 2:
              final result = await PostRepo.search(
                key: event.key,
                page: event.page,
                limit: event.limit,
                provinceId: event.provinceId,
              );
              yield SearchAppSuccess(listPost: result);
              break;

            /// In case the user seaching with category is hashtag.
            case 3:
              final result = await HashtagRepo.search(
                key: event.key,
                page: event.page,
                limit: event.limit,
              );
              yield SearchAppSuccess(listHashtag: result);
              break;

            /// In case the user seaching with category is account.
            case 4:
              final result = await AccountRepo.search(
                key: event.key,
                page: event.page,
                limit: event.limit,
                provinceId: event.provinceId,
              );
              yield SearchAppSuccess(listAccount: result);
              break;

            /// In case the category is null then search all.
            default:
              final result = await AppSearchRepo.searchAll(
                key: event.key,
                provinceId: event.provinceId,
              );
              yield SearchAppSuccess(appSearch: result);
          }
        } catch (e) {
          yield SearchAppFailed();
        }
      }
    }
  }
}
