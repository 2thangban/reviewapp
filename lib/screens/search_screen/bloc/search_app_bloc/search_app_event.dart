part of 'search_app_bloc.dart';

abstract class SearchAppEvent {}

class SearchingAppEvent extends SearchAppEvent {
  final String key;
  final int page;
  final int limit;
  final int category;
  final int provinceId;
  SearchingAppEvent(
    this.key,
    this.category, {
    this.page = 0,
    this.limit = 10,
    this.provinceId,
  });
}
