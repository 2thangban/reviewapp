part of 'search_hashtag_bloc.dart';

abstract class SearchHashtagState {}

class SearchHashtagInitial extends SearchHashtagState {}

class SearchHashtagLoading extends SearchHashtagState {}

class SearchHashtagSuccess extends SearchHashtagState {
  final List<Hashtag> listHashtag;
  SearchHashtagSuccess(this.listHashtag);
}

class SearchHashtagFailed extends SearchHashtagState {}
