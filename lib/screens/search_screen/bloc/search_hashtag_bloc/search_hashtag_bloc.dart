import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/hashtag/hashtag.dart';
import '../../../../models/hashtag/hashtag_repo.dart';

part 'search_hashtag_event.dart';
part 'search_hashtag_state.dart';

class SearchHashtagBloc extends Bloc<SearchHashtagEvent, SearchHashtagState> {
  SearchHashtagBloc() : super(SearchHashtagInitial());

  @override
  Stream<SearchHashtagState> mapEventToState(SearchHashtagEvent event) async* {
    if (event is SearchingHashtagEvent) {
      yield SearchHashtagLoading();
      try {
        final result = await HashtagRepo.search(
          key: event.key,
          page: event.page,
          limit: event.limit,
        );
        yield SearchHashtagSuccess(result);
      } catch (e) {
        yield SearchHashtagFailed();
      }
    }
  }
}
