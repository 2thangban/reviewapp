part of 'search_hashtag_bloc.dart';

abstract class SearchHashtagEvent {}

class SearchingHashtagEvent extends SearchHashtagEvent {
  final String key;
  final int page;
  final int limit;

  SearchingHashtagEvent({this.key, this.limit, this.page});
}
