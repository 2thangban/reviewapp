import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';

class MainCategoryCubit extends Cubit<int> {
  int index;
  final ScrollController scrollController;
  MainCategoryCubit(this.index, this.scrollController) : super(index);

  void change(int indexSelected) {
    if (indexSelected == 4) {
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        duration: Duration(milliseconds: 200),
        curve: Curves.linear,
      );
    } else if (indexSelected == 0) {
      scrollController.animateTo(
        scrollController.position.minScrollExtent,
        duration: Duration(milliseconds: 200),
        curve: Curves.linear,
      );
    }
    emit(this.index = indexSelected);
  }
}
