part of 'search_account_bloc.dart';

abstract class SearchAccountEvent {}

class SearchingAccountEvent extends SearchAccountEvent {
  final String key;
  final int page;
  final int limit;
  final provinceId;
  SearchingAccountEvent({this.key, this.page, this.limit, this.provinceId});
}
