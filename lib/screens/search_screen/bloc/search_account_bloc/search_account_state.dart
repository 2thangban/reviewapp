part of 'search_account_bloc.dart';

abstract class SearchAccountState {}

class SearchAccountInitial extends SearchAccountState {}

class SearchAccountLoading extends SearchAccountState {}

class SearchAccountFailed extends SearchAccountState {}

class SearchAccountSuccess extends SearchAccountState {
  final List<Account> listAccount;
  SearchAccountSuccess(this.listAccount);
}
