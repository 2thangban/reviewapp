import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../models/user/user.dart';
import '../../../../models/user/user_repo.dart';

part 'search_account_event.dart';
part 'search_account_state.dart';

class SearchAccountBloc extends Bloc<SearchAccountEvent, SearchAccountState> {
  SearchAccountBloc() : super(SearchAccountInitial());

  @override
  Stream<SearchAccountState> mapEventToState(SearchAccountEvent event) async* {
    if (event is SearchingAccountEvent) {
      yield SearchAccountLoading();
      try {
        final result = await AccountRepo.search(
          key: event.key,
          page: event.page,
          limit: event.limit,
          provinceId: event.provinceId,
        );
        yield SearchAccountSuccess(result);
      } catch (e) {
        yield SearchAccountFailed();
      }
    }
  }
}
