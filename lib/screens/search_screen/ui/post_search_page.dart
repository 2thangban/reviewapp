import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../commons/widgets/sliver_grid_list.dart';
import '../../../localizations/app_localization.dart';
import '../../../models/post/post.dart';
import '../../../theme.dart';
import '../bloc/search_post_bloc/search_post_bloc.dart';

class PostSearchPage extends StatefulWidget {
  final List<Post> _listpost;
  final String _keySearch;

  PostSearchPage(this._listpost, this._keySearch, {Key key}) : super(key: key);

  @override
  _PostSearchPageState createState() => _PostSearchPageState();
}

class _PostSearchPageState extends State<PostSearchPage> {
  final SearchPostBloc _searchPostBloc = SearchPostBloc();

  final ScrollController _scrollController = ScrollController();

  final int limit = 5;

  int page = 1;

  bool isLastPage;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    isLastPage = (widget._listpost.length < limit);
    final sizeScreen = SizedConfig.heightMultiplier;

    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent &&
            isLastPage == false) {
          _searchPostBloc.add(
            SearchingPostEvent(
              key: widget._keySearch,
              page: page++,
              limit: limit,
            ),
          );
        }
      },
    );

    return widget._listpost.length == 0
        ? Container(
            height: sizeScreen * 80,
            alignment: Alignment.center,
            child: Text(
              AppLocalization.of(context).locaized('searchPostsEmpty'),
              style: theme.textTheme.headline6.copyWith(
                fontWeight: AppFontWeight.regular,
                fontStyle: FontStyle.italic,
                color: AppColors.subLightColor,
              ),
            ),
          )
        : BlocBuilder<SearchPostBloc, SearchPostState>(
            bloc: _searchPostBloc,
            builder: (_, state) {
              if (state is SearchPostSuccess) {
                widget._listpost.addAll(state.listPost);
                isLastPage = (state.listPost.length < limit);
              }
              return Container(
                height: sizeScreen * 78,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Column(
                    children: [
                      PostList(
                        widget._listpost,
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        padding: const EdgeInsets.symmetric(
                          horizontal: 15.0,
                        ),
                      ),
                      state is SearchPostLoading
                          ? Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 10.0),
                              child: AppLoading.threeBounce(size: 20.0),
                            )
                          : const SizedBox(),
                    ],
                  ),
                ),
              );
            },
          );
  }
}
