import 'package:flutter/material.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/widgets/list_account.dart';
import '../../../models/user/user.dart';

class AccountSearchPage extends StatelessWidget {
  final List<Account> _listAccount;
  final String searchKey;

  AccountSearchPage(this._listAccount, this.searchKey);
  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    return Container(
      height: sizeScreen * 80,
      child: ListAccountWidget(
        _listAccount,
        searchKey: searchKey,
        shrinkWrap: false,
      ),
    );
  }
}
