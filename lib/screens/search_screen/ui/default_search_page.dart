import 'package:flutter/material.dart';

import '../../../commons/untils/app_image.dart';

class DefaultSearchPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 100),
          Image.asset(
            AppAssets.locationSearch,
            height: 300,
            width: 300,
          ),
        ],
      ),
    );
  }
}
