import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/widgets/list_location.dart';
import '../../../localizations/app_localization.dart';
import '../../../models/location/location.dart';
import '../../../theme.dart';
import '../../location/search_location_screen/bloc/search_location_bloc/search_location_bloc.dart';

class LocationSearchPage extends StatefulWidget {
  final List<Location> _listLocation;
  final String _keySearch;

  LocationSearchPage(this._listLocation, this._keySearch);

  @override
  _LocationSearchPageState createState() => _LocationSearchPageState();
}

class _LocationSearchPageState extends State<LocationSearchPage> {
  final SearchLocationBloc _searchLocationBloc = SearchLocationBloc();

  final ScrollController _scrollController = ScrollController();

  final int limit = 10;

  int page = 1;

  bool isLastPage;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    isLastPage = (widget._listLocation.length < limit);
    final sizeScreen = SizedConfig.heightMultiplier;

    _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
                _scrollController.position.maxScrollExtent &&
            isLastPage == false) {
          _searchLocationBloc.add(
            GetListLocationEvent(
              key: widget._keySearch,
              page: page++,
              limit: limit,
            ),
          );
        }
      },
    );

    return widget._listLocation.length == 0
        ? Container(
            height: sizeScreen * 80,
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              AppLocalization.of(context).locaized('searchLocationEmpty'),
              style: theme.textTheme.headline6.copyWith(
                fontWeight: AppFontWeight.regular,
                fontStyle: FontStyle.italic,
                color: AppColors.subLightColor,
              ),
            ),
          )
        : BlocBuilder<SearchLocationBloc, SearchLocationState>(
            bloc: _searchLocationBloc,
            builder: (_, state) {
              if (state is SearchLocationSuccess) {
                widget._listLocation.addAll(state.listLocation);
                isLastPage = (state.listLocation.length < limit);
              }
              return Container(
                padding: const EdgeInsets.symmetric(vertical: 10.0),
                height: sizeScreen * 78,
                child: ListLocationWidget(
                  widget._listLocation,
                ),
              );
            },
          );
  }
}
