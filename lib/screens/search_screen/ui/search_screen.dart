import '../../../commons/untils/app_color.dart';
import '../../../commons/untils/app_icon.dart';
import '../../../commons/helper/debouncer.dart';
import '../../../commons/helper/size_config.dart';
import '../../../commons/widgets/app_bar.dart';
import '../../../commons/widgets/app_button.dart';
import '../../../commons/widgets/app_loading.dart';
import '../../../commons/widgets/more_func.dart';
import '../../../commons/widgets/silver_app_bar.dart';
import '../../../localizations/app_localization.dart';
import '../../../models/province/province.dart';
import '../../app_common_bloc/location_bloc/app_location_bloc.dart';
import '../../app_common_bloc/province_bloc/province_bloc.dart';
import '../bloc/cubit/current_province_cubit.dart';
import '../bloc/main_category_cubit/main_category_cubit.dart';
import '../bloc/search_app_bloc/search_app_bloc.dart';
import 'default_search_page.dart';
import 'location_search_page.dart';
import 'post_search_page.dart';
import 'search_all_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'account_search_page.dart';
import 'hashtag_search_page.dart';

class SearchScreen extends StatefulWidget {
  const SearchScreen({Key key}) : super(key: key);
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  final double sizeScreen = SizedConfig.heightMultiplier;
  final double sizeText = SizedConfig.textMultiplier;
  final Debouncer searchDelay = Debouncer(delay: Duration(milliseconds: 300));
  final ScrollController scrollController = ScrollController();
  MainCategoryCubit mainCategoryCubit;
  CurrentProvinceCubit currentProvinceCubit;
  final SearchAppBloc searchAppBloc = SearchAppBloc();
  final int limit = 5;
  int page = 0;
  bool isLastPage = false;
  AppLocationBloc appLocationBloc;
  String textSearch = '';
  ProvinceBloc provinceBloc;

  @override
  void initState() {
    super.initState();
    mainCategoryCubit = MainCategoryCubit(0, scrollController);
    appLocationBloc = BlocProvider.of<AppLocationBloc>(context);
    provinceBloc = BlocProvider.of<ProvinceBloc>(context);
    currentProvinceCubit = CurrentProvinceCubit(provinceBloc.currentProvince);
  }

  SliverPersistentHeader appBarSearch(BuildContext context) {
    final theme = Theme.of(context);
    final AppLocalization appLanguage = AppLocalization.of(context);
    final List<String> listCategory = [
      appLanguage.locaized('allTabSearch'),
      appLanguage.locaized('locationTabSearch'),
      appLanguage.locaized('postTabSearch'),
      appLanguage.locaized('hashTagTabSearch'),
      appLanguage.locaized('userTabSearch'),
    ];
    return SliverPersistentHeader(
      pinned: true,
      delegate: SliverAppbarDelegate(
        maxHeight: sizeScreen * 12,
        minHeight: sizeScreen * 12,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          color: Theme.of(context).backgroundColor,
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 5.0,
                      ),
                      height: sizeScreen * 5.8,
                      child: Row(
                        children: [
                          Flexible(
                            child: TextFormField(
                              style: TextStyle(fontSize: sizeText * 2),
                              cursorColor: AppColors.primaryColor,
                              cursorHeight: 20,
                              decoration: InputDecoration(
                                prefixIcon: Icon(
                                  AppIcon.search,
                                  size: sizeScreen * 3,
                                ),
                                hintText: appLanguage.locaized('hintSearch'),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(30.0),
                                    bottomLeft: Radius.circular(30.0),
                                  ),
                                  borderSide: BorderSide.none,
                                ),
                                filled: true,
                                fillColor:
                                    AppColors.subLightColor.withOpacity(0.15),
                                contentPadding: const EdgeInsets.symmetric(
                                    horizontal: 15.0),
                              ),
                              onChanged: (String input) {
                                textSearch = input.trim();
                                page = 0;
                                isLastPage = false;
                                return searchDelay.debounce(
                                  () => searchAppBloc.add(
                                    SearchingAppEvent(
                                      textSearch,
                                      mainCategoryCubit.index,
                                      limit: limit,
                                      page: page,
                                      provinceId:
                                          currentProvinceCubit.province.id,
                                    ),
                                  ),
                                );
                              },
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.fromLTRB(
                              0.0,
                              5.0,
                              5.0,
                              5.0,
                            ),
                            decoration: BoxDecoration(
                              color: AppColors.subLightColor.withOpacity(0.15),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30.0),
                                bottomRight: Radius.circular(30.0),
                              ),
                            ),
                            child: BlocBuilder(
                              bloc: currentProvinceCubit,
                              builder: (context, province) => AppButton.text(
                                label: province.name,
                                onTap: () async {
                                  final Province result =
                                      await showChangeAddress(
                                    context,
                                    theme,
                                    appLocationBloc.listProvince,
                                  );
                                  if (result != null) {
                                    currentProvinceCubit.change(result);
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  AppButton.text(
                    label: appLanguage.locaized('Cancel'),
                    labelStyle: theme.textTheme.headline6.copyWith(
                      color: theme.primaryColor,
                    ),
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      Future.delayed(
                        const Duration(milliseconds: 200),
                        () => Navigator.pop(context),
                      );
                    },
                  ),
                ],
              ),
              BlocBuilder<MainCategoryCubit, int>(
                bloc: mainCategoryCubit,
                builder: (context, state) {
                  return Container(
                    height: sizeScreen * 6,
                    child: ListView.builder(
                      controller: scrollController,
                      scrollDirection: Axis.horizontal,
                      itemCount: listCategory.length,
                      itemBuilder: (context, index) => ActionChip(
                        pressElevation: 0.0,
                        backgroundColor: mainCategoryCubit.index == index
                            ? AppColors.primaryColor.withOpacity(0.1)
                            : theme.backgroundColor,
                        label: Text(
                          listCategory[index],
                          style: theme.textTheme.bodyText2.copyWith(
                            color: mainCategoryCubit.index == index
                                ? AppColors.primaryColor
                                : null,
                          ),
                        ),
                        onPressed: () {
                          if (index != state) {
                            page = 0;
                            isLastPage = false;

                            if (index != 0) {
                              searchAppBloc.add(
                                SearchingAppEvent(
                                  textSearch,
                                  index,
                                  page: page,
                                  limit: limit,
                                  provinceId: currentProvinceCubit.province.id,
                                ),
                              );
                            } else {
                              searchAppBloc.add(
                                SearchingAppEvent(
                                  textSearch,
                                  index,
                                  provinceId: currentProvinceCubit.province.id,
                                ),
                              );
                            }
                            mainCategoryCubit.change(index);
                          }
                        },
                      ),
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Scaffold(
      appBar: CommonAppBar(
        statusBarColor: theme.backgroundColor,
        height: 0,
      ),
      body: BlocProvider<MainCategoryCubit>(
        create: (_) => mainCategoryCubit,
        child: CustomScrollView(
          slivers: [
            appBarSearch(context),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  BlocProvider<SearchAppBloc>(
                    create: (context) => searchAppBloc,
                    child: BlocBuilder<SearchAppBloc, SearchAppState>(
                      bloc: searchAppBloc,
                      builder: (context, state) {
                        if (state is SearchAppInitial) {
                          return Flexible(child: DefaultSearchPage());
                        } else if (state is SearchAppLoading) {
                          return Container(
                            height: sizeScreen * 50,
                            alignment: Alignment.center,
                            child: AppLoading.threeBounce(size: 20.0),
                          );
                        } else if (state is SearchAppSuccess) {
                          switch (mainCategoryCubit.index) {
                            case 1:
                              return LocationSearchPage(
                                state.listLocation,
                                textSearch,
                              );
                            case 2:
                              return PostSearchPage(
                                state.listPost,
                                textSearch,
                              );
                            case 3:
                              return HashtagSearchPage(
                                state.listHashtag,
                                textSearch,
                              );
                            case 4:
                              return AccountSearchPage(
                                state.listAccount,
                                textSearch,
                              );
                            default:
                              return SearchAllPage(
                                state.appSearch,
                                textSearch,
                              );
                          }
                        } else if (state is SearchAppFailed) {
                          return Container();
                        }
                        return Container();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
