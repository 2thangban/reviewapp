import '../../../commons/animation/fade_anim.dart';
import '../../../commons/untils/app_color.dart';
import '../../../commons/widgets/app_button.dart';
import '../../../commons/widgets/sliver_grid_list.dart';
import '../../../localizations/app_localization.dart';
import '../../../models/search/search.dart';
import '../bloc/main_category_cubit/main_category_cubit.dart';
import '../bloc/search_app_bloc/search_app_bloc.dart';
import '../../../commons/widgets/list_account.dart';
import '../../../commons/widgets/list_hashtag.dart';
import '../../../commons/widgets/list_location.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../theme.dart';

class SearchAllPage extends StatelessWidget {
  final AppSearch appSearch;
  final searchKey;

  SearchAllPage(this.appSearch, this.searchKey);
  @override
  Widget build(BuildContext context) {
    final MainCategoryCubit mainCategoryCubit =
        BlocProvider.of<MainCategoryCubit>(context);
    final SearchAppBloc searchAppBloc = BlocProvider.of<SearchAppBloc>(context);
    final theme = Theme.of(context);
    final AppLocalization lang = AppLocalization.of(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        /// The location list
        const Divider(color: AppColors.backgroundSearchColor, thickness: 5.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('locationTabSearch'),
            style: theme.textTheme.headline5,
          ),
        ),
        const Divider(height: 5.0, thickness: 1.0),
        FadeAnim(
          child: ListLocationWidget(
            appSearch.listLocation,
            physics: NeverScrollableScrollPhysics(),
            padding: const EdgeInsets.symmetric(vertical: 10.0),
          ),
        ),
        const Divider(height: 5.0, thickness: 1.0),
        appSearch.listLocation.isNotEmpty
            ? Container(
                alignment: Alignment.center,
                child: AppButton.text(
                  label: lang.locaized('more'),
                  labelStyle: theme.textTheme.headline6.copyWith(
                    color: theme.primaryColor,
                  ),
                  onTap: () {
                    searchAppBloc.add(SearchingAppEvent(searchKey, 1));
                    mainCategoryCubit.change(1);
                  },
                ),
              )
            : const SizedBox(),

        ///The account list
        const Divider(color: AppColors.backgroundSearchColor, thickness: 5.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('userTabSearch'),
            style: theme.textTheme.headline5,
          ),
        ),
        const Divider(height: 5.0, thickness: 1.0),
        FadeAnim(child: ListAccountWidget(appSearch.listAccount)),
        const Divider(height: 5.0, thickness: 1.0),
        appSearch.listAccount.isNotEmpty
            ? Container(
                alignment: Alignment.center,
                child: AppButton.text(
                  label: lang.locaized('more'),
                  labelStyle: theme.textTheme.headline6.copyWith(
                    color: theme.primaryColor,
                  ),
                  onTap: () {
                    searchAppBloc.add(SearchingAppEvent(searchKey, 4));
                    mainCategoryCubit.change(4);
                  },
                ),
              )
            : const SizedBox(),

        /// The hashtag list
        const Divider(color: AppColors.backgroundSearchColor, thickness: 5.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('hashTagTabSearch'),
            style: theme.textTheme.headline5,
          ),
        ),
        const Divider(height: 5.0, thickness: 1.0),
        FadeAnim(child: ListHashtagWidget(appSearch.listHashtag)),
        const Divider(height: 5.0, thickness: 1.0),
        appSearch.listHashtag.isNotEmpty
            ? Container(
                alignment: Alignment.center,
                child: AppButton.text(
                  label: lang.locaized('more'),
                  labelStyle: theme.textTheme.headline6.copyWith(
                    color: theme.primaryColor,
                  ),
                  onTap: () {
                    searchAppBloc.add(SearchingAppEvent(searchKey, 3));
                    mainCategoryCubit.change(3);
                  },
                ),
              )
            : const SizedBox(),

        /// The post list
        const Divider(color: AppColors.backgroundSearchColor, thickness: 5.0),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            lang.locaized('postTabSearch'),
            style: theme.textTheme.headline5,
          ),
        ),
        appSearch.listPost.isEmpty
            ? Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(vertical: 20.0),
                child: Text(
                  AppLocalization.of(context).locaized('searchHashtagEmpty'),
                  style: theme.textTheme.headline6.copyWith(
                    fontWeight: AppFontWeight.regular,
                    fontStyle: FontStyle.italic,
                    color: AppColors.subLightColor,
                  ),
                ),
              )
            : FadeAnim(
                child: PostList(
                  appSearch.listPost,
                  physics: NeverScrollableScrollPhysics(),
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                ),
              ),
        appSearch.listPost.isNotEmpty
            ? Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.symmetric(vertical: 5.0),
                child: AppButton.text(
                  label: lang.locaized('more'),
                  labelStyle: theme.textTheme.headline6.copyWith(
                    color: theme.primaryColor,
                  ),
                  onTap: () {
                    searchAppBloc.add(SearchingAppEvent(searchKey, 2));
                    mainCategoryCubit.change(2);
                  },
                ),
              )
            : const SizedBox(),
      ],
    );
  }
}
