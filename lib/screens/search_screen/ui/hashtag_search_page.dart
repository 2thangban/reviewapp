import 'package:flutter/material.dart';

import '../../../commons/helper/size_config.dart';
import '../../../commons/widgets/list_hashtag.dart';
import '../../../models/hashtag/hashtag.dart';

class HashtagSearchPage extends StatelessWidget {
  final List<Hashtag> _listHashtag;
  final String _key;

  HashtagSearchPage(this._listHashtag, this._key);
  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    return SizedBox(
      height: sizeScreen * 85,
      child: ListHashtagWidget(
        _listHashtag,
        keySearch: _key,
        shrinkWrap: false,
      ),
    );
  }
}
