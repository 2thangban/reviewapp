import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../commons/untils/app_icon.dart';
import '../../../commons/untils/app_image.dart';
import '../../../commons/widgets/app_bar.dart';
import '../../../commons/widgets/app_button.dart';
import '../bloc/page_cubit/page_cubit.dart';

class ShowImageScreen extends StatelessWidget {
  final List<String> listUrlImage;
  const ShowImageScreen(this.listUrlImage);
  @override
  Widget build(BuildContext context) {
    final PageCubit pageCubit = PageCubit();
    return Scaffold(
      appBar: CommonAppBar(
        leftBarButtonItem: AppButton.icon(
          icon: AppIcon.popNavigate,
          onTap: () => Navigator.pop(context),
        ),
      ),
      body: Container(
        alignment: Alignment.center,
        child: AspectRatio(
          aspectRatio: 2 / 3,
          child: BlocBuilder<PageCubit, int>(
            bloc: pageCubit,
            builder: (_, int) => PageView.builder(
              itemCount: listUrlImage.length,
              onPageChanged: (indexSelected) => pageCubit.change(indexSelected),
              itemBuilder: (context, index) => Padding(
                padding: const EdgeInsets.all(2.0),
                child: InteractiveViewer(
                  maxScale: 10,
                  child: CachedNetworkImage(
                    imageUrl: AppAssets.baseUrl + listUrlImage[index],
                    fit: BoxFit.cover,
                    fadeOutDuration: Duration(milliseconds: 100),
                    fadeInDuration: Duration(milliseconds: 300),
                    fadeInCurve: Curves.easeInCirc,
                    placeholder: (_, url) => const Center(
                      child: SizedBox(
                        height: 30.0,
                        width: 30.0,
                        child: CircularProgressIndicator(strokeWidth: 3.0),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
