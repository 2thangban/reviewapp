part of 'post_by_gallery_bloc.dart';

abstract class PostByGalleryEvent {}

class GetPostByGalleryEvent extends PostByGalleryEvent {
  final int page;
  final int limit;
  final int provinceId;
  final int galleryId;
  final bool isAuth;
  GetPostByGalleryEvent(
    this.galleryId,
    this.page,
    this.limit,
    this.isAuth, {
    this.provinceId,
  });
}
