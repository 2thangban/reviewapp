part of 'post_by_gallery_bloc.dart';

abstract class PostByGalleryState {}

class PostByGalleryInitial extends PostByGalleryState {}

class PostByGalleryRefresh extends PostByGalleryState {}

class PostByGalleryLoading extends PostByGalleryState {}

class PostByGallerySuccess extends PostByGalleryState {
  final List<Post> list;
  PostByGallerySuccess(this.list);
}

class PostByGalleryFailed extends PostByGalleryState {
  final String mgs;
  PostByGalleryFailed(this.mgs);
}
