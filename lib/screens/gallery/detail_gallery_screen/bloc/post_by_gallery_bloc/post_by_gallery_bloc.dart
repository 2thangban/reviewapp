import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/gallery/gellary_repo.dart';
import '../../../../../models/post/post.dart';

part 'post_by_gallery_event.dart';
part 'post_by_gallery_state.dart';

class PostByGalleryBloc extends Bloc<PostByGalleryEvent, PostByGalleryState> {
  PostByGalleryBloc() : super(PostByGalleryInitial());

  @override
  Stream<PostByGalleryState> mapEventToState(
    PostByGalleryEvent event,
  ) async* {
    if (event is GetPostByGalleryEvent) {
      if (event.page == 0) {
        yield PostByGalleryRefresh();
      } else {
        yield PostByGalleryLoading();
      }
      try {
        final result = await GalleryRepo.getPostByGallery(
          event.galleryId,
          page: event.page,
          limit: event.limit,
          isAuth: event.isAuth,
        );
        yield PostByGallerySuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {}
      }
    }
  }
}
