import 'package:bloc/bloc.dart';

import '../../../../../models/gallery/gellery.dart';

class UpdateGalleryCubit extends Cubit<Gallery> {
  UpdateGalleryCubit(Gallery gallery) : super(gallery);

  void update(Gallery gallery) => emit(gallery);
}
