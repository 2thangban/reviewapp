import 'package:app_review/screens/home_screen/user/profile_page.dart/ui/gallery_by_user/ui/gallery_by_user.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../commons/widgets/app_shimmer.dart';
import '../../../../commons/widgets/item_list_post.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/gallery/gellery.dart';
import '../../../../models/post/post.dart';
import '../../../../routes/route_name.dart';
import '../../../../theme.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../create_gallery_screen/bloc/gallery_bloc/gallery_bloc.dart';
import '../bloc/post_by_gallery_bloc/post_by_gallery_bloc.dart';
import '../bloc/update_gallery_cubit/update_gallery_cubit.dart';

class MenuItem {
  final IconData icon;
  final String title;
  MenuItem(this.icon, this.title);
}

class DetailGalleryScreen extends StatefulWidget {
  final Gallery gallery;
  const DetailGalleryScreen(this.gallery);
  @override
  _DetailGalleryScreenState createState() => _DetailGalleryScreenState();
}

class _DetailGalleryScreenState extends State<DetailGalleryScreen> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final PostByGalleryBloc _postByGalleryBloc = PostByGalleryBloc();
  UpdateGalleryCubit updateGalleryCubit;
  final GalleryBloc galleryBloc = GalleryBloc();
  final int _limit = 10;
  final List<Post> _listPost = [];
  int _page = 0;
  bool _isLastPage = false;
  AuthBloc authBloc;
  Gallery editGalley;
  @override
  void initState() {
    super.initState();
    updateGalleryCubit = UpdateGalleryCubit(widget.gallery);
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    _postByGalleryBloc.close();
    updateGalleryCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: () async {
          _page = 0;
          Future.delayed(
            Duration(seconds: 2),
            () => _postByGalleryBloc.add(
              GetPostByGalleryEvent(
                widget.gallery.id,
                _page++,
                _limit,
                authBloc.isChecked,
              ),
            ),
          );
        },
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: true,
              floating: true,
              elevation: 0.0,
              systemOverlayStyle: SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
              ),
              backgroundColor: Colors.transparent,
              leading: AppButton.icon(
                icon: AppIcon.popNavigate,
                iconColor: theme.backgroundColor,
                onTap: () => Navigator.pop(
                  context,
                  ResultGalley(
                    editGalley != null ? 2 : 0,
                    gallery: editGalley,
                  ),
                ),
              ),
              actions: [
                authBloc.isChecked
                    ? widget.gallery.name.compareTo('Xem sau') != 0
                        ? BlocBuilder<GalleryBloc, GalleryState>(
                            bloc: galleryBloc,
                            builder: (context, state) {
                              if (state is DeleteGallerySuccess) {
                                Future.delayed(
                                  Duration(milliseconds: 500),
                                  () => Navigator.pop(context, true),
                                );
                              }
                              return PopupMenuButton<int>(
                                icon: Icon(AppIcon.more_hori),
                                iconSize: sizeScreen * 4.5,
                                onSelected: (int index) async {
                                  switch (index) {
                                    case 0:
                                      final newGallery =
                                          await Navigator.pushNamed(
                                        context,
                                        RouteName.createGallery,
                                        arguments: widget.gallery,
                                      );
                                      if (newGallery != null) {
                                        updateGalleryCubit.update(newGallery);
                                      }
                                      break;
                                    case 1:
                                      final result = await showDialog(
                                        context: context,
                                        builder: (context) => AlertDialog(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(10.0),
                                          ),
                                          title: Text(
                                            "Xóa bộ sưu tập",
                                            style: theme.textTheme.headline4,
                                          ),
                                          actions: [
                                            AppButton.text(
                                              label: "KHÔNG",
                                              onTap: () =>
                                                  Navigator.pop(context, false),
                                            ),
                                            AppButton.text(
                                              label: "CÓ",
                                              onTap: () async {
                                                Navigator.pop(context, true);
                                              },
                                            ),
                                          ],
                                        ),
                                      );
                                      if (result != null && result) {
                                        Navigator.pop(context, ResultGalley(1));
                                      }
                                      break;
                                    default:
                                  }
                                },
                                itemBuilder: (context) => [
                                  PopupMenuItem<int>(
                                    value: 0,
                                    child: Row(
                                      children: [
                                        Icon(
                                          AppIcon.block,
                                          size: sizeScreen * 2.5,
                                          color: AppColors.darkThemeColor,
                                        ),
                                        const SizedBox(width: 5.0),
                                        Text('Sửa'),
                                      ],
                                    ),
                                  ),
                                  PopupMenuItem<int>(
                                    value: 1,
                                    child: Row(
                                      children: [
                                        Icon(
                                          AppIcon.remove,
                                          size: sizeScreen * 2.5,
                                          color: AppColors.darkThemeColor,
                                        ),
                                        const SizedBox(width: 5.0),
                                        Text('Xóa'),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                          )
                        : const SizedBox()
                    : const SizedBox(),
              ],
              expandedHeight: sizeScreen * 30,
              flexibleSpace: FlexibleSpaceBar(
                collapseMode: CollapseMode.pin,
                background: BlocBuilder<UpdateGalleryCubit, Gallery>(
                  bloc: updateGalleryCubit,
                  builder: (context, gallery) {
                    return Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: gallery.image.isNotEmpty
                              ? NetworkImage(
                                  AppAssets.baseUrl + widget.gallery.image,
                                )
                              : AssetImage(
                                  AppAssets.imgBanner1,
                                ),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                gallery.name,
                                style: theme.textTheme.headline4.copyWith(
                                  color: theme.backgroundColor,
                                ),
                              ),
                            ),
                            Visibility(
                              visible: gallery.bio.isNotEmpty,
                              child: Text(
                                gallery.bio,
                                style: theme.textTheme.headline6.copyWith(
                                  color: theme.backgroundColor,
                                  fontWeight: AppFontWeight.regular,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 2.5),
                              child: Text(
                                gallery.numOfPost != 0
                                    ? '${gallery.numOfPost} ${lang.locaized('post')}'
                                    : lang.locaized('emptyListOfGallery'),
                                style: theme.textTheme.headline6.copyWith(
                                  color: theme.backgroundColor,
                                  fontWeight: AppFontWeight.regular,
                                ),
                              ),
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(vertical: 2.5),
                              child: Text.rich(
                                TextSpan(
                                  style: theme.textTheme.headline6.copyWith(
                                    color: theme.backgroundColor,
                                    fontWeight: AppFontWeight.regular,
                                  ),
                                  children: [
                                    WidgetSpan(
                                      child: Padding(
                                        padding:
                                            const EdgeInsets.only(right: 5.0),
                                        child: Icon(
                                          gallery.private == 0
                                              ? AppIcon.openlock
                                              : AppIcon.block,
                                          color: theme.backgroundColor,
                                          size: sizeScreen * 2.7,
                                        ),
                                      ),
                                    ),
                                    TextSpan(
                                      text: gallery.private == 1
                                          ? lang.locaized('private')
                                          : lang.locaized('public'),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  BlocBuilder<PostByGalleryBloc, PostByGalleryState>(
                    bloc: _postByGalleryBloc,
                    builder: (context, state) {
                      if (state is PostByGalleryInitial) {
                        _postByGalleryBloc.add(
                          GetPostByGalleryEvent(widget.gallery.id, _page++,
                              _limit, authBloc.isChecked),
                        );
                      } else if (state is PostByGalleryRefresh) {
                        return AppShimmer.list();
                      } else if (state is PostByGallerySuccess) {
                        _listPost.addAll(state.list);
                        _isLastPage =
                            _listPost.length <= widget.gallery.numOfPost;
                      } else if (state is PostByGalleryFailed) {
                        ///
                      }
                      return _listPost.isEmpty
                          ? Container(
                              color: AppColors.backgroundSearchColor,
                              height: sizeScreen * 60.0,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    AppIcon.box,
                                    size: sizeScreen * 15,
                                    color: AppColors.darkThemeColor.withOpacity(
                                      0.4,
                                    ),
                                  ),
                                ],
                              ),
                            )
                          : Column(
                              children: [
                                StaggeredGridView.extentBuilder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 15.0,
                                    vertical: 5.0,
                                  ),
                                  mainAxisSpacing: 5.0,
                                  crossAxisSpacing: 5.0,
                                  maxCrossAxisExtent: 200,
                                  staggeredTileBuilder: (index) =>
                                      StaggeredTile.extent(
                                          1, index.isEven ? 280 : 250),
                                  itemBuilder: (context, index) =>
                                      ItemListPost(_listPost[index]),
                                  itemCount: _listPost.length,
                                ),
                                _isLastPage == false
                                    ? AppButton.icon(
                                        padding: const EdgeInsets.all(5.0),
                                        icon: AppIcon.moreDown,
                                        iconColor: theme.primaryColor,
                                        onTap: () => _postByGalleryBloc.add(
                                          GetPostByGalleryEvent(
                                            widget.gallery.id,
                                            _page++,
                                            _limit,
                                            authBloc.isChecked,
                                          ),
                                        ),
                                      )
                                    : (state is PostByGalleryLoading)
                                        ? Padding(
                                            padding: const EdgeInsets.all(10.0),
                                            child: AppLoading.threeBounce(
                                                size: 20.0),
                                          )
                                        : const SizedBox(),
                              ],
                            );
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
