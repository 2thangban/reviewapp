import 'dart:io';

import 'package:bloc/bloc.dart';

class AddImageGalleryCubit extends Cubit<bool> {
  File image;
  String urlImage;
  AddImageGalleryCubit(this.urlImage) : super(false);

  void add(File image) {
    this.image = image;
    emit(true);
  }
}
