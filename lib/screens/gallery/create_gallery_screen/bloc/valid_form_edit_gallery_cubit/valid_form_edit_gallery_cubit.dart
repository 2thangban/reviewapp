import 'package:bloc/bloc.dart';

import '../../../../../models/gallery/gellery.dart';

class ValidFormEditGalleryCubit extends Cubit<bool> {
  final Gallery gallery;
  ValidFormEditGalleryCubit(this.gallery) : super(false);

  void valid(Gallery gallery) => emit(this.gallery != gallery);
}
