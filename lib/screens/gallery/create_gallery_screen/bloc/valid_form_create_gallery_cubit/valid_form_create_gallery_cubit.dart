import 'package:bloc/bloc.dart';

import '../../../../../commons/helper/validator.dart';
import '../../../../../models/gallery/gellery.dart';

class ValidFormCreateGalleryCubit extends Cubit<bool> {
  Gallery gallery;
  ValidFormCreateGalleryCubit(this.gallery) : super(false);

  void validation() =>
      !Validator.isEmptyString(gallery.name) ? emit(false) : emit(true);
}
