import 'package:bloc/bloc.dart';

class ChangeSecureGalleryCubit extends Cubit<int> {
  ChangeSecureGalleryCubit(int status) : super(status);

  void change(int status) => emit(status);
}
