part of 'gallery_bloc.dart';

abstract class GalleryEvent {}

class CreateGalleryEvent extends GalleryEvent {
  final Gallery gallery;
  CreateGalleryEvent(this.gallery);
}

class EditGalleryEvent extends GalleryEvent {
  final Gallery gallery;
  EditGalleryEvent(this.gallery);
}

class DeleteGalleryEvent extends GalleryEvent {
  final int galleryId;
  DeleteGalleryEvent(this.galleryId);
}
