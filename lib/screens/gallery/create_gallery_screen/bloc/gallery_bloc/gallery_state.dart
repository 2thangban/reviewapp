part of 'gallery_bloc.dart';

abstract class GalleryState {}

/// In case Initial
class GalleryInitial extends GalleryState {}

class GalleryLoading extends GalleryState {}

/// Create Gallery
class CreateGallerySuccess extends GalleryState {
  final Gallery gallery;
  CreateGallerySuccess(this.gallery);
}

class CreateGalleryFailed extends GalleryState {
  final String msg;
  CreateGalleryFailed(this.msg);
}

/// Edit Gallery
class EditGallerySuccess extends GalleryState {
  final Gallery gallery;
  EditGallerySuccess(this.gallery);
}

class EditGalleryFailed extends GalleryState {}

/// Delete gallery
class DeleteGallerySuccess extends GalleryState {
  final ResponseActionUser responseActionUser;
  DeleteGallerySuccess(this.responseActionUser);
}

class DeleteGalleryFailed extends GalleryState {}
