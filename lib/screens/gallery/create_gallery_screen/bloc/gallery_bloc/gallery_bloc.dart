import 'dart:async';

import 'package:bloc/bloc.dart';

import '../../../../../api/api_response.dart';
import '../../../../../models/gallery/gellary_repo.dart';
import '../../../../../models/gallery/gellery.dart';
import '../../../../../models/token/token_repo.dart';
import '../../../../../models/user/reponse_action_user.dart';

part 'gallery_event.dart';
part 'gallery_state.dart';

class GalleryBloc extends Bloc<GalleryEvent, GalleryState> {
  GalleryBloc() : super(GalleryInitial());

  @override
  Stream<GalleryState> mapEventToState(GalleryEvent event) async* {
    /// In case the user create a new gallery
    if (event is CreateGalleryEvent) {
      yield GalleryLoading();
      try {
        final result = await GalleryRepo.create(event.gallery);
        yield CreateGallerySuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Gallery result;
          await TokenRepo.refreshToken().then((newToken) async {
            TokenRepo.write(newToken).whenComplete(() async {
              result = await GalleryRepo.create(event.gallery);
            });
          });
          yield CreateGallerySuccess(result);
        } else {
          yield CreateGalleryFailed(e.message);
        }
      } catch (e) {
        yield CreateGalleryFailed(e.toString());
      }
    }

    /// In case the user edit information of the gallery
    else if (event is EditGalleryEvent) {
      yield GalleryLoading();
      try {
        final result = await GalleryRepo.edit(event.gallery);
        yield EditGallerySuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          Gallery result;
          await TokenRepo.refreshToken().then((newToken) async {
            TokenRepo.write(newToken).whenComplete(() async {
              result = await GalleryRepo.edit(event.gallery);
            });
          });
          yield EditGallerySuccess(result);
        } else {
          yield EditGalleryFailed();
        }
      } catch (e) {
        yield EditGalleryFailed();
      }
    }

    /// In case the user removes a gallery from the gallery list
    else if (event is DeleteGalleryEvent) {
      try {
        final result = await GalleryRepo.delete(event.galleryId);
        yield DeleteGallerySuccess(result);
      } on ErrorResponse catch (e) {
        if (e.staticCode == 401) {
          ResponseActionUser result;
          await TokenRepo.refreshToken().then((newToken) async {
            TokenRepo.write(newToken).whenComplete(() async {
              result = await GalleryRepo.delete(event.galleryId);
            });
          });
          yield DeleteGallerySuccess(result);
        } else {
          yield DeleteGalleryFailed();
        }
      } catch (e) {
        yield DeleteGalleryFailed();
      }
    }
  }
}
