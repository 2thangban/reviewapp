import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../commons/helper/size_config.dart';
import '../../../../commons/helper/validator.dart';
import '../../../../commons/untils/app_color.dart';
import '../../../../commons/untils/app_icon.dart';
import '../../../../commons/untils/app_image.dart';
import '../../../../commons/untils/app_picker.dart';
import '../../../../commons/untils/app_shower.dart';
import '../../../../commons/widgets/app_bar.dart';
import '../../../../commons/widgets/app_button.dart';
import '../../../../commons/widgets/app_loading.dart';
import '../../../../commons/widgets/app_textfield.dart';
import '../../../../localizations/app_localization.dart';
import '../../../../models/gallery/gellery.dart';
import '../../../app_common_bloc/auth_bloc/auth_bloc.dart';
import '../bloc/add_image_gallery_cubit/add_image_gallery_cubit.dart';
import '../bloc/change_secure_gallery_cubit/change_secure_gallery_cubit.dart';
import '../bloc/gallery_bloc/gallery_bloc.dart';
import '../bloc/valid_form_create_gallery_cubit/valid_form_create_gallery_cubit.dart';
import '../bloc/valid_form_edit_gallery_cubit/valid_form_edit_gallery_cubit.dart';

class CreateGalleryScreen extends StatefulWidget {
  final Gallery gallery;
  const CreateGalleryScreen(this.gallery, {Key key}) : super(key: key);
  @override
  _CreateGalleryScreenState createState() => _CreateGalleryScreenState();
}

class _CreateGalleryScreenState extends State<CreateGalleryScreen> {
  AuthBloc authBloc;
  final GalleryBloc galleryBloc = GalleryBloc();
  TextEditingController _titleController;
  TextEditingController _contentController;
  AddImageGalleryCubit addImageGalleryCubit;
  ValidFormCreateGalleryCubit validFormGalleryCubit;
  ValidFormEditGalleryCubit validFormEditGalleryCubit;
  ChangeSecureGalleryCubit secureGalleryCubit;
  Gallery gallery;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);
    if (widget.gallery != null) {
      gallery = widget.gallery.clone();
      validFormEditGalleryCubit = ValidFormEditGalleryCubit(
        widget.gallery.clone(),
      );
    } else {
      gallery = Gallery(
        name: '',
        bio: '',
        private: 0,
      );
    }
    secureGalleryCubit = ChangeSecureGalleryCubit(gallery.private);

    addImageGalleryCubit = AddImageGalleryCubit(gallery.image);
    validFormGalleryCubit = ValidFormCreateGalleryCubit(gallery);

    _titleController = new TextEditingController(text: gallery.name);
    _contentController = new TextEditingController(text: gallery.bio);
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    final lang = AppLocalization.of(context);
    return BlocConsumer<GalleryBloc, GalleryState>(
      bloc: galleryBloc,
      listenWhen: (previous, current) => (current is CreateGalleryFailed ||
              current is CreateGallerySuccess ||
              current is EditGallerySuccess)
          ? true
          : false,
      listener: (context, state) {
        if (state is CreateGalleryFailed) {
          return AppShower.showFlushBar(AppIcon.errorCheck, context,
              theme: theme,
              iconColor: AppColors.errorColor,
              message: state.msg);
        } else if (state is CreateGallerySuccess) {
          Future.delayed(Duration(milliseconds: 500), () {
            Navigator.pop(context, state.gallery);
          });
        } else if (state is EditGallerySuccess) {
          Future.delayed(Duration(milliseconds: 500), () {
            Navigator.pop(context, state.gallery);
          });
        }
      },
      builder: (_, state) => Stack(
        children: [
          Scaffold(
            appBar: CommonAppBar(
              leftBarButtonItem: AppButton.icon(
                icon: AppIcon.popNavigate,
                onTap: () => Navigator.pop(context),
              ),
              rightBarButtonItems: [
                widget.gallery != null
                    ? BlocBuilder<ValidFormEditGalleryCubit, bool>(
                        bloc: validFormEditGalleryCubit,
                        builder: (context, status) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2.5),
                            child: AppButton.common(
                              labelText: lang.locaized('editGalleryBtn'),
                              labelStyle: theme.textTheme.bodyText1.copyWith(
                                color: status
                                    ? theme.backgroundColor
                                    : AppColors.subLightColor,
                              ),
                              backgroundColor: status
                                  ? AppColors.primaryColor
                                  : AppColors.backgroundSearchColor,
                              contentPadding: 8,
                              width: sizeScreen * 12,
                              radius: 10.0,
                              shadowColor: theme.backgroundColor,
                              onPressed: () => status
                                  ? {galleryBloc.add(EditGalleryEvent(gallery))}
                                  : null,
                            ),
                          );
                        },
                      )
                    : BlocBuilder<ValidFormCreateGalleryCubit, bool>(
                        bloc: validFormGalleryCubit,
                        builder: (context, status) {
                          return Padding(
                            padding: const EdgeInsets.symmetric(vertical: 2.5),
                            child: AppButton.common(
                              labelText: lang.locaized('CreateGalleryBtn'),
                              labelStyle: theme.textTheme.bodyText1.copyWith(
                                color: status
                                    ? theme.backgroundColor
                                    : AppColors.subLightColor,
                              ),
                              backgroundColor: status
                                  ? AppColors.primaryColor
                                  : AppColors.backgroundSearchColor,
                              contentPadding: 8,
                              radius: 10.0,
                              shadowColor: theme.backgroundColor,
                              onPressed: () => status
                                  ? {
                                      galleryBloc.add(
                                        CreateGalleryEvent(
                                          validFormGalleryCubit.gallery,
                                        ),
                                      )
                                    }
                                  : null,
                            ),
                          );
                        },
                      ),
              ],
            ),
            body: SingleChildScrollView(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  BlocBuilder<AddImageGalleryCubit, bool>(
                    bloc: addImageGalleryCubit,
                    builder: (context, bool) {
                      return GestureDetector(
                        onTap: () async {
                          final lstImage =
                              await AppPicker.getImageFromGallery();
                          if (lstImage.isNotEmpty) {
                            addImageGalleryCubit.urlImage = null;
                            if (widget.gallery != null) {
                              gallery.imageUpload = lstImage.first;
                              validFormEditGalleryCubit.valid(gallery);
                            } else {
                              validFormGalleryCubit.gallery.imageUpload =
                                  lstImage.first;
                            }
                            addImageGalleryCubit.add(lstImage.first);
                          }
                        },
                        child: (addImageGalleryCubit.image != null ||
                                addImageGalleryCubit.urlImage != null)
                            ? Container(
                                width: sizeScreen * 14,
                                height: sizeScreen * 14,
                                margin: EdgeInsets.symmetric(horizontal: 5.0),
                                clipBehavior: Clip.antiAlias,
                                decoration: BoxDecoration(
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image: addImageGalleryCubit.urlImage != null
                                        ? NetworkImage(
                                            AppAssets.baseUrl +
                                                addImageGalleryCubit.urlImage,
                                          )
                                        : FileImage(addImageGalleryCubit.image),
                                  ),
                                  borderRadius: BorderRadius.circular(20.0),
                                ),
                              )
                            : Container(
                                width: sizeScreen * 14,
                                decoration: BoxDecoration(
                                  color:
                                      AppColors.subLightColor.withOpacity(0.1),
                                  borderRadius: BorderRadius.circular(15.0),
                                ),
                                padding: const EdgeInsets.symmetric(
                                  horizontal: 15.0,
                                  vertical: 20.0,
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.all(sizeScreen * 0.4),
                                      child: SvgPicture.asset(
                                        AppAssets.addImageIcon,
                                        height: sizeScreen * 5,
                                      ),
                                    ),
                                    Text(
                                      lang.locaized('addImageGallery'),
                                      style: theme.textTheme.bodyText1,
                                    ),
                                  ],
                                ),
                              ),
                      );
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(
                      lang.locaized('nameGalley'),
                      style: theme.textTheme.headline5,
                    ),
                  ),
                  AppTextField.common(
                    controller: _titleController,
                    hintText: lang.locaized('hintFieldNameGalley'),
                    backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                    radius: 10.0,
                    onChange: (String input) {
                      if (widget.gallery != null) {
                        gallery.name = input;
                        validFormEditGalleryCubit.valid(gallery);
                      } else {
                        if (Validator.isEmptyString(
                                validFormGalleryCubit.gallery.name) &&
                            !Validator.isEmptyString(input)) {
                          validFormGalleryCubit.validation();
                        } else if (!Validator.isEmptyString(
                                validFormGalleryCubit.gallery.name) &&
                            Validator.isEmptyString(input)) {
                          validFormGalleryCubit.validation();
                        }
                      }
                      validFormGalleryCubit.gallery.name = input;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Text(lang.locaized('descGallery'),
                        style: theme.textTheme.headline5),
                  ),
                  AppTextField.common(
                    controller: _contentController,
                    hintText: lang.locaized('hintDescGallery'),
                    backgroundColor: AppColors.subLightColor.withOpacity(0.1),
                    radius: 10.0,
                    onChange: (String input) {
                      gallery.bio = input;
                      if (widget.gallery != null) {
                        validFormEditGalleryCubit.valid(gallery);
                      }
                      validFormGalleryCubit.gallery.bio = input;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          lang.locaized('labelPrivateGallery'),
                          style: theme.textTheme.headline5,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 5.0,
                            horizontal: 8.0,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              SizedBox(
                                width: MediaQuery.of(context).size.width - 100,
                                child: Text(
                                  lang.locaized('desPrivateGallery'),
                                  maxLines: 2,
                                  style: theme.textTheme.subtitle1,
                                ),
                              ),
                              BlocBuilder<ChangeSecureGalleryCubit, int>(
                                bloc: secureGalleryCubit,
                                builder: (context, status) {
                                  return Switch(
                                    value: !(status == 0),
                                    onChanged: (bool value) {
                                      gallery.private = value ? 1 : 0;
                                      if (widget.gallery != null) {
                                        validFormEditGalleryCubit
                                            .valid(gallery);
                                      } else {
                                        validFormGalleryCubit.gallery.private =
                                            value ? 1 : 0;
                                      }
                                      secureGalleryCubit.change(value ? 1 : 0);
                                    },
                                  );
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Visibility(
            visible: (state is GalleryLoading),
            child: Container(
              color: AppColors.darkThemeColor.withOpacity(0.7),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  AppLoading.doubleBounce(size: 30.0),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
