class KeyStore {
  static const String accessToken = 'AccessTokenKeystores';
  static const String refreshToken = 'RefreshTokenKeystores';
  static const String userLocale = 'UserLocale';
}
