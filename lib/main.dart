import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;

import 'commons/helper/simple_bloc_delegate.dart';
import 'commons/helper/size_config.dart';
import 'localizations/app_localization.dart';
import 'localizations/bloc/app_language_bloc.dart';
import 'models/location/location.dart';
import 'models/post/post.dart';
import 'models/province/province.dart';
import 'routes/route.dart';
import 'routes/route_name.dart';
import 'screens/app_common_bloc/auth_bloc/auth_bloc.dart';
import 'screens/app_common_bloc/location_bloc/app_location_bloc.dart';
import 'screens/app_common_bloc/province_bloc/province_bloc.dart';
import 'screens/app_common_bloc/push_notification_bloc/push_notification_bloc.dart';
import 'theme.dart';

Future main() async {
  /// initial bloc observer
  Bloc.observer = SimpleBlocObserver();

  /// initial firebase
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  /// initial hive
  var appDocumentDirectory =
      await pathProvider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(PostAdapter());
  Hive.registerAdapter(LocationAdapter());
  Hive.registerAdapter(ProvinceAdapter());

  SystemChrome.setSystemUIOverlayStyle(
    SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ),
  );
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> with WidgetsBindingObserver {
  final Routes routes = Routes();
  final AppLanguageBloc appLanguageBloc = AppLanguageBloc();
  final ProvinceBloc provinceBloc = ProvinceBloc();
  final PushNotificationBloc pushNotificationBloc = PushNotificationBloc();
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    appLanguageBloc.close();
    pushNotificationBloc.close();
    provinceBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addObserver(this);

    appLanguageBloc.add(FetchLanguageEvent());

    provinceBloc.add(InitProvinceEvent());

    return LayoutBuilder(
      builder: (context, constraints) {
        return OrientationBuilder(
          builder: (context, orientation) {
            SizedConfig().init(constraints, orientation);
            return GestureDetector(
              onTap: () =>
                  WidgetsBinding.instance.focusManager.primaryFocus?.unfocus(),
              child: MultiBlocProvider(
                providers: [
                  /// Dynamic language
                  BlocProvider<AppLanguageBloc>(create: (_) => appLanguageBloc),

                  /// Check current Postion user
                  BlocProvider<AppLocationBloc>(
                      create: (_) => AppLocationBloc()),

                  // Check Auth User
                  BlocProvider<AuthBloc>(create: (_) => AuthBloc()),

                  /// List province from API
                  BlocProvider<ProvinceBloc>(create: (_) => provinceBloc),

                  /// Listen event local notification
                  BlocProvider<PushNotificationBloc>(
                    create: (_) => pushNotificationBloc,
                  ),
                ],
                child: BlocBuilder<AppLanguageBloc, AppLanguageState>(
                  builder: (context, state) {
                    return NotificationListener<
                        OverscrollIndicatorNotification>(
                      onNotification: (overscroll) {
                        overscroll.disallowGlow();
                        return true;
                      },
                      child: MaterialApp(
                        debugShowCheckedModeBanner: false,
                        title: 'App Review',
                        theme: AppTheme().lightTheme,
                        locale: (state is AppLanguageComplete)
                            ? state.locale
                            : Locale('vi'),
                        supportedLocales: AppLocalization.supportedLocale,
                        localizationsDelegates: [
                          AppLocalization.delegate,
                          GlobalMaterialLocalizations.delegate,
                          GlobalWidgetsLocalizations.delegate,
                          GlobalCupertinoLocalizations.delegate,
                        ],
                        initialRoute: RouteName.initial,
                        onGenerateRoute: routes.routePage,
                      ),
                    );
                  },
                ),
              ),
            );
          },
        );
      },
    );
  }
}
