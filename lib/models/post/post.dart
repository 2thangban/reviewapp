import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:hive/hive.dart';

import '../../api/decodable.dart';
import '../../commons/helper/app_format.dart';
import '../hashtag/hashtag.dart';
import '../location/location.dart';
import '../user/user.dart';

part 'post.g.dart';

@HiveType(typeId: 0)
class Post extends Decodable with HiveObjectMixin, EquatableMixin {
  @HiveField(0)
  int id;
  @HiveField(1)
  String titlePost;
  @HiveField(2)
  String content;
  @HiveField(3)
  String dateCreatePost;
  @HiveField(4)
  Location location;
  @HiveField(5)
  List<String> hashTag;
  @HiveField(6)
  int rating;
  @HiveField(7)
  int category;
  List<Hashtag> listHashTag;
  Account account;
  List<String> listImage;
  String urlVideo;
  int locationId;
  int view;
  int like;
  int numOfComment;
  bool isLiked;
  bool isFavorite;
  int isLocked;
  List<File> imageUpload;
  File videoUpload;

  Post({
    this.id,
    this.account,
    this.location,
    this.titlePost,
    this.content,
    this.dateCreatePost,
    this.category,
    this.like,
    this.view,
    this.numOfComment,
    this.listImage,
    this.urlVideo,
    this.rating,
    List<String> hashTag,
    this.listHashTag,
    List<File> imageUpload,
    this.videoUpload,
    this.isLiked,
    this.isLocked,
    this.isFavorite,
    this.locationId,
  }) {
    this.hashTag = hashTag ?? [];
    this.imageUpload = imageUpload ?? [];
  }

  /// custom date create post
  get customeDate => AppFormat.durationToDate(
        AppFormat.stringToDate(this.dateCreatePost),
      );
  get dafaultDate => AppFormat.dateToString(
        AppFormat.stringToDate(this.dateCreatePost),
      );
  get customeView => AppFormat.number(this.view);

  static List<String> listNameHashtag(List<Hashtag> listHashTag) {
    final List<String> list = [];
    listHashTag.forEach((element) => list.add(element.title));
    return list;
  }

  factory Post.fromJson(Map<String, dynamic> json) => Post(
        id: json['post_id'],
        account:
            json['user'] != null ? Account.fromJson(json['user']) : Account(),
        location: json['location'] == null
            ? null
            : Location.fromJson(json['location']),
        titlePost: json['post_title'] ?? '',
        content: json['post_content'] ?? '',
        dateCreatePost: json['post_date'] ?? "",
        like: json['like'] ?? 0,
        numOfComment: json['comment_quantity'] ?? 0,
        isLiked: json['isLiked'] ?? false,
        isFavorite: json['isSaved'] ?? false,
        view: json['view'] ?? 0,
        locationId: json['location_id'] ?? null,
        listImage: json['post_image'] != null
            ? listStringFormJson(json['post_image'])
            : [],
        category: json['category_id'],
        urlVideo: json['post_video'] ?? '',
        rating: json['post_rating'] ?? 0,
        listHashTag: json['hashtag'] != null
            ? Hashtag.fromJsonList(json['hashtag'])
            : [],
        hashTag: Post.listNameHashtag(
          json['hashtag'] != null ? Hashtag.fromJsonList(json['hashtag']) : [],
        ),
        isLocked: json['is_locked'] ?? 0,
      );

  List<dynamic> toHashtagJsonList(List<Hashtag> listHashtag) {
    if (listHashTag == null) return [];
    final List<dynamic> list = [];
    listHashtag.forEach((element) => list.add(element.toJson()));
    return list;
  }

  @override
  List<Object> get props => [
        this.titlePost,
        this.content,
        this.category,
        this.hashTag,
        this.rating,
        this.listImage,
        this.imageUpload,
      ];

  static List<Post> formJsonList(List<dynamic> json) {
    final List<Post> listPost = [];
    for (var item in json) {
      listPost.add(Post.fromJson(item));
    }
    return listPost;
  }

  static List<String> listStringFormJson(List<dynamic> json) {
    final List<String> list = [];
    for (var item in json) {
      list.add(item.toString());
    }
    return list;
  }

  Map<String, String> toJson() {
    String listHashTag = '';
    if (this.hashTag.isNotEmpty) {
      this.hashTag.forEach((element) {
        listHashTag += "$element ";
      });
    }
    return <String, String>{
      'title': this.titlePost,
      'content': this.content,
      'hastag': listHashTag,
      'rating': this.rating.toString(),
      'location': this.location.id.toString(),
      'category': this.category.toString(),
    };
  }

  Map<String, String> toEditJson() {
    String listHashTag = '';
    String listImage = '';
    if (this.hashTag.isNotEmpty) {
      this.hashTag.forEach((element) => listHashTag += "$element ");
    }
    if (this.listImage.isNotEmpty) {
      this.listImage.forEach((element) => listImage += "$element ");
    }

    return <String, String>{
      'title': this.titlePost,
      'content': this.content,
      'hashtag': listHashTag,
      'uploaded_image': listImage,
      'rating': this.rating.toString(),
      'category': this.category.toString(),
    };
  }

  Post cloneWith({Post post}) => new Post(
        id: post?.id ?? id,
        titlePost: post?.titlePost ?? titlePost,
        content: post?.content ?? content,
        category: post?.category ?? category,
        account: post?.account ?? account,
        hashTag: post?.hashTag ?? hashTag,
        listHashTag: post?.listHashTag ?? listHashTag,
        imageUpload: post?.imageUpload ?? imageUpload,
        listImage: post?.listImage ?? listImage,
        rating: post?.rating ?? rating,
        dateCreatePost: post?.dateCreatePost ?? dateCreatePost,
        isLiked: post?.isLiked ?? isLiked,
        like: post?.like ?? like,
        isFavorite: post?.isFavorite ?? isFavorite,
      );

  @override
  Post decode(data) => Post.fromJson(data);
}
