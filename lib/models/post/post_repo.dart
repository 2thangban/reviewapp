import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import '../user/reponse_action_user.dart';
import 'post.dart';

class PostRepo {
  /// increament view of post
  static Future<ResponseActionUser> incrementView(final int postID) async {
    final param = 'post=${postID.toString()}';

    final apiManager = APIManager(
      type: APIType.increamentView,
      routeParams: param,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// search post
  static Future<List<Post>> search({
    final String key = '',
    final int page = 0,
    final int limit = 10,
    final int cate,
    final int provinceId,
  }) async {
    final String param =
        'key=$key&page=$page&limit=$limit&province=$provinceId';
    final apiManager = APIManager(
      type: APIType.searchPost,
      routeParams: param,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }

    throw result.error;
  }

  /// get all video
  static Future<List<Post>> getAllVideo({
    int page = 0,
    int limit = 5,
    bool isAuth = false,
  }) async {
    String params = 'limit=$limit&page=$page';
    Token token;
    String accessToken;
    if (isAuth) {
      token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllVideos,
      routeParams: params,
      token: accessToken,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse<Post>(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  ///get all like post
  static Future<List<Post>> getAllLikePost({
    int page = 0,
    int limit = 5,
  }) async {
    String params = 'limit=$limit&page=$page';

    Token token;
    String accessToken;

    token = await TokenRepo.read();
    accessToken = token.accessToken;

    final apiManager = APIManager(
      type: APIType.getLikePosts,
      routeParams: params,
      token: accessToken,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse<Post>(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  ///get all post
  static Future<List<Post>> getAllPost({
    int page = 0,
    int limit = 5,
    bool isAuth = false,
    int provinceId,
    int categoryId,
  }) async {
    String params = 'limit=$limit&page=$page';
    params += provinceId != null ? '&province=$provinceId' : '';
    params += categoryId != 0 ? '&category=$categoryId' : '';

    Token token;
    String accessToken;
    if (isAuth) {
      token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllPosts,
      routeParams: params,
      token: accessToken,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse<Post>(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get detail post
  static Future<Post> detailPost(int postId, {bool isAuth = false}) async {
    String param = 'id=$postId';
    Token token;
    String accessToken;
    if (isAuth) {
      token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getPostDetail,
      routeParams: param,
      token: accessToken,
    );

    final result = await APIController.request<APIResponse<Post>>(
      manager: apiManager,
      create: () => APIResponse<Post>(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Edit post
  static Future<Post> editPost(Post post) async {
    /// get token
    Token userToken = await TokenRepo.read();
    List<MultipartFile> listImgUpload = [];

    MultipartFile video;

    ///check add list img to request
    for (var item in post.imageUpload) {
      await MultipartFile.fromPath('new_image[]', item.path,
              contentType: MediaType('image', 'jpg'))
          .then((img) => listImgUpload.add(img));
    }

    /// check add video to request
    if (post.videoUpload?.path != null && post.videoUpload.path != '') {
      video = await MultipartFile.fromPath('video', post.videoUpload.path,
          contentType: MediaType('video', 'mp4'));
    }

    final apiManager = APIManager(
      type: APIType.editPost,
      token: userToken.accessToken,
      routeParams: 'id=${post.id}',
    );

    final result = await APIController.request<APIResponse<Post>>(
      manager: apiManager,
      body: post.toEditJson(),
      listFile: listImgUpload,
      sigleFile: video,
      create: () => APIResponse(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Create post
  static Future<Post> createPost(Post post) async {
    Token userToken = await TokenRepo.read();

    List<MultipartFile> listImgUpload = [];
    MultipartFile video;

    ///check add list img to request
    for (var item in post.imageUpload) {
      await MultipartFile.fromPath('image[]', item.path,
              contentType: MediaType('image', 'jpg'))
          .then((img) => listImgUpload.add(img));
    }

    /// check add video to request
    if (post.videoUpload.path != null && post.videoUpload.path != '') {
      video = await MultipartFile.fromPath('video', post.videoUpload.path,
          contentType: MediaType('video', 'mp4'));
    }

    final apiManager = APIManager(
      type: APIType.createPost,
      token: userToken.accessToken,
    );

    final result = await APIController.request<APIResponse<Post>>(
      manager: apiManager,
      body: post.toJson(),
      listFile: listImgUpload,
      sigleFile: video,
      create: () => APIResponse(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
