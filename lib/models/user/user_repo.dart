import 'dart:io';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../../configs/constrain.dart';
import '../post/post.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import 'reponse_action_user.dart';
import 'user.dart';

enum SignUpType {
  social,
  phone,
}
const String idKey = 'id';
const String usernameKey = 'userName';
const String avatarKey = 'avatar';

class AccountRepo {
  /// Get the list of follower
  static Future<ResponseActionUser> checkPhone(String phone) async {
    final apiManager = APIManager(type: APIType.checkPhone);

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
      body: <String, dynamic>{
        "phone": phone,
      },
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Get the list of follower
  static Future<List<Account>> getTopUser({
    int page = 0,
    int limit = 10,
  }) async {
    final param = '?page=$page&limit=$limit';

    final apiManager = APIManager(
      routeParams: param,
      type: APIType.getTopUser,
    );

    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  ///save account into local storage
  static Future<bool> write(Account account) async {
    final secureToken = new FlutterSecureStorage();
    await secureToken
        .write(key: idKey, value: account.id.toString())
        .whenComplete(
      () async {
        await secureToken
            .write(key: usernameKey, value: account.userName)
            .whenComplete(
          () async {
            await secureToken.write(
              key: avatarKey,
              value: account.avatar,
            );
          },
        );
      },
    );
    return false;
  }

  /// read account from local storage
  static Future<Account> read() async {
    final secureToken = new FlutterSecureStorage();
    String id = await secureToken.read(key: idKey);
    String userName = await secureToken.read(key: usernameKey);
    String avatar = await secureToken.read(key: avatarKey);
    return (id != null && userName != null)
        ? Account(
            id: int.parse(id),
            userName: userName,
            avatar: avatar,
          )
        : Future.error("account is not exist !!");
  }

  /// is check token exist
  static Future<bool> check() async {
    final secureToken = new FlutterSecureStorage();
    bool islogged = await secureToken.containsKey(key: KeyStore.accessToken);
    return islogged;
  }

  /// delete all token
  static Future<bool> deleteAll() async {
    final secureToken = new FlutterSecureStorage();
    if (await secureToken.containsKey(key: KeyStore.accessToken) == true) {
      secureToken.deleteAll();
      return true;
    }
    return false;
  }

  /// search account
  static Future<List<Account>> search({
    String key,
    int page,
    int limit,
    int provinceId,
  }) async {
    final String param =
        'key=$key&page=$page&limit=$limit&province=$provinceId';
    final apiManager = APIManager(
      type: APIType.searchAccount,
      routeParams: param,
    );

    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Get the list of follower
  static Future<List<Account>> getFollowingList({
    int page = 0,
    int limit = 10,
    int userId,
  }) async {
    final param = '?user=$userId&page=$page&limit=$limit';

    final token = await TokenRepo.read();

    final apiManager = APIManager(
      routeParams: param,
      type: APIType.getListFollowing,
      token: token.accessToken,
    );

    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Get the list of follower
  static Future<List<Account>> getFollowerList({
    int page = 0,
    int limit = 10,
    int userId,
  }) async {
    final param = '?user=$userId&page=$page&limit=$limit';

    final token = await TokenRepo.read();

    final apiManager = APIManager(
      routeParams: param,
      type: APIType.getListFollower,
      token: token.accessToken,
    );

    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Get the list of 100 most follow users
  static Future<List<Account>> getTopUserList({
    int page = 0,
    int limit = 10,
  }) async {
    final param = '?page=$page&limit=$limit';

    final apiManager = APIManager(
      routeParams: param,
      type: APIType.getAllUser,
    );

    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Change password by user
  static Future<Token> changePassword(Map<String, String> body) async {
    final Token token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.changePassword,
      token: token.refreshToken,
    );

    final result = await APIController.request<APIResponse<Token>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Token(),
      ),
      body: body,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Change avatar by user
  static Future<ResponseActionUser> changeAvatar(File image) async {
    final Token token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.changeAvtUser,
      token: token.accessToken,
    );
    final newAvatar = await MultipartFile.fromPath('avatar', image.path,
        contentType: MediaType('image', 'jpg'));
    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
      sigleFile: newAvatar,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Edit profile by user
  static Future<Account> editprofile(Account account, {File image}) async {
    final token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.editProfile,
      token: token.accessToken,
    );
    MultipartFile newAvatar;
    if (image != null && image.path != null && image.path.trim() != '') {
      newAvatar = await MultipartFile.fromPath(
        'avatar',
        image.path,
        contentType: MediaType('image', 'jpg'),
      );
    }
    final result = await APIController.request<APIResponse<Account>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Account(),
      ),
      body: account.toJson(),
      sigleFile: newAvatar,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Like comment
  static Future<ResponseActionUser> likeComment(
    int commentId,
  ) async {
    final String param = 'comment=$commentId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.likeComment,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// unlike comment
  static Future<ResponseActionUser> unlikeComment(
    int commentId,
  ) async {
    final String param = 'comment=$commentId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.unlikeComment,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Like post by user
  static Future<ResponseActionUser> likePost(
    int postId,
  ) async {
    final String param = 'post=$postId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.likePost,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// Unlike post by user
  static Future<ResponseActionUser> unLikePost(int postId) async {
    final String param = 'post=$postId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.unlikePost,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// follow user
  static Future<ResponseActionUser> follow(int userId) async {
    final String param = 'user=$userId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.followUser,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// unfollow user
  static Future<ResponseActionUser> unFollow(int userId) async {
    final String param = 'user=$userId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.unfollowUser,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get all post by user
  static Future<List<Post>> getAllPost({
    int page = 0,
    int limit = 10,
    int userId,
    bool isAuth = false,
  }) async {
    String params = 'limit=$limit&page=$page&user=$userId';
    String accessToken;
    if (isAuth) {
      final Token token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getPostByUser,
      token: accessToken,
      routeParams: params,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse<Post>(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get info
  static Future<Account> profile() async {
    final token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.getProfile,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<Account>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get info another user
  static Future<Account> anotherUser(
    int userId, {
    bool isAuth = false,
  }) async {
    final String param = '?id=$userId';
    String accessToken;
    if (isAuth) {
      final token = await TokenRepo.read();
      accessToken = token.accessToken;
    }

    final apiManager = APIManager(
        type: APIType.getAnotherProfile,
        token: accessToken,
        routeParams: param);

    final result = await APIController.request<APIResponse<Account>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// logout

  /// login
  static Future<Account> login(Account account) async {
    final apiManager = APIManager(type: APIType.login);

    final result = await APIController.request<APIResponse<Account>>(
      manager: apiManager,
      create: () => APIResponse<Account>(
        create: () => Account(),
      ),
      body: account.toLoginJson(),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// register phone/ authentication with fb, gg
  static Future<Account> signUp(Account account, SignUpType type) async {
    APIManager apiManager;
    switch (type) {
      case SignUpType.social:
        apiManager = APIManager(type: APIType.signUpSocial);
        break;
      case SignUpType.phone:
        apiManager = APIManager(type: APIType.register);
        break;
      default:
        return null;
    }

    MultipartFile image;
    if (account.imageUpLoad != null) {
      image = await MultipartFile.fromPath(
        'avatar',
        account.imageUpLoad.path,
        contentType: MediaType('image', 'jpg'),
      );
    }

    final result = await APIController.request<APIResponse<Account>>(
      manager: apiManager,
      create: () => APIResponse<Account>(
        create: () => Account(),
      ),
      body: account.toJson(),
      sigleFile: image,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
