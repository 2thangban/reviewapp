import 'dart:io';

import 'package:equatable/equatable.dart';

import '../../api/decodable.dart';
import '../../commons/helper/app_format.dart';
import '../post/post.dart';
import '../province/province.dart';

class Account extends Decodable with EquatableMixin {
  int id;
  String userName;
  String password;
  String email;
  String phone;
  String firstName;
  String lastName;
  int gender;
  String birthDay;
  String avatar;
  File imageUpLoad;
  String fbId;
  String ggId;
  int expPoint;
  int rank;
  int numOfPostByUser;
  int numOfFollower;
  int numOfFollowing;
  String refreshToken;
  String accessToken;
  String firebaseMessagingToken;
  bool isFollowed;
  String bio;
  bool passIsNull;
  Province province;
  List<String> listImagePost;
  List<Post> postbyUser;

  get customDate => AppFormat.dateToString(
        AppFormat.stringToDate(this.birthDay, isTime: false),
        showTime: false,
      );
  Account({
    this.id,
    this.userName = '',
    this.password = '',
    this.phone = '',
    this.email = '',
    this.lastName = '',
    this.firstName = '',
    this.avatar = '',
    this.birthDay = '',
    this.gender = 0,
    this.expPoint = 0,
    this.rank = 0,
    this.numOfFollower = 0,
    this.numOfFollowing = 0,
    this.numOfPostByUser = 0,
    this.accessToken,
    this.refreshToken,
    this.fbId = '',
    this.ggId = '',
    this.isFollowed = false,
    this.bio,
    this.passIsNull,
    this.province,
    this.postbyUser,
    this.listImagePost,
    this.firebaseMessagingToken = '',
  });

  @override
  List<Object> get props => [
        firstName,
        lastName,
        userName,
        gender,
        birthDay,
        province,
      ];

  factory Account.fromJson(Map<String, dynamic> json) => Account(
        id: json['user_id'],
        userName: json['user_name'] ?? "",
        lastName: json['last_name'] ?? "",
        firstName: json['first_name'] ?? "",
        email: json['email'] ?? "",
        phone: json['phone'] ?? "",
        gender: json['gender'] ?? 0,
        birthDay: json['birthday'] ?? "",
        avatar: json['avatar'] ?? '',
        expPoint: json['exp_point'] ?? 0,
        rank: json['rank'] ?? 1,
        bio: json['bio'] ?? '',
        passIsNull: json['passIsNull'] ?? true,
        province: json['province'] != null
            ? Province.fromJson(json['province'])
            : null,
        postbyUser:
            json['listPost'] != null ? Post.formJsonList(json['listPost']) : [],
        isFollowed: json['isFollowed'] ?? false,
        listImagePost: listStringFormJson(json['post_image'] ?? []),
        accessToken: json['access_token'] ?? '',
        refreshToken: json['refresh_token'] ?? '',
        numOfPostByUser: json['post_by_user'] ?? 0,
        numOfFollower: json['followers'] ?? 0,
        numOfFollowing: json['following'] ?? 0,
        fbId: json['fb_id'] ?? '',
        ggId: json['gg_id'] ?? '',
      );

  static List<Account> fromJsonList(List<dynamic> jsonAccountList) {
    final List<Account> listAccount = [];
    for (var account in jsonAccountList) {
      listAccount.add(Account.fromJson(account));
    }
    return listAccount;
  }

  static List<String> listStringFormJson(List<dynamic> json) {
    final List<String> list = [];
    for (var item in json) {
      list.add(item.toString());
    }
    return list;
  }

  Map<String, String> toJson() => <String, String>{
        "user_name": this.userName ?? '',
        "last_name": this.lastName ?? '',
        "first_name": this.firstName ?? '',
        "gender": this.gender.toString() ?? '0',
        "phone": this.phone ?? '',
        "password": this.password ?? '',
        "birthday": this.birthDay ?? '',
        'email': this.email ?? '',
        'firebase_token': this.firebaseMessagingToken,
        'province': this.province?.id.toString() ?? null,
        'g_uid': this.ggId ?? null,
      };
  Map<String, String> toLoginJson() => <String, String>{
        "phone": this.phone ?? '',
        "password": this.password ?? '',
        "firebase_token": this.firebaseMessagingToken,
      };
  Account clone() => Account(
        userName: this.userName,
        firstName: this.firstName,
        lastName: this.lastName,
        avatar: this.avatar,
        gender: this.gender,
        birthDay: this.birthDay,
        province: this.province,
      );

  @override
  Account decode(data) => Account.fromJson(data);
}
