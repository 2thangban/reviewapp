import '../../api/decodable.dart';

class ResponseActionUser extends Decodable {
  final String message;
  final String status;
  final String avatar;
  final bool phoneIsExisted;
  ResponseActionUser({
    this.message,
    this.status,
    this.avatar,
    this.phoneIsExisted,
  });

  factory ResponseActionUser.fromJson(Map<String, dynamic> json) =>
      ResponseActionUser(
        message: json['message'] ?? '',
        status: json['status'] ?? '',
        avatar: json['avatar'] ?? '',
        phoneIsExisted: json['phoneIsExisted'],
      );

  @override
  decode(data) => ResponseActionUser.fromJson(data);
}
