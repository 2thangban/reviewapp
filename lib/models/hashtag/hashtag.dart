import '../../api/decodable.dart';

class Hashtag extends Decodable {
  int _id;
  final String title;
  final int numOfPost;

  get id => this._id;

  Hashtag({int id, this.title, this.numOfPost}) {
    this._id = id;
  }

  factory Hashtag.fromJson(Map<String, dynamic> json) => Hashtag(
        id: json['hashtag_id'],
        title: json['hashtag'] ?? '',
        numOfPost: json['numOfPost'] ?? 0,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'hashtag_id': this.id,
        'hashtag': this.title,
        'post_by_hashtag': this.numOfPost,
      };
  static fromJsonList(List<dynamic> jsonList) {
    if (jsonList == null) return [];
    final List<Hashtag> listHashtag = [];
    for (var item in jsonList) {
      listHashtag.add(Hashtag.fromJson(item));
    }
    return listHashtag;
  }

  @override
  decode(data) => Hashtag.fromJson(data);
}
