import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../post/post.dart';
import 'hashtag.dart';

class HashtagRepo {
  static Future<List<Hashtag>> search({
    String key = '',
    page = 0,
    limit = 10,
  }) async {
    final param = 'key=$key&page=$page&limit=$limit';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.searchHashtag,
    );
    final result = await APIController.request<APIListReponse<Hashtag>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Hashtag(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  static Future<Hashtag> detail(int id) async {
    final param = 'id=$id';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.detailHashtag,
    );
    final result = await APIController.request<APIResponse<Hashtag>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Hashtag(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  static Future<List<Post>> postByHashtag(
    int idHashtag, {
    page = 0,
    limit = 10,
  }) async {
    final param = 'id=$idHashtag&page=$page&limit=$limit';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.getAllPostByHashtag,
    );
    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
