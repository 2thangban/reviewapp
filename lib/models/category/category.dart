import '../../api/decodable.dart';

class Category extends Decodable {
  final int id;
  final String name;

  Category({this.id, this.name});

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        id: json['category_id'],
        name: json['category_name'],
      );
  @override
  decode(data) => Category.fromJson(data);
}
