import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import 'category.dart';

class CategoryRepo {
  /// get all categories
  static Future<List<Category>> getAll() async {
    final apimanager = APIManager(type: APIType.getAllCategories);

    final result = await APIController.request<APIListReponse<Category>>(
      manager: apimanager,
      create: () => APIListReponse(
        create: () => Category(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
