import 'dart:io';

import '../../api/decodable.dart';
import '../../commons/helper/app_format.dart';
import '../user/user.dart';

class Comment extends Decodable {
  final int id;
  final Account account;
  final String date;
  final String urlImage;
  final int numOfLike;
  final int numOfReply;
  final bool isLike;
  String content;
  int postId;
  File imageUpload;

  get customeDate =>
      AppFormat.durationToDate(AppFormat.stringToDate(this.date));

  Comment({
    this.id,
    this.account,
    this.content,
    this.urlImage,
    this.date,
    this.numOfLike,
    this.numOfReply,
    this.isLike,
    this.postId,
    this.imageUpload,
  });

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id: json['comment_id'],
        account:
            json['user'] != null ? Account.fromJson(json['user']) : Account(),
        content: json['content'] ?? '',
        urlImage: json['comment_image'] ?? '',
        date: json['comment_date'] ?? '',
        numOfLike: json['like'] ?? 0,
        numOfReply: json['num_of_reply'] ?? 0,
        isLike: json['isLiked'] ?? false,
      );

  Map<String, String> toJson() => <String, String>{
        'post_id': this.postId.toString(),
        'content': this.content ?? '',
      };

  @override
  Comment decode(data) => Comment.fromJson(data);
}
