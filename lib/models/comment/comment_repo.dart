import 'dart:io';

import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import '../user/reponse_action_user.dart';
import 'comment.dart';

class CommentRepo {
  /// get all comment
  static Future<Comment> edit(
    int commentId, {
    String content,
    File imageUpload,
  }) async {
    final param = 'id=$commentId';
    final Token token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.editComment,
      token: token.accessToken,
      routeParams: param,
    );

    MultipartFile image;
    if (imageUpload != null) {
      image = await MultipartFile.fromPath(
        'image',
        imageUpload.path,
        contentType: MediaType('image', 'jpg'),
      );
    }

    final result = await APIController.request<APIResponse<Comment>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Comment(),
      ),
      body: <String, String>{"content": content},
      sigleFile: image,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get all comment
  static Future<Comment> getDetail(int commentId) async {
    final param = 'id=$commentId';
    final Token token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.getDetailComment,
      token: token.accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIResponse<Comment>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Comment(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get all comment
  static Future<List<Comment>> getAll(
    int post, {
    int page,
    int limit,
    bool isAuth = false,
  }) async {
    String accessToken;
    final param = 'page=$page&limit=$limit&post=$post';
    if (isAuth) {
      final Token token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllComment,
      token: accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIListReponse<Comment>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Comment(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get all reply comment
  static Future<List<Comment>> getAllReply(
    int commentId, {
    int page,
    int limit,
    bool isAuth = false,
  }) async {
    String accessToken;
    final param = 'page=$page&limit=$limit&comment=$commentId';
    if (isAuth) {
      final Token token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllReplyComment,
      token: accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIListReponse<Comment>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Comment(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// comment
  static Future<Comment> comment(
    Comment comment, {
    int commentId,
  }) async {
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.comment,
      token: token.accessToken,
    );
    final Map<String, String> jsonBody = comment.toJson();
    MultipartFile imageUpload;
    if (comment.imageUpload != null) {
      imageUpload = await MultipartFile.fromPath(
        'image',
        comment.imageUpload.path,
        contentType: MediaType('image', 'jpg'),
      );
    }

    /// case reply comment then param 'comment_id' != null
    if (commentId != null) {
      jsonBody.addEntries({
        MapEntry<String, String>('comment_id', commentId.toString()),
      });
    }
    final result = await APIController.request<APIResponse<Comment>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Comment(),
      ),
      body: jsonBody,
      sigleFile: imageUpload,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// like comment
  static Future<ResponseActionUser> like(int commentId) async {
    final Token token = await TokenRepo.read();
    final param = 'comment=$commentId';
    final apiManager = APIManager(
      type: APIType.likeComment,
      token: token.accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
