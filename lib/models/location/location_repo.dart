import 'package:flutter/cupertino.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../post/post.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import 'location.dart';

class LocationRepo {
  /// create location
  static Future<Location> create(Location location) async {
    /// get token
    final Token userToken = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.createLocation,
      token: userToken.accessToken,
    );

    final result = await APIController.request<APIResponse<Location>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Location(),
      ),
      body: location.toJson(),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// search location
  static Future<List<Location>> search({
    @required String key,
    final int page = 0,
    final int limit = 10,
    final int type,
    final int provinceId,
  }) async {
    final params =
        "?key=${key ?? ''}&page=$page&limit=$limit&province=$provinceId";

    final apiManager = APIManager(
      type: APIType.searchLocation,
      routeParams: params,
    );

    final result = await APIController.request<APIListReponse<Location>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Location(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// detail location
  static Future<Location> detail(int id) async {
    final params = "?id=$id";

    final apiManager = APIManager(
      type: APIType.detailLocation,
      routeParams: params,
    );

    final result = await APIController.request<APIResponse<Location>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Location(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get top locations
  static Future<List<Location>> getTopLocation({
    final int page = 0,
    final int limit = 10,
  }) async {
    final params = "?page=$page&limit=$limit";

    final apiManager = APIManager(
      type: APIType.getTopLocation,
      routeParams: params,
    );

    final result = await APIController.request<APIListReponse<Location>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Location(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get list post by location
  static Future<List<Post>> listPost(
    int locationId, {
    int page = 0,
    int limit = 5,
    bool isAuth = false,
  }) async {
    final params = "?page=$page&limit=$limit&location=$locationId";
    String accessToken;
    if (isAuth) {
      Token userToken = await TokenRepo.read();
      accessToken = userToken.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllPostLocation,
      routeParams: params,
      token: accessToken,
    );

    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Post(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
