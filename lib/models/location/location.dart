import 'dart:io';

import 'package:hive_flutter/hive_flutter.dart';

import '../../api/decodable.dart';
import '../../commons/helper/app_format.dart';
import '../province/province.dart';

part 'location.g.dart';

@HiveType(typeId: 1)
class Location extends Decodable with HiveObjectMixin {
  @HiveField(0)
  int id;

  @HiveField(1)
  String name;

  @HiveField(2)
  List<dynamic> image;

  @HiveField(3)
  String address;

  @HiveField(4)
  num rating;

  @HiveField(5)
  Province province;

  String openTime;
  String closeTime;
  int lowestPrice;
  int hightgestPrice;
  int postByLocation;
  String phone;
  File imageUpload;

  /// custome time
  get open => AppFormat.timeToString(this.openTime);
  get closed => AppFormat.timeToString(this.closeTime);

  /// custom money
  get lowest => AppFormat.number(this.lowestPrice);
  get hightest => AppFormat.number(this.hightgestPrice);
  String mgsTime() {
    final DateTime currentDate = DateTime.now();
    final open = AppFormat.time(this.openTime);
    final closed = AppFormat.time(this.closeTime);
    if (open.hour > currentDate.hour) return 'Chưa mở cửa';
    if (currentDate.hour >= closed.hour) return 'Đã đóng cửa';
    return 'Đang mở cửa';
  }

  Location({
    this.id,
    this.name = 'Chưa cập nhật',
    this.image,
    this.address = 'Chưa cập nhật',
    this.rating = 0,
    this.openTime = 'Chưa cập nhật',
    this.closeTime = 'Chưa cập nhật',
    this.phone = 'Chưa cập nhật',
    this.lowestPrice = 0,
    this.hightgestPrice = 0,
    this.postByLocation = 0,
    this.province,
  });

  factory Location.fromJson(Map<String, dynamic> json) => Location(
        id: json['location_id'],
        name: json['location_name'] ?? '',
        image: json['location_image'] ?? [],
        address: json['address'] ?? 'Chưa cập nhật',
        openTime: json['open_time'] ?? 'Chưa cập nhật',
        closeTime: json['closed_time'] ?? 'Chưa cập nhật',
        phone: json['phone'] != null && json['phone'] != ''
            ? json['phone']
            : 'Chưa cập nhật',
        province: json['province'] != null
            ? Province.fromJson(json['province'])
            : Province(),
        lowestPrice: json['lowest_price'] ?? 0,
        hightgestPrice: json['hightgest_price'] ?? 0,
        rating: json['location_rating'] ?? 0,
        postByLocation: json['post_by_location'] ?? 0,
      );

  static List<Location> fromJsonList(List<dynamic> jsonLocationList) {
    final List<Location> listLocation = [];
    for (var location in jsonLocationList) {
      listLocation.add(Location.fromJson(location));
    }
    return listLocation;
  }

  Map<String, String> toJson() {
    Map<String, String> jsonLocation = {
      'location_name': this.name ?? '',
      'address': this.address ?? '',
      'open_time': this.openTime ?? '',
      'phone_number': this.phone,
      'lowest_price': this.lowestPrice.toString() ?? '0',
      'hightgest_price': this.hightgestPrice.toString() ?? '0',
      'province': this.province.id.toString(),
    };
    return jsonLocation;
  }

  @override
  Location decode(data) => Location.fromJson(data);
}
