import 'dart:io';

import 'package:equatable/equatable.dart';

import '../../api/decodable.dart';

class Gallery extends Decodable with EquatableMixin {
  int _id;
  String name;
  String image;
  String bio;
  int private;

  int numOfPost;
  File imageUpload;

  get id => this._id;

  Gallery({
    int id,
    this.name,
    this.bio,
    this.private,
    this.numOfPost,
    this.image,
  }) {
    this._id = id;
  }

  @override
  List<Object> get props => [name, bio, private, image];

  factory Gallery.fromJson(Map<String, dynamic> json) => Gallery(
        id: json['gallery_id'],
        name: json['gallery_name'] ?? '',
        image: json['cover_photo'] ?? '',
        bio: json['description'] ?? '',
        private: json['is_private'] ? 1 : 0,
        numOfPost: json['num_of_post'] ?? 0,
      );

  Map<String, String> createGalleryToJson() => <String, String>{
        'gallery_name': this.name,
        'description': this.bio,
        'is_private': this.private.toString(),
      };

  Map<String, String> editGalleryToJson() => <String, String>{
        'gallery_name': this.name,
        'description': this.bio,
        'is_private': this.private.toString(),
      };

  Gallery clone() => Gallery(
        id: this._id,
        name: this.name,
        bio: this.bio,
        private: this.private,
        image: this.image,
      );
  @override
  decode(data) => Gallery.fromJson(data);
}
