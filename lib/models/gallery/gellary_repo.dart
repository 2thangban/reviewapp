import 'package:http/http.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../post/post.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import '../user/reponse_action_user.dart';
import 'gellery.dart';

class GalleryRepo {
  /// The user gets the gallery list.
  static Future<List<Gallery>> getAll(
    final int userId, {
    final int page = 0,
    final int limit = 10,
    bool isAuth = true,
  }) async {
    final String param = '?page=$page&limit=$limit&user=$userId';
    String accessToken;
    if (isAuth) {
      final Token token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getAllGallery,
      token: accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIListReponse<Gallery>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Gallery(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user gets the post list from the gallery.
  static Future<List<Post>> getPostByGallery(
    final int galleryId, {
    final int page = 0,
    final int limit = 10,
    final isAuth = true,
  }) async {
    final String param = '?page=$page&limit=$limit&gallery=$galleryId';
    String accessToken;
    if (isAuth) {
      final Token token = await TokenRepo.read();
      accessToken = token.accessToken;
    }
    final apiManager = APIManager(
      type: APIType.getPostGallery,
      token: accessToken,
      routeParams: param,
    );
    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Post(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user creates the gallery .
  static Future<Gallery> create(final Gallery gallery) async {
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.createGallery,
      token: token.accessToken,
    );

    MultipartFile image;
    if (gallery.imageUpload != null) {
      image =
          await MultipartFile.fromPath('cover_photo', gallery.imageUpload.path);
    }
    final result = await APIController.request<APIResponse<Gallery>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Gallery(),
      ),
      body: gallery.createGalleryToJson(),
      sigleFile: image,
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user creates the gallery .
  static Future<Gallery> edit(final Gallery gallery) async {
    String params = "id=${gallery.id}";
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.editGallery,
      token: token.accessToken,
      routeParams: params,
    );

    MultipartFile image;
    if (gallery.imageUpload != null) {
      image =
          await MultipartFile.fromPath('cover_photo', gallery.imageUpload.path);
    }
    final result = await APIController.request<APIResponse<Gallery>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Gallery(),
      ),
      body: gallery.editGalleryToJson(),
      sigleFile: image,
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user creates the gallery .
  static Future<ResponseActionUser> delete(final int galleryId) async {
    final param = '?gallery=$galleryId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.removeGallery,
      token: token.accessToken,
      routeParams: param,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );
    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user adds a post into the gallery
  static Future<ResponseActionUser> addPostToGallery(
      int postId, int galleryId) async {
    final String param = 'post=$postId&gallery=$galleryId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.addPostToGallery,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user remove a post from gallery
  static Future<ResponseActionUser> removePostFromGallery(int postId) async {
    final String param = 'post=$postId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.removePostFromGallery,
      routeParams: param,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
