import 'package:equatable/equatable.dart';
import 'package:hive_flutter/hive_flutter.dart';

import '../../api/decodable.dart';

part 'province.g.dart';

@HiveType(typeId: 2)
class Province extends Decodable with HiveObjectMixin, EquatableMixin {
  @HiveField(0)
  final int id;
  @HiveField(1)
  final String name;

  Province({this.id, this.name = 'Chưa cập nhật'});
  @override
  List<Object> get props => [id, name];

  factory Province.fromJson(Map<String, dynamic> json) => Province(
        id: json['province_id'],
        name: json['province_name'] ?? '',
      );

  @override
  decode(data) => Province.fromJson(data);
}
