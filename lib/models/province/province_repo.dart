import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import 'province.dart';

class ProvinceRepo {
  /// get all provinces
  static Future<List<Province>> getAll() async {
    final apimanager = APIManager(type: APIType.getAllProvinces);

    final result = await APIController.request<APIListReponse<Province>>(
      manager: apimanager,
      create: () => APIListReponse(
        create: () => Province(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
