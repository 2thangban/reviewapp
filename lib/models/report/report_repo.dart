import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import '../user/reponse_action_user.dart';
import 'report.dart';

class ReportPostRepo {
  /// get list title report
  static Future<List<ReportPost>> getList() async {
    final Token token = await TokenRepo.read();

    final apiManager = APIManager(
      type: APIType.getAllReportTitle,
      token: token.accessToken,
    );

    final result = await APIController.request<APIListReponse<ReportPost>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => ReportPost(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }

    throw result.error;
  }

  /// the user report post
  static Future<ResponseActionUser> report(
    int postId,
    int reportId, {
    String content = '',
  }) async {
    final Token token = await TokenRepo.read();
    final Map<String, String> body = <String, String>{
      'report_title': reportId.toString(),
      'post_id': postId.toString(),
      'report_content': content,
    };
    final apiManager = APIManager(
      type: APIType.reportPost,
      token: token.accessToken,
    );

    final result = await APIController.request<APIResponse<ResponseActionUser>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseActionUser(),
      ),
      body: body,
    );

    if (result.response?.data != null) {
      return result.response.data;
    }

    throw result.error;
  }
}
