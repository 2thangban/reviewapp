import '../../api/decodable.dart';

class ReportPost extends Decodable {
  final int id;
  final String title;

  ReportPost({this.id, this.title});

  factory ReportPost.formJson(Map<String, dynamic> json) => ReportPost(
        id: json['report_title_id'],
        title: json['report_title'],
      );
  @override
  decode(data) => ReportPost.formJson(data);
}
