import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../hashtag/hashtag.dart';
import '../post/post.dart';
import '../user/user.dart';
import 'search.dart';

class AppSearchRepo {
  /// The user is searching with category is null
  static Future<AppSearch> searchAll({
    final String key = '',
    final int provinceId,
  }) async {
    final String param = 'key=$key&province=$provinceId';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.searchAll,
    );
    final result = await APIController.request<APIResponse<AppSearch>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => AppSearch(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user is searching with category is post
  static Future<List<Post>> searchPost({
    String key = '',
    int page = 0,
    int limit = 10,
    final int provinceId,
  }) async {
    final String param =
        'key=$key&page=$page&limit=$limit&province=$provinceId';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.searchPost,
    );
    final result = await APIController.request<APIListReponse<Post>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Post(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user is searching with category is account
  static Future<List<Account>> searchAccount({
    final String key = '',
    final int page = 0,
    final int limit = 10,
    final int provinceId,
  }) async {
    final String param =
        'key=$key&page=$page&limit=$limit&province=$provinceId';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.searchAccount,
    );
    final result = await APIController.request<APIListReponse<Account>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Account(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// The user is searching with category is hashtag
  static Future<List<Hashtag>> searchHashtag({
    String key = '',
    int page = 0,
    int limit = 10,
  }) async {
    final String param = 'key=$key&page=$page&limit=$limit';
    final apiManager = APIManager(
      routeParams: param,
      type: APIType.searchHashtag,
    );
    final result = await APIController.request<APIListReponse<Hashtag>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => Hashtag(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
