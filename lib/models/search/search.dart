import '../../api/decodable.dart';
import '../hashtag/hashtag.dart';
import '../location/location.dart';
import '../post/post.dart';
import '../user/user.dart';

class AppSearch extends Decodable {
  final List<Post> listPost;
  final List<Account> listAccount;
  final List<Location> listLocation;
  final List<Hashtag> listHashtag;
  AppSearch({
    this.listPost,
    this.listAccount,
    this.listLocation,
    this.listHashtag,
  });

  factory AppSearch.fromJson(Map<String, dynamic> json) => AppSearch(
        listPost: Post.formJsonList(json['post']),
        listAccount: Account.fromJsonList(json['user']),
        listLocation: Location.fromJsonList(json['location']),
        listHashtag: Hashtag.fromJsonList(json['hashtag']),
      );
  @override
  decode(data) => AppSearch.fromJson(data);
}
