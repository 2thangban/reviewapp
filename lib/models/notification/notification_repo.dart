import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../token/token.dart';
import '../token/token_repo.dart';
import 'notification.dart';
import 'numOfNotify.dart';
import 'response_notification.dart';

class NotificationRepo {
  /// get the notification list
  static Future<List<NotificationApp>> list({
    int page = 0,
    int limit = 10,
  }) async {
    final String param = 'page=$page&limit=$limit';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.getAllNotity,
      routeParams: param,
      token: token.accessToken,
    );
    final result = await APIController.request<APIListReponse<NotificationApp>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => NotificationApp(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get the notification list
  static Future<List<NotificationApp>> getNowList({
    int page = 0,
    int limit = 10,
  }) async {
    final String param = 'page=$page&limit=$limit';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.listNowNotity,
      routeParams: param,
      token: token.accessToken,
    );
    final result = await APIController.request<APIListReponse<NotificationApp>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => NotificationApp(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get the notification list
  static Future<List<NotificationApp>> getOldList({
    int page = 0,
    int limit = 10,
  }) async {
    final String param = 'page=$page&limit=$limit';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.listOldNotity,
      routeParams: param,
      token: token.accessToken,
    );
    final result = await APIController.request<APIListReponse<NotificationApp>>(
      manager: apiManager,
      create: () => APIListReponse(
        create: () => NotificationApp(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// get the notification list
  static Future<NumOfNotification> numOfUnseen() async {
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.getCountNewNotify,
      token: token.accessToken,
    );
    final result = await APIController.request<APIResponse<NumOfNotification>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => NumOfNotification(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// set seen all the notification list
  static Future<ResponseNotify> seenAll() async {
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.seenAllNotify,
      token: token.accessToken,
    );
    final result = await APIController.request<APIResponse<ResponseNotify>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseNotify(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// set read the notification
  static Future<ResponseNotify> realdNotification(int notifyId) async {
    final String param = 'noti=$notifyId';
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.readNotify,
      routeParams: param,
      token: token.accessToken,
    );
    final result = await APIController.request<APIResponse<ResponseNotify>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseNotify(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  /// set read all the notification list
  static Future<ResponseNotify> realdAllNotification() async {
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.readAllNotify,
      token: token.accessToken,
    );
    final result = await APIController.request<APIResponse<ResponseNotify>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => ResponseNotify(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }
}
