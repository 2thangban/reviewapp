import '../../api/decodable.dart';

class ResponseNotify extends Decodable {
  final bool status;
  ResponseNotify({this.status});

  factory ResponseNotify.fromJson(Map<String, dynamic> json) =>
      ResponseNotify(status: json['status'] != null ? true : false);

  @override
  decode(data) => ResponseNotify.fromJson(data);
}
