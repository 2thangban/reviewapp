import '../../api/decodable.dart';

class NumOfNotification extends Decodable {
  final int numOfUnSeen;

  NumOfNotification({this.numOfUnSeen});

  factory NumOfNotification.fromJson(Map<String, dynamic> json) =>
      NumOfNotification(numOfUnSeen: json['count'] ?? 0);
  @override
  decode(data) => NumOfNotification.fromJson(data);
}
