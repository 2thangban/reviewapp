import '../../api/decodable.dart';
import '../../commons/helper/app_format.dart';
import '../user/user.dart';

class NotificationApp extends Decodable {
  final int id;
  final String title;
  final int type;
  final bool seen;
  final bool click;
  final String date;
  final int postId;
  final Account account;
  final int commentId;
  final int replyCommetnId;

  get isToday {
    final DateTime date = AppFormat.stringToDate(this.date);
    return date.day == DateTime.now().day ? true : false;
  }

  get customdate => AppFormat.durationToDate(AppFormat.stringToDate(this.date));

  NotificationApp({
    this.id,
    this.title,
    this.date,
    this.type,
    this.account,
    this.commentId,
    this.postId,
    this.replyCommetnId,
    this.seen,
    this.click,
  });

  factory NotificationApp.fromJson(Map<String, dynamic> json) =>
      NotificationApp(
        id: json['notification_id'],
        title: json['notification_title'] ?? '',
        type: json['type'],
        date: json['created_at'] ?? '',
        seen: json['seen'] ?? false,
        click: json['clicked'] ?? false,
        postId: json['post_id'] ?? null,
        account: json['user'] != null ? Account.fromJson(json['user']) : null,
        commentId: json['comment_id'] ?? null,
        replyCommetnId: json['reply_comment_id'] ?? null,
      );

  @override
  NotificationApp decode(data) => NotificationApp.fromJson(data);
}
