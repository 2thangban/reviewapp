import 'dart:developer';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../api/api_controller.dart';
import '../../api/api_manager.dart';
import '../../api/api_response.dart';
import '../../configs/constrain.dart';
import 'token.dart';

class TokenRepo {
  /// refreshToken
  static Future<Token> refreshToken() async {
    log('refresh token');
    final Token token = await TokenRepo.read();
    final apiManager = APIManager(
      type: APIType.refreshToken,
      token: token.refreshToken,
    );

    final result = await APIController.request<APIResponse<Token>>(
      manager: apiManager,
      create: () => APIResponse(
        create: () => Token(),
      ),
    );

    if (result.response?.data != null) {
      return result.response.data;
    }
    throw result.error;
  }

  ///save token into local storage
  static Future<bool> write(Token token) async {
    final secureToken = new FlutterSecureStorage();
    await secureToken.write(
        key: KeyStore.accessToken, value: token.accessToken);
    await secureToken.write(
        key: KeyStore.refreshToken, value: token.refreshToken);
    return true;
  }

  /// read token from local storage
  static Future<Token> read() async {
    final secureToken = new FlutterSecureStorage();
    String accessToken = await secureToken.read(key: KeyStore.accessToken);
    String refreshToken = await secureToken.read(key: KeyStore.refreshToken);
    return (accessToken != null && refreshToken != null)
        ? Token(accessToken: accessToken, refreshToken: refreshToken)
        : Future.error("Token is not exist !!");
  }

  /// is check token exist
  static Future<bool> check() async {
    final secureToken = new FlutterSecureStorage();
    bool islogged = await secureToken.containsKey(key: KeyStore.accessToken);
    return islogged;
  }

  /// delete all token
  static Future<bool> deleteALl() async {
    final secureToken = new FlutterSecureStorage();
    if (await secureToken.containsKey(key: KeyStore.accessToken) == true) {
      secureToken.deleteAll();
      return true;
    }
    return false;
  }
}
