import '../../api/decodable.dart';

class Token extends Decodable {
  final String accessToken;
  final String refreshToken;

  Token({
    this.accessToken,
    this.refreshToken,
  });

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        accessToken: json['access_token'],
        refreshToken: json['refresh_token'],
      );
  @override
  decode(data) => Token.fromJson(data);

  @override
  String toString() {
    return 'Token :\n accessToken: ${this.accessToken}' +
        ' \n refreshToken: ${this.refreshToken}';
  }
}
