import 'package:flutter/material.dart';

import '../../theme.dart';
import '../untils/app_color.dart';
import 'app_button.dart';

// ignore: must_be_immutable
class FollowByUser extends StatelessWidget {
  final double widthFollow;
  final double widthUnFollow;
  final double padding;
  final double contentPadding;
  bool isFollow;
  Function onTap;

  FollowByUser({
    @required this.widthFollow,
    @required this.widthUnFollow,
    this.padding,
    this.contentPadding,
    this.isFollow = false,
    this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Padding(
      padding: EdgeInsets.symmetric(vertical: padding ?? 14.0),
      child: AppButton.common(
        contentPadding: contentPadding,
        labelText: !isFollow ? "Theo dõi" : "Đã theo dõi",
        labelStyle: theme.textTheme.headline6.copyWith(
          color: !isFollow ? theme.backgroundColor : theme.iconTheme.color,
          fontWeight: AppFontWeight.bold,
        ),
        backgroundColor: !isFollow
            ? AppColors.primaryColor
            : AppColors.subLightColor.withOpacity(0.1),
        shadowColor: theme.backgroundColor,
        radius: 7.0,
        width: !isFollow ? widthFollow * 10 : widthUnFollow * 12,
        onPressed: onTap,
      ),
    );
  }
}
