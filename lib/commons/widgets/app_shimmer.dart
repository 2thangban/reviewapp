import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

Widget itemShimmer({double height, double width, double radius}) => Container(
      height: height ?? 50,
      width: width,
      decoration: BoxDecoration(
        color: Colors.grey[400],
        borderRadius: BorderRadius.circular(radius ?? 10.0),
      ),
    );

Widget circleShimmer({double size, double radius}) => Container(
      height: size ?? 50,
      width: size,
      decoration: BoxDecoration(
        color: Colors.grey[400],
        borderRadius: BorderRadius.circular(radius ?? 10.0),
      ),
    );

class AppShimmer {
  static Widget home() => Scaffold(
        body: SafeArea(
          child: Shimmer.fromColors(
            period: Duration(seconds: 1),
            direction: ShimmerDirection.ltr,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Column(
                  children: [
                    Row(
                      children: [
                        itemShimmer(width: 150),
                        Spacer(),
                        itemShimmer(width: 80, height: 40),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: itemShimmer(),
                    ),
                    itemShimmer(
                      height: 100,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: itemShimmer(height: 8.0, width: 50.0),
                    ),
                    itemShimmer(
                      height: 150,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Row(
                        children: [
                          itemShimmer(width: 120, height: 40),
                          const SizedBox(width: 20),
                          itemShimmer(width: 120, height: 40),
                        ],
                      ),
                    ),
                    itemShimmer(height: 40),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Row(
                        children: [
                          Expanded(child: itemShimmer(height: 150)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 150)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Row(
                        children: [
                          Expanded(child: itemShimmer(height: 150)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 150)),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            baseColor: Colors.grey[300],
            highlightColor: Colors.white,
          ),
        ),
      );

  static Widget video() => SafeArea(
        child: Shimmer.fromColors(
          period: Duration(seconds: 1),
          direction: ShimmerDirection.ltr,
          baseColor: Colors.grey[300],
          highlightColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                children: [
                  Row(
                    children: [
                      itemShimmer(width: 50),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: [
                            itemShimmer(width: 80, height: 20, radius: 5),
                            const SizedBox(height: 2),
                            itemShimmer(width: 80, height: 20, radius: 5),
                          ],
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: itemShimmer(height: 250),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      itemShimmer(height: 20, width: 100, radius: 5),
                      Spacer(),
                      itemShimmer(height: 30, width: 30, radius: 5),
                    ],
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: itemShimmer(height: 20, width: 100, radius: 5),
                  ),
                  const SizedBox(height: 10),
                  Row(
                    children: [
                      itemShimmer(width: 50),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: Column(
                          children: [
                            itemShimmer(width: 80, height: 20, radius: 5),
                            const SizedBox(height: 2),
                            itemShimmer(width: 80, height: 20, radius: 5),
                          ],
                        ),
                      )
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: itemShimmer(height: 250),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      itemShimmer(height: 20, width: 100, radius: 5),
                      Spacer(),
                      itemShimmer(height: 30, width: 30, radius: 5),
                    ],
                  ),
                  Align(
                    alignment: Alignment.topLeft,
                    child: itemShimmer(height: 20, width: 100, radius: 5),
                  ),
                ],
              ),
            ),
          ),
        ),
      );

  static Widget list() => SafeArea(
        child: Shimmer.fromColors(
          period: Duration(seconds: 1),
          direction: ShimmerDirection.ltr,
          baseColor: Colors.grey[300],
          highlightColor: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: SingleChildScrollView(
              physics: NeverScrollableScrollPhysics(),
              child: Column(
                children: [
                  Row(
                    children: [
                      Expanded(child: itemShimmer(height: 200)),
                      const SizedBox(width: 10.0),
                      Expanded(child: itemShimmer(height: 200)),
                    ],
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    children: [
                      Expanded(child: itemShimmer(height: 200)),
                      const SizedBox(width: 10.0),
                      Expanded(child: itemShimmer(height: 200)),
                    ],
                  ),
                  const SizedBox(
                    height: 5.0,
                  ),
                  Row(
                    children: [
                      Expanded(child: itemShimmer(height: 200)),
                      const SizedBox(width: 10.0),
                      Expanded(child: itemShimmer(height: 200)),
                    ],
                  ),
                  const SizedBox(
                    height: 5.0,
                  )
                ],
              ),
            ),
          ),
        ),
      );

  static Widget user() => Scaffold(
        body: SafeArea(
          child: Shimmer.fromColors(
            period: Duration(seconds: 1),
            direction: ShimmerDirection.ltr,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                child: Column(
                  children: [
                    const SizedBox(height: 50.0),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        circleShimmer(size: 100, radius: 100),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              const SizedBox(height: 5),
                              itemShimmer(height: 20, width: 130),
                              const SizedBox(height: 5),
                              itemShimmer(height: 17.0, width: 70),
                              const SizedBox(height: 5),
                              itemShimmer(height: 50, width: 200),
                            ],
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        children: [
                          Expanded(child: itemShimmer(height: 50)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 50)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 50)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 50),
                      child: itemShimmer(height: 100),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10.0),
                      child: Row(
                        children: [
                          Expanded(child: itemShimmer(height: 50)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 50)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 50)),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 5.0),
                      child: Row(
                        children: [
                          Expanded(child: itemShimmer(height: 200)),
                          const SizedBox(width: 5),
                          Expanded(child: itemShimmer(height: 200)),
                        ],
                      ),
                    ),
                    Row(
                      children: [
                        Expanded(child: itemShimmer(height: 200)),
                        const SizedBox(width: 5),
                        Expanded(child: itemShimmer(height: 200)),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            baseColor: Colors.grey[300],
            highlightColor: Colors.white,
          ),
        ),
      );
}
