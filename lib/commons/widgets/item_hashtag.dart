import 'package:flutter/material.dart';

import '../../localizations/app_localization.dart';
import '../../models/hashtag/hashtag.dart';
import '../animation/fade_anim.dart';
import '../untils/app_color.dart';

class ItemHashtag extends StatelessWidget {
  final Hashtag hashtag;
  final ThemeData theme;
  const ItemHashtag(this.hashtag, this.theme);
  @override
  Widget build(BuildContext context) {
    return FadeAnim(
      child: ListTile(
        minLeadingWidth: 0.0,
        minVerticalPadding: 0.0,
        horizontalTitleGap: 10.0,
        contentPadding: const EdgeInsets.only(left: 10.0),
        leading: Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 12.0,
            vertical: 6.0,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            border: Border.all(
              color: AppColors.backgroundSearchColor,
              width: 1.5,
            ),
          ),
          child: Text(
            '#',
            style: theme.textTheme.headline3,
          ),
        ),
        title: Text(
          '#${hashtag.title}',
          style: theme.textTheme.headline6,
        ),
        subtitle: Text(
          '${hashtag.numOfPost} ${AppLocalization.of(context).locaized('post')}',
          style: theme.textTheme.bodyText2,
        ),
      ),
    );
  }
}
