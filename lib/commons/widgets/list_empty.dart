import 'package:flutter/widgets.dart';

import '../untils/app_image.dart';

class AppObjectEmpty {
  static Widget list() => Container(
        child: Image.asset(
          AppAssets.emptyList,
          fit: BoxFit.fill,
        ),
      );
}
