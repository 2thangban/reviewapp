import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../localizations/app_localization.dart';
import '../../routes/route_name.dart';
import '../../screens/app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../screens/home_screen/user/profile_page.dart/ui/gallery_by_user/bloc/list_gallery_bloc/list_gallery_bloc.dart';
import '../animation/fade_anim.dart';
import '../helper/size_config.dart';
import '../untils/app_color.dart';
import '../untils/app_icon.dart';
import '../untils/app_image.dart';
import 'app_button.dart';
import 'app_loading.dart';

class ListGalleryWidget extends StatefulWidget {
  @override
  _ListGalleryWidgetState createState() => _ListGalleryWidgetState();
}

class _ListGalleryWidgetState extends State<ListGalleryWidget> {
  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeImage = SizedConfig.imageSizeMultiplier;
  final ScrollController scrollListGalleryController = ScrollController();
  final ListGalleryBloc listGalleryBloc = ListGalleryBloc();
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    authBloc = BlocProvider.of<AuthBloc>(context);

    Future.delayed(
      const Duration(milliseconds: 200),
      () => listGalleryBloc.add(GetListGalleryEvent(authBloc.account.id)),
    );

    scrollListGalleryController.addListener(() {
      if (scrollListGalleryController.position.pixels ==
              scrollListGalleryController.position.maxScrollExtent &&
          !listGalleryBloc.isLastPage) {
        ///
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final lang = AppLocalization.of(context);
    return Container(
      padding: const EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: theme.backgroundColor,
        borderRadius: BorderRadius.vertical(
          top: const Radius.circular(10.0),
        ),
      ),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(
              vertical: 10.0,
              horizontal: 5.0,
            ),
            child: Row(
              children: [
                Expanded(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      lang.locaized('labelGallery'),
                      style: theme.textTheme.headline4,
                    ),
                  ),
                ),
                AppButton.icon(
                  icon: AppIcon.exitNavigate,
                  iconSize: sizeScreen * 3.5,
                  onTap: () => Navigator.pop(
                    context,
                    listGalleryBloc.firstGallery,
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 5.0),
            child: AppButton.common(
              labelText: lang.locaized('CreateGalleryBtn'),
              labelStyle: theme.textTheme.headline5.copyWith(
                color: theme.backgroundColor,
              ),
              backgroundColor: theme.primaryColor,
              shadowColor: theme.backgroundColor,
              contentPadding: 12.0,
              radius: 10.0,
              onPressed: () async => await Future.delayed(
                const Duration(milliseconds: 200),
                () => Navigator.pushNamed(context, RouteName.createGallery),
              ).then(
                (newGallery) {
                  if (newGallery != null) {
                    listGalleryBloc.add(
                      AddNewGalleryEvent(newGallery),
                    );
                  }
                },
              ),
            ),
          ),
          Flexible(
            child: BlocBuilder<ListGalleryBloc, ListGalleryState>(
              bloc: listGalleryBloc,
              builder: (context, state) {
                if (state.status == ListGalleryStatus.initial) {
                  listGalleryBloc.add(
                    GetListGalleryEvent(authBloc.account.id),
                  );
                  return const SizedBox();
                } else if (state.status == ListGalleryStatus.failure) {
                  return Center(
                    child: SizedBox(
                      height: sizeScreen * 20,
                      child: AppButton.text(
                        label: lang.locaized('reConnect'),
                        labelStyle: theme.textTheme.headline5.copyWith(
                          color: AppColors.subLightColor,
                        ),
                        onTap: () {},
                      ),
                    ),
                  );
                }
                return ListView.builder(
                  controller: scrollListGalleryController,
                  itemCount: (state.status == ListGalleryStatus.loading)
                      ? state.list.length + 1
                      : state.list.length,
                  itemBuilder: (_, index) => index == state.list.length
                      ? AppLoading.threeBounce(size: sizeScreen * 3)
                      : Padding(
                          padding: const EdgeInsets.symmetric(
                            vertical: 5.0,
                          ),
                          child: FadeAnim(
                            child: ListTile(
                              minLeadingWidth: 5.0,
                              contentPadding: const EdgeInsets.all(0.0),
                              minVerticalPadding: 0.0,
                              onTap: () => Navigator.pop(
                                context,
                                state.list[index],
                              ),
                              leading: ClipRRect(
                                clipBehavior: Clip.antiAlias,
                                borderRadius: BorderRadius.circular(5.0),
                                child: state.list[index].image != ''
                                    ? Image.network(
                                        AppAssets.baseUrl +
                                            state.list[index].image,
                                        width: sizeImage * 15,
                                        height: sizeImage * 20,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.asset(
                                        AppAssets.img1,
                                        width: sizeImage * 15,
                                        height: sizeImage * 20,
                                        fit: BoxFit.fill,
                                      ),
                              ),
                              title: SizedBox(
                                width: sizeScreen * 30,
                                child: Text(state.list[index].name,
                                    style: theme.textTheme.headline6),
                              ),
                              trailing: state.list[index].private == 1
                                  ? const Icon(AppIcon.block)
                                  : const SizedBox(),
                            ),
                          ),
                        ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
