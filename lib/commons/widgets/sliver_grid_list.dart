import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

import '../../models/post/post.dart';
import 'item_list_post.dart';

class PostList extends StatelessWidget {
  final List<Post> listPost;
  final bool shrinkWrap;
  final ScrollPhysics physics;
  final ScrollController scrollController;
  final EdgeInsetsGeometry padding;
  const PostList(
    this.listPost, {
    this.physics,
    this.shrinkWrap,
    this.scrollController,
    this.padding,
  });

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.extentBuilder(
      padding: padding,
      controller: scrollController,
      physics: physics,
      shrinkWrap: shrinkWrap ?? true,
      itemCount: listPost.length,
      staggeredTileBuilder: (index) =>
          StaggeredTile.extent(1, index.isEven ? 270 : 230),
      maxCrossAxisExtent: 200,
      itemBuilder: (context, index) => ItemListPost(listPost[index]),
    );
  }
}
