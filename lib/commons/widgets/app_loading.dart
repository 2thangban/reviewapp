import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import '../untils/app_color.dart';

const int dafaultSizeIndicator = 20;
const int dafaultDurationLoading = 800; // milliseconds

class AppLoading {
  ///chasing dots loading
  static Widget chasingDots({
    @required final double size,
    final Color color,
    final int duration,
  }) =>
      SpinKitChasingDots(
        color: color ?? AppColors.primaryColor,
        size: size ?? dafaultSizeIndicator,
        duration: Duration(milliseconds: duration ?? dafaultDurationLoading),
      );

  /// three bounce loading
  static Widget threeBounce({
    @required final double size,
    final Color color,
    final int duration,
  }) =>
      SpinKitThreeBounce(
        color: color ?? AppColors.primaryColor,
        size: size ?? dafaultSizeIndicator,
        duration: Duration(milliseconds: duration ?? dafaultDurationLoading),
      );

  /// double bounce loading
  static Widget doubleBounce({
    @required final double size,
    final Color color,
    final int duration,
  }) =>
      SpinKitDoubleBounce(
        color: color ?? AppColors.primaryColor,
        size: size ?? dafaultSizeIndicator,
        duration: Duration(milliseconds: duration ?? dafaultDurationLoading),
      );
}
