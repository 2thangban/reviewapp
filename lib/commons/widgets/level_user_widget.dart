import 'package:flutter/material.dart';

import '../helper/size_config.dart';
import '../untils/app_color.dart';

/// common widget used showing user's level
class LevelUser extends StatelessWidget {
  final int level;
  const LevelUser(this.level);
  @override
  Widget build(BuildContext context) {
    final sizeText = SizedConfig.textMultiplier;
    final theme = Theme.of(context);
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
      decoration: BoxDecoration(
        color: AppColors.primaryColor,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Text(
        "Lvl ${this.level}",
        style: theme.textTheme.subtitle1.copyWith(
            fontSize: sizeText * 1.3, color: AppColors.lightThemeColor),
      ),
    );
  }
}
