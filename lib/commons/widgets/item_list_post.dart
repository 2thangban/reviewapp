import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/post/post.dart';
import '../../routes/route_name.dart';
import '../../screens/app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../screens/app_common_bloc/like_cubit/like_post_cubit.dart';
import '../animation/fade_anim.dart';
import '../animation/icon_button_anim.dart';
import '../helper/size_config.dart';
import '../untils/app_color.dart';
import '../untils/app_icon.dart';
import '../untils/app_image.dart';
import 'app_button.dart';
import 'circle_avt.dart';

class ItemListPost extends StatelessWidget {
  final Post post;
  const ItemListPost(this.post, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sizeScreen = SizedConfig.heightMultiplier;
    final sizeText = SizedConfig.textMultiplier;
    final AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
    final LikePostCubit likeCubit = LikePostCubit(post.isLiked, post.like);
    final theme = Theme.of(context);
    final ran = Random();
    return FadeAnim(
      child: Card(
        clipBehavior: Clip.antiAlias,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: Column(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: AppColors.listBgColor[ran.nextInt(4)],
                  image: DecorationImage(
                    image:
                        NetworkImage(AppAssets.baseUrl + post.listImage.first),
                    fit: BoxFit.cover,
                  ),
                ),
                child: InkWell(
                  onTap: () async {
                    await Future.delayed(
                      const Duration(milliseconds: 150),
                      () => Navigator.of(context)
                          .pushNamed(
                        RouteName.postDetail,
                        arguments: post.id,
                      )
                          .then(
                        (like) {
                          if (like == null) return;
                          if (like != likeCubit.state) {
                            likeCubit.update(like);
                          }
                        },
                      ),
                    );
                  },
                  splashColor: AppColors.primaryColor,
                  highlightColor: AppColors.primaryColor.withOpacity(0.1),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(0.7 * sizeScreen),
              child: Column(
                children: [
                  SizedBox(
                    width: sizeScreen * 20,
                    child: Text(
                      post.titlePost,
                      style: theme.textTheme.bodyText1
                          .copyWith(fontSize: sizeText * 1.5),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: 0.5 * SizedConfig.heightMultiplier,
                    ),
                    child: GestureDetector(
                      onTap: () => Navigator.pushNamed(
                        context,
                        RouteName.profile,
                        arguments: post.account.id,
                      ),
                      child: Row(
                        children: [
                          CircleAvt(
                            image: AppAssets.baseUrl + post.account.avatar,
                            radius: 10.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 5),
                            child: SizedBox(
                              width: 9 * sizeScreen,
                              child: Text(
                                post.account.userName,
                                style: theme.textTheme.subtitle1
                                    .copyWith(fontSize: sizeText * 1.3),
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                          ),
                          const Spacer(),

                          /// This provider handle while user click like button
                          BlocBuilder<LikePostCubit, bool>(
                            bloc: likeCubit,
                            builder: (_, status) => Padding(
                              padding: EdgeInsets.symmetric(
                                horizontal: 0.3 * SizedConfig.heightMultiplier,
                              ),
                              child: Row(
                                children: [
                                  IconButtonAnim(
                                    status: status,
                                    child: AppButton.icon(
                                      icon: status
                                          ? AppIcon.liked
                                          : AppIcon.unLike,
                                      iconSize: sizeScreen * 2,
                                      iconColor: status
                                          ? AppColors.likeColor
                                          : AppColors.subLightColor,
                                      onTap: () async {
                                        /// if user not login then require user login
                                        if (authBloc.isChecked) {
                                          likeCubit.change(
                                            status,
                                            postId: post.id,
                                          );
                                        } else {
                                          await Navigator.pushNamed(
                                            context,
                                            RouteName.login,
                                          );
                                        }
                                      },
                                    ),
                                  ),
                                  Text(
                                    likeCubit.numOfLike.toString(),
                                    style: TextStyle(
                                      fontSize: 1.6 * sizeText,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
