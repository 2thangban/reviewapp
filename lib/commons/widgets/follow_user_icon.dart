import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../routes/route_name.dart';
import '../../screens/app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../screens/app_common_bloc/follow_cubit/follow_user_cubit.dart';
import '../../screens/app_common_bloc/sign-up_bloc/sign_up_bloc.dart';
import '../animation/flip_anim.dart';
import '../untils/app_color.dart';
import '../untils/app_icon.dart';

class AppFollowIcon extends StatefulWidget {
  const AppFollowIcon({Key key}) : super(key: key);
  @override
  _AppFollowIconState createState() => _AppFollowIconState();
}

class _AppFollowIconState extends State<AppFollowIcon> {
  final FlipAnimController controller = FlipAnimController();
  FollowUserCubit followUserCubit;
  SignUpBloc signUpBloc;
  AuthBloc authBloc;
  @override
  void initState() {
    super.initState();
    followUserCubit = BlocProvider.of<FollowUserCubit>(context);
    signUpBloc = BlocProvider.of<SignUpBloc>(context);
    authBloc = BlocProvider.of<AuthBloc>(context);
  }

  @override
  void dispose() {
    super.dispose();
    followUserCubit.close();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return GestureDetector(
      onTap: () async {
        if (authBloc.isChecked) {
          controller.flip();
          followUserCubit.change(followUserCubit.state);
        } else {
          await Navigator.pushNamed(
            context,
            RouteName.login,
          ).then((isLogin) {
            if (!isLogin) return;
            Navigator.pushNamedAndRemoveUntil(
              context,
              RouteName.home,
              (route) => false,
            );
          });
        }
      },
      child: FlipAnim(
        controller: controller,
        status: followUserCubit.state,
        frontWidget: frontWidget(theme),
        backWidget: backWidget(theme),
      ),
    );
  }

  Widget frontWidget(final ThemeData theme) {
    return Container(
      decoration: BoxDecoration(
        color: theme.backgroundColor,
        borderRadius: BorderRadius.circular(20.0),
        border: Border.all(width: 1.0, color: AppColors.primaryColor),
      ),
      child: Icon(AppIcon.follow, size: 15.0, color: AppColors.primaryColor),
    );
  }

  Widget backWidget(final ThemeData theme) {
    return Container(
      decoration: BoxDecoration(
        color: theme.backgroundColor,
        borderRadius: BorderRadius.circular(20.0),
        border: Border.all(width: 1.0, color: AppColors.sussess),
      ),
      child: Icon(AppIcon.followed, size: 15.0, color: AppColors.sussess),
    );
  }
}
