import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../models/location/location.dart';
import '../../theme.dart';
import '../animation/fade_anim.dart';
import '../helper/size_config.dart';
import '../untils/app_color.dart';
import '../untils/app_image.dart';

class ItemLocation extends StatelessWidget {
  final Location location;
  ItemLocation(this.location);

  final sizeScreen = SizedConfig.heightMultiplier;
  final sizeImage = SizedConfig.imageSizeMultiplier;
  final sizeText = SizedConfig.textMultiplier;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final ran = Random();
    return FadeAnim(
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(right: 10.0),
              height: sizeImage * 17,
              width: sizeImage * 17,
              clipBehavior: Clip.antiAlias,
              decoration: BoxDecoration(
                color: AppColors.listBgColor[ran.nextInt(4)],
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: location.image.isEmpty
                  ? Image.asset(
                      AppAssets.img1,
                      fit: BoxFit.cover,
                    )
                  : Image.network(
                      AppAssets.baseUrl + location.image.first,
                      fit: BoxFit.fill,
                    ),
            ),
            SizedBox(
              width: sizeScreen * 36,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    location.name,
                    style: theme.textTheme.bodyText1,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 3.0),
                    child: Text(
                      location.address,
                      style: theme.textTheme.bodyText2,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Row(
                    children: [
                      SvgPicture.asset(
                        AppAssets.ratingIcon,
                        height: sizeScreen * 2,
                      ),
                      const SizedBox(width: 5.0),
                      Padding(
                        padding: const EdgeInsets.only(top: 2),
                        child: RichText(
                          text: TextSpan(
                            style: theme.textTheme.bodyText1,
                            text: '${location.rating} ',
                            children: [
                              TextSpan(
                                text: ' (${location.postByLocation} Riviu)',
                                style: theme.textTheme.bodyText1.copyWith(
                                  color:
                                      AppColors.darkThemeColor.withOpacity(0.8),
                                  fontWeight: AppFontWeight.regular,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      // Spacer(),
                      // Text('47.0 Km'),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
