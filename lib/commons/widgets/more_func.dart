import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../models/gallery/gellery.dart';
import '../../models/province/province.dart';
import '../../routes/route_name.dart';
import '../../screens/app_common_bloc/auth_bloc/auth_bloc.dart';
import '../../screens/app_common_bloc/province_bloc/province_bloc.dart';
import '../../screens/home_screen/user/profile_page.dart/ui/gallery_by_user/bloc/list_gallery_bloc/list_gallery_bloc.dart';
import '../../theme.dart';
import '../animation/transaction_anim.dart';
import '../untils/app_color.dart';
import '../untils/app_icon.dart';
import '../untils/app_image.dart';
import 'app_bar.dart';
import 'app_button.dart';
import 'app_loading.dart';

Future<Gallery> showListGalley(
  ThemeData theme,
  double sizeImage,
  double sizeScreen,
  BuildContext context,
  ScrollController scrollGalleryListController,
) async {
  final ListGalleryBloc listGalleryBloc =
      BlocProvider.of<ListGalleryBloc>(context);
  final AuthBloc authBloc = BlocProvider.of<AuthBloc>(context);
  return showModalBottomSheet(
    context: context,
    backgroundColor: Colors.transparent,
    barrierColor: AppColors.darkThemeColor.withOpacity(0.3),
    isScrollControlled: true,
    builder: (_) => GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: () => Navigator.pop(context, listGalleryBloc.firstGallery),
      child: GestureDetector(
        onTap: () {},
        child: DraggableScrollableSheet(
          initialChildSize: 0.6,
          maxChildSize: 0.6,
          builder: (_, _scrollController) {
            return Container(
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: theme.backgroundColor,
                borderRadius: BorderRadius.vertical(
                  top: const Radius.circular(10.0),
                ),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 10.0,
                      horizontal: 5.0,
                    ),
                    child: Row(
                      children: [
                        Expanded(
                          child: Align(
                            alignment: Alignment.center,
                            child: Text(
                              'Bộ sưu tập',
                              style: theme.textTheme.headline4,
                            ),
                          ),
                        ),
                        AppButton.icon(
                          icon: AppIcon.exitNavigate,
                          iconColor: AppColors.subLightColor,
                          onTap: () => Navigator.pop(
                            context,
                            listGalleryBloc.firstGallery,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const Divider(
                    color: AppColors.subLightColor,
                    thickness: .7,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5.0),
                    child: AppButton.common(
                      labelText: '+Tạo bộ sưu tập',
                      labelStyle: theme.textTheme.headline5.copyWith(
                        color: theme.backgroundColor,
                      ),
                      backgroundColor: theme.primaryColor,
                      shadowColor: theme.backgroundColor,
                      contentPadding: 12.0,
                      onPressed: () async => await Navigator.pushNamed(
                              context, RouteName.createGallery)
                          .then((newGallery) {
                        if (newGallery != null) {
                          listGalleryBloc.add(
                            AddNewGalleryEvent(newGallery),
                          );
                        }
                      }),
                    ),
                  ),
                  Flexible(
                    child: BlocBuilder<ListGalleryBloc, ListGalleryState>(
                      bloc: listGalleryBloc,
                      builder: (context, state) {
                        if (state.status == ListGalleryStatus.initial) {
                          listGalleryBloc.add(
                            GetListGalleryEvent(authBloc.account.id),
                          );
                          return const SizedBox();
                        } else if (state.status == ListGalleryStatus.failure) {
                          return Center(
                            child: SizedBox(
                              height: sizeScreen * 20,
                              child: AppButton.text(
                                label: 'Thử lại',
                                labelStyle: theme.textTheme.headline5.copyWith(
                                  color: AppColors.subLightColor,
                                ),
                                onTap: () {},
                              ),
                            ),
                          );
                        }
                        return ListView.builder(
                          controller: scrollGalleryListController,
                          itemCount: (state.status == ListGalleryStatus.loading)
                              ? state.list.length + 1
                              : state.list.length,
                          itemBuilder: (_, index) => index == state.list.length
                              ? AppLoading.threeBounce(size: sizeScreen * 3)
                              : Padding(
                                  padding: const EdgeInsets.symmetric(
                                    vertical: 5.0,
                                  ),
                                  child: ListTile(
                                    minLeadingWidth: 5.0,
                                    contentPadding: const EdgeInsets.all(0.0),
                                    minVerticalPadding: 0.0,
                                    onTap: () => Navigator.pop(
                                      context,
                                      state.list[index],
                                    ),
                                    leading: ClipRRect(
                                      clipBehavior: Clip.antiAlias,
                                      borderRadius: BorderRadius.circular(5.0),
                                      child: state.list[index].image != ''
                                          ? Image.network(
                                              AppAssets.baseUrl +
                                                  state.list[index].image,
                                              width: sizeImage * 15,
                                              height: sizeImage * 20,
                                              fit: BoxFit.fill,
                                            )
                                          : Image.asset(
                                              AppAssets.img1,
                                              width: sizeImage * 15,
                                              height: sizeImage * 20,
                                              fit: BoxFit.fill,
                                            ),
                                    ),
                                    title: SizedBox(
                                      width: sizeScreen * 30,
                                      child: Text(state.list[index].name,
                                          style: theme.textTheme.headline6),
                                    ),
                                    trailing: state.list[index].private == 1
                                        ? const Icon(AppIcon.block)
                                        : const SizedBox(),
                                  ),
                                ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    ),
  );
}

Future<Province> showChangeAddress(
  BuildContext context,
  ThemeData theme,
  List<Province> listprovince,
) async {
  ProvinceBloc provinceBloc = BlocProvider.of(context);
  return showGeneralDialog(
    context: context,
    barrierDismissible: true,
    barrierLabel: '',
    transitionDuration: Duration(milliseconds: 350),
    transitionBuilder: (_, _animation, _secondaryAnimation, child) =>
        TransactionAnima.fromRight(_animation, _secondaryAnimation, child),
    pageBuilder: (_, _animation, _secondaryAnimation) => Dialog(
      elevation: 0.0,
      insetAnimationDuration: Duration(milliseconds: 200),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15.0),
      ),
      insetPadding: const EdgeInsets.all(0.0),
      child: Scaffold(
        appBar: CommonAppBar(
          leftBarButtonItem: AppButton.icon(
            icon: AppIcon.popNavigate,
            onTap: () => Navigator.pop(context, null),
          ),
          titleWidget: Text(
            'Chọn khu vực',
            style: theme.textTheme.headline4,
          ),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          padding: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10.0,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Text(
                  "Các địa điểm chọn gần đây",
                  style: theme.textTheme.headline5,
                ),
              ),
              GridView.count(
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                crossAxisCount: 3,
                childAspectRatio: 5 / 2,
                padding: const EdgeInsets.only(top: 10.0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: provinceBloc.boxProvince.values
                    .map(
                      (e) => MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(
                            color: AppColors.subLightColor,
                          ),
                        ),
                        elevation: 0.0,
                        color: theme.backgroundColor,
                        onPressed: () => Navigator.pop(context, e),
                        child: Text(
                          e.name,
                          style: theme.textTheme.bodyText1.copyWith(
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 25.0),
                child: Text(
                  "Các tỉnh thành",
                  style: theme.textTheme.headline5,
                ),
              ),
              GridView.count(
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
                crossAxisCount: 3,
                childAspectRatio: 5 / 2,
                padding: const EdgeInsets.only(top: 10.0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                children: listprovince
                    .map(
                      (e) => MaterialButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(
                            color: AppColors.subLightColor,
                          ),
                        ),
                        elevation: 0.0,
                        color: theme.backgroundColor,
                        onPressed: () => Navigator.pop(context, e),
                        child: Text(
                          e.name,
                          style: theme.textTheme.bodyText1.copyWith(
                            fontWeight: AppFontWeight.regular,
                          ),
                        ),
                      ),
                    )
                    .toList(),
              ),
            ],
          ),
        ),
      ),
    ),
  );
}
