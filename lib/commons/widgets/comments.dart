import 'package:flutter/material.dart';

class CommentAppWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return Container(
      height: MediaQuery.of(context).size.height,
      padding: const EdgeInsets.only(top: 40.0),
      decoration: BoxDecoration(
        color: theme.backgroundColor,
        borderRadius: BorderRadius.only(
          topLeft: const Radius.circular(15.0),
          topRight: const Radius.circular(15.0),
        ),
      ),
      child: Scaffold(
        body: Column(
          children: [
            Text('Bình luận', style: theme.textTheme.headline6),
            const Divider(thickness: 1),
            Flexible(
              child: ListView.builder(
                padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
                itemCount: 50,
                itemBuilder: (_, index) => Text('Hello'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
