import 'dart:io';

import 'package:flutter/material.dart';

import '../untils/app_color.dart';
import '../untils/app_image.dart';

// ignore: must_be_immutable
class CircleAvt extends StatelessWidget {
  final String image;
  final bool isIcon;
  final IconData icon;
  final Color iconBackground;
  final Color iconColor;
  final double iconSize;
  final double radius;
  final double padding;
  final Function onIconTap;
  final Function onTap;
  bool isFile;
  CircleAvt({
    @required this.image,
    this.icon,
    this.iconBackground,
    this.iconSize,
    this.iconColor,
    this.isIcon = false,
    this.radius,
    this.padding,
    this.onTap,
    this.onIconTap,
    this.isFile = false,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Stack(
      alignment: Alignment.bottomRight,
      children: [
        GestureDetector(
          child: Container(
            margin: const EdgeInsets.only(right: 5.0),
            child: CircleAvatar(
              radius: radius ?? 15.0,
              backgroundImage: AssetImage(AppAssets.img1),
              foregroundImage: image != '' && image != null
                  ? isFile
                      ? FileImage(File(image))
                      : NetworkImage(image)
                  : null,
            ),
          ),
          onTap: onTap,
        ),
        isIcon == true
            ? InkWell(
                onTap: onIconTap,
                child: Container(
                  decoration: BoxDecoration(
                    color: iconBackground ?? AppColors.primaryColor,
                    borderRadius: BorderRadius.circular(20.0),
                    border: Border.all(
                      width: 1.0,
                      color: theme.backgroundColor,
                    ),
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(padding ?? 0.0),
                    child: Icon(
                      icon,
                      size: iconSize ?? 12.0,
                      color: iconColor ?? Theme.of(context).backgroundColor,
                    ),
                  ),
                ),
              )
            : const SizedBox(height: 1),
      ],
    );
  }
}
