import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import '../untils/app_color.dart';
import '../untils/app_string.dart';

class AppButton {
  static Widget common({
    @required String labelText,
    @required VoidCallback onPressed,
    double width,
    double contentPadding,
    TextStyle labelStyle,
    double radius,
    Color backgroundColor,
    Color shadowColor,
    bool isShadow = true,
    Widget loadingWidget,
  }) =>
      InkWell(
        child: Container(
          width: width,
          decoration: BoxDecoration(
            color: backgroundColor ?? AppColors.darkThemeColor,
            borderRadius: BorderRadius.all(
              Radius.circular(radius ?? 25.0),
            ),
            boxShadow: [
              isShadow
                  ? BoxShadow(
                      color: shadowColor?.withOpacity(0.2) ??
                          AppColors.darkThemeColor.withOpacity(0.2),
                      spreadRadius: 3,
                      blurRadius: 4,
                      offset: Offset(0, 3),
                    )
                  : BoxShadow()
            ],
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(
              vertical: contentPadding ?? 2.0,
              horizontal: 10.0,
            ),
            child: Center(
              child: loadingWidget ??
                  Text(
                    labelText ?? AppString.errorLabelButton,
                    style: labelStyle,
                  ),
            ),
          ),
        ),
        onTap: onPressed,
      );

  static Widget outline({
    @required String labelText,
    double contentPadding,
    double width,
    double radius,
    double borderSize,
    Color outLineColor,
    Color backgroundColor,
    TextStyle labelStyle,
    Function onPressed,
    Widget icon,
  }) =>
      InkWell(
        child: Container(
          width: width,
          decoration: BoxDecoration(
            border: Border.all(
              color: outLineColor ?? AppColors.primaryColor,
              width: borderSize ?? 1,
            ),
            color: backgroundColor ?? AppColors.primaryColor,
            borderRadius: BorderRadius.all(
              Radius.circular(radius ?? 25.0),
            ),
          ),
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: contentPadding ?? 6),
            child: Row(
              children: [
                icon != null
                    ? Padding(
                        padding: const EdgeInsets.only(left: 40, right: 10),
                        child: icon,
                      )
                    : const SizedBox(),
                Expanded(
                  child: Expanded(
                    child: Align(
                      alignment: icon != null
                          ? Alignment.centerLeft
                          : Alignment.center,
                      child: Text(labelText ?? AppString.errorLabelButton,
                          style: labelStyle),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        onTap: onPressed,
      );

  static Widget icon({
    @required IconData icon,
    @required Function onTap,
    EdgeInsetsGeometry margin,
    EdgeInsetsGeometry padding,
    Color backgroundColor,
    Color iconColor,
    double iconSize,
  }) {
    return Container(
      margin: margin ?? EdgeInsets.zero,
      padding: padding ?? EdgeInsets.zero,
      color: backgroundColor,
      child: GestureDetector(
        child: Icon(
          icon ?? Icons.cancel_rounded,
          size: iconSize ?? 28.0,
          color: iconColor ?? AppColors.darkThemeColor,
        ),
        onTap: onTap,
      ),
    );
  }

  static Widget text({
    @required String label,
    @required Function onTap,
    TextStyle labelStyle,
    double contentPadding,
  }) {
    return GestureDetector(
      child: Padding(
        padding: EdgeInsets.all(contentPadding ?? 10.0),
        child: Text(
          label ?? AppString.errorLabelButton,
          style: labelStyle ??
              TextStyle(
                color: AppColors.darkThemeColor,
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
        ),
      ),
      onTap: onTap,
    );
  }

  static Widget custom({
    @required IconData icon,
    @required String label,
    @required bool isLeftIcon,
    @required Function onPress,
    TextStyle textStyle,
    Color backgroundColor,
    Color iconColor,
    double radius = 20.0,
    double iconSize,
    double width,
    EdgeInsetsGeometry iconPadding = const EdgeInsets.all(5.0),
  }) {
    isLeftIcon ??= false;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10.0),
      width: width ?? double.infinity,
      height: radius * 2,
      decoration: BoxDecoration(
        color: backgroundColor ?? AppColors.lightThemeColor,
        borderRadius: BorderRadius.circular(radius),
      ),
      child: InkWell(
        onTap: onPress,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isLeftIcon == true
                ? Icon(
                    icon,
                    color: iconColor,
                    size: iconSize,
                  )
                : const SizedBox(),
            Text(
              label ?? AppString.errorLabelButton,
              style: textStyle,
            ),
            isLeftIcon == false
                ? Padding(
                    padding: iconPadding,
                    child: Icon(
                      icon,
                      color: iconColor ?? AppColors.backgroundCommonColor,
                      size: iconSize,
                    ),
                  )
                : const SizedBox(),
          ],
        ),
      ),
    );
  }
}
