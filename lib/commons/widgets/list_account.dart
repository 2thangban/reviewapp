import 'package:flutter/material.dart';

import '../../localizations/app_localization.dart';
import '../../models/user/user.dart';
import '../../routes/route_name.dart';
import '../../theme.dart';
import '../untils/app_color.dart';
import 'item_account.dart';

class ListAccountWidget extends StatefulWidget {
  final List<Account> _listAccount;
  final String searchKey;
  final bool shrinkWrap;
  final ScrollPhysics physics;
  ListAccountWidget(
    this._listAccount, {
    this.searchKey,
    this.shrinkWrap,
    this.physics,
  });

  @override
  _ListAccountWidgetState createState() => _ListAccountWidgetState();
}

class _ListAccountWidgetState extends State<ListAccountWidget> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        ///
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return widget._listAccount.isEmpty
        ? Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              AppLocalization.of(context).locaized('searchUserEmpty'),
              style: theme.textTheme.headline6.copyWith(
                fontWeight: AppFontWeight.regular,
                fontStyle: FontStyle.italic,
                color: AppColors.subLightColor,
              ),
            ),
          )
        : ListView.builder(
            controller: _scrollController,
            shrinkWrap: widget.shrinkWrap ?? true,
            physics: widget.physics,
            itemCount: widget._listAccount.length,
            itemBuilder: (_, index) {
              final Account account = widget._listAccount[index];
              return GestureDetector(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteName.profile,
                  arguments: account.id,
                ),
                child: ItemAccount(account),
              );
            },
          );
  }
}
