import 'package:flutter/material.dart';

import '../../localizations/app_localization.dart';
import '../../models/location/location.dart';
import '../../routes/route_name.dart';
import '../../theme.dart';
import '../untils/app_color.dart';
import 'item_location.dart';

class ListLocationWidget extends StatelessWidget {
  final List<Location> _listLocation;
  final String searchKey;
  final double shrinkWrap;
  final ScrollPhysics physics;
  final EdgeInsetsGeometry padding;
  ListLocationWidget(
    this._listLocation, {
    this.physics,
    this.shrinkWrap,
    this.padding,
    this.searchKey,
  });
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return _listLocation.isEmpty
        ? Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              AppLocalization.of(context).locaized('searchLocationEmpty'),
              style: theme.textTheme.headline6.copyWith(
                fontWeight: AppFontWeight.regular,
                fontStyle: FontStyle.italic,
                color: AppColors.subLightColor,
              ),
            ),
          )
        : ListView.separated(
            padding: padding,
            physics: physics,
            shrinkWrap: shrinkWrap ?? true,
            itemCount: _listLocation.length,
            itemBuilder: (context, index) => GestureDetector(
              onTap: () => Navigator.pushNamed(
                context,
                RouteName.detailLocation,
                arguments: _listLocation[index].id,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10.0),
                child: ItemLocation(_listLocation[index]),
              ),
            ),
            separatorBuilder: (_, index) => Padding(
              padding: const EdgeInsets.symmetric(vertical: 5.0),
              child: const Divider(
                height: 5.0,
                thickness: 1.0,
              ),
            ),
          );
  }
}
