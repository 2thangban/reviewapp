import 'package:flutter/material.dart';

import '../../localizations/app_localization.dart';
import '../../models/user/user.dart';
import '../animation/fade_anim.dart';
import '../helper/size_config.dart';
import '../untils/app_image.dart';
import 'circle_avt.dart';

class ItemAccount extends StatelessWidget {
  final Account account;
  ItemAccount(this.account);
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final sizeScreen = SizedConfig.heightMultiplier;
    return FadeAnim(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ListTile(
              dense: true,
              horizontalTitleGap: 10,
              contentPadding: EdgeInsets.only(left: 10.0),
              leading: CircleAvt(
                radius: 18.0,
                image: account.avatar == ''
                    ? null
                    : AppAssets.baseUrl + account.avatar,
                isIcon: false,
              ),
              title: Row(
                children: [
                  Container(
                    constraints: BoxConstraints(
                      maxWidth: sizeScreen * 20,
                    ),
                    child: InkWell(
                      child: Text(
                        account.userName,
                        style: theme.textTheme.bodyText1,
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ),
                  const SizedBox(width: 5.0),
                ],
              ),
              subtitle: Text(
                "${account.numOfFollower} ${AppLocalization.of(context).locaized('follower')}",
                style: TextStyle(
                  color: Colors.grey,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
