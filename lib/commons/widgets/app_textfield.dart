import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../helper/size_config.dart';
import '../untils/app_color.dart';
import '../untils/app_string.dart';

class AppTextField {
  static Widget create({
    TextEditingController controller,
    String hindText,
    String initText,
    bool isTop = false,
    bool isContent = false,
    TextStyle hintStyle,
    Function onChange,
  }) =>
      Container(
        clipBehavior: Clip.antiAlias,
        decoration: BoxDecoration(
          border: Border.all(
            color: AppColors.subLightColor.withOpacity(0.7),
            width: 0.5,
          ),
          borderRadius: isContent
              ? BorderRadius.zero
              : isTop == false
                  ? BorderRadius.only(
                      bottomLeft: Radius.circular(20.0),
                      bottomRight: Radius.circular(20.0),
                    )
                  : BorderRadius.only(
                      topLeft: Radius.circular(20.0),
                      topRight: Radius.circular(20.0),
                    ),
        ),
        child: TextFormField(
          initialValue: initText,
          controller: controller,
          minLines: isContent ? 17 : 1,
          maxLines: isContent
              ? 17
              : isTop
                  ? 3
                  : 5,
          cursorColor: AppColors.primaryColor,
          cursorHeight: 25.0,
          decoration: InputDecoration(
            fillColor: AppColors.subLightColor.withOpacity(0.05),
            filled: true,
            hintText: hindText ?? AppString.errorLabelField,
            hintStyle: hintStyle,
            border: OutlineInputBorder(
              borderSide: BorderSide.none,
            ),
          ),
          onChanged: onChange,
        ),
      );

  static Widget custom({
    Function onChange,
    bool isSearch = false,
    Widget suffixWidget,
    String hindText,
    String initText,
  }) =>
      TextFormField(
        initialValue: initText,
        cursorColor: AppColors.primaryColor,
        cursorHeight: 25.0,
        decoration: InputDecoration(
          hintText: hindText,
          fillColor: isSearch
              ? AppColors.backgroundSearchColor
              : AppColors.backgroundEditColor,
          filled: true,
          prefixIcon: isSearch
              ? Icon(
                  Icons.search,
                  color: AppColors.backgroundCommonColor,
                )
              : null,
          suffixIcon: suffixWidget,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: BorderSide.none,
          ),
        ),
        onChanged: (String input) => onChange,
      );

  ///common textfield
  static Widget common({
    @required String hintText,
    TextEditingController controller,
    FocusNode focusNode,
    String errorText,
    String suffixText,
    bool isPassWord = false,
    bool readOnly = false,
    TextStyle hintStyle,
    TextAlign textAlign,
    double textSize,
    double height,
    double width,
    double radius,
    int maxLines,
    Color cursorColor,
    Color backgroundColor,
    Color textColor,
    List<TextInputFormatter> textInputFormatter,
    Function onTap,
    Function onSaved,
    Function onChange,
    BorderSide boderSide,
    TextInputType type,
  }) =>
      Container(
        height: height,
        decoration: BoxDecoration(
          color: backgroundColor ?? AppColors.lightThemeColor,
          borderRadius: BorderRadius.circular(radius ?? 30.0),
        ),
        padding: const EdgeInsets.symmetric(vertical: 2.0),
        child: TextFormField(
          focusNode: focusNode,
          keyboardType: type,
          textAlign: textAlign ?? TextAlign.start,
          style: TextStyle(
            color: textColor ?? AppColors.darkThemeColor,
            fontSize: textSize ?? null,
          ),
          maxLines: maxLines ?? 1,
          minLines: 1,
          controller: controller,
          obscureText: isPassWord,
          readOnly: readOnly,
          inputFormatters: textInputFormatter,
          cursorColor: cursorColor ?? AppColors.primaryColor,
          decoration: InputDecoration(
            suffixText: suffixText,
            hintText: hintText ?? AppString.errorLabelField,
            hintStyle: hintStyle,
            errorText: errorText,
            errorStyle: TextStyle(
              fontSize: SizedConfig.heightMultiplier * 1.7,
              color: AppColors.lightThemeColor,
            ),
            errorBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(30.0),
              borderSide: BorderSide(
                color: AppColors.errorColor,
              ),
            ),
            border: OutlineInputBorder(
              borderSide: boderSide ?? BorderSide.none,
            ),
            contentPadding: const EdgeInsets.symmetric(
              horizontal: 16.0,
              vertical: 0.0,
            ),
          ),
          onTap: onTap,
          onSaved: onSaved,
          onChanged: onChange,
        ),
      );
}
