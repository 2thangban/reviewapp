import 'package:flutter/material.dart';

import '../../localizations/app_localization.dart';
import '../../models/hashtag/hashtag.dart';
import '../../routes/route_name.dart';
import '../../screens/search_screen/bloc/search_hashtag_bloc/search_hashtag_bloc.dart';
import '../../theme.dart';
import '../untils/app_color.dart';
import 'item_hashtag.dart';

class ListHashtagWidget extends StatefulWidget {
  final List<Hashtag> _listHashtag;
  final String keySearch;
  final bool shrinkWrap;
  final ScrollPhysics physic;
  ListHashtagWidget(
    this._listHashtag, {
    this.keySearch,
    this.physic,
    this.shrinkWrap,
  });

  @override
  _ListHashtagWidgetState createState() => _ListHashtagWidgetState();
}

class _ListHashtagWidgetState extends State<ListHashtagWidget> {
  final ScrollController _scrollController = ScrollController();
  final SearchHashtagBloc _searchHashtagBloc = SearchHashtagBloc();
  int page = 1;
  final int limit = 10;
  bool isLastPage;

  @override
  void initState() {
    super.initState();

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
              _scrollController.position.maxScrollExtent &&
          isLastPage == false) {
        _searchHashtagBloc.add(
          SearchingHashtagEvent(
            key: widget.keySearch,
            page: page++,
            limit: limit,
          ),
        );
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    isLastPage = (widget._listHashtag.length < limit);

    return widget._listHashtag.isEmpty
        ? Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.symmetric(vertical: 20.0),
            child: Text(
              AppLocalization.of(context).locaized('searchHashtagEmpty'),
              style: theme.textTheme.headline6.copyWith(
                fontWeight: AppFontWeight.regular,
                fontStyle: FontStyle.italic,
                color: AppColors.subLightColor,
              ),
            ),
          )
        : ListView.builder(
            controller: _scrollController,
            itemCount: widget._listHashtag.length,
            shrinkWrap: widget.shrinkWrap ?? true,
            physics: widget.physic,
            itemBuilder: (_, index) {
              final Hashtag hashtag = widget._listHashtag[index];
              return GestureDetector(
                onTap: () => Navigator.pushNamed(
                  context,
                  RouteName.detailHashtag,
                  arguments: widget._listHashtag[index].id,
                ),
                child: ItemHashtag(hashtag, theme),
              );
            },
          );
  }
}
