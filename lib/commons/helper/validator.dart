class Validator {
  //check String is Empty
  static bool isEmptyString(String str) => (str == null || str.trim() == "");

  //check Password lenght
  static bool isValidPass(String str) => (str.length != 8);

  //check format Email
  static bool isValidEmail(String email) {
    final _emailRegExp = RegExp(
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
    return _emailRegExp.hasMatch(email);
  }

  //check confirm Password
  static bool isConfirmString(String str, String confirmStr) =>
      (str.compareTo(confirmStr)) == 0 ? true : false;

  ///Check format phone number (VN)
  static bool isValidPhone(String phone) =>
      RegExp(r"(84|0[3|5|7|8|9])+([0-9]{8})\b").hasMatch(phone);
}
