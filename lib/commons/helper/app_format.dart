import 'package:intl/intl.dart';

class AppFormat {
  /// Format number.
  /// funtion NumberFormat('pattern')
  /// Example: 100000 => 100,000đ
  static String number(num number) => NumberFormat('###,###').format(number);

  /// custom time
  static String timeToString(String inputTime) {
    DateFormat format = DateFormat('HH:mm:ss');
    DateTime dateTime = format.parse(inputTime);
    if (dateTime.minute != 0) {
      if (dateTime.hour != 0) {
        return "${dateTime.hour}h ${dateTime.minute}'";
      } else
        return "${dateTime.minute}'";
    } else
      return '${dateTime.hour}h ';
  }

  /// convert string to time
  static DateTime time(String inputTime) =>
      DateFormat('HH:mm:ss').parse(inputTime);

  /// Convert string to datetime
  static DateTime stringToDate(String inputDateTime, {bool isTime = true}) {
    DateFormat format =
        isTime ? DateFormat('yyyy-MM-dd HH:mm:ss') : DateFormat('yyyy-MM-dd');
    return format.parse(inputDateTime);
  }

  /// custom duration
  static String durationToDate(DateTime inputDateTime) {
    final currentDate = DateTime.now();
    final duration = currentDate.difference(inputDateTime);
    final int days = duration.inDays;
    if (days != 0) {
      if (days < 30)
        return '$days ngày ';
      else
        return AppFormat.dateToString(inputDateTime);
    } else {
      final int hours = duration.inHours % 24;
      final int minutes = (duration.inMinutes % 60);
      if (hours != 0) {
        return "$hours giờ";
      } else if (minutes != 0) {
        return '$minutes phút';
      } else
        return 'Mới đây';
    }
  }

  /// Convert datetime to string
  static String dateToString(DateTime inputDateTime, {bool showTime = true}) {
    DateFormat dateFormat = showTime
        ? DateFormat("dd/MM/yyyy 'lúc' HH'h'")
        : DateFormat("dd/MM/yyyy");
    return dateFormat.format(inputDateTime);
  }
}
