import 'dart:developer';

import 'package:google_sign_in/google_sign_in.dart';

import '../../../models/user/user.dart';

class AuthGoogle {
  final _googleSignIn = GoogleSignIn();
  GoogleSignInAccount _user;

  Future<Account> login() async {
    Account account;
    try {
      final googleUser = await _googleSignIn.signIn();
      if (googleUser == null) return null;
      _user = googleUser;
      account = new Account(
        ggId: _user.id,
        userName: _user.displayName,
        email: _user.email,
        avatar: _user.photoUrl,
      );
      return account;
    } catch (e) {
      return null;
    }
  }

  Future<bool> logout() async {
    try {
      await _googleSignIn.disconnect();
      return true;
    } catch (e) {
      log(e.toString());
      return false;
    }
  }
}
