import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';

import '../../../models/user/user.dart';

class AuthFacebook {
  FirebaseAuth _auth = FirebaseAuth.instance;
  FacebookLogin _facebookLogin = FacebookLogin();

  Future<Account> login() async {
    Account account;
    FacebookLoginResult _result = await _facebookLogin.logIn(['email']);
    switch (_result.status) {
      case FacebookLoginStatus.cancelledByUser:
        Future.error('Cancelled by user');
        break;
      case FacebookLoginStatus.error:
        Future.error('Error');
        break;
      case FacebookLoginStatus.loggedIn:
        FacebookAccessToken _accessToken = _result.accessToken;
        AuthCredential _credential =
            FacebookAuthProvider.credential(_accessToken.token);
        var instance = await _auth.signInWithCredential(_credential);
        account = Account(
          userName: instance.user.displayName,
          avatar: instance.user.photoURL,
          phone: instance.user.phoneNumber ?? null,
          fbId: instance.user.uid,
        );
        return account;
        break;
      default:
    }
    return null;
  }

  Future<bool> logout() async {
    try {
      await _facebookLogin.logOut();
      return true;
    } catch (e) {
      print(e.toString());
      return false;
    }
  }
}
