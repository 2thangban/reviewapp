import 'package:flutter/material.dart';

class SizedConfig {
  static double _screenWidth;
  static double _screenHeight;
  static double _blockSizeHorizontal;
  static double _blocSizeVertical;

  static double textMultiplier;
  static double imageSizeMultiplier;
  static double heightMultiplier;

  void init(BoxConstraints constraints, Orientation orientation) {
    if (orientation == Orientation.portrait) {
      _screenHeight = constraints.maxHeight;
      _screenWidth = constraints.maxWidth;
    } else {
      _screenHeight = constraints.maxWidth;
      _screenWidth = constraints.maxHeight;
    }
    // _screenHeight = constraints.maxHeight;
    // _screenWidth = constraints.maxWidth;

    _blocSizeVertical = _screenHeight / 100;
    _blockSizeHorizontal = _screenWidth / 100;

    textMultiplier = _blocSizeVertical;
    imageSizeMultiplier = _blockSizeHorizontal;
    heightMultiplier = _blocSizeVertical;
  }
}
