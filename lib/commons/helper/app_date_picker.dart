import 'package:flutter/material.dart';

import '../untils/app_string.dart';

class AppDatePicker {
  static Future<String> getDate(
    BuildContext context,
  ) async {
    final date = await showDatePicker(
      helpText: "Select birth day",
      errorFormatText: '${AppString.birthDay} ${AppString.isEmpty}',
      errorInvalidText: '${AppString.birthDay}  ${AppString.isEmpty}',
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime.now(),
    );
    if (date != null) {
      final birthDay = date.toString().split(' ');
      return birthDay[0];
    }
    return '';
  }

  static Future<String> getTime(
    BuildContext context,
  ) async =>
      await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
      ).then((time) => time != null
          ? '${time.hour.toString().padLeft(2, '0')}' +
              ':' +
              '${time.minute.toString().padLeft(2, '0')}:00'
          : null);
}
