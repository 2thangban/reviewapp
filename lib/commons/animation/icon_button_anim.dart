import 'package:flutter/material.dart';

class IconButtonAnim extends StatefulWidget {
  final bool status;
  final Widget child;
  final VoidCallback onTap;
  final Duration duration;
  IconButtonAnim({
    Key key,
    @required this.status,
    @required this.child,
    this.duration = const Duration(milliseconds: 120),
    this.onTap,
  }) : super(key: key);
  @override
  _IconButtonAnimState createState() => _IconButtonAnimState();
}

class _IconButtonAnimState extends State<IconButtonAnim>
    with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> scale;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    scale = Tween<double>(begin: 1.0, end: 1.3).animate(animationController);
  }

  @override
  void didUpdateWidget(covariant IconButtonAnim oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.status != oldWidget.status) {
      doAnimation();
    }
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  Future doAnimation() async {
    if (animationController.isAnimating) return;
    if (widget.status != null) {
      await animationController.forward();
      await animationController.reverse();
      if (widget.onTap != null) {
        widget.onTap();
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: scale,
      child: widget.child,
    );
  }
}
