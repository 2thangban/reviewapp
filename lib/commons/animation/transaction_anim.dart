import 'package:flutter/material.dart';

class TransactionAnima {
  /// slide animation from left to right
  static fromRight(
    Animation<double> _animation,
    Animation<double> _secondaryAnimation,
    Widget _widget,
  ) =>
      SlideTransition(
        child: _widget,
        position: Tween<Offset>(
          end: Offset.zero,
          begin: Offset(1.0, 0.0),
        ).animate(_animation),
      );

  /// slide animation with grow effect
  static grow(
    Animation<double> _animation,
    Animation<double> _secondaryAnimation,
    Widget _widget,
  ) =>
      ScaleTransition(
        child: _widget,
        scale: Tween<double>(
          begin: 0.0,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: _animation,
            curve: Interval(0.00, 0.50, curve: Curves.linear),
          ),
        ),
      );

  /// slide animation with grow effect
  static shink(
    Animation<double> _animation,
    Animation<double> _secondaryAnimation,
    Widget _widget,
  ) =>
      ScaleTransition(
        child: _widget,
        scale: Tween<double>(
          begin: 1.2,
          end: 1.0,
        ).animate(
          CurvedAnimation(
            parent: _animation,
            curve: Interval(0.50, 1.00, curve: Curves.linear),
          ),
        ),
      );
}
