import 'dart:math';

import 'package:flutter/material.dart';

class FlipAnimController {
  _FlipAnimState _flipController;

  Future flip() => _flipController.flip();
}

class FlipAnim extends StatefulWidget {
  final Widget frontWidget;
  final Widget backWidget;
  final bool status;
  final Duration duration;
  final FlipAnimController controller;

  const FlipAnim({
    Key key,
    @required this.controller,
    @required this.frontWidget,
    @required this.backWidget,
    this.status,
    this.duration = const Duration(milliseconds: 300),
  })  : assert(frontWidget != null && backWidget != null && controller != null),
        super(key: key);
  @override
  _FlipAnimState createState() => _FlipAnimState();
}

class _FlipAnimState extends State<FlipAnim> with TickerProviderStateMixin {
  AnimationController animationController;
  bool isFlip;
  double anglesPlus = 0;

  Future flip() async {
    if (animationController.isAnimating) return;
    if (isFlip) {
      animationController.forward();
    } else {
      animationController.reverse();
    }
    isFlip = !isFlip;
  }

  bool isFrontWidget(double angle) {
    final degress90 = pi / 2;
    final degress270 = 3 * pi / 2;

    return angle <= degress90 || angle >= degress270;
  }

  @override
  void initState() {
    super.initState();
    isFlip = widget.status;
    widget.controller._flipController = this;
    animationController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animationController,
      builder: (_, child) {
        double angle = animationController.value * -pi;
        if (isFlip) angle += anglesPlus;
        final transform = Matrix4.identity()
          ..setEntry(3, 2, 0.001)
          ..rotateX(angle);
        return Transform(
          transform: transform,
          alignment: Alignment.center,
          child: isFrontWidget(angle.abs())
              ? widget.frontWidget
              : Transform(
                  transform: Matrix4.identity()..rotateX(pi),
                  alignment: Alignment.center,
                  child: widget.backWidget,
                ),
        );
      },
    );
  }
}
