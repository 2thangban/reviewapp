import 'package:flutter/material.dart';

class FadeAnim extends StatefulWidget {
  final Widget child;
  final Duration duration;

  const FadeAnim({
    Key key,
    @required this.child,
    this.duration = const Duration(milliseconds: 400),
  })  : assert(child != null),
        super(key: key);
  @override
  _FadeAnimState createState() => _FadeAnimState();
}

class _FadeAnimState extends State<FadeAnim> with TickerProviderStateMixin {
  AnimationController animationController;
  Animation<double> fadeAmin;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: widget.duration,
    );
    fadeAmin = Tween<double>(begin: 0.0, end: 1.0).animate(animationController);
  }

  @override
  void dispose() {
    super.dispose();
    animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return FadeTransition(opacity: fadeAmin, child: widget.child);
  }
}
