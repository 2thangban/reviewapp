import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../theme.dart';
import '../helper/debouncer.dart';
import '../helper/size_config.dart';
import 'app_color.dart';

const int dafaultFadeDuration = 200; // milliseconds
const int dafaultShowDuration = 3000; // milliseconds

class AppToast {
  static show(
    String message,
    BuildContext context, {
    ToastGravity gravity,
    double margin,
    int duration,
  }) {
    final FToast appToast = FToast();
    appToast.init(context);
    Widget customMsg() => Container(
          alignment: Alignment.center,
          width: SizedConfig.heightMultiplier * 35,
          margin: const EdgeInsets.only(bottom: 50.0),
          padding: const EdgeInsets.symmetric(
            vertical: 10.0,
            horizontal: 10.0,
          ),
          decoration: BoxDecoration(
            color: AppColors.lightThemeColor,
            borderRadius: BorderRadius.circular(15.0),
            border: Border.all(
              color: AppColors.subLightColor,
              width: 0.5,
            ),
          ),
          child: Text(
            message,
            style: Theme.of(context).textTheme.bodyText1.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
          ),
        );
    appToast.showToast(
      child: customMsg(),
      gravity: gravity ?? ToastGravity.BOTTOM,
      toastDuration: const Duration(milliseconds: dafaultShowDuration),
      fadeDuration: defaultDuration,
    );
  }
}
