import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class AppAssets {
  static const String baseUrl = "http://10.0.2.2/review-app/public/";
  //static const String baseUrl = "http://reviewapp.hopto.org/";

  static const String locationSearch = 'assets/images/search_default.png';

  ///icon app
  static const String appIcon = 'assets/images/logo_Review.png';
  static const String facebookIcon = 'assets/icons/facebook.svg';
  static const String messengerIcon = 'assets/icons/messenger_icon.svg';
  static const String googleIcon = 'assets/icons/google_icon.svg';
  static const String copyLinkIcon = 'assets/icons/link.svg';

  static const String addImageIcon = 'assets/icons/add_image.svg';
  static const String addVideoIcon = 'assets/icons/add_video.svg';

  static const String pinIcon = 'assets/icons/pin_grey.svg';
  static const String notePadIcon = 'assets/icons/notepad.svg';
  static const String ratingIcon = 'assets/icons/star_on.svg';
  static const String shareIcon = 'assets/icons/share.svg';
  static const String viewIcon = 'assets/icons/view.svg';
  static const String commentIcon = 'assets/icons/chat.svg';
  static const String sentIcon = 'assets/icons/send_sms.svg';
  static const String bellIcon = 'assets/icons/bell.svg';
  static const String searchIcon = 'assets/icons/search.png';
  static const String favoriIcon = 'assets/icons/bookmark.svg';
  static const String favoriOnIcon = 'assets/icons/bookmark_on.svg';

  /// icon tab
  static const String homeTab = 'assets/icons/home.svg';
  static const String homeActiveTab = 'assets/icons/home_on.svg';
  static const String videoTab = 'assets/icons/play.svg';
  static const String videoActiveTab = 'assets/icons/play_on.svg';
  static const String addPostTab = 'assets/icons/add_post.svg';
  static const String notifyTab = 'assets/icons/notify.svg';
  static const String notifyActiveTab = 'assets/icons/notify_on.svg';
  static const String accountTab = 'assets/icons/user.svg';
  static const String accountActiveTab = 'assets/icons/user_on.svg';

  ///image app
  static const String img1 = 'assets/images/image_3.jpg';
  static const String emptyList = 'assets/images/empty_list.png';
  static const String networkTemplate =
      "https://www.thaistreet.com.vn/wp-content/uploads/2021/04/Food.jpg";
  static const String avtNetWork =
      'https://supperclean.vn/wp-content/uploads/2021/01/386f47c88a7aaa497ec6edc1c02cc9b6-scaled.jpg';
  static const String imgBanner1 = 'assets/images/banner1.jpg';
}

class AppImage {
  static network(
    String url, {
    BoxFit fit,
    Function placeholder,
    Duration fadeInDuration,
    Duration fadeOutDuration,
    double width,
    double height,
  }) =>
      CachedNetworkImage(
        imageUrl: url,
        placeholder: placeholder,
        fadeInDuration: fadeInDuration,
        fadeOutDuration: fadeOutDuration,
        width: width,
        height: height,
      );
}
