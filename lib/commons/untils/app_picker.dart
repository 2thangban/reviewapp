import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class AppPicker {
  AppPicker._();

  ///get image from camera
  static Future<File> getImageFromCamera() async {
    ImagePicker picker = ImagePicker();
    try {
      final pickerImg = await picker.getImage(source: ImageSource.camera);
      return pickerImg?.path != null && pickerImg.path.isNotEmpty
          ? File(pickerImg.path)
          : null;
    } on PlatformException {
      return null;
    }
  }

  /// video from gallery
  static Future<File> pickVideoFile() async {
    File fileVideo;
    final result = await FilePicker.platform.pickFiles(type: FileType.video);
    if (result != null) fileVideo = File(result.files.single.path);
    return fileVideo;
  }

  ///get image from gallery
  static Future<List<File>> getImageFromGallery() async {
    List<File> listFile = [];
    try {
      FilePickerResult result = await FilePicker.platform.pickFiles(
        allowMultiple: true,
        type: FileType.image,
      );
      if (result != null) {
        result.files.forEach((element) {
          listFile.add(File(element.path));
        });
      }
    } on Exception catch (_) {
      return [];
    }
    return listFile;
  }
}
