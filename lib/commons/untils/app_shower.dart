import 'dart:io';

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';

import '../../routes/route_name.dart';
import '../../theme.dart';
import '../animation/transaction_anim.dart';
import 'app_color.dart';
import 'app_picker.dart';

class AppShower {
  static showSnackBar(
    BuildContext context, {
    String label = '',
    TextStyle labelStyle,
    Widget iconLeading,
    String labelAction,
    Color labelActionColor = AppColors.primaryColor,
    double elevetor = 0.0,
    Duration duration,
    EdgeInsetsGeometry margin,
    EdgeInsetsGeometry padding,
  }) =>
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Row(
            children: [
              iconLeading ?? const SizedBox(),
              const SizedBox(width: 5.0),
              label != ''
                  ? Text(
                      label,
                      style:
                          labelStyle ?? Theme.of(context).textTheme.headline6,
                    )
                  : const SizedBox(),
            ],
          ),
          backgroundColor: Theme.of(context).backgroundColor,
          duration: duration ?? const Duration(seconds: 1),
          shape: RoundedRectangleBorder(
            side: BorderSide(color: AppColors.backgroundSearchColor),
            borderRadius: BorderRadius.circular(5.0),
          ),
          margin: margin ??
              const EdgeInsets.symmetric(
                horizontal: 10.0,
                vertical: 10.0,
              ),
          padding: padding,
          elevation: elevetor,
          behavior: SnackBarBehavior.floating,
          action: SnackBarAction(
            label: labelAction,
            textColor: labelActionColor,
            onPressed: () {},
          ),
        ),
      );

  static Widget showFlushBar(
    IconData icon,
    BuildContext context, {
    String message,
    ThemeData theme,
    EdgeInsetsGeometry margin = const EdgeInsets.symmetric(
      vertical: 20.0,
      horizontal: 10.0,
    ),
    Color iconColor,
    Color boderColor,
    int duration = 1200,
    FlushbarPosition position = FlushbarPosition.TOP,
  }) =>
      Flushbar(
        duration: Duration(milliseconds: duration),
        shouldIconPulse: false,
        flushbarPosition: position,
        backgroundColor: theme.backgroundColor,
        margin: margin,
        borderRadius: BorderRadius.circular(10.0),
        borderColor: iconColor,
        icon: Icon(icon, color: iconColor, size: 30.0),
        messageText: Text(
          message ?? "Chưa có message !",
          style: theme.textTheme.bodyText1,
        ),
      )..show(context);

  /// show options of avatar user
  static Future<File> showOptionAvatar(
    ThemeData theme,
    BuildContext context, {
    List<String> showAvatar,
  }) async =>
      showGeneralDialog(
        barrierDismissible: true,
        barrierLabel: '',
        context: context,
        transitionDuration: Duration(milliseconds: 200),
        transitionBuilder: (_, _animation, _secondaryAnimation, child) =>
            TransactionAnima.shink(_animation, _secondaryAnimation, child),
        pageBuilder: (_, _animation, _secondaryAnimation) => SimpleDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
          title: Text('Ảnh đại diện', style: theme.textTheme.headline5),
          children: [
            Visibility(
              visible: showAvatar != null,
              child: SimpleDialogOption(
                child: Text(
                  'Xem ảnh đại diện',
                  style: theme.textTheme.bodyText1.copyWith(
                    fontWeight: AppFontWeight.regular,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pushNamed(
                    context,
                    RouteName.image,
                    arguments: showAvatar,
                  );
                },
              ),
            ),
            SimpleDialogOption(
              child: Text(
                'Chụp ảnh mới',
                style: theme.textTheme.bodyText1.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
              ),
              onPressed: () async {
                File image = await AppPicker.getImageFromCamera();
                if (image?.path != null && image.path.isNotEmpty) {
                  Navigator.pop(context, image);
                }
              },
            ),
            SimpleDialogOption(
              child: Text(
                'Chọn ảnh từ thư viện',
                style: theme.textTheme.bodyText1.copyWith(
                  fontWeight: AppFontWeight.regular,
                ),
              ),
              onPressed: () async {
                final newImage = await AppPicker.getImageFromGallery();
                if (newImage.isNotEmpty && newImage != null) {
                  Navigator.pop(context, File(newImage.first?.path));
                }
              },
            ),
          ],
        ),
      );
}
