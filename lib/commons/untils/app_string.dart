class AppString {
  AppString._();

  ///
  static const String titleApp = 'APP RIVIEW';

  /// Errors
  static const String errorLabelButton = 'Button not label !!';
  static const String errorLabelField = 'Field not text hint !!';

  /// Validation
  static const String isEmpty = "is not empty !!";
  static const String formatPass = "Password must be 8 characters correct !!";
  static const String formatEmail = "Email format is correct !!";
  static const String isConfirm = "Confirm and newPass must be the same !!";
  static const String isExisted = "Account is existed !!";

  // Login/signup screen
  static const String subTitleLogin = 'Welcome to';
  static const String subTitlesignUp = 'Sign Up with';
  static const String titleformLogin = 'Please login to continue';
  static const String labelEmailField = 'Email';
  static const String labelPassField = 'Password';
  static const String labelBtnLogin = 'LOGIN';
  static const String labelBtnSignUp = 'SIGN UP';
  static const String labelForgotPass = 'FORGOT PASSWORD';
  static const String labelExistedUser = 'Existed user?';
  static const String labelSignUpOption = 'Or Sign up with';
  static const String labelOption = 'OR';

  /// Registor information
  static const String labelRegis = 'REGISTER INFORMATION';
  static const String firstName = 'First Name';
  static const String lastName = 'Last Name';
  static const String birthDay = 'BirthDay';
  static const String titleGender = 'Gender';
  static const String male = 'Male';
  static const String feMale = 'Female';
  static const String address = 'Address';

  /// Tab name
  /// Home screen
  static const String homeTab = 'Home';
  static const String videoTab = 'Video';
  static const String notifyTab = 'Notify';
  static const String profileTab = 'Profile';
  static const String btnPost = 'Post';
  static const String hintSearch = 'Tìm review, địa điểm,...';
}
