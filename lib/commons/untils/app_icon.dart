import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AppIcon {
  static const IconData topPlaces = Icons.stars_outlined;
  static const IconData topUser = Icons.supervised_user_circle;

  /// Account
  static const IconData follow = Icons.add_rounded;
  static const IconData followed = Icons.check_rounded;
  static const IconData member = FontAwesomeIcons.solidAddressCard;

  /// fuction icon
  static const IconData seeMore = FontAwesomeIcons.chevronRight;
  static const IconData search = Icons.search;
  static const IconData comment = Icons.sms_outlined;
  static const IconData send = Icons.send_rounded;
  static const IconData edit = Icons.edit_outlined;
  static const IconData liked = Icons.favorite_rounded;
  static const IconData unLike = Icons.favorite_border_rounded;
  static const IconData rating = Icons.star_rate_rounded;
  static const IconData view = FontAwesomeIcons.eye;
  static const IconData share = Icons.open_in_new_rounded;
  static const IconData unsaveGallery = Icons.bookmark_border_outlined;
  static const IconData saveGallery = Icons.bookmark;
  static const IconData delete = Icons.cancel_sharp;
  static const IconData remove = FontAwesomeIcons.trashAlt;
  static const IconData post = Icons.article_outlined;
  static const IconData box = FontAwesomeIcons.boxOpen;
  static const IconData block = Icons.lock;
  static const IconData openlock = Icons.lock_open;
  static const IconData moreDown = FontAwesomeIcons.angleDoubleDown;
  static const IconData reload = Icons.autorenew_rounded;

  /// media icon
  static const IconData image = Icons.image_outlined;
  static const IconData addImage = Icons.add_photo_alternate_outlined;
  static const IconData add = Icons.add_rounded;
  static const IconData addVideo = Icons.ondemand_video_outlined;
  static const IconData playVideo = Icons.play_arrow_rounded;
  static const IconData pauseVideo = Icons.pause_circle_outline;
  static const IconData clock = Icons.access_time_outlined;
  static const IconData phoneNumber = Icons.phone_outlined;
  static const IconData phoneNumber2 = Icons.call;

  /// check icon
  static const IconData successCheck = Icons.check_circle_outline_rounded;
  static const IconData failedCheck = Icons.highlight_off_rounded;
  static const IconData errorCheck = Icons.report_sharp;

  /// location icon
  static const IconData pin = Icons.place_outlined;
  static const IconData money = Icons.attach_money;
  static const IconData range = Icons.minimize_rounded;

  /// tab icon
  static const IconData homeTab = Icons.home_outlined;
  static const IconData videoTab = Icons.video_label_outlined;
  static const IconData notifyTab = Icons.notifications;
  static const IconData profileTab = Icons.person_outlined;

  /// navigate icon
  static const IconData exitNavigate = CupertinoIcons.clear_thick;
  static const IconData popNavigate = Icons.arrow_back_outlined;
  static const IconData menu = Icons.menu;
  static const IconData more_vert = Icons.more_vert;
  static const IconData more_hori = Icons.more_horiz;
  static const IconData dropDown = FontAwesomeIcons.caretDown;
}
