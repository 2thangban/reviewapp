import 'package:flutter/material.dart';

class AppColors {
  ///main color app
  static const Color lightThemeColor = Color(0xFFFFFFFF);
  static const Color darkThemeColor = Color(0xFF1C1C1C);
  static const Color subLightColor = Colors.grey;
  static const Color subdarkColor = Color(0xDD000000);
  static const Color primaryColor = Color(0xFFFF6600);
  static const Color secondaryColor = Color(0xFFFFCC00);

  static const listBgColor = [
    Color(0xFFFFFDE7),
    Color(0xFFE0F7FA),
    Color(0xFFE8F5E9),
    Color(0xFFE0F2F1),
    Color(0xFFECEFF1),
  ];

  static const Color backgroundCommonColor = Color(0xFFFFF3E0);
  static const Color backgroundSearchColor = Color(0xFFEEEEEE);
  static const Color backgroundEditColor = Color(0xFFE0E0E0);

  static const Color errorColor = Colors.red;
  static const Color likeColor = Colors.red;
  static const Color sussess = Colors.green;

  /// button
  static const Color disibleColor = Color(0xFFEF6C00);
}
