import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../configs/constrain.dart';

class AppLanguage {
  String currentLocale = 'en';

  final _storage = FlutterSecureStorage();

  /// get initial locale
  Future<void> fetchLocale() async {
    final locale = await _storage.read(key: KeyStore.userLocale);
    if (locale == null) {
      currentLocale = 'vi';
    } else {
      currentLocale = locale;
    }
  }

  /// handle change locale
  Future<void> changeLocale(String locale) async {
    currentLocale = locale;
    await _storage.write(key: KeyStore.userLocale, value: locale);
  }
}
