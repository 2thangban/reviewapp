import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppLocalization {
  static const supportedLocale = [
    Locale('vi'),
    Locale('en'),
  ];

  final Locale locale;

  /// Map locale read form json file
  Map<String, String> _localizationString;

  AppLocalization(this.locale);

  static const LocalizationsDelegate<AppLocalization> delegate =
      _AppLocalizationDelegate();

  static AppLocalization of(BuildContext context) =>
      Localizations.of(context, AppLocalization);

  ///Handle load json file
  Future<bool> load() async {
    final jsonStr =
        await rootBundle.loadString('lang/${locale.languageCode}.json');

    Map<String, dynamic> json = jsonDecode(jsonStr);

    _localizationString =
        json.map((key, value) => MapEntry(key, value.toString()));
    return true;
  }

  String locaized(String key) => _localizationString[key] ?? key;
}

class _AppLocalizationDelegate extends LocalizationsDelegate<AppLocalization> {
  const _AppLocalizationDelegate();

  @override
  bool isSupported(Locale locale) =>
      AppLocalization.supportedLocale.contains(locale);

  @override
  Future<AppLocalization> load(Locale locale) async {
    final localizations = AppLocalization(locale);
    await localizations.load();
    return localizations;
  }

  @override
  bool shouldReload(covariant LocalizationsDelegate<AppLocalization> old) =>
      false;
}
