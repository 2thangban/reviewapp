part of 'app_language_bloc.dart';

@immutable
abstract class AppLanguageState {}

class AppLanguageInitial extends AppLanguageState {}

class AppLanguageComplete extends AppLanguageState {
  final Locale locale;
  AppLanguageComplete({this.locale});
}
