part of 'app_language_bloc.dart';

@immutable
abstract class AppLanguageEvent {}

class FetchLanguageEvent extends AppLanguageEvent {}

class ChangeLanguageEvent extends AppLanguageEvent {
  final String locale;

  ChangeLanguageEvent({this.locale});
}
