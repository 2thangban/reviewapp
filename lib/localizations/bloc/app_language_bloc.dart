import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/widgets.dart';
import 'package:meta/meta.dart';

import '../app_language.dart';

part 'app_language_event.dart';
part 'app_language_state.dart';

class AppLanguageBloc extends Bloc<AppLanguageEvent, AppLanguageState> {
  AppLanguageBloc() : super(AppLanguageInitial());
  final AppLanguage appLanguage = AppLanguage();
  @override
  Stream<AppLanguageState> mapEventToState(AppLanguageEvent event) async* {
    if (event is FetchLanguageEvent) {
      await appLanguage.fetchLocale();
      yield AppLanguageComplete(locale: Locale(appLanguage.currentLocale));
      print(appLanguage.currentLocale);
    } else if (event is ChangeLanguageEvent) {
      await appLanguage.changeLocale(event.locale);
      yield AppLanguageComplete(locale: Locale(event.locale));
    }
  }
}
